# coding = utf-8
import itertools

# @filename: C:/Users/Ryan/git/rytutorial/book/effective-python/ep30.py
# @desc: effective python 第30条，不返回list，而是返回里面值的迭代
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022年3月21日

'''简单地讲，yield 的作用就是把一个函数变成一个 generator，
带有 yield 的函数不再是一个普通函数，Python 解释器会将其视为一个 generator，
调用 fab(5) 不会执行 fab 函数，而是返回一个 iterable 对象！
'''

def index_file( file_handle ):
    offset = 0
    for line in file_handle:
        print('line', line)
        if line:
            yield offset
            print('offset1:', offset)
        for letter in line:
            offset += 1
            if letter.isspace():
                yield offset
                print('offset2:', offset)

def main():
    with open('ep30.txt', 'r', encoding='utf-8') as f:
        it = index_file(f)
        result = itertools.islice(it, 0, 10)
        print(list(result))
    
if __name__ == '__main__':
    main()