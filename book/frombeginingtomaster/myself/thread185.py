# coding=utf-8

# @title: 
# @desc: 多进程
# @author: RyanLin
# @date: 2022/02/07

import threading, time

def process():
    for i in range(3):
        time.sleep(2)
        print('thread name is {}'.format(threading.current_thread().name))
        time.sleep(1)

if __name__ == '__main__' :
    print('主线程开始'.center(20,'-'))
    threads = [threading.Thread(target=process) for i in range(4)]  #创建4个线程
    for t in threads:
        t.start()
    for t in threads:
        t.join()
    print('主线程结束'.center(20,'-'))