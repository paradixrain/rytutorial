# coding=utf-8

# @title: 
# @desc: 多进程的消息传递方式，queue
# @author: RyanLin
# @date: 2022/02/07

from queue import Queue
import random, threading, time

# 生产者类
class Producer(threading.Thread):
    def __init__(self, name, queue):
        threading.Thread.__init__(self, name=name)
        self.name = name
        self.data=queue
    def run(self):
        for i in range(5):
            print("生产者{} 将产品{}加入队列".format(self.name, i))
            self.data.put(i)
            time.sleep(random.random())
        print("生产者{} 完成!".format(self.name))

# 消费者类
class Consumer(threading.Thread):
    def __init__(self, name, queue):
        threading.Thread.__init__(self, name=name)
        self.name = name
        self.data=queue
    def run(self):
        for i in range(5):
            val = self.data.get()
            print("消费者{}将产品{}从队列中取出！".format(self.name, val))
            time.sleep(random.random())
        print("消费者{} 完成!".format(self.name))
        
if __name__ == '__main__':
    print("主线程开始".center(20,'-'))
    queue = Queue()
    producer = Producer('Producer', queue)
    consumer = Consumer('Consumer', queue)
    producer.start()
    producer.join()
    consumer.start()
    consumer.join()
    print("主线程结束".center(20,'-'))
