# coding=utf-8

# @title: 
# @desc: 多进程之间的消息传递，用队列
# @author: RyanLin
# @date: 2022/02/07

from multiprocessing import Queue, Process
import time

def write_task(q):
    # 向队列种写入数据
    if not q.full():
        for i in range(100):
            message = '消息{}'.format(i)
            q.put(message)
            print('写入:{}'.format(message))
        time.sleep(5)
        for i in range(101, 200):
            message = '消息{}'.format(i)
            q.put(message)
            print('写入:{}'.format(message))

def read_task(q):
    # 从队列种读取数据
    time.sleep(1)
    while not q.empty():
        print('读取:{}'.format(q.get(True, 2)))   #等待2秒，如果还没读到任何消息，则抛出Queue.Empty异常

if __name__ == '__main__':
    print('父进程开始'.center(20,'-'))
    q = Queue()
    pw = Process(target=write_task, args=(q,))
    pr = Process(target=read_task, args=(q,))
    pw.start()
    pr.start()
    pw.join(10)
    pr.join(10)                 # 设置了超时时间之后，pr读完100条后就结束了，然后pw又写了100条，但是这时候进程已经结束
    print('父进程结束'.center(20,'-'))