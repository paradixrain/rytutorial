# coding = utf-8

# @title: 
# @desc: 
# @author: RyanLin
# @date: 2022/02/06



from multiprocessing import Process
import time
import os
from asyncio import __main__

# def test(interval):
#     print('我是子进程')
#
# def main():
#     print('主进程开始')
#     p = Process(None, test, args=(3,))
#     p.start()
#     print('主进程结束')

# if __name__ == '__main__':
#     main()


def child1(interval):
    print("子进程1: (%s) 开始执行，父进程为(%s) " % (os.getpid(), os.getppid()))
    t_start = time.time()
    print("子进程1开始执行, 时间: {}".format(time.time()).center(40, '-'))
    time.sleep(interval)
    t_end = time.time()
    print("子进程1结束, 时间: {}".format(time.time()).center(40, '-'))
    print("子进程1: (%s) 执行时间为 '%0.2f'秒" % (os.getpid(), t_end - t_start))

def child2(interval):
    print("子进程2: (%s) 开始执行，父进程为(%s) " % (os.getpid(), os.getppid()))
    t_start = time.time()
    print("子进程2开始执行, 时间: {}".format(time.time()).center(40, '-'))
    time.sleep(interval)
    t_end = time.time()
    print("子进程2结束, 时间: {}".format(time.time()).center(40, '-'))
    print("子进程2: (%s) 执行时间为 '%0.2f'秒" % (os.getpid(), t_end - t_start))

if __name__ == "__main__":
    print("父进程开始执行, 时间: {}".format(time.time()).center(40, '-'))
    print("父进程pid: {}".format(os.getpid()))
    p1 = Process(None, child1, args=(1,))
    p2 = Process(None, child2, name="mrsoft", args=(2,))
    p1.start()
    p2.start()
    
    # 同时父进程还在往下执行
    print("p1.is_alive = {}".format(p1.is_alive()))
    print("p2.is_alive = {}".format(p2.is_alive()))
    
    print("p1.name = {}, p1.pid={}".format(p1.name, p1.pid))
    print("p2.name = {}, p2.pid={}".format(p2.name, p2.pid))
    
    print("等待子进程".center(40, '-'))
    p1.join()
    # p2.join()
    
    print("父进程要睡觉了, 时间: {}".format(time.time()).center(40,'-'))
    time.sleep(5)
    print("父进程睡觉醒了, 时间: {}".format(time.time()).center(40,'-'))
    print("父进程执行结束, 时间: {}".format(time.time()).center(40,'-'))