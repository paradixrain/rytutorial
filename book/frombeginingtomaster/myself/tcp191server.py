# coding = utf-8
import socket
import time, os
host = '127.0.0.1'
port = 8080
web = socket.socket()
web.bind((host, port))
web.listen(5)           # 设置最多连接数
print('PID=', os.getpid())
print('服务端等待客户端连接')

nowtime = time.time()
endtime = nowtime + 100
# 开启监听死循环
while nowtime < endtime:
    nowtime = time.time()
    print('nowtime,', nowtime)
    conn, addr = web.accept()
    data = conn.recv(1024)      # 获取客户端请求数据
    print(data)
    conn.sendall(b'HTTP/1.1 200 OK \r\n\r\nHello World')    # 向客户端发送数据
    conn.close()


