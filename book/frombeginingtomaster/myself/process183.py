# coding=utf-8

# @title: 
# @desc: 多进程
# @author: RyanLin
# @date: 2022/02/06

from multiprocessing import Pool
import os, time

def task(name):
    print('子进程 ({}) 执行task {} ...'.format( os.getpid(), name))
    time.sleep(2)
    

if __name__ == '__main__':
    print('父进程 ({})...'.format( os.getpid()))
    p = Pool(3)
    for i in range(10):
        p.apply_async(task, args=(i,))
    print('等待所有子进程结束...')
    p.close()
    p.join()
    print('所有子进程结束')

