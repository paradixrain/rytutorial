# coding=utf-8

# @title: 
# @desc: 多进程的互斥锁
# @author: RyanLin
# @date: 2022/02/07

from threading import Thread, Lock
import time

n = 100 #共100张票

def task():
    global n
    mutex.acquire()
    temp = n
    time.sleep(0.1)
    n = temp - 1
    print('购买成功，剩余{}张电影票'.format(n))
    mutex.release()

if __name__ == '__main__':
    mutex = Lock()
    t_l = []
    for i in range(10):
        t = Thread(target=task)
        t_l.append(t)
        t.start()
    for t in t_l:
        t.join()