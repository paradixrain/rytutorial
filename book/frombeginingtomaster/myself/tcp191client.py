# coding = utf-8
import socket
import os

host = '127.0.0.1'
port = 8080
s = socket.socket()
s.connect((host, port))

print('客户端发起连接')
print('PID=', os.getpid())
send_data = input("请输入要发送的数据:")
s.send(send_data.encode())

recvdata = s.recv(1024).decode()      # 获取服务端反馈消息，最大1024
print("接收到的数据为", recvdata)

s.close()


