# coding=utf-8

# @title: 矩阵转置
# @desc: 
'''
编写一个程序，将一个3行3列的矩阵进行转置
'''
# @author: RyanLin
# @date: 2022/01/25

matrixA = [[1,2,3],[4,5,6],[7,8,9]]

def trans(matrix):
    return [list(i) for i in zip(*matrix)]

# print(trans(matrixA))

# def maxinmatrix(matrix):
#     return max(matrix)
#
# print(maxinmatrix(matrixA))