# coding=utf-8

# @title: 平分7筐鱼
# @desc: 
'''
甲、乙、丙三位渔夫出海打鱼，他们随船带了21只箩筐。当晚返航时，他们发现有7筐装满了鱼，还有7筐装了半筐鱼，另外7筐是空的，
由于他们没有秤，只好通过目测认为7个满筐鱼的重量是相等的，7个半筐鱼的重量是相等的。在不将鱼倒出来的前提下，怎样将鱼和筐平分为三份？
'''
# @author: RyanLin
# @date: 2022/01/23

# 思路，每人最后可以拿到3.5筐鱼，每人一共可以拿到7个筐
alist=[]    # 甲的鱼
for i in range(7+1):
    for j in range(7-i+1):
        a = []
        k = 7-i-j
        for m in range(i):
            a.append(1)
        for m in range(j):
            a.append(0.5)
        for m in range(k):
            a.append(0)
        # print(i,j,k,",",a)
        alist.append(a.copy())

# for i in range(len(alist)):
#     print(i, alist[i])
    
# 最后得出，7个筐的装法一共有35种，分别从7个0到7个1，
blist = alist.copy()
clist = alist.copy()

# 对abclist进行条件便利
for i in alist:
    for j in blist:
        for k in clist:
            if sum(i) == 3.5 and sum(j) == 3.5 and sum(k) == 3.5 \
            and i.count(1)+j.count(1)+k.count(1)==7 \
            and i.count(0.5)+j.count(0.5)+k.count(0.5)==7:
                print(i,j,k)