# coding=utf-8
from _operator import itemgetter

# @title: 选美比赛
# @desc: 
'''
一批选手参加比赛，比赛的规则是最后得分越高，名次越低。当半决赛结束时，
要在现场按照选手的出场顺序宣布最后得分和最后的名次，获得相同分数的选手具有相同的名次，
名次连续编号，不用考虑同名次的选手人数。
例如：
选手序号：1，2，3，4，5，6，7
选手得分：5，3，4，7，3，5，6
输出名次为：3，1，2，5，1，3，4
'''
# @author: RyanLin
# @date: 2022/01/25

player = list(range(1,8))
score = [5,3,4,7,3,5,6]

# 针对得分进行排名，然后返回索引值

s = sorted(list(set(score)))
# print('排序后的分数:', s)
print("最终名次为：")
for i in score:
    print(s.index(i)+1, end = ' ')
