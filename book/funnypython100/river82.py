# coding=utf-8

# @title: 农夫过河
# @desc: 
'''
一个农夫在河边带了一匹狼、一只羊和一棵白菜，他需要把这三样东西用船带到河的对岸。
然而，这艘船只能容下农夫本人和另外一样东西。如果农夫不在场的话，狼会吃掉羊，羊也会吃掉白菜。请编程为农夫解决这个过河问题。
'''
# @author: RyanLin
# @date: 2022/01/24

import copy

# 构建一个多叉数来完成广度遍历
# 一个表来存放所有路径，包含， id，pid，level,content
# 用[[1,2,3,4],[]]来表示当前状态，当变成[[],[1,2,3,4]]的时候，就是最终状态

#条件：农夫1必移动；农夫1不在场时候，23以及34不能单独存在；农夫1可带一个或不带

# 初始化树的根节点
class node:
    count = 0

    def __init__(self,id,pid,level,west,east):
        self.id = id
        self.pid = pid
        self.level = level
        self.west = west
        self.east = east
        
    def valid(self):
        if 1 in self.west:
            if (2 in self.east and 3 in self.east) or (3 in self.east and 4 in self.east):
                return False
        if 1 in self.east:
            if (2 in self.west and 3 in self.west) or (3 in self.west and 4 in self.west):
                return False
        return True
    
    def verify(self):
        if self.east == [1,2,3,4] and self.west == []:
            return True
        return False
    
    @classmethod
    def nextid(cls):
        cls.count += 1
        return cls.count
    
    def __str__(self):
        return('id=',self.id, 'pid=', self.pid, 'level=', self.level, 'west=', self.west, 'east=', self.east)
    
    def posiblemove(self,type=1):
        # type=1只渔夫动，2带狼，3带羊，4带菜
        if 1 in self.west:
            # print('possible move in west with type: ', type)
            self.west.remove(1)
            self.east.append(1)
            if type > 1:
                if type in self.west:
                    self.west.remove(type)
                    self.east.append(type)
                else:
                    return False
        else:
            # print('possible move in east with type: ', type)
            self.east.remove(1)
            self.west.append(1)
            if type > 1:
                if type in self.east:
                    self.east.remove(type)
                    self.west.append(type)
                else:
                    return False
        # print('after possible move, west:', self.west, 'east:', self.east)
        return self.valid()

def check(node, nodelist):
    for inode in nodelist:
        if inode.west == node.west and inode.east == node.east and node.level > inode.level:
            return False
    return True

leveladd = 0                    # 广度插入时候，当前level插入了多少个值，如果遍历完毕后当前level插入为0，则突出循环体
curlevel = 0                    # 当前level
nodelist=[]                     # 所有的移动状态
leveladd = 1                    # 插入根节点后，当前level变成了1，
curlevel = 1
root = node(node.nextid(),0,curlevel,[1,2,3,4],[]) # 根节点
nodelist.append(root)           # 插入根节点

# print(id(root),root.__str__())

while leveladd > 0:
    # 只要新增的不为0，则不停遍历 插入子节点，完成所有节点的插入
    leveladd = 0    # 进入下一level，清空当前level新增值
    for inode in nodelist:
        # print(id(inode), inode.__str__())
        if inode.level == curlevel:  # 只遍历新增的节点，这些节点的level一定是刚插入的最大的
            for type in range(1,5):    # 尝试所有type的移动
                nodetry = copy.deepcopy(inode)
                result = nodetry.posiblemove(type)
                if result:
                    # 只对移动后， 不存在与现有nodelist里的节点入库，否则跳出循环
                    # print('try node, with type', type)
                    # print(nodetry.__str__())
                    nodetmp = node(node.nextid(), nodetry.id, curlevel+1, sorted(nodetry.west), sorted(nodetry.east))
                    if check(nodetmp, nodelist):
                        nodelist.append(nodetmp)
                        # print('new node add')
                        # print(nodetry.__str__())
                        leveladd += 1
    curlevel += 1

print('final list:')
for i in nodelist:
    print(id(i),i.__str__())

# 遍历图，对外输出，使用逆序pop找到路径
# 先得到所有叶子节点 lnode
leafnode = set( inode.id for inode in nodelist )
pnode = set( inode.pid for inode in nodelist if inode.pid != 0)
lnode = leafnode - pnode
print(leafnode, pnode, lnode)

# 从level = max(level)的叶子节点开始遍历，
for inode in lnode:
    # 从叶节点开始的路径
    path = []
    while True:
        pathnode = [i for i in nodelist if i.id == inode]
        path.append(pathnode)
        inode = pathnode[0].pid
        if inode == 0:
            break
    
    # 逆序，从根节点输出路径
    pathfromroot = []
    while path:
        pathfromroot.append(path.pop())
        
    step = 0
    print('路径如下： 1表示农民，2表示狼，3表示羊，4表示菜')
    for i in pathfromroot:
        step += 1
        print('第 {} 步：西岸：{}, 东岸：{}'.format(step, i[0].west, i[0].east))
        # print('第 {} 步：西岸：{:<15s}, 东岸：{:<15s}'.format(step, ''.join(i[0].west), ''.join(i[0].east)))
    # 找到这个节点的父节点，加入路径
    # 依次递归，直到根节点