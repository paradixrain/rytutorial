# coding=utf-8

# @title: 掷骰子
# @desc: 
'''
骰子是一个有6个面的正方体，每个面分别印有1～6个小圆点代表点数。假设这个游戏的规则是两个人轮流掷骰子6次，
并将每次投掷的点数累加起来，点数多者获胜，点数相同则为平局。要求编写程序模拟这个游戏的过程，并求出玩100盘之后谁是最终的获胜者
'''
# @author: RyanLin
# @date: 2022/01/22

import random

s1, s2 = 0, 0
for i in range(100):
    s1 += random.randint(1,6)
    s2 += random.randint(1,6)
    print('第 {} 轮，s1={}, s2={}'.format(i, s1, s2))

print('{}赢了'.format('s1' if s1>s2 else 's2' if s1<s2 else "平局"))