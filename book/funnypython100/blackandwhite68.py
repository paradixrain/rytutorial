# coding=utf-8

# @title: 5个黑白人
# @desc: 
'''
有A、B、C、D、E这5个人，每个人额头上都贴了一张黑或白的纸。5人对坐，每个人都可以看到其他人额头上纸的颜色。
A说：“我看见有3人额头上贴的是白纸，1人额头上贴的是黑纸。”
B说：“我看见其他4人额头上贴的都是黑纸。”
C说：“我看见1人额头上贴的是白纸，其他3人额头上贴的是黑纸。”
D说：“我看见4人额头上贴的都是白纸。”
E什么也没说。现在已知额头上贴黑纸的人说的都是谎话，额头上贴白纸的人说的都是实话。
问这5人谁的额头上贴的是白纸，谁的额头上贴的是黑纸？
'''
# @author: RyanLin
# @date: 2022/01/21

for i in range(2):
    for j in range(2):
        for k in range(2):
            for m in range(2):
                for n in range(2):
                    if ((i and j+k+m+n == 3) or (not i and j+k+m+n != 3 )) \
                    and ((j and i+k+m+n == 0) or (not j and i+k+m+n != 0 )) \
                    and ((k and i+j+m+n == 1) or (not k and i+j+m+n != 1 )) \
                    and ((m and i+j+k+n) == 4 or (not m and i+j+k+n != 4 )):
                        print(i,j,k,m,n)
