# coding=utf-8

# @title: 马踏棋盘
# @desc: 
'''
国际象棋的棋盘为8×8的方格棋盘。现将“马”放在任意指定的方格中，按照“马”走棋的规则将“马”进行移动。
要求每个方格只能进入一次，最终使得“马”走遍棋盘的64个方格。
编写一个Python程序，实现马踏棋盘操作，要求用1～64这64个数字标注“马”移动的路径，
也就是按照求出的行走路线，将数字1～64依次填入棋盘的方格中，并输出。
'''
# @author: RyanLin
# @date: 2022/01/26

# 找到基于(x,y)的下一个可走的位置
def nextxy(x, y, count):
    if count == 0 and x + 2 <= X - 1 and y - 1 >= 0 and chess[x + 2][y - 1] == 0:
        #找到坐标(x+2, y-1)
        x = x + 2
        y = y - 1
        flag = True
    elif count == 1 and x + 2 <= X - 1 and y + 1 <= Y - 1 and chess[x + 2][y + 1] == 0:
        #找到坐标(x+2, y+1)
        x = x + 2
        y = y + 1
        flag = True
    elif count == 2 and x + 1 <= X - 1 and y -2 >= 0 and chess[x + 1][y - 2] == 0:
        #找到坐标(x+1, y-2)
        x = x + 1
        y = y - 2
        flag = True
    elif count == 3 and x + 1 <= X - 1 and y + 2 <= Y - 1 and chess[x + 1][y + 2] == 0:
        #找到坐标(x+1, y+2)
        x = x + 1
        y = y + 2
        flag = True
    elif count == 4 and x - 2 >= 0 and y - 1 >= 0 and chess[x - 2][y - 1] == 0:
        #找到坐标(x-2, y-1)
        x = x - 2
        y = y - 1
        flag = True
    elif count == 5 and x - 2 >= 0 and y + 1 <= Y - 1 and chess[x - 2][y + 1] == 0:
        #找到坐标(x-2, y+1)
        x = x - 2
        y = y + 1
        flag = True
    elif count == 6 and x - 1 >= 0 and y - 2 >= 0 and chess[x - 1][y - 2] == 0:
        #找到坐标(x-1, y-2)
        x = x - 1
        y = y - 2
        flag = True
    elif count == 7 and x - 1 >= 0 and y + 2 <= Y - 1 and chess[x - 1][y + 2] == 0:
        #找到坐标(x-1, y+2)
        x = x - 1
        y = y + 2
        flag = True
    else:
        flag = False
    return flag, x, y

# 深度优先搜索
def TravelChessBoard(x, y, tag):
    x1, y1, flag, count = x, y, False, 0
    chess[x][y] = tag
    # print('tag=',tag)
    if tag == X*Y - 4:      # 源代码这里是60，奇怪，走完棋盘不是64吗
        return True
    flag, x1, y1 = nextxy(x1, y1, count)
    while not flag and count < 7:
        count = count + 1
        flag, x1, y1 = nextxy(x1, y1, count)
    while flag:
        if TravelChessBoard(x1, y1, tag + 1):    #递归
            return True
        x1 = x
        y1 = y
        count = count + 1
        flag, x1, y1 = nextxy(x1, y1, count)
        while not flag and count < 7:
            count = count +1
            flag, x1, y1 = nextxy(x1, y1, count)
    if not flag:
        chess[x][y] = 0
    return False

if __name__ == '__main__':
    X = 8
    Y = 8
    chess = [[0]*X for i in range(Y)]
    if TravelChessBoard(0, 0, 1):
        for i in range(X):
            for j in range(Y):
                print("%-5d" % chess[i][j], end='')
            print()
        print("The horse has travel the chess board")
    else:
        print("The horse cannot travel the chess board")