# coding = utf-8

# 爬取北京地区的花粉值和花粉程度，以及当天天气
# 地址： http://www.weather.com.cn/forecast/hf_index.shtml?id=101010100
import time

from selenium import webdriver
from bs4 import BeautifulSoup


def get_pollen(code):
    # 0. 构建浏览器， 用chrome
    # chrome_opts = webdriver.ChromeOptions()
    # chrome_opts.add_argument("--headless")
    #
    # # browser = webdriver.Chrome(options=chrome_opts)
    # browser = webdriver.Chrome()

    # 0. 构建浏览器， 用Edge
    options = webdriver.EdgeOptions()
    options.use_chromium = True
    options.add_argument('--headless')
    browser = webdriver.Edge(options=options)

    # 1. 打开网页
    base_url = "http://www.weather.com.cn/forecast/hf_index.shtml?id=" + code
    browser.get(base_url)
    data = browser.page_source
    # print(data)

    # 2. 解析网页
    # 使用BeautifulSoup解析，抓取<div class="huazhi_img"><div class="h_numcard"><p class="h_num">的值
    # 以及<div class="h_numcard"><p class="h_word">的值
    # 存入到 num 和 word 中
    soup = BeautifulSoup(data, 'html.parser')
    num = soup.select('.h_numcard .h_num')[0].text
    word = soup.select('.h_numcard .h_word')[0].text
    # print(num, word)

    # 使用BeautifulSoup抓取 今天的日期， 最高气温， 最低气温， 天气， 风向， 风力， 综合评价
    today_date = soup.select('.week_obs_box .datatable .p_data')[0].text
    tmp = soup.select('.week_obs_box .tem')[0].text
    high_temp, low_temp = (i.text for i in soup.select('.week_obs_box .tem')[0])
    weather = soup.select('.week_obs_box .weather')[0].text
    wind_direction = soup.select('.week_obs_box .wind')[0].text
    overall_evaluation = soup.select('.week_obs_box .aqi')[0].text
    # print(today_date, tmp, weather, wind_direction, overall_evaluation)

    # 3. 关闭浏览器，返回结果
    browser.quit()
    return f"{today_date} {weather}, 花粉指数{num}{word}, 最高{high_temp}, 最低{low_temp}, {wind_direction}, AQI{overall_evaluation}"


if __name__ == '__main__':
    area_code = "101010100"
    get_pollen(area_code)
