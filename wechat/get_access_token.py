# coding = utf-8

# @filename: C:/Users/Ryan/git/rytutorial/wechat/get_access_token.py
# @desc: 获取微信access_token，根据openid定向群发微信消息
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022年4月2日


import requests
import json
import time

#appid = 'wx1b19ca1e3bd58663'
#AppSecret = '2426bddb812c71854da049eb9f21ca7f'
appid = 'wx6533db2d0fe1970b'
AppSecret = '58d0b8d3cf81b73ee1ad5c131bd45358'
tokenFile = 'token.txt'
wx_url = f'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={AppSecret}' 

#openid = 'oakSB6sKPRYjYb5BIf5xTd7i08uk'  # 工单的openid
openid = 'oakSB6mGGQ3W_mMlWIkWU7X2PbFY'  # 加隆的openid
MAX_CHAR = 500


def get_accessToken(wx_url):
    # 开始解析，得到需要的数据
    try:
        res = requests.get(wx_url)
        res = res.text
        print('res',res)
        json_res = json.loads(res)
        access_token = json_res['access_token']
        print('access_token:', access_token)
        with open(tokenFile, 'w') as f:
            f.write(access_token)
        return access_token
    except Exception as e:
        print(e)

def sendmsg(access_token, openid, msg):
    body = {
        "touser": openid,
        "msgtype": "text",
        "text": {
            "content": msg
        }
    }
    response = requests.post(
        url="https://api.weixin.qq.com/cgi-bin/message/custom/send",
        params={
            'access_token': access_token
        },
        data=bytes(json.dumps(body, ensure_ascii=False), encoding='utf-8')
    )
    # 这里可根据回执code进行判定是否发送成功(也可以根据code根据错误信息)
    result = response.json()
    print(result)

if __name__ == '__main__':
    wx_url = f'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={AppSecret}' 
    access_token = get_accessToken(wx_url)
    # 读取base64文件，一段一段发
    # base64file = 'test.txt'
    base64file = r'C:\Users\Ryan\git\rytutorial\src\base64_handle\dzzzone.zip.txt'
    blockcount = 0
    with open(base64file, "r") as fr:
        content = fr.read(MAX_CHAR)
        while content != '':
            content = base64file + str(blockcount).rjust(20,'0') + ':' + content + ': block'
            print('content block',blockcount,':',content )
            blockcount += 1
            # sendmsg(access_token, openid, content)
            # print(content)
            content = fr.read(MAX_CHAR)
            # time.sleep(2)
        # sendmsg(access_token, openid, '发送结束，共有block: '+str(blockcount))
    # print(access_token)
