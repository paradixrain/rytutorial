# coding = utf-8

# @filename: C:/Users/Ryan/git/rytutorial/wechat/upload_material.py
# @desc: 通过本地上传蔬菜
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022年4月2日


import requests
import json

appid = 'wx1b19ca1e3bd58663'
AppSecret = '2426bddb812c71854da049eb9f21ca7f'

def get_accessToken():
    try:
        wx_url = f'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={AppSecret}'
        res = requests.get(wx_url)
        res = res.text
        json_res = json.load(res)
        access_token = json_res.access_token
        print('access_token:', access_token)
        return access_token
    except Exception as e:
        print(e)

def main(upfile):
    upload_url = 'https://api.weixin.qq.com/cgi-bin/media/upload'
    token = get_accessToken()
    params = {"access_token": token, "type": 'image'}
    upload_file = {}
    file_params = { 'media' : ('tmp.jpg', content, content_type)}
    res = requests.post(url, params=params, files=files)
    res = json.loads(str(res.content, 'utf8'))

if __name__ == '__main__':
    upload_file = 'tt.png'
    main(upload_file)