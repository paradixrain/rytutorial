'''
Created on 2020年5月13日

@author: Ryan
'''
from builtins import str

'''
请求模板：
【问题标题】YBX-流水单处理报错
【报错描述】流水单行程待开的时候报错。提示是合计金额超过最大的开票限额0
【企业名称】英大财险
【纳税人识别号】9144030069119【联系电话】17778051202

响应模板：
1、@wxid_jaxefzifkx1c22 您的问题已接收，如有处理结果我们会第一时间反馈给您，感谢您的等候。
2、@wxid_jaxefzifkx1c22 「YBX-流水单处理报错」
- - - - - - - - - - - - - - -
基础信息管理下纳税主体管理中先找到税号以后点击关联机构，先取消对勾的机构点击保存关闭页面；在重新点击关联机构，先对勾的机构点击保存关闭页面；最后在税控设备管理中税控服务器管理下同步一下核心板信息

个人信息：
{'wxid': 'paradixrain', 'remark': '林宇', 'nickname': '索隆', 
'hd_profilephoto_url': 'http://wx.qlogo.cn/mmhead/ver_1/yloyMyQq4crqdYeRgnZALTm3vStww7JdsqxoTq0zkCmeQbeJrPlTBs6WHlzT1Ld2iborOVABcaatOlQS9eNfctIhJVzboU5wW41K9Zmib8ia4U/0', 'profilephoto_url': 'http://wx.qlogo.cn/mmhead/ver_1/yloyMyQq4crqdYeRgnZALTm3vStww7JdsqxoTq0zkCmeQbeJrPlTBs6WHlzT1Ld2iborOVABcaatOlQS9eNfctIhJVzboU5wW41K9Zmib8ia4U/132',
 'sex': 1, 'country': 'AX', 'type': 2, 'pid': 14184}

'''

from PyWeChatSpy import WeChatSpy
from lxml import etree
import requests
import re
import time
import sqlite3
import subprocess
import sys

import json
import urllib3
import hmac
import hashlib
import base64
import urllib.parse
import zipfile
import os
import socket
from datetime import date, timedelta, datetime
import shutil
import pickle
import random

from multiprocessing import Process
from pollen import get_pollen

# 配置 start
overtime_job = False  # 未执行的计划，是否在启动后继续执行,Ture 执行，False不执行
JOBENABLED = False  # 是否启动job轮询
dburl = './pywechatspy.db3'  # sqlite 数据库
chathistoryfile = './chathistoryfile' + time.strftime("%Y-%m-%d") + '.txt'  # 消息文件
recordchat = True  # 是否存储消息记录
dict_AutoUpdate = True  # 是否随时更新联系人
dict_pickle_contact = './contact.pkl'  # 联系人pickle 联系人文件，是一个字典
PICKLE_GAPTIME = 600  # 联系人 pickle 隔 PICKLE_GAPTIME 秒存储一次
STATUS_GAP_TIME = 60 * 60 * 8  # 状态检查 每隔 STATUS_GAP_TIME 秒执行一次
DB_GAPTIME = 3600  # 群组消息 每  PICKLE_GAPTIME 秒存进DB一次

PIDFILE = 'pid.txt'  # 存放当前进程的pid文件
# tmpFileDir = 'C:\\Users\\Long\\Downloads\\' # 临时文件目录
tmpFileDir = 'C:\\pywechat\\'  # 临时文件目录
# FILESTORAGEDIR = 'C:\\Users\\Long\\Documents\\WeChat Files\\paradixrain\\FileStorage\\File\\' + time.strftime("%Y-%m") + '\\' # 默认微信接收文件的路径
FILESTORAGEDIR = 'C:\\WeChat Files\\snowluxury\\FileStorage\\File\\' + time.strftime("%Y-%m") + '\\'  # 默认微信接收文件的路径
# FILESTORAGEDIR = 'D:\\work\\workspace\\pywechatspy\\file\\'

TERMINATEBEFORSHUTDOWN = True
GroupSetTag = True  # 是否开启群类型设置功能（该功能，通常在完成所有群设置以后关闭，在有新增群的时候打开）

replyButton = False  # 是否响应问题模板
replySelf = 1  # 是否响应自己发的消息（会导致陷入死循环，千万别开启，以后有空再调试），0表示别人发的消息，1表示自己发的消息
toDingding = True  # 是否转发到钉钉
robotUrl = 'https://oapi.dingtalk.com/robot/send?access_token=7ac883db2830f490a249853ee31b84c8f2191282a50f88e36c58cc95508ac211'
secret = 'SEC8b5043e355e10c0571f2362ae39b19e60df52ea29f791e3d933cf17615869b24'  # 钉钉机器人密码
DingAll = False  # 转发时是否钉所有人
ryanMessage = {  # 消息模板
    "msgtype": "text",
    "text": {"content": ""},
    "at": {"atMobiles": [],
           "isAtAll": False}
}

# 给ryanServer发送消息
serverIP = '172.28.30.150'
serverPORT = 29999
# 配置 end

# 心跳时间文件
BUFSIZE = 1024
client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
HEARTBEAT_CHECK = True
HEARTBEAT_GAPTIME = 60

# 检测税局网络
CHEKSJWL = False  # 检测开关
SJIPFILE = 'ip.txt'  # 税局ip地址清单
CHECKSJWL_GAPTIME = 3600 * 24  # 每gaptime检测税局网络

# 默认值 start
contact_dict = {}
Ryan_chatroomid = '20193490113@chatroom'  # 汇总群ID默认值，使用特殊命令可修改该值，用来将消息转发志：工单服务
Ryan_annouceto = '21775802506@chatroom'  # 通知群ID默认值，开发时候调试用 ID：21775802506@chatroom
# Ryan_annouceto='20076986511@chatroom'  # 通知群ID默认值，每10分钟，朝该id发送服务正常的信号，若未收到消息，则说明有异常
Ryan_Adminroom = '20076986511@chatroom'  # 管理群ID，只在这个群中，可执行特殊命令
qdtime = time.time()  # 服务启动时间
# 默认值 end

# 正则匹配 start
rePaternWxid1 = re.compile('@wxid_[a-z0-9]{14,} 「')  # 群里，点名
rePaternWxid2 = re.compile('YBX-@wxid_[a-z0-9]{14,}')  # 群名
# 正则匹配 end

# 重点客户超时预警机制配置 start
group_resp_dict = {}  # 组 清单及 responsetime，程序启动时从数据库读取
group_lastmessage_dict = {}  # 组 清单及最后一条消息的时间，程序启动时默认为空
group_announcer_dict = {}  # 组消息告知清单

annouceType = 2  # 超时报警机制，1为钉钉群报警，2为微信群+个人报警，0为不报警
MESSAGE_GAP_TIME = 60  # 每 MESSAGE_GAP_TIME 秒，进行一次超时消息检查
NEXT_WARN_TIME = 60  # 预警后下一次间隔的预警时间
MAX_WARN_COUNT = 5  # 做多预警 MAX_WARN_COUNT 次
# 重点客户超时预警机制配置 end

# 消息记录存储到DB start
logIntoDb = True
# 消息记录存储到DB end

# 使用sendzip命令的时候，每次读取多少行（PS：每行80个字符），且base64会自动忽略换行符
LINE_SPLIT = 20
MAX_CHAR = 500
WX_GZH_DEBUG = 'gh_2f85869732f2'  # 南落师门
WX_GZH = 'gh_8858fbf6b5a1'  # 正式环境
WX_GZH_CESHI = 'gh_296a220c9a1f'  # paradixrain的接口测试
RECEIVE_ZIP_FILE = ""
RECEIVER = ''
SEND_FLAG = False

# 定义一个变量，是否开启定时推送健康接龙
JIE_LONG = False
# 人员到岗状态
REN_YUAN_STATUS = {'马丽丽': 1, '何永康': 1, '王强': 1, '王文': 1, '陈丹丹': 1, '孟雪滢': 1, '邹志伟': 1, '娄蒙蒙': 0, '余玲鑫': 1}
REN_YUAN = ['马丽丽', '何永康', '王强', '王文', '陈丹丹', '孟雪滢', '邹志伟', '娄蒙蒙', '余玲鑫']

# 定义一个变量，是否定时获取花粉信息
GET_HUAFEN_INFO = True

# 程序启动时，从pickle预加载联系人列表
if (dict_AutoUpdate == True):
    try:
        with open(dict_pickle_contact, 'rb') as f:
            contact_dict = pickle.load(f)
            f.close()
    except Exception as err:
        print('加载pickle文件失败', err)

# 程序启动时，从 数据库 预加载群response_time, 预加载群 announcer
try:
    sqlite3Conn = sqlite3.connect(dburl)
    sqliteCU = sqlite3Conn.cursor()
    sqlite3RyanSql = 'select groupwxid, groupname, responseTime, announceTo from groups'

    for row in sqliteCU.execute(sqlite3RyanSql):
        # print(row)
        # 每行都写入到 group_resp_dict 里
        group_resp_dict[row[0]] = row[2]
        group_announcer_dict[row[0]] = row[3]
        print('各个群  的responseTime 时间，通知者Announcer ', row[0], row[1], row[2], row[3])
except Exception as err:
    print('加载组信息发生异常', err)
finally:
    sqliteCU.close
    sqlite3Conn.commit()
    sqlite3Conn.close()

# 白名单，白名单的人发送的模板，只能在，Ryan_chatroomid中发送才会被识别
dict_whitelist = ['xugaizhen',  # 杨盖
                  'wangqiang900755',  # 王强
                  'paradixrain',  # 林宇
                  'wxid_jaxefzifkx1c22',  # 蒙蒙
                  'wxid_o4darlzy78ws21',  # 俞敏
                  'ym1843287917',  # 俞敏
                  'wxid_9iang7yrswms22',  # 松干
                  'GSG12138',  # 松干
                  'wxid_62z0re6jpmp322',  # 丹丹
                  'xin950108',  # 玲鑫
                  'xin950210',  # 玲鑫
                  'wxid_a0axtrldhyml32',  # 银保信工单
                  'wxid_lykyp5itn5rc22',  # 孟雪滢
                  'wxid_3757447575912',  # 王文
                  'jimokongchengzui003',  # 邹志伟
                  'wxid_32j7zeibgbt422'  # 永康
                  ]

# 管理员list，只有管理员才能调用特殊命令，设置群属性
dict_admlist = ['xugaizhen',  # 杨盖
                'wangqiang900755',  # 王强
                'paradixrain',  # 林宇
                'wxid_jaxefzifkx1c22',  # 蒙蒙
                'wxid_o4darlzy78ws21',  # 俞敏
                'ym1843287917',  # 俞敏
                'wxid_9iang7yrswms22',  # 松干
                'GSG12138',  # 松干
                'wxid_62z0re6jpmp322',  # 丹丹
                'xin950108',  # 玲鑫
                'xin950210',  # 玲鑫
                'wxid_a0axtrldhyml32',  # 银保信工单
                't23582516',  # 国元 李刚
                'wxid_lykyp5itn5rc22',  # 孟雪滢
                'wxid_3757447575912',  # 王文
                'jimokongchengzui003',  # 邹志伟
                'wxid_32j7zeibgbt422'  # 永康
                ]
# 配置，自动升级 start ,在每台机器路径都不一样，必须配置
'''
Ryan:20200611:18:57自动升级权限及风险高，全部停用
RUNPATH = 'D:\\work\\workspace\\pywechatspy\\' # 正常程序执行路径
BAKPATH = 'D:\\work\\workspace\\pywechatspyBAK\\' # 正常程序执行路径
ALLREMOTEUPDATE = True # 允许远程升级
UPDATEZIP='\\updateLuLu.zip' # 远程升级包名，此处未经调试，估计需要修改
UNZIPFLAG=False # 升级包解压是否顺利
'''


# 配置，自动升级 end


def ryanMoveFile(srcfile, dstfile):
    try:
        fpath, fname = os.path.split(dstfile)  # 分离文件名和路径
        if not os.path.exists(fpath):
            os.makedirs(fpath)  # 创建路径
        shutil.move(srcfile, dstfile)  # 移动文件
        print(srcfile, "归档至", dstfile, "完成")
    except Exception as err:
        print(err)


def zip2base64(zipfile, base64file):
    with open(zipfile, "rb") as fr:
        with open(base64file, "wb") as fw:
            base64.encode(fr, fw)


def base642zip(base64file, zipfile):
    with open(base64file, "r") as fr:
        with open(zipfile, "wb") as fw:
            base64.decode(fr, fw)


def send2wechat2(base64file):
    with open(base64file, "r") as fr:
        blockcount = 0
        content = fr.read(MAX_CHAR)
        while content != '':
            content = 'contentblock' + str(blockcount).rjust(20, '0') + ':' + content
            print('content block', blockcount, ':', content)
            blockcount += 1
            spy.send_text(WX_GZH, content)
            content = fr.read(MAX_CHAR)
            time.sleep(1)


def ryanArchieveLog(TIME):
    """
    method:归档当前时间戳之前的所有日期的文件，在入参处控制，time只能是2020年8月24日 00:00:05也就是时间戳1598198405相差 86400 正负5秒
    .才触发归档动作，保证一天只归档一次
    """
    try:
        yesterday = (date.today() + timedelta(days=-1)).strftime("%Y-%m-%d")
        ryanArchievSrc = './chathistoryfile' + yesterday + '.txt'
        ryanArchievDst = './chathistoryfile/chathistoryfile' + yesterday + '.txt'
        ryanMoveFile(ryanArchievSrc, ryanArchievDst)
    except Exception as err:
        print(err)
    finally:
        pass


def matchPercent(orgin_content):
    # 判断提问的问题的匹配度，最高100，最低0
    score = 0
    if (orgin_content.find('【问题标题】') >= 0
            and orgin_content.find('【报错描述】') >= 0
            and orgin_content.find('【企业名称】') >= 0
            and orgin_content.find('【纳税人识别号】') >= 0
            and orgin_content.find('【联系电话】') >= 0):
        score = 100
    else:
        if (orgin_content.find('问题标题') >= 0):
            score += 15
        if (orgin_content.find('报错描述') >= 0):
            score += 15
        if (orgin_content.find('企业名称') >= 0):
            score += 15
        if (orgin_content.find('纳税人识别号') >= 0):
            score += 15
        if (orgin_content.find('联系电话') >= 0):
            score += 15
        score += orgin_content.count('【') * 2.5
        score += orgin_content.count('】') * 2.5
    # print('score',score)
    return score


def rename(pwd: str, filename=''):
    """压缩包内部文件有中文名, 解压后出现乱码，进行恢复"""
    path = f'{pwd}/{filename}'
    if os.path.isdir(path):
        for i in os.scandir(path):
            rename(path, i.name)
    newname = filename.encode('cp437').decode('gbk')
    os.rename(path, f'{pwd}/{newname}')


def get_encrypt(secret):
    """对消息进行加密，满足钉钉机器人发送需求"""
    secret_enc = secret.encode('utf-8')
    timestamp = str(round(time.time() * 1000))
    string_to_sign = '{}\n{}'.format(timestamp, secret)
    string_to_sign_enc = string_to_sign.encode('utf-8')
    hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
    sign = urllib.parse.quote_plus(base64.b64encode(hmac_code))
    return timestamp, sign


def get_reply(data):
    url = f"http://api.douqq.com/?key=&msg={data}"  # key获取地址http://xiao.douqq.com/
    resp = requests.get(url)
    return resp.text


def send_Ding(orgin_content, nickname):
    # 如果钉钉状态打开，则将消息转发到钉钉
    # 目前只用isAtAll 控制是否圈全员，以后配置完值班人员以后，就可以圈每天的值班人员了
    if (toDingding == True):
        if (nickname == None):
            ryanMessage["text"]["content"] = "主人，收到一个新工单，请处理:\n" + orgin_content
        else:
            ryanMessage["text"]["content"] = "主人，收到一个来自:" + nickname + ":的新工单，请处理:\n" + orgin_content
        ryanMessage["at"]["isAtAll"] = DingAll

        # print(ryanMessage)
        # 将字典数据编码为json数据
        encoded_data = json.dumps(ryanMessage).encode('utf-8')
        httpPM = urllib3.PoolManager()
        getEncrypt = get_encrypt(secret)
        postUrl = robotUrl + '&timestamp=' + getEncrypt[0] + '&sign=' + getEncrypt[1]
        r = httpPM.request('POST', postUrl, body=encoded_data,
                           headers={'Content-Type': 'application/json', "Accept": "text/plain"})
        print(r.data.decode('utf-8'))
    return True


def check_SJWL(SJIPFILE):
    arr = []  # 税局ip数组
    failedArr = []  # 联通失败的数组
    if 'win*' == sys.platform:
        config = sys.path[0] + '\ip.dat'
    else:
        config = sys.path[0] + '/ip.dat'

    try:
        file = open(config, "r")
        for line in file.readlines():
            if line.startswith("#"):
                continue
            else:
                arr.append((line.replace('\n', '').split(' ')))
    finally:
        file.close()

    for arra in arr:
        sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sk.settimeout(2)
        ip = (str(arra[0]), int(arra[1]))
        try:
            sk.connect(ip)
            print(arra[0] + '的端口' + arra[1] + '通')
        except Exception:
            print(arra[0] + '的端口' + arra[1] + '不通')
            try:
                failedArr.append(arr)
            except Exception:
                print('failedArr failed')
        finally:
            sk.close()
    print('税局网络检测结束')
    return failedArr


def parser(data):
    # print("data:",data)
    global RECEIVE_ZIP_FILE, RECEIVER, SEND_FLAG

    # 全局变量声明
    global group_lastmessage_dict
    global chathistoryfile
    global contact_dict
    global dict_admlist
    nowtime = time.time()

    if data["type"] == 1:
        # 登录信息
        print(data)
        # 查询联系人列表
        # spy.query_contact_list()
    elif data["type"] == 2:
        # 联系人信息，登录信息
        print("type=2:", data)
        try:
            wxid, nickname = data["wxid"], data["nickname"]
            contact_dict[wxid] = nickname
        except Exception as err:
            print('get nickname error', err)
        finally:
            pass
    elif data["type"] == 200:
        # 微信每5秒的登录状态验证，计划任务也通过该类型触发
        chathistoryfile = './chathistoryfile' + time.strftime("%Y-%m-%d") + '.txt'  # 消息文件

        # 推送socket
        if (HEARTBEAT_CHECK == True and (nowtime - qdtime) % HEARTBEAT_GAPTIME < 5):
            # print('HEARBEAT_CHECK STARTS')
            try:
                BUFSIZE = 1024
                client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

                msg = str(nowtime)
                ip_port = ('172.28.30.150', 39999)
                client.sendto(msg.encode('utf-8'), ip_port)
                # print(' HEARTBEAT_CHECK DONE')
            except Exception as err:
                print(err)
            finally:
                client.close()

        # 定时检查有无超时群消息
        if (annouceType != 0 and (nowtime - qdtime) % MESSAGE_GAP_TIME < 5):
            # 每隔 MESSAGE_GAP_TIME 遍历一次所有群，
            # 如果推送消息开启，超过报警时间的，就报警一次，然后移除，否则pass
            for groupwxid in list(group_lastmessage_dict.keys()):
                print('检查群组超时情况：', groupwxid)
                if ((group_lastmessage_dict[groupwxid] < nowtime)):
                    # 根据 annouceType 推送预警消息
                    print('推送群预警消息', groupwxid, group_lastmessage_dict[groupwxid])
                    try:
                        if (annouceType == 2):
                            spy.send_text(Ryan_annouceto, '群：' + contact_dict[groupwxid] + ' 的消息待回复')
                            # 微信个人报警
                            time.sleep(2)
                            spy.send_text(group_announcer_dict[groupwxid], '群：' + contact_dict[groupwxid] + ' 的消息待回复')
                        elif (annouceType == 1):
                            # 钉钉报警
                            pass
                        else:
                            pass
                    except Exception as err:
                        print('发送预警消息失败，', err)

                    # 发送完毕后，pop删除消息
                    group_lastmessage_dict.pop(groupwxid)
                    print('预警消息发送完毕，从message队列中删除之')
                else:
                    print('群，', groupwxid, ',的消息未超时')

        # 每 10*60*60秒，发送一次状态消息
        if ((nowtime - qdtime) % STATUS_GAP_TIME < 5.1):
            anncementmsg = time.strftime("%Y-%m-%d %H:%M:%S")
            spy.send_text(Ryan_annouceto, '现在时间是：' + anncementmsg + ',守护进程每' + '10' + '小时，汇报状态正常')
            # 测试：   spy.send_file(Ryan_annouceto, FILESTORAGEDIR + '2020-06-05.log')
            # 读sqlite中的job表
            if (JOBENABLED == True):
                try:
                    sqlite3Conn = sqlite3.connect(dburl)
                    sqliteCU = sqlite3Conn.cursor()
                    sqlite3RyanSql = 'select jobtime,touser,usermsg,type,ID from job where flag=\'Y\' order by jobtime asc'

                    for row in sqliteCU.execute(sqlite3RyanSql):
                        print(row)
                        # 判断时间是否符合范围，符合则发送
                        struct_time = time.strptime(row[0], "%Y%m%d%H%M%S")
                        jobtimestamp = time.mktime(struct_time)
                        # 按asc 排序后，第一条记录的执行时间都没到，则中断
                        gap_time = nowtime - jobtimestamp
                        if ((gap_time < 0)):
                            print(jobtimestamp, end='')
                            print('：任务时间未到，pass')
                            break

                        if ((gap_time > 600) and (overtime_job == False)):
                            print(jobtimestamp, end='')
                            print('任务超时10分钟，pass')
                            break
                        else:
                            print('现在开始执行:', end='')
                            print(jobtimestamp)
                            if (row[3] == 'TEXT'):
                                if (row[4] == None):  # 有ID则指定人
                                    spy.send_text(row[1], row[2])
                                    print('send_text:, ', row[1], row[2])
                                else:
                                    spy.send_text(row[1], row[2], row[4])
                                    print('send_text:', row[1], row[2], row[4])
                            else:
                                print('Wrong type:', row[0])

                            # 标记处理完成
                            sqliteCU.execute('update job set flag =? where jobtime = ?',
                                             ('N', row[0]))
                except Exception as err:
                    print('sqlite定时计划报错', err)
                finally:
                    sqliteCU.close
                    sqlite3Conn.commit()
                    sqlite3Conn.close()

        if ((nowtime - qdtime) % DB_GAPTIME < 5.1):
            # 每 DB_GAPTIME ，将DB写入磁盘
            try:
                # 添加至数据库报警群里
                sqlite3Conn = sqlite3.connect(dburl)
                sqliteCU = sqlite3Conn.cursor()

                for everyGroup in group_announcer_dict:
                    sqlite3RyanSql = 'select groupwxid from groups where groupwxid = \'' + everyGroup + '\''
                    ryanResult = sqliteCU.execute(sqlite3RyanSql)
                    rowcount = len(ryanResult.fetchall())
                    # print('sqlite3RyanSql',sqlite3RyanSql, rowcount)
                    if rowcount < 1:  # 空记录，使用insert
                        print('insert 群消息通知者', everyGroup, contact_dict[everyGroup], group_announcer_dict[everyGroup],
                              group_resp_dict[everyGroup])
                        sqlite3RyanSql = 'insert into groups(groupwxid, groupname, announceTo, responseTime ) values(?, ?, ?, ?)'
                        sqliteCU.execute(sqlite3RyanSql, (
                        everyGroup, contact_dict[everyGroup], group_announcer_dict[everyGroup],
                        group_resp_dict[everyGroup]))
                        # print(sqlite3RyanSql)
                    else:  # 已存在，使用update
                        print('update 群消息执行者', everyGroup, group_announcer_dict[everyGroup])
                        sqlite3RyanSql = 'update groups SET announceTo = ? where groupwxid=? '
                        sqliteCU.execute(sqlite3RyanSql, (group_announcer_dict[everyGroup], everyGroup))
                        # print(sqlite3RyanSql)
            except Exception as err:
                print('设置群消息，报错，', err)
            finally:
                sqlite3Conn.commit()
                sqliteCU.close
                sqlite3Conn.close()
                print('群组pickle定时存储至数据库，完毕')
        if SEND_FLAG:
            spy.send_text(WX_GZH_CESHI, '收到')

        if ((nowtime - qdtime) % PICKLE_GAPTIME < 5.1):
            # 每10分钟，将pickle文件写入磁盘
            try:
                with open(dict_pickle_contact, 'wb') as f:
                    pickle.dump(contact_dict, f)
                    f.close()
            except Exception as err:
                print('保存pickle进磁盘出错', err)
            finally:
                print('联系人，每10分钟备份一次，完成')

        # 每60*60*100秒发送client的时间给ryanServer
        '''
        if( int(nowtime % 60*60*100) < 6):
            try:
                print('send package to',serverIP,serverPORT)
                client = socket.socket()
                client.connect((serverIP,serverPORT))
                with client:
                    s = str(time.time())
                    print('clientTime sent:',s)
                    client.sendall(bytes(s, encoding="utf8"))
                    data = client.recv(1024)
                print('Received:',data)
            except ConnectionRefusedError:
                # 连接ryanServer服务器失败，发送消息
                warnMSG="主人，监控服务 "+serverIP+" 无响应！\n现在时间是："+time.strftime("%Y-%m-%d %H:%M:%S")
                send_Ding(warnMSG)
            except Exception as err:
                print(err)
            finally:
                client.close()
        '''

        # 每一天（3600*24秒）归档前一天的日志文件
        if (int(nowtime % 86400) < 6):
            try:
                print('开始归档')
                ryanArchieveLog(time.localtime(nowtime))
            except Exception as err:
                print(err)
            finally:
                print('归档进程完成')

        # 每天早上9点，根据JIE_LONG是否开启，判断是否发送消息
        if JIE_LONG and (now := datetime.now()) and now.hour == 8 and now.minute == 18 and 0 <= now.second <= 6:
            try:
                random.shuffle(REN_YUAN)
                rtn_str = ''
                # 判断今天是否为周末
                if now.weekday() == 5 or now.weekday() == 6:
                    # 所有REN_YUAN_STATUS都按0处理
                    for index, people in enumerate(REN_YUAN):
                        rtn_str += f"{index + 1}. {people}{now.month}月{now.day}日未到岗，本人及共同居住人均无异常\n"
                else:
                    for index, people in enumerate(REN_YUAN):
                        if REN_YUAN_STATUS[people] == 1:
                            status = '正常到岗'
                        else:
                            status = '居家办公'
                        rtn_str += f"{index + 1}. {people}{now.month}月{now.day}日{status}，本人及共同居住人均无异常\n"
                rtn_str = rtn_str[2:]
                print(rtn_str)
                spy.send_text("xin950108", rtn_str)
            except Exception as err:
                print(err)

        # 每天早上8点30，根据GET_HUAFEN_INFO是否开启，判断是否发送消息
        if GET_HUAFEN_INFO and (now := datetime.now()) and now.hour == 8 and now.minute == 55 and 0 <= now.second <= 6:
            try:
                weather = get_pollen("101010100")
                spy.send_text('paradixrain', weather)
            except Exception as err:
                print(err)

        # 定时检测税局网络，每 CHECKSJWL_GAPTIME
        if (int(nowtime % CHECKSJWL_GAPTIME) < 6 and (CHEKSJWL == True)):
            failedArrs = check_SJWL(SJIPFILE)
            try:
                for failedArr in failedArrs:
                    print(failedArr[2], 'failed')
            except Exception as err:
                print(err)

    elif data["type"] == 203:
        # 微信登出
        print("微信退出登录")
    elif data["type"] == 5:
        # 消息
        for item in data["data"]:
            # print('item:', item) # 这里记录了联系人的信息，以后每次使用这个进行联系人信息的维护
            if item["self"] == replySelf:
                # 是否是自己发的消息
                break
            if item["msg_type"] == 1:
                global replyButton

                # 普通聊天消息，分析内容，尝试抽取 手法用户、及消息
                wxid1, wxid2, orgin_content = item["wxid1"], item.get("wxid2"), item["content"]

                # 根据开关记录消息
                if (recordchat == True):
                    with open(chathistoryfile, mode='a', encoding='utf-8') as f:
                        cur_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                        if wxid2:
                            str_buf = cur_time + ' ' + wxid1 + ' | ' + wxid2 + ' | ' + orgin_content + '\n'
                        else:
                            str_buf = cur_time + ' ' + wxid1 + ' | | ' + orgin_content + '\n'
                        print(str_buf)
                        f.write(str_buf)
                        f.close()

                    if (logIntoDb == True):
                        try:
                            # 添加消息记录到数据库里
                            sqlite3Conn = sqlite3.connect(dburl)
                            sqliteCU = sqlite3Conn.cursor()

                            sqlite3RyanSql = 'insert into chathistory(msgtime, fromuser, msg, chatroomid) values(?, ?, ?, ?)'
                            sqliteCU.execute(sqlite3RyanSql, (cur_time, wxid2, orgin_content, wxid1))
                        except Exception as err:
                            print('设置群消息，报错，', err)
                        finally:
                            sqlite3Conn.commit()
                            sqliteCU.close
                            sqlite3Conn.close()
                            print('群组pickle定时存储至数据库，完毕')

                # reply = get_reply(item["content"])
                # spy.send_text(wxid1, reply)

                contact = contact_dict.get(wxid1)
                if contact:
                    print("消息来源，联系人： ", contact)
                else:
                    print("没找到联系人。")
                    spy.query_contact_details(wxid1)

                try:
                    if wxid1.endswith("chatroom") and wxid2:
                        # 记录群last message time
                        print('群消息，入库', wxid1)
                        if (wxid1 in group_resp_dict):
                            print(wxid1, '的预警时间是', group_resp_dict[wxid1], '消息通知者是:', wxid2)
                            # 如果是需要预警的群（即，群存在于数据库表中）
                            # 如果 wxid2 是管理员，则说明消息已经回复，可以删除；否则，是客户消息，将下次预警时间  + 允许间隔时间 保存
                            if (wxid2 in dict_admlist):
                                print('管理员已回复，移除')
                                # print('移除：',wxid1, group_lastmessage_dict[wxid1])
                                if (wxid1 in group_lastmessage_dict):
                                    group_lastmessage_dict.pop(wxid1)
                            else:
                                print('last mesage time更新')
                                # print('更新',group_resp_dict[wxid1])
                                # print('更新：',wxid1, group_lastmessage_dict[wxid1])
                                group_lastmessage_dict[wxid1] = nowtime + group_resp_dict[wxid1]
                        else:
                            print('非预警群，略过')
                except Exception as err:
                    print('存储last message time异常，略过,', err)

                    # 打印消息来源，如果不在联系人列表中，则进行查询
                    contact = contact_dict.get(wxid2)
                    if contact:
                        print("消息来源，群聊： ", contact)
                    else:
                        # print("没找到群聊：")
                        spy.query_contact_details(wxid2, wxid1)

                # step0. todo

                # step1. 抓特殊命令
                '''
                    1、checkstatus:服务器健康状态
                    2、setLuLuhere:设置当前群为汇总群
                '''
                # 特殊命令1.匹配服务健康状态
                if (orgin_content == 'checkstatus'):
                    spy.send_text(Ryan_annouceto, '服务器正常')
                    print('校验服务状态，完成')
                    break

                # 特殊命令2.将当前群设置为汇总群
                elif (orgin_content == 'setLuLuhere'):
                    global Ryan_chatroomid
                    Ryan_chatroomid = wxid1
                    print('Ryan_chatroomid set')
                    # spy.logger.info('Ryan_chatroomid '+ wxid1 +' set')
                    break

                # 特殊命令3.关机
                elif ((orgin_content == 'Lulushutdown') and (wxid1 == Ryan_Adminroom)):  # 管理群
                    # 目前只接收管理群的命令
                    print('begin to shutdown')
                    shutdownDelay = 120
                    shutdownCommand = "shutdown /s /t " + str(shutdownDelay)

                    # 使用兼容性更好的subprocess方法
                    cmdstatus, cmdoutput = subprocess.getstatusoutput(shutdownCommand)
                    print('程序执行结果：', cmdoutput)
                    if (cmdstatus != 0):
                        reply = '执行失败，返回代码' + str(cmdstatus)
                        spy.send_text(Ryan_annouceto, reply)
                    else:
                        spy.send_text(Ryan_annouceto, '系统将会在' + str(shutdownDelay) + '秒后关机')

                    # 废弃的os.systm方法
                    # cmdoutput = os.system(shutdownCommand)

                    if (TERMINATEBEFORSHUTDOWN == True):  # 关机前自杀
                        with open(PIDFILE, 'r') as f:
                            PID = f.readline()  # 读取第一行的PID
                        taskkillCmd = 'taskkill /F /T /PID ' + PID
                        spy.send_text(Ryan_annouceto, '韦小鹿下线咯')
                        os.system(taskkillCmd)
                    else:
                        pass
                    break

                # 特殊命令4.远程升级，unDone， 20200611尝试多次未果，该命令不可用
                elif ((orgin_content == 'LuluUpdate') and (wxid1 == Ryan_Adminroom)):  # 管理群
                    # 目前只接收管理群的命令
                    '''需要满足以下条件：
                    0. 停用自动升级，风险高
                    1、py文件被压缩成zip包；
                    2、pywechatspy.py文件名不能写错
                    ''
                    '' 
                    if( ALLREMOTEUPDATE == True ):
                        # 找当前升级包
                        YearDay=time.strftime("%Y-%m")
                        UPDATEPACKAGE=FILESTORAGEDIR + YearDay + UPDATEZIP
                        file_exists = os.path.exists(UPDATEPACKAGE)

                        if(file_exists == True):
                            print('升级包找到，开始升级')
                            try:
                                zipFile = zipfile.ZipFile(UPDATEPACKAGE)
                                dirname = UPDATEPACKAGE.replace('.zip', '')
                                # 如果存在与压缩包同名文件夹 提示信息并跳过
                                if os.path.exists(dirname):
                                    print(f'{zipFile} dir has already existed')
                                    spy.send_text(Ryan_annouceto,'升级文件已存在')
                                    
                                else:
                                    os.mkdir(dirname)
                                    zipFile.extractall(dirname)
                                    zipFile.close()
                                    # 递归修复编码，文件名中有中文时候需要修复
                                    rename(dirname)
                                    UNZIPFLAG=True
                                    print('解压完成')
                            except:
                                print(f'{zipFile} unzip fail')
                                spy.send_text(Ryan_annouceto,'解压失败')
                        else:
                            spy.send_text(Ryan_annouceto,'升级文件未找到')
                        
                        # 解压完成后开始替换升级
                        # 备份
                        bakDir=BAKPATH+time.strftime("%Y-%m-%d-%H-%M-%S")
                        os.mkdir(bakDir)
                        bakCMD="xcopy /Y /s " + RUNPATH + '* ' + bakDir
                        cmdstatus, cmdoutput = subprocess.getstatusoutput(bakCMD)
                        print('备份命令：', bakCMD, '执行结果', cmdstatus, cmdoutput)
                        if(cmdstatus != 0):
                            spy.send_text(Ryan_annouceto,'备份失败，退出')
                            break
                        
                        # 覆盖升级
                        ''
                        ''
                        with open(PIDFILE,'r') as f:
                            PID=f.readline()    #读取第一行的PID
                        updateCMD1 = 'taskkill /F /PID ' + PID
                        updateCMD2 = 'xcopy /Y /s ' + UPDATEPACKAGE.replace('.zip', '') + '\* ' + RUNPATH
                        updateCMD3 = 'python ' + RUNPATH + 'pywechatspy.py'
                        updateCMD = updateCMD1 + ' && ' + updateCMD2 + ' && ' + updateCMD3
                        print('升级命令' ,updateCMD)
                        ''
                        # cmdstatus, cmdoutput = subprocess.getstatusoutput(updateCMD)
                        # print('升级命令' ,updateCMD, '执行结果', cmdstatus, cmdoutput) # 因为进程被杀死，所以这行也不会被执行，考虑使用多进程来实现
                        # 因为受环境变量影响，启动路径总是C盘，而不是D，所以需要将subprocess的方案改成bat方案，并调用多进程的方式来启动，子进程需要sleep 10秒来保证父进程能打印出消息
                        # todo
                        ''

                        updateBat = RUNPATH + 'update.bat'
                        diskPF = RUNPATH[:2]
                        with open(updateBat, 'w') as f:
                            f.writelines('@echo off')
                            f.writelines(updateCMD1)
                            f.writelines(updateCMD2)
                            f.writelines(updateCMD3)
                            f.writelines('cd %1')
                            f.writelines(diskPF)
                            f.writelines('python %1\pywechatspy.py')
                        # todo
                        # 或者使用进程通信的方式，使用主文件main.py来启动pywechatspy.py和update.py，然后pywechatspy处理完毕后，回传message给
                        # update.py，update.py收到消息后，关闭pywechatspy并替换文件，然后main.py重启pywechatspy.
                        ''
                        ''
                        # 再或者使用自更新，网上说，程序执行的时候，不能删除，但是可以重命名,试了下果然如此，这个方法比较简单
                        # 重命名 
                        filename1=RUNPATH+'pywechatspy.py'
                        filename2=dirname
                        os.rename(filename1,RUNPATH+'pywechatspy.py'+time.strftime("%Y-%m-%d-%H-%M-%S"))
                        # 新包复制到RUNPATH
                        print('filename1,filename2 ', filename1,filename2)
                        copyCMD='copy \"'+ filename2 + '\" \"' + filename1 + '\"'
                        cmdstatus, cmdoutput = subprocess.getstatusoutput(copyCMD)
                        #os.system('copy %s %s' % (filename2, filename1))
                        print('执行copy,',copyCMD,'结果:', cmdstatus,cmdoutput)
                        # 复制完毕删除FileStore目录下的updateLulu
                        delCMD='rd /s /q \"' + dirname + '\"'
                        cmdstatus, cmdoutput = subprocess.getstatusoutput(delCMD)
                        print('执行del,',delCMD,'结果:', cmdstatus,cmdoutput)
                        
                        #os.system('rd /s /q %s' % (dirname))
                        # 重启服务
                        python = sys.executable
                        os.execl(python, python, '\"'+filename1+'\"')
                        
                        
                        pass
                    else:
                        # 远程升级开关未打开
                        spy.send_text(Ryan_annouceto,'远程升级开关未打开')
                        pass
                    '''
                    break

                # 特殊命令5.自由命令.谨慎使用
                elif ((orgin_content.find('LuluCmd') > -1) and (wxid1 == Ryan_Adminroom)):  # 管理群
                    # 目前只接收管理群的命令
                    # cmdLoc = orgin_content.find('LuluCmd')
                    ryanCommand = orgin_content[8:]
                    print('执行LuLuCmd:', ryanCommand)
                    cmdstatus, cmdoutput = subprocess.getstatusoutput(ryanCommand)
                    print('执行结果', cmdstatus, cmdoutput)

                    # 因为有时候字符串超长会导致微信崩溃，使用文件来发送执行结果
                    randomFileName = str(round(time.time() * 1000))
                    tmpFile = tmpFileDir + randomFileName + '.txt'
                    # print(tmpFile)
                    with open(tmpFile, 'w') as f:
                        f.write(cmdoutput)
                    print(tmpFile, '写入完成，准备发送')
                    spy.send_file(Ryan_Adminroom, tmpFile)

                    # 删掉临时文件
                    '''ryanCommand = 'del ' + tmpFile
                    cmdstatus, cmdoutput = subprocess.getstatusoutput(ryanCommand)
                    print(tmpFile ,' deleted. return code:' ,cmdstatus)
                    '''
                    break

                # 特殊命令6.group标签设置命令
                elif ((orgin_content.find('SetGroupType') > -1) and GroupSetTag == True and (wxid2 in dict_admlist)):
                    # 使用命令 "SetGroupType SK QT"，"SetGroupType JX QTJX"，的方式
                    # 第一个参数为字段类型，第二个参数为字段值
                    # 字段类型可以为：SHORTNAME,SK,JX,DZ,QZ,FLAG
                    updateGroup = orgin_content.split(' ', 3)
                    if ((updateGroup[0] != 'SetGroupType') or (
                            updateGroup[1] not in {'SHORTNAME', 'SK', 'JX', 'DZ', 'QZ', 'FLAG', 'PJHY', 'KCRZ',
                                                   'JDCHY'})):
                        spy.send_text(wxid1, '参数错误')
                        break
                    try:
                        sqlite3Conn = sqlite3.connect(dburl)
                        sqliteCU = sqlite3Conn.cursor()

                        sqlite3RyanSql = 'select 1 from groups where groupwxid = \'' + wxid1 + '\''
                        ryanResult = sqliteCU.execute(sqlite3RyanSql)
                        rowcount = len(ryanResult.fetchall())
                        if rowcount < 1:  # 空记录，使用insert
                            sqlite3RyanSql = 'insert into groups(groupwxid, groupname, ' + updateGroup[
                                1] + ') values(?, ?, ?)'
                            sqliteCU.execute(sqlite3RyanSql, (wxid1, '待更新', updateGroup[2]))
                            # print(sqlite3RyanSql)
                            # sqliteCU.execute(sqlite3RyanSql)
                            print('insert 执行完毕')
                        else:  # 已存在，使用update
                            sqlite3RyanSql = 'update groups SET ' + updateGroup[1] + '= ? where groupwxid=? '
                            sqliteCU.execute(sqlite3RyanSql, (updateGroup[2], wxid1))
                            # print(sqlite3RyanSql)
                            # sqliteCU.execute(sqlite3RyanSql)
                            print('update 执行完毕')

                    except Exception as err:
                        print('使用SetGroupType命令设置组出错', err)
                    finally:
                        sqliteCU.close
                        sqlite3Conn.commit()
                        sqlite3Conn.close()
                    break

                # 特殊命令7. 设置群消息通知人员
                elif ((orgin_content.startswith('GroupAnnounceMe')) and (wxid1.endswith("chatroom")) and (
                        wxid2 in dict_admlist)):
                    # 插入dict
                    group_announcer_dict[wxid1] = wxid2
                    # 添加默认值
                    try:
                        ryanSplit = orgin_content.split()
                        group_resp_dict[wxid1] = int(ryanSplit[1])
                    except Exception:
                        group_resp_dict[wxid1] = 300
                    finally:
                        print('添加群消息通知人员完毕.')

                # 特殊命令8. 取消群消息通知，需要管理者操作
                elif ((orgin_content == 'GroupAnnounceCancel') and (wxid1.endswith("chatroom")) and (
                        wxid2 in dict_admlist)):
                    # 删除通知者
                    try:
                        group_announcer_dict.pop(wxid1)
                        group_resp_dict.pop(wxid1)
                    except Exception as err:
                        print('remove error', err)
                    try:
                        # 删除数据库中的通知者
                        sqlite3Conn = sqlite3.connect(dburl)
                        sqliteCU = sqlite3Conn.cursor()
                        sqlite3RyanSql = 'delete from groups where groupwxid = \'' + wxid1 + '\''
                        ryanResult = sqliteCU.execute(sqlite3RyanSql)
                    except Exception as err:
                        print(err)
                    finally:
                        sqlite3Conn.commit()
                        sqliteCU.close
                        sqlite3Conn.close()
                        print('解除群消息通知，完毕')

                # 特殊命令9. 将zip包发送到微信公众号 ,
                elif (orgin_content.startswith('sendzip') and (wxid1 in dict_admlist)):
                    # 命令： sendzip  1.zip
                    # 会将zip 编码为 base64，并分段（每段MAX_LINE）发给公众号
                    try:
                        print('开始执行 sendzip 命令')
                        spy.send_text(wxid1, '发送开始')
                        send_Ding("文件发送开始", None)
                        _, file = orgin_content.split()
                        filename = FILESTORAGEDIR + file
                        # 读取文件，分段
                        base64file = filename + '.txt'
                        zip2base64(filename, base64file)
                        # 等秒，防止磁盘写入问题
                        time.sleep(2)
                        # 读取文件
                        # send2wechat2(base64file)

                        blockcount = 0
                        with open(base64file, "r") as fr:
                            content = fr.read(MAX_CHAR)
                            while content != '':
                                content = file + str(blockcount).rjust(20, '0') + ':' + content + ': block'
                                print('content block', blockcount, ':', content)
                                blockcount += 1
                                spy.send_text(WX_GZH, content)
                                if WX_GZH_DEBUG:
                                    spy.send_text(WX_GZH_DEBUG, content)
                                content = fr.read(MAX_CHAR)
                                time.sleep(1)
                        spy.send_text(wxid1, '发送结束，共有block: ' + str(blockcount))
                        send_Ding('发送结束，共有block: ' + str(blockcount), None)
                    except Exception as err:
                        print('命令有误', err)
                # 特殊命令10. 根据到岗状态修改REN_YUAN_STATUS
                elif (orgin_content in ('到岗', '未到岗') and (wxid1 in dict_admlist)):
                    try:
                        spy.send_text("paradixrain", '宇哥，有人考勤状态变了，修改下这个人状态吧')
                    except Exception as err:
                        print('命令有误', err)
                # 特殊命令11. 准备接收zip文件
                elif (orgin_content.startswith('receivezip') and (wxid1 in dict_admlist)):
                    try:
                        # 命令 receivezip 1.zip
                        # 会发送消息给测试公众号，然后配合微信服务端的 replywechat.py 1.zip 发送程序
                        # 配合特殊命令12, 把文件进行还原 WX_GZH_CESHI
                        if RECEIVER == '':
                            RECEIVER = wxid1
                            SEND_FLAG = True
                            _, RECEIVE_ZIP_FILE = orgin_content.split()
                            print('接收的文件名：', RECEIVE_ZIP_FILE)
                            spy.send_text(WX_GZH_CESHI, orgin_content)
                            time.sleep(2)
                            spy.send_text(wxid1, '准备完毕，请在服务端使用 replywechat.py 发起推送')
                        else:
                            spy.send_text(wxid1, '有其他文件正在传输中，请稍后再试')
                    except Exception as err:
                        print('命令有误', err)
                # 特殊命令12. 数据接收流
                elif (orgin_content.startswith('receiveb64zip') and (wxid1 == WX_GZH_CESHI)):
                    # 'receiveb64zip:base64string:block'
                    try:
                        _, base64string, block = orgin_content.split(':')
                        b64file = 'b64file.b64.txt'
                        with open(b64file, 'a') as fw:
                            fw.write(base64string)
                        if block == 'blockend':
                            SEND_FLAG = False
                            # 将RECEIVER, RECEIVE_ZIP_FILE 清空，把RECEIVE_ZIP_FILE 删除
                            print('接收完毕，重置RECEIVER，开始还原', b64file, RECEIVER, RECEIVE_ZIP_FILE)
                            # 收到结束标识，将b64文件还原
                            base642zip(b64file, RECEIVE_ZIP_FILE)
                            # 将还原后的zip 文件发送回给RECEIVER
                            spy.send_file(RECEIVER, RECEIVE_ZIP_FILE)
                            spy.send_text(RECEIVER, '文件已发送，请接收')
                            print('文件已发送')
                            os.remove(RECEIVE_ZIP_FILE)
                            RECEIVER = ''
                    except Exception as e:
                        print(e)
                        RECEIVER = ''
                        os.remove(b64file)
                        SEND_FLAG = False
                        RECEIVE_ZIP_FILE = ''
                else:
                    pass

                # step2. 匹配响应模板1
                # @wxid_a0axtrldhyml32 您的问题已接收，如有处理结果我们会第一时间反馈给您，感谢您的等候。
                # 这里因为转发时候，不保留任何原消息，所以，无法知道该消息是反馈给谁的，需要让刘方明增加一个标志，如，问题名称和id，否则无法原路返回原群
                # 删除@串，原路返回
                if (orgin_content.find('您的问题已接收，如有处理结果我们会第一时间反馈给您，感谢您的等候')) > 0:
                    pass

                # step3. 匹配响应模板2——从工单系统反馈回来的消息
                if (orgin_content.find('- - - - - - - - - - - - - - -') > 0
                        # and rePaternWxid1.match(orgin_content)
                        and orgin_content.find('「')
                        and orgin_content.find('」')
                ):
                    if (replyButton == False):
                        print('回复开关关闭，不响应改请求')
                        break
                    try:
                        # 新方法，使用 切割来获取wxid1和wxid2，目的是为了兼容wxid2中出现的异常字符，而正则表达式无法匹配的场景
                        contentstitle = orgin_content.split('- - - - - - - - - - - - - - -', 2)
                        contentssplit2 = contentstitle[0].split(':', 6)

                        if (len(contentstitle) < 2 or len(contentssplit2) < 6):
                            print('切割字符串失败，请检查返回报文')
                        else:
                            print('切割字符串成功，发送消息到群', contentssplit2[2], contentssplit2[4])
                            spy.send_text(contentssplit2[2], contentstitle[1], contentssplit2[4])

                        ''' 老方法，弃之不用
                        #rematchresponse = re.findall( ':fromroom:([0-9a-zA-Z_@]{1,}):fromuser:([0-9a-zA-Z-_]{1,}):', orgin_content)
                        if( rematchresponse ):
                            fromcu = rematchresponse[0]
                            print('response model match, fromroom: ' + fromcu[0] + ' ,fromuser: ' + fromcu[1])
                            # 加工消息，
                            Ryan_content = orgin_content.replace('YBX-:fromroom:' + fromcu[0] + ':fromuser:' + fromcu[1] + ':','')
                            location = Ryan_content.find( '「')
                            spy.send_text(fromcu[0],Ryan_content[location:], fromcu[1])
                        else:
                            print('response model not match')
                        '''
                        # send_Ding(orgin_content)
                    except Exception as err:
                        print('尝试解析工单系统返回消息出错', err)
                    break

                # step4. 消息、文件通知发送
                if (((orgin_content.find('发送消息给：') == 0) or (orgin_content.find('发送文件给：') == 0))
                        and len(orgin_content.split('：', 4)) == 4
                        and (wxid1 == Ryan_Adminroom)):
                    try:
                        sqlite3Conn = sqlite3.connect(dburl)
                        sqliteCU = sqlite3Conn.cursor()
                        # 发送消息给：税控：半托：abcde
                        sendMSG = orgin_content.split('：', 4)
                        # sendToWhere=' 1=1 '
                        sqlite3RyanSql = ''
                        AnnouceType = ''
                        if (sendMSG[1] == '税控'):
                            AnnouceType = 'SK'
                        elif (sendMSG[1] == '进项'):
                            AnnouceType = 'JX'
                        elif (sendMSG[1] == '电子发票'):
                            AnnouceType = 'DZ'
                        elif (sendMSG[1] == '前置'):
                            AnnouceType = 'QZ'
                        elif (sendMSG[1] == '群名称'):
                            AnnouceType = 'groupname'
                        elif (sendMSG[1] == '简称'):
                            AnnouceType = 'shortname'
                        else:
                            print('命令错误，退出')
                            break

                        sqlite3RyanSql = 'SELECT GROUPWXID FROM groups WHERE flag = \'Y\' AND ' + AnnouceType
                        # print(sqlite3RyanSql)

                        if (sendMSG[2] == '所有'):
                            sqlite3RyanSql = sqlite3RyanSql + ' IS NOT NULL '
                            # sqliteCU.execute(sqlite3RyanSql)
                        else:
                            sendTo = ''
                            if (sendMSG[2] == '全托'):
                                sendTo = 'QT'
                            elif (sendMSG[2] == '半托'):
                                sendTo = 'BT'
                            elif (sendMSG[2] == '独立部署增值税'):
                                sendTo = 'DLZZS'
                            elif (sendMSG[2] == '独立部署税控'):
                                sendTo = 'DLSK'
                            elif (sendMSG[2] == '管理'):
                                sendTo = 'GL'
                            else:
                                # 不在所有范围内的其他税控，直接传输原值
                                # spy.send_text(wxid1, 'sendTo参数异常')
                                # break
                                sendTo = sendMSG[2]

                            sqlite3RyanSql = sqlite3RyanSql + ' = ' + '\'' + sendTo + '\''
                            print('sqlite3RyanSql', sqlite3RyanSql)

                        for row in sqliteCU.execute(sqlite3RyanSql):
                            if (orgin_content.find('发送消息给：') == 0):
                                spy.send_text(row[0], sendMSG[3])
                                print('消息发给：', row[0])
                            elif (orgin_content.find('发送文件给：') == 0):
                                # 注意：文件名不能含有中文，且文件夹深度不能超过8
                                filepath = FILESTORAGEDIR + sendMSG[3]
                                print(filepath)
                                print('文件发给：', row[0])
                                spy.send_file(row[0], filepath)
                            else:
                                pass

                    except Exception as err:
                        print('step4,消息通知发送失败', err)
                    finally:
                        sqliteCU.close
                        sqlite3Conn.commit()
                        sqlite3Conn.close()
                    break

                # step5. 匹配请求模板
                '''
                    1、【问题标题】【报错描述】【企业名称】【纳税人识别号】【联系电话】
                '''
                # 处理乱码 start
                # 处理乱码： \xc2\xa0
                orgin_content = orgin_content.replace(chr(0xa0), '')
                #  print('orgin_content, after replace', orgin_content)
                # 处理乱码 end
                matchScore = matchPercent(orgin_content)
                if (matchScore > 70):
                    if (replyButton == False):
                        # 回复开关关闭，直接退出
                        print('回复开关关闭，不响应改请求')
                        break

                    if (matchScore < 100):
                        # 内容近似匹配，返回提醒
                        spy.send_text(wxid1,
                                      '识别到您的问题与模板不匹配，清检查是否【问题标题】【报错描述】【企业名称】【纳税人识别号】【联系电话】都已提供。请注意不要修改括弧中的内容，建议修改后重新提交',
                                      wxid2)
                        break

                    '''
                    delete : 这段使用白名单替代
                    if(orgin_content.find('问题模板') >0 ):
                        print('问题模板,不往后传递')
                        # "问题模板"不往后传递
                        break
                    '''

                    global dict_whitelist  # 运营组白名单
                    if (wxid2 in dict_whitelist and wxid1 != Ryan_chatroomid):
                        print('白名单里的人发的消息不往后传递')
                        break

                    try:
                        # 匹配成功，加工，Ryan_content，并转发
                        # print('request model match,wxid1=',wxid1,"wxid2=",wxid2)
                        Ryan_content_loc = orgin_content.find('问题标题')
                        Ryan_content = orgin_content[Ryan_content_loc - 1:]
                        if (wxid1 == None and wxid2 != None):
                            Ryan_content = Ryan_content.replace('【问题标题】',
                                                                '【问题标题】YBX-:fromroom:' + 'default' + ':fromuser:' + wxid2 + ':')
                        elif (wxid1 != None and wxid2 != None):
                            Ryan_content = Ryan_content.replace('【问题标题】',
                                                                '【问题标题】YBX-:fromroom:' + wxid1 + ':fromuser:' + wxid2 + ':')
                        elif (wxid1 != None and wxid2 == None):
                            Ryan_content = Ryan_content.replace('【问题标题】',
                                                                '【问题标题】YBX-:fromroom:' + wxid1 + ':fromuser:' + 'default' + ':')
                        else:
                            Ryan_content = Ryan_content.replace('【问题标题】', '【问题标题】YBX-:')
                        # print(Ryan_content)

                        # 此处处理 电话中只能有数字和破折号的限制
                        try:
                            teleStart_loc = Ryan_content.find('联系电话')
                            teleEnd_loc = Ryan_content.find('【', teleStart_loc + 1, len(Ryan_content))
                            if (teleEnd_loc == -1):
                                teleEnd_loc = len(Ryan_content)
                            else:
                                pass
                            teleOrgin = Ryan_content[teleStart_loc + 5:teleEnd_loc]
                            teleMatch = re.findall('([0-9a-z-]{1,})', teleOrgin)
                            if (teleEnd_loc == len(Ryan_content)):
                                Ryan_content = Ryan_content[:teleStart_loc + 5 + len(teleMatch[0])]
                            else:
                                Ryan_content = Ryan_content[:teleStart_loc + 5 + len(teleMatch[0])] + Ryan_content[
                                                                                                      teleEnd_loc:]
                        except Exception as err:
                            print(err)

                        spy.send_text(Ryan_chatroomid, Ryan_content)
                        # 发送成功后，原群给一个消息
                        spy.send_text(wxid1, '您的问题已经接收，如有处理结果我们会第一时间反馈给您，感谢您的等候。', wxid2)
                        send_Ding(orgin_content, contact_dict.get(wxid1))
                    except Exception as err:
                        print('step5，尝试发送消息出错', err)
                    finally:
                        break


            elif item["msg_type"] == 37:
                # 好友请求消息
                obj = etree.XML(item["content"])
                encryptusername, ticket = obj.xpath("/msg/@encryptusername")[0], obj.xpath("/msg/@ticket")[0]
                spy.accept_new_contact(encryptusername, ticket)
    elif data["type"] == 2:
        # 联系人详情
        print(data)
    elif data["type"] == 3:
        # 联系人列表
        for contact in data["data"]:
            print(contact)
    elif data["type"] == 9527:
        spy.logger.warning(data)


if __name__ == '__main__':
    ryan_pid = str(os.getpid())
    with open(PIDFILE, 'w') as f:
        f.write(ryan_pid)

    spy = WeChatSpy(parser=parser)
    spy.add_log_output_file()  # 添加日志输出文件
    try:
        qdtime = time.time()
        spy.run()
        # spy.send_text(Ryan_annouceto,'服务已启动','paradixrain') 无效语句
    except Exception as err:
        print('main 程序报错', err)
