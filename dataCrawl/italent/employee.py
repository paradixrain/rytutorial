# coding=utf-8

# @title: 爬取italent里的所有员工信息，保存为json
# @desc:  钉钉里无法导出和爬取，所以用网页italent来爬
#    地址为：https://www.italent.cn/portal/convoy/menuroute?_userSpecialType=1&xpuid=141840425&_menuId=31618f3c-b7c3-40a9-925f-c6af1a78c88c&_menuRootId=9027d6ad-d015-4c62-83d8-3fddb015b59d&_pageType=external&_pageTitle=%E9%80%9A%E8%AE%AF%E5%BD%95&_restoreHref=https%3A%2F%2Fwww.italent.cn%2Fmenuroute%3FmenuId%3D10758%26roleId%3D#widget/italent?iTalentFrameType=iframe&roleId=&iTalentFrame=https%3A%2F%2Faccount.italent.cn%2FContacts%2FIndex%3Fsource%3D1%23%2FCompanyContacts
#    问题，需要填写用户名，密码，并“同意”隐私协议
# @author: RyanLin
# @date: 2022/07/01


# coding = utf-8

# @title: 自动填写表单
# @desc:
# @author: RyanLin
# @date: 2022/06/22
import json
import math
import time
import pandas as pd

import requests
from selenium import webdriver
from selenium.webdriver.common.by import By

"""
selenium定位方法
通过id定位元素：find_element_by_id(“id_vaule”)
通过name定位元素：find_element_by_name(“name_vaule”)
通过tag_name定位元素：find_element_by_tag_name(“tag_name_vaule”)
通过class_name定位元素：find_element_by_class_name(“class_name”)
通过css定位元素：find_element_by_css_selector();
通过xpath定位元素：find_element_by_xpath(“xpath”)
通过link定位：find_element_by_link_text(“text_vaule”)或者find_element_by_partial_link_text()
"""

# 全局变量
TOTAL_EMPLOY = 771
PAGE_SIZE = 50

# def is_visible(driver, locator, timeout=10):
#     try:
#         ui.WebDriverWait(driver, timeout).until(EC.visibility_of_element_located(locator))
#         return True
#     except TimeoutException:
#         return False


def get_page(url, params_by_id, checkbox_class, commit_button_class):
    Header = {'User-Agent' :'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'}
    with webdriver.Chrome('chromedriver') as driver:
        driver.get(url)
        html = driver.page_source
        # print(html)
        for k, v in params_by_id.items():
            # print('param', k, v)
            time.sleep(1)
            driver.find_element(By.ID, k).click()
            driver.find_element(By.ID, k).send_keys(v)

        # checkbox_class
        time.sleep(1)
        driver.find_element(By.CLASS_NAME, checkbox_class).click()

        # summit_class
        time.sleep(1)
        driver.find_element(By.XPATH, commit_button_class).click()

        # 查看结果
        time.sleep(5)
        # print(driver.page_source)

        # 跳转到员工清单页，爬取所有员工信息
        jump_url = "https://www.italent.cn/portal/convoy/menuroute?_userSpecialType=1&xpuid=141840425&_menuId=31618f3c-b7c3-40a9-925f-c6af1a78c88c&_menuRootId=9027d6ad-d015-4c62-83d8-3fddb015b59d&_pageType=external&_pageTitle=%E9%80%9A%E8%AE%AF%E5%BD%95&_restoreHref=https%3A%2F%2Fwww.italent.cn%2Fmenuroute%3FmenuId%3D10758%26roleId%3D#widget/italent?iTalentFrameType=iframe&roleId=&iTalentFrame=https%3A%2F%2Faccount.italent.cn%2FContacts%2FIndex%3Fsource%3D1%23%2FCompanyContacts"
        driver.get(jump_url)
        time.sleep(10)
        # 开始解决payload 的问题
        # 直接从F12里面找到payload，最后一个id值不知道是干嘛的，可能是随机值
        pages = math.ceil(TOTAL_EMPLOY // PAGE_SIZE)

        # 构造会话
        sess = requests.session()
        sess.headers.setdefault('Host', 'account.italent.cn')
        sess.headers.setdefault('Connection', 'keep-alive')
        sess.headers.setdefault('Accept', '*/*')
        sess.headers.setdefault('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36')
        sess.headers.setdefault('Content-Type', 'application/json')
        sess.headers.setdefault('Referer', 'https://account.italent.cn/Contacts/Index?source=1')
        sess.headers.setdefault('Accept-Encoding', 'gzip, deflate, br')
        sess.headers.setdefault('Accept-Language', 'zh-CN,zh;q=0.9')

        # print('ryan Header', Header)

        # 拿到构造cookie
        cookies = driver.get_cookies()
        # print('ryan cookies : ')
        for cookie in cookies:
            # print(cookie['name'], cookie['value'])
            sess.cookies.set(cookie['name'], cookie['value'])

        # 循环每页遍历，将结果存入employees的数组里
        employees = []
        for index in range(1, pages+2):
            dataURL = f'https://account.italent.cn/Contacts/staffV2?page_num={index}&page_size={PAGE_SIZE}&department_id=0&_v=1656645177683'
            r = sess.get(dataURL, headers=Header)
            # print('ryan raw r', r)
            r = r.text
            # print('ryan r:', r)

            # 将所需的结果存到 json 里
            r = json.loads(r)
            # print('ryan data:', r)
            employee = r['Data']['users']
            employees.extend(employee)
            # print('ryan dataLEN', dataLEN)

        # 所有员工数据存在employees数组里
        print('employees are:',len(employees), employees)
        return employees


def array_extract(array):
    """
    对指定列进行筛选，只保留需要的列，并返回结果集
    """
    rtn_arr = []
    for item in array:
        line = [item['userId'],
                item['name'],
                item['department'],
                item['departmentPath'],
                item['infoValues'][0]['value'],
                item['infoValues'][3]['value']]
        rtn_arr.append(line)
    return rtn_arr


def array_to_csv(array, csv_file):
    """
    将数组写入csv文件
    """
    array = array_extract(array)
    col_name = ['userID', 'name', 'department', 'departmentPath', 'sex', 'mobilephone']
    df = pd.DataFrame(data=array, columns=col_name)
    df.to_csv(csv_file, index=False)


def run(x):
    url = 'https://www.italent.cn/Login?entrytype=1&tid=111610&isvid=10001&lt=zh_CN'
    params = {'form-item-account': '18910218998',
              'form-item-password': 'Admin123'}
    checkbox_class = 'phoenix-checkbox__input'
    commit_button_class = '//*[@id="bsMain"]/div/div[2]/div/div[1]/div[2]/div[3]/div/div'

    employees = get_page(url, params, checkbox_class, commit_button_class)
    array_to_csv(employees, 'employees.csv')
    time.sleep(x)


if __name__ == '__main__':
    run(10)
