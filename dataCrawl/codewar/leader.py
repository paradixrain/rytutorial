# coding = utf-8

# 爬codewar的前500名
# 地址： https://www.codewars.com/users/leaderboard

import requests
import re


def downloadDZfp(url):
    r = requests.get(url)
    honor = re.findall(r'<td class="honor">((\d+,)+\d+)</td>', r.text)
    l = [int(i[0].replace(',', '')) for i in honor]
    print(l)


if __name__ == '__main__':
    url = "https://www.codewars.com/users/leaderboard"
    downloadDZfp(url)
