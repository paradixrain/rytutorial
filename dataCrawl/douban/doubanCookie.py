# coding=utf-8

# @title: doubanCookie
# @desc: 测试获取和使用cookie防止多次登录
# @author: RyanLin
# @date: 2022/03/01

# 成功

from selenium import webdriver
import time
import json

def main(peopleID):
    brower = webdriver.Chrome()
    baseURL = 'https://www.douban.com/people/' + peopleID + '/contacts'
    brower.get(baseURL)
    
    time.sleep(10)
    cookie1 = brower.get_cookies()
    print(cookie1)
    for cookie in cookie1:
        print(cookie)
    with open('cookie1.txt', 'w') as cf:
        json.dump(cookie1, cf)
    brower.quit()
    
    brower2 = webdriver.Chrome()
    baseURL = 'https://www.douban.com/people/' + peopleID + '/contacts'
    brower2.get(baseURL)
    
    # 复制brower1 的cookie
    time.sleep(1)
    with open('cookie1.txt', 'r') as cf:
        cookies = json.load(cf)
        for cookie in cookies:
            try:
                print('add cookie2 now', cookie)
                brower2.add_cookie(cookie)
            except Exception as e:
                print('add cookie2 error')
                print(e)
    brower2.get(baseURL)
    
    # 更新cookie以后看看效果
    time.sleep(10)
    brower2.quit()
    
    
def getCookie():
    return cookies

if __name__ == "__main__":
    main('paradixrain')