# coding = utf-8

# 给定特定人的地址，爬取豆瓣和某人互粉的所有人
# 地址： 阿不的个人主页：https://www.douban.com/people/hotncold/?_i=6097771l0okQI_
#     阿不的好友： https://www.douban.com/people/hotncold/contacts

# 把和阿不互粉的结果打印出来

import requests
from selenium import webdriver
import time
from bs4 import BeautifulSoup 


def getpeopleList(brower, peopleID):
    '''根据peopleID返回字典，id:title的字典
    '''
    baseURL = 'https://www.douban.com/people/' + peopleID + '/contacts'
    brower.get(baseURL)
    
    # 等5秒打开网页
    time.sleep(5)
    
    data = brower.page_source
    soup = BeautifulSoup(data, 'html.parser')
    contacts = soup.select('.obu dd a')

    peopleList = {}
    try:
        for contact in contacts:
            href = contact['href']
            title = contact.text
            peopleID = href.split('/')[-2]
            # print(title, peopleID, href)
            peopleList[peopleID] = title
    except Exception as e:
        print(e)
        
    return peopleList

def checkPeople(mainPeople, fanID):
    '''根据fanID 是否在mainPeople里，判断是否互粉，存在则返回粉丝名，否则None
    '''
    return mainPeople.get(fanID)

def main(name):
    # 0. 构建浏览器
    brower = webdriver.Chrome()
    baseURL = 'https://www.douban.com/people/' + name + '/contacts'
    brower.get(baseURL)
    # sleep并登录
    time.sleep(10)
    data = brower.page_source
    
    # 1. 得到主用户A的联系人清单
    mainPeople = getpeopleList(brower, name)
    follower = []
    # 2. 遍历，得到主用户A所有联系人Ls的清单
    for people in mainPeople:
        print('开始查询粉丝：', people)
        fansPeople = getpeopleList(brower, people)
        
    # 3. 判断A是否存在于Ls中
    # 4. 打印用户
        if name in fansPeople.keys():
            print('找到一个互粉', people)
            follower.append(mainPeople[people])
    print('互粉清单如下：')
    print(follower)

if __name__ == '__main__':
    main('paradixrain')