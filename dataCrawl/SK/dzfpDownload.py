# coding = utf-8
import requests

# 给定特定人的地址，爬取税局电子发票链接，并下载pdf文件,类型3ofd，类型12pdf
# https://skfp.shanghai.chinatax.gov.cn:9008/preview.html?code=031002000113_16475017_20220507_G6D5F2E5&type=3
# https://skfp.shanghai.chinatax.gov.cn:9008/api?action=getDoc&code=031002000113_16475017_20220507_G6D5F2E5&type=12
# https://skfp.shanghai.chinatax.gov.cn:9008/api?action=getDoc&code=031002000113_16475017_20220507_G6D5F2E5&type=3

sjdz = {'shanghai': 'https://skfp.shanghai.chinatax.gov.cn:9008/api?action=getDoc&'}


def downloadDZfp(sf, fpdm, fphm, kprq, jym, filetype):
    downloadURL = f'{sjdz[sf]}&code={fpdm}_{fphm}_{kprq}_{jym}&type={filetype}'
    downloadFileName = f'{fpdm}_{fphm}_{kprq}_{jym}.' + f"{'ofd' if filetype == 3 else 'pdf'}"

    print("downloading with requests", downloadURL, downloadFileName)
    r = requests.get(downloadURL)
    with open(downloadFileName, "wb") as d:
        d.write(r.content)


if __name__ == '__main__':
    sf = 'shanghai'
    fpdm = '031002000113'
    fphm = '16475017'
    kprq = '20220507'
    jym = 'G6D5F2E5'
    filetype = 12
    downloadDZfp('shanghai', fpdm, fphm, kprq, jym, filetype)
