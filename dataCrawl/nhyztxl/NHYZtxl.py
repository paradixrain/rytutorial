# coding = utf-8

# 爬取宁化一中历届同学通讯录，保存成txt方便以后查询
# 地址： http://www.fjnhyz.com/channels/130.html
# contents/130/777.html 到 contents/130/623.html

# 把结果存到txt里

import requests
import urllib
import chardet
import codecs
from bs4 import BeautifulSoup

# 读取网页的编码，几种获取网页编码的方式
def getMetaCharset(URL):
    ''' 
        # 1.探测网页编码
    '''
    
    resRaw = urllib.request.urlopen(URL).read()
    # print(resRaw)        # 输出为空
    code = chardet.detect(resRaw)
    print('coding = ', code)
    return code['encoding'].lower()

def getResCoding(URL):
    '''
        # 2.使用requests获取编码，当网站没有设置字符集时，返回值通常有误
    '''
    res = requests.get(URL, headers = Header)
    resCoding = res.encoding       # ISO-8859-1，当网站没有设置字符集时，默认会读取到这个值
    return resCoding



def loopYear(outFile, loopURL):

    Header = {'User-Agent' :'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'}
    print('loopURL:', loopURL)
    
    res = requests.get(loopURL, headers = Header)
    res.encoding = 'gbk'            # 因为有生僻字无法打印，猜测使用gbk进行尝试，得到解决！
    res = res.text
    
    # 开始解析，得到需要的数据
    soup = BeautifulSoup(res, 'html.parser')
    # 获取年份名
    yearName = soup.select('.a_title')[0].text
    print('年份:', yearName)

    # 获取班级名及学生名
    tds = soup.select('.a_content table tbody tr td')
    with open(outFile, 'a', encoding = 'utf-8') as opfile:
        opfile.writelines('\n\n\n' + yearName + '\n')
        for i in range(len(tds)):
            print('tds[i]', tds[i])
            text = tds[i].text.strip()
            if len(text) == 0:
                continue
            # if tds[i].get('strong'):    # 不能get到
            #     print('班级1: ')
            if tds[i].find('strong') or tds[i].find('b') or tds[i].find('B'):
                # print('班级: ', text)
                opfile.writelines('\n' + tds[i].text + '\n')
            else:
                # print(i, tds[i].text)
                classMates = text.replace(' ','')
                classMates = classMates.replace(' ','')
                opfile.writelines(classMates + ' , ')



if __name__ == '__main__':
    baseURL = 'http://www.fjnhyz.com/contents/130/' 
    startNo = 777
    endNo = 623
    sufix = '.html'
    outFile = '宁化一中历年校友录.txt'
    for year in range(startNo, endNo-1, -1):
        loopURL = baseURL + str(year) + sufix
        loopYear(outFile, loopURL)
    
    
    # loopURL = baseURL + str(762) + sufix
    # loopYear(outFile, loopURL)