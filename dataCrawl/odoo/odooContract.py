# encoding:utf-8

# @title: 爬取odoo里的合同回款明细数据
# @desc:  允许根据id遍历所有回款数据，
#    地址为：http://odoo.51baiwang.com/web/dataset/call_kw/business.payment/read
#    问题1：id自增可能会很多，为了防止被阻挡，需要限制爬取的时间
#    问题2：需要使用selenium来抓取数据，因为浏览器使用了渲染
# @author: RyanLin
# @date: 2022/02/25


import requests
from selenium import webdriver
import time
import json
import random
import pandas as pd

# 每次抓取的间隔时间（秒）
GAP_TIME = 5
# 连续几次失败后，往上加几次
GET_NULL_TIMES = 5
PASS_NULL_GAP = 1000
failed_count = 0

# 读取"回款明细.csv"，将已经获取到的id，保存到一个数组里，下面如果遍历遇到，就退出
ids = pd.read_csv("../data/回款明细.csv", usecols=[0])
print("一共有数据：",len(ids))




loginURL = "http://odoo.51baiwang.com/"
Header = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'}
outputFile = '../data/回款明细.csv'

# 常规方式启动，缺点；慢，偶尔还启动不起来
browser = webdriver.Chrome()
browser.get(loginURL)

# 使用已启动的浏览器，需要先在命令行启动msedge.exe --remote-debugging-port=9222 --user-data-dir="c:\edgeselenium\"
# "C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe" --remote-debugging-port=9222 --user-data-dir="d:\edgeselenium\"
# option = webdriver.EdgeOptions()
# option.add_experimental_option("debuggerAddress", "127.0.0.1:9222")
# browser = webdriver.Edge(options=option)


# 等20秒扫描登录页面
time.sleep(20)
# data = brower.page_source
# print('ryan login html:')
# print(data)

# 默认会进到收件箱页面
contractURL = "http://odoo.51baiwang.com/web#action=515&model=business.payment&view_type=list&menu_id=97"
browser.get(contractURL)
data = browser.page_source
time.sleep(10)
# print('ryan targit html:')
# print(data)


# 找到实际页面 http://odoo.51baiwang.com/web/dataset/search_read
# 发现无法得到结果，原因是有payload的请求方式
# dataURL = 'http://odoo.51baiwang.com/web/dataset/search_read'
# brower.get(contractURL)
# data = brower.page_source
# time.sleep(10)
# print('ryan data html:')
# print(data)


# 开始解决payload 的问题
# 直接从F12里面找到payload，最后一个id值不知道是干嘛的，可能是随机值
dataURL = 'http://odoo.51baiwang.com/web/dataset/call_kw/business.payment/read'
payloadSTR = '''
{
	"jsonrpc": "2.0",
	"method": "call",
	"params": {
		"args": [
			[39031],
			["business_code", "business_code2", "filing_date", "implement_salesmen_user_id", "implement_department_id", "customer_partner_string", "customer_partner", "customer_partner_id_string", "note", "write_type", "collection_date", "collection_amount", "login_date", "login_state", "payment_number", "serial_number", "write_off_state", "write_off_user", "write_off_time", "line_ids", "display_name"]
		],
		"model": "business.payment",
		"method": "read",
		"kwargs": {
			"context": {
				"lang": "zh_CN",
				"tz": "Asia/Shanghai",
				"uid": 36,
				"bin_size": true
			}
		}
	},
	"id": 700445889
}
'''

payloadJSON = json.loads(payloadSTR)

# Header['host'] = 'odoo.51baiwang.com'
# Header['X-Requested-With'] = 'XMLHttpRequest'
# Header['Content-Type'] = 'application/json'
# print('ryan Header', Header)

# 构造会话
sess = requests.session()

# sess.headers.setdefault('POST', '/web/dataset/search_read HTTP/1.1')
sess.headers.setdefault('Host', 'odoo.51baiwang.com')
sess.headers.setdefault('Connection', 'keep-alive')
sess.headers.setdefault('Content-Length', '575')
sess.headers.setdefault('Accept', 'application/json, text/javascript, */*; q=0.01')
sess.headers.setdefault('X-Requested-With', 'XMLHttpRequest')
sess.headers.setdefault('User-Agent',
                        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                        'Chrome/98.0.4758.102 Safari/537.36')
sess.headers.setdefault('Content-Type', 'application/json')
sess.headers.setdefault('Origin', 'http://odoo.51baiwang.com')
sess.headers.setdefault('Referer', 'http://odoo.51baiwang.com/web')
sess.headers.setdefault('Accept-Encoding', 'gzip, deflate')
sess.headers.setdefault('Accept-Language', 'zh-CN,zh;q=0.9')

# print('ryan Header', Header)

# 拿到构造cookie
cookies = browser.get_cookies()
# print('ryan cookies : ')
for cookie in cookies:
    # print(cookie['name'], cookie['value'])
    sess.cookies.set(cookie['name'], cookie['value'])

# 根据uid遍历所有项目回款
# 从临时文件中读取当前执行到的id
try:
    with open('ID.txt', 'r') as rf:
        startID = int(rf.readline())
except FileNotFoundError:
    startID = 0


# nowID = 0
# 默认nowID从startID开始遍历
nowID=startID


# for nowID in range(startID + 1, startID + 10000):
while nowID < startID + 10000:
    nowID += 1
    # 每个ID间隔GAP_TIME休息
    time.sleep(GAP_TIME)

    # 随机生成9位数
    tmpID = random.randint(100000000, 999999999)
    payloadJSON['id'] = str(tmpID)
    # payloadJSON['params']['limit'] = 3000

    """
    412杨盖，220娄蒙蒙，36林宇，1546何成敏 , 312: "袁洪芬"，553翟明， 259陈曦，255王焱磊，50申奇
    """
    # 维护一个用户list
    user_list = [50, 119, 335, 238, 179, 582, 19, 576, 227, 248, 214, 46, 99, 338, 255, 388, 96, 569, 583, 37, 586, 506, 193, 507, 553, 441, 415, 312, 443, 83, 259, 520, 233, 364, 522, 455, 444, 263, 756, 311, 552, 86, 125, 612, 462, 198, 746, 581, 210, 176, 438, 307, 110, 823, 714, 950, 751, 383, 106, 725, 991, 946, 829, 320, 156, 195, 910, 138, 18, 1130, 601, 1107, 1171, 1006, 808, 747, 1080, 1142, 1176, 926, 1163, 944, 92, 1081, 1175, 995, 1194, 936, 124, 1250, 1290, 1132, 1269, 330, 820, 1385, 1251, 1478, 315, 590, 1494, 44, 1580, 1594, 491, 1577, 1457, 1616, 1763, 117, 1647, 1795, 445, 292, 634, 647, 414, 159, 329, 281, 1644, 1984, 828, 1888, 1546, 1639]
    # 随机选取一个用户
    user = random.choice(user_list)
    # 用户id
    payloadJSON['params']['kwargs']['context']['uid'] = user
    # 合同id
    payloadJSON['params']['args'][0] = nowID

    # 保存参数
    payloadSTR = json.dumps(payloadJSON)

    ryand = sess.post(dataURL, data=json.dumps(payloadJSON), headers=Header)
    # print('ryan raw r', r)
    r = ryand.text
    # print('ryan r:', r)

    # 将所需的结果存到 json 里
    try:
        r = json.loads(r)
        print('ryan data:', r)
        # collection_amount = r['result'][0]['collection_amount']
        # print('ryan collection_amount', collection_amount)
    except Exception as e:
        print('ryan errors:', e)
        try:
            with open('failed.txt', 'a') as af:
                af.write(r)
                af.write("\n")
        except Exception as e:
            with open('failed.txt', 'a') as af:
                af.write(f"nowID={nowID} 保存失败\n")
        continue

    # 打印出来看看
    # 导出到csv 文件，供以后分析

    header_list = ['回款ID',
                   '合同ID',
                   '合同名称/编号',
                   '归档日期',
                   '销售人员ID',
                   '销售人员名称',
                   '销售人员部门ID',
                   '销售人员部门名称',
                   '客户ID',
                   '客户名称',
                   '客户详细名称',
                   '备注',
                   '回款日期',
                   '回款金额',
                   '序列号',
                   '是否核销']

    try:
        money_result = r['result'][0]
        money_id = money_result['id']

        if money_result['business_code']:
            business_id = money_result['business_code'][0]
            business_name = money_result['business_code'][1]
        else:
            business_id = 'None'
            business_name = 'None'

        filing_date = money_result['filing_date']
        salesman_name = money_result['implement_salesmen_user_id'][1]
        salesman_id = money_result['implement_salesmen_user_id'][0]
        salesman_depart = money_result['implement_department_id'][0]
        salesman_depart_name = money_result['implement_department_id'][1]

        if money_result['customer_partner']:
            customer_id = money_result['customer_partner'][0]
            customer_name = money_result['customer_partner'][1]
        else:
            customer_id = 'None'
            customer_name = 'None'

        customer_partner_id_string = money_result['customer_partner_id_string']
        note = money_result['note']
        collection_date = money_result['collection_date']
        collection_amount = money_result['collection_amount']
        serial_number = money_result['serial_number']
        written_off = "未核销" if money_result['write_off_state'] == 'not_written_off' else (
            "已核销" if money_result['write_off_state'] == 'written_off' else money_result['write_off_state'])
        data = [[money_id, business_id, business_name, filing_date, salesman_name,
                 salesman_id, salesman_depart, salesman_depart_name, customer_id, customer_name,
                 collection_date, collection_amount, serial_number, written_off, customer_partner_id_string, note]]
        df = pd.DataFrame(data, columns=header_list)
        df.to_csv(outputFile, mode='a', index=False, header=False)

        # 把ID回写到文件里
        # 从临时文件中读取当前执行到的id
        with open('ID.txt', 'w') as wf:
            wf.write(str(nowID))
        # 如果有命中，则重置当前failed_count，以避免连续5次id+1失败
        failed_count = 0

    except IndexError as e:
        # 如果5次连续+1的id都失败，则加1000，并将跳过的id段写入到SKIPID.txt里
        print("IndexError: " + str(e) + "nowID=" + str(nowID))
        failed_count += 1
        if failed_count > GET_NULL_TIMES:
            newID = nowID + PASS_NULL_GAP
            failed_count = 0
            with open("SKIPID.txt", 'a') as sf:
                sf.write(f"skip from {nowID} to {newID}\n")
            nowID = newID
            with open('ID.txt', 'w') as wf:
                wf.write(str(newID))
        else:
            with open("SKIPID.txt", 'a') as sf:
                sf.write(f"skip from {nowID} to {nowID+1}\n")
    except Exception as e:
        print('ryan error:', e)
        with open('failed.txt', 'a') as af:
            af.write(f"nowID\t{ryand.text}\n")

# 结束,看结果
time.sleep(3)
