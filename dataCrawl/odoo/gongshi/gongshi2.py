# coding = utf-8

# @filename: gongshi.py
# @desc: 导出工时，为钉钉中的项目工时管理提供依据，查询部门：保信+时间：过去30天的工时，每个工时都有唯一id，使用
# @author: PC\RyanLin
# @version: Python 3.12
# @date: 2024/8/30

"""
  url= http://odoo.51baiwang.com/web/dataset/search_read
  payload = {"jsonrpc":"2.0","method":"call","params":{"model":"account.analytic.line","domain":[["department_id","ilike","保信"],"&",["date",">=","2024-07-31"],["date","<","2024-08-30"]],"fields":["date","user_id","department_id","name","project_id","task_id","unit_amount"],"limit":80,"sort":"create_date DESC","context":{"lang":"zh_CN","tz":"Asia/Shanghai","uid":36,"active_id":16148,"active_ids":[16148],"params":{"action":500,"active_id":16148,"model":"account.analytic.line","view_type":"list","menu_id":347},"search_default_project_id":[16148],"default_project_id":16148,"search_disable_custom_filters":true}},"id":601419137}
  return = {
    "jsonrpc": "2.0",
    "id": 556932843,
    "result": {
        "length": 1,
        "records": [
            {
                "id": 363412,
                "date": "2024-08-23",
                "user_id": [
                    36,
                    "\u6797\u5b87"
                ],
                "department_id": [
                    91,
                    "\u767e\u671b\u80a1\u4efd\u6709\u9650\u516c\u53f8 / \u670d\u52a1\u8fd0\u8425\u4e2d\u5fc3 / \u91d1\u878d\u6210\u529f\u90e8 / \u4fdd\u4fe1\u6210\u529f\u7ec4"
                ],
                "name": "\u4e0a\u7ebf\u652f\u6301\n\u5468\u4f1a",
                "project_id": [
                    481,
                    "\u4fdd\u4fe1\u589e\u503c\u7a0e\u5408\u4f5c\u5171\u5efa\u9879\u76ee--\u6301\u7eed\u4f18\u5316/PJ190132"
                ],
                "task_id": [
                    927,
                    "[T0923] \u589e\u503c\u7a0e\u9500\u9879\u7cfb\u7edf\u6301\u7eed\u4f18\u5316"
                ],
                "unit_amount": 8.0
            }
        ]
    }
}
"""
import os
from datetime import datetime, timedelta

import requests
from selenium import webdriver
import time
import json
import random
import pandas as pd

loginURL = "http://odoo.51baiwang.com/"
Header = {'User-Agent' :'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'}
outputFile = r'D:\Working\项目管理\工时汇总表.xlsx'

brower = webdriver.Chrome()
brower.get(loginURL)

# 等20秒扫描登录页面
time.sleep(20)
# data = brower.page_source
# print('ryan login html:')
# print(data)

# 默认会进到收件箱页面，所以要试试跳转
contractURL = "http://odoo.51baiwang.com/web#action=497&model=project.project&view_type=list&menu_id=347"
brower.get(contractURL)
data = brower.page_source
time.sleep(5)


# 开始解决payload 的问题
# 直接从F12里面找到payload，最后一个id值不知道是干嘛的，可能是随机值
dataURL = 'http://odoo.51baiwang.com/web/dataset/search_read'
today = datetime.now().date()
start_day = today - timedelta(days=100)
end_day = today - timedelta(days=0)

# 格式化日期为字符串
start_str = start_day.strftime('%Y-%m-%d')
end_str = end_day.strftime('%Y-%m-%d')
input_dict = {"department": "保信", "days_before_30": start_str, "today": end_str}
employee_list = ["刘宇舰", "李娥"]

# print(input_dict)

for employee in employee_list:

    payloadSTR = '''
    {
        "jsonrpc": "2.0",
        "method": "call",
        "params": {
            "model": "account.analytic.line",
            "domain": [
                [
                    "employee_id",
                    "ilike",
                    "{employee}"
                ],
                "&",
                [
                    "date",
                    ">=",
                    "{days_before_30}"
                ],
                [
                    "date",
                    "<",
                    "{today}"
                ]
            ],
            "fields": [
                "date",
                "user_id",
                "department_id",
                "name",
                "project_id",
                "task_id",
                "unit_amount"
            ],
            "limit": 800,
            "sort": "create_date DESC",
            "context": {
                "lang": "zh_CN",
                "tz": "Asia/Shanghai",
                "uid": 36,
                "active_id": 16148,
                "active_ids": [
                    16148
                ],
                "params": {
                    "action": 500,
                    "active_id": 16148,
                    "model": "account.analytic.line",
                    "view_type": "list",
                    "menu_id": 347
                },
                "search_default_project_id": [
                    16148
                ],
                "default_project_id": 16148,
                "search_disable_custom_filters": true
            }
        },
        "id": 601419137
    }
    '''

    payloadSTR = payloadSTR.replace("{employee}", employee)
    payloadSTR = payloadSTR.replace("{days_before_30}", input_dict["days_before_30"])
    payloadSTR = payloadSTR.replace("{today}", input_dict["today"])
    payloadJSON = json.loads(payloadSTR)

    # print(payloadJSON)
    # 随机生成9位数
    tmpID = random.randint(100000000,999999999)
    # print('ryan tmpID ', tmpID)
    payloadJSON['id'] = str(tmpID)
    payloadJSON['params']['limit'] = 9000


    # 保存参数
    payloadSTR = json.dumps(payloadJSON)


    # 构造会话
    sess = requests.session()

    sess.headers.setdefault('Host', 'odoo.51baiwang.com')
    sess.headers.setdefault('Connection', 'keep-alive')
    sess.headers.setdefault('Content-Length', '655')
    sess.headers.setdefault('Accept', 'application/json, text/javascript, */*; q=0.01')
    sess.headers.setdefault('X-Requested-With', 'XMLHttpRequest')
    sess.headers.setdefault('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36')
    sess.headers.setdefault('Content-Type', 'application/json')
    sess.headers.setdefault('Origin', 'http://odoo.51baiwang.com')
    sess.headers.setdefault('Referer', 'http://odoo.51baiwang.com/web')
    sess.headers.setdefault('Accept-Encoding', 'gzip, deflate')
    sess.headers.setdefault('Accept-Language', 'zh-CN,zh;q=0.9')

    # print('ryan Header', Header)

    # 拿到构造cookie
    cookies = brower.get_cookies()
    # print('ryan cookies : ')
    for cookie in cookies:
        # print(cookie['name'], cookie['value'])
        sess.cookies.set(cookie['name'], cookie['value'])

    r = sess.post(dataURL, data=json.dumps(payloadJSON), headers=Header)
    # print('ryan raw r', r)
    r = r.text
    # print('ryan r:', r)

    # 将所需的结果存到 json 里
    r = json.loads(r)
    # print('ryan data:', r)
    dataLEN = r['result']['length']
    # print('ryan dataLEN', dataLEN)

    # 打印出来看看
    # 导出到csv 文件，供以后分析
    header_list = ['id_id',
                   'task_date',
                   'user_id',
                   'user_name',
                   'department_id',
                   'department_name',
                   # 'name',
                   'project_id',
                   'project_name',
                   'task_id',
                   'task_name',
                   'unit_amount']


    try:
        data = []

        for record in r['result']['records']:
            print(record['id'],
                  record['date'],
                  record['user_id'],
                  record['department_id'],
                  # record['name'],
                  record['project_id'],
                  record['task_id'],
                  record['unit_amount'])
            id_id = record['id']
            task_date = record['date']
            user_id = record['user_id'][0]
            user_name = record['user_id'][1]
            department_id = record['department_id'][0]
            department_name = record['department_id'][1]
            # name = record['name']
            project_id = record['project_id'][0]
            project_name = record['project_id'][1]
            try:
                task_id = record['task_id'][0]
                task_name = record['task_id'][1]
            except (TypeError, IndexError, KeyError):
                task_id = "default"
                task_name = "default"
            unit_amount = record['unit_amount']

            data.append([id_id,
                         task_date,
                         user_id,
                         user_name,
                         department_id,
                         department_name,
                         # name,
                         project_id,
                         project_name,
                         task_id,
                         task_name,
                         unit_amount])
        df = pd.DataFrame(data, columns=header_list)

        # 找出在现有数据中不存在的行
        if os.path.exists(outputFile):
            existing_data = pd.read_excel(outputFile, engine='openpyxl')
            id_list = existing_data['id_id']
            new_rows = df[~df['id_id'].isin(id_list)]

            # 将新行追加到现有的数据中
            updated_data = pd.concat([existing_data, new_rows])

            # 保存更新后的 DataFrame 回文件
            updated_data.to_excel(outputFile, index=False, engine='openpyxl')
        else:
            df.to_excel(outputFile, index=False, engine='openpyxl', sheet_name="Sheet1")
    except Exception as e:
        print('ryan error', e)


# 结束,看结果
time.sleep(10)
