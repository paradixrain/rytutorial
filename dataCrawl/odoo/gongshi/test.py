# coding = utf-8

# @filename: test.py
# @desc: 
# @author: PC\RyanLin
# @version: Python 3.12
# @date: 2024/8/30
import pandas as pd
outputFile1 = r'D:\Working\项目管理\output.xlsx'
outputFile2 = r'D:\Working\项目管理\output2.xlsx'
outputFile3 = r'D:\Working\项目管理\output3.xlsx'

# Reading an .xls file
df1 = pd.read_excel(outputFile1, engine='openpyxl')
df2 = pd.read_excel(outputFile2, engine='openpyxl')

# Writing an .xls file
df1_ids = df1['id_id']
df2_not_in_df1 = df2[~df2['id_id'].isin(df1_ids)]
df3 = pd.concat([df1, df2_not_in_df1])
df3.to_excel(outputFile3, engine='openpyxl', index=False)
