# coding=utf-8

# @title: 像odoo工时表里提交工时信息
# @desc:  因为挨个填工时太麻烦，要等菜单展开时间，而且odoo工时不能导入，所以使用post方式导入，
#    地址为：
"""
curl "http://odoo.51baiwang.com/web/dataset/call_kw/application.for.working.hours.line.child/create" ^
  -H "Accept: application/json, text/javascript, */*; q=0.01" ^
  -H "Accept-Language: zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6" ^
  -H "Connection: keep-alive" ^
  -H "Content-Type: application/json" ^
  -H "Cookie: session_id=c0bc129d506bf4e6642dbd6834e2d0cf2ab72aa7" ^
  -H "Origin: http://odoo.51baiwang.com" ^
  -H "Referer: http://odoo.51baiwang.com/web" ^
  -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.35" ^
  -H "X-Requested-With: XMLHttpRequest" ^
  --data-raw "^{^\^"jsonrpc^\^":^\^"2.0^\^",^\^"method^\^":^\^"call^\^",^\^"params^\^":^{^\^"args^\^":^[^{^\^"working_content^\^":^\^"^[ ^] ^处^理^安^信^农^业^的^问^题^\^\n^\^",^\^"working_time^\^":^\^"8^\^"^}^],^\^"model^\^":^\^"application.for.working.hours.line.child^\^",^\^"method^\^":^\^"create^\^",^\^"kwargs^\^":^{^\^"context^\^":^{^\^"lang^\^":^\^"zh_CN^\^",^\^"tz^\^":^\^"Asia/Shanghai^\^",^\^"uid^\^":36^}^}^},^\^"id^\^":589712359^}" ^
  --compressed ^
  --insecure
"""
#    问题1：可能面临扫码登录的问题，需要控制
#    问题2：需要使用selenium来抓取数据，因为浏览器使用了渲染
# @author: RyanLin
# @date: 2022/11/11
# 还未完成

# {"jsonrpc":"2.0","method":"call","params":{"args":[{"working_content":"[ ] 处理安信农业的问题\n","working_time":"8"}],"model":"application.for.working.hours.line.child","method":"create","kwargs":{"context":{"lang":"zh_CN","tz":"Asia/Shanghai","uid":36}}},"id":589712359}

import requests
from selenium import webdriver
import time
import json
import random
import pandas as pd

loginURL = "http://odoo.51baiwang.com/"
Header = {'User-Agent' :'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'}
outputFile = '回款明细.csv'

brower = webdriver.Chrome()
brower.get(loginURL)

# 等20秒扫描登录页面
time.sleep(20)
data = brower.page_source
# print('ryan login html:')
# print(data)

# 默认会进到收件箱页面，所以要试试跳转，tmd，发现不行
contractURL = "http://odoo.51baiwang.com/web#action=515&model=business.payment&view_type=list&menu_id=97"
brower.get(contractURL)
data = brower.page_source
time.sleep(10)
# print('ryan targit html:')
# print(data)


# 找到实际页面 http://odoo.51baiwang.com/web/dataset/search_read
# 发现无法得到结果，原因是有payload的请求方式
# dataURL = 'http://odoo.51baiwang.com/web/dataset/search_read'
# brower.get(contractURL)
# data = brower.page_source
# time.sleep(10)
# print('ryan data html:')
# print(data)


# 开始解决payload 的问题
# 直接从F12里面找到payload，最后一个id值不知道是干嘛的，可能是随机值
dataURL = 'http://odoo.51baiwang.com/web/dataset/search_read'
payloadSTR = '''
{
    "jsonrpc": "2.0", 
    "method": "call", 
    "params": {
        "model": "business.payment", 
        "domain": [ ], 
        "fields": [
            "business_code", 
            "customer_partner_id_string", 
            "collection_date", 
            "collection_amount", 
            "implement_salesmen_user_id", 
            "login_state", 
            "note", 
            "write_off_state"
        ], 
        "limit": 80, 
        "offset": 0, 
        "sort": "", 
        "context": {
            "lang": "zh_CN", 
            "tz": "Asia/Shanghai", 
            "uid": 36, 
            "params": {
                "action": 515, 
                "model": "business.payment", 
                "view_type": "list", 
                "menu_id": 97
            }
        }
    }, 
    "id": 521423206
}
'''
payloadJSON = json.loads(payloadSTR)

# 随机生成9位数
tmpID = random.randint(100000000,999999999)
# print('ryan tmpID ', tmpID)
payloadJSON['id'] = str(tmpID)
payloadJSON['params']['limit'] = 3000

# 试试更改uid，越权拉取uid为2的用户的数据，发现失败
# payloadJSON['params']['context']['uid'] = 2

# 保存参数
payloadSTR = json.dumps(payloadJSON)


# Header['host'] = 'odoo.51baiwang.com'
# Header['X-Requested-With'] = 'XMLHttpRequest'
# Header['Content-Type'] = 'application/json'
# print('ryan Header', Header)

# 构造会话
sess = requests.session()

sess.headers.setdefault('POST', '/web/dataset/search_read HTTP/1.1')
sess.headers.setdefault('Host', 'odoo.51baiwang.com')
sess.headers.setdefault('Connection', 'keep-alive')
sess.headers.setdefault('Content-Length', '433')
sess.headers.setdefault('Accept', 'application/json, text/javascript, */*; q=0.01')
sess.headers.setdefault('X-Requested-With', 'XMLHttpRequest')
sess.headers.setdefault('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36')
sess.headers.setdefault('Content-Type', 'application/json')
sess.headers.setdefault('Origin', 'http://odoo.51baiwang.com')
sess.headers.setdefault('Referer', 'http://odoo.51baiwang.com/web')
sess.headers.setdefault('Accept-Encoding', 'gzip, deflate')
sess.headers.setdefault('Accept-Language', 'zh-CN,zh;q=0.9')

# print('ryan Header', Header)

# 拿到构造cookie
cookies = brower.get_cookies()
# print('ryan cookies : ')
for cookie in cookies:
    # print(cookie['name'], cookie['value'])
    sess.cookies.set(cookie['name'], cookie['value'])

r = sess.post(dataURL, data=json.dumps(payloadJSON), headers=Header)
# print('ryan raw r', r)
r = r.text
# print('ryan r:', r)

# 将所需的结果存到 json 里
r = json.loads(r)
# print('ryan data:', r)
dataLEN = r['result']['length']
# print('ryan dataLEN', dataLEN)

# 打印出来看看
# 导出到csv 文件，供以后分析
header_list = ['回款ID', 
               '合同ID',
               '合同名称',
               '合同编号',
               '付款方名称',
               '付款时间',
               '付款金额',
               '销售人员ID',
               '销售人员名称',
               '备注',
               '是否核销' ]


try:
    data = []
    
    for record in r['result']['records']:
        print(record['id'],
              record['business_code'],
              record['customer_partner_id_string'],
              record['collection_date'],
              record['collection_amount'],
              record['implement_salesmen_user_id'],
              record['note'],
              record['write_off_state'])
        business_id = record['business_code'][0]
        business_name = record['business_code'][1].split('/')[0]
        business_name = business_name.replace('\n','')  # 数据中出现了换行符
        business_NO = record['business_code'][1].split('/')[1]
        saler_id = record['implement_salesmen_user_id'][0]
        saler_name = record['implement_salesmen_user_id'][1]
        note = record['note'] if record['note'] != False else ""
        written_off = "未核销" if record['write_off_state'] == 'not_written_off' else ("已核销" if record['write_off_state'] == 'written_off' else record['write_off_state'])
        data.append([record['id'],
                     business_id,
                     business_name,
                     business_NO,
                     record['customer_partner_id_string'],
                     record['collection_date'],
                     record['collection_amount'],
                     saler_id,
                     saler_name,
                     note,
                     written_off ])
    df = pd.DataFrame(data, columns=header_list)
    df.to_csv(outputFile, index=False)
except Exception as e:
    print('ryan error', e)

# 结束,看结果
time.sleep(10)