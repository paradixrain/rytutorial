# coding=utf-8
import json
import time

import oracledb

DEFAULT_MAIN_USER = "bxoldxxfp"
DEFAULT_MAIN_USER_PWD = "bxoldxxfp"
DEFAULT_CONNECT_STRING = "oracle1.test.baiwang-inner.com:1521/testdb1"


# 连接数据库
def connect_db(user=DEFAULT_MAIN_USER, password=DEFAULT_MAIN_USER_PWD, connect_string=DEFAULT_CONNECT_STRING):
    try:
        connection = oracledb.connect(user=user, password=password, dsn=connect_string)
        return connection
    except oracledb.DatabaseError as e:
        print(e)


# 查询数据
def query_db(connection, sql):
    try:
        with connection.cursor() as cursor:
            cursor.execute(sql)
            query_result = cursor.fetchall()
            return query_result
    except Exception as e:
        print(e)
        return None


def get_jqbh_fromdb(output_json_file):
    """
    通过oracle数据库查询方式，获取税控未上传发票的机器编号，并存入列表中
    """
    # 获取当前时间的年和月
    year_month = time.strftime("%Y%m", time.localtime())
    print(year_month)
    try:
        with connect_db() as connection:
            # 查询数据
            sql = f"select distinct jqbh from pj_zzsz_fpmx where scbz=0 and kprq like '{year_month}%'"
            query_result = query_db(connection, sql)

            # 将查询结果写入json文件
            with open(output_json_file, "w", encoding="utf-8") as f:
                json.dump(query_result, f, ensure_ascii=False)

            return query_result
    except Exception as e:
        print(e)


if __name__ == '__main__':
    jqbh = get_jqbh_fromdb('jqbh.json')
    print('待上传发票的机器编号：', jqbh)
