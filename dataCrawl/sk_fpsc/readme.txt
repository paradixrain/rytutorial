发票自动上传程序使用方式

1、 在堡垒机上执行， save_cookie.py， 并将cookie保存为  cookie.txt 的文件
2、 将 cookie.txt 文件放到全托目录下
3、 将需要触发定时上传的jqbh放到 jqbh.txt 文件中——（原计划从数据库读取，但是考虑数据库的查询效率，暂时不用数据库）
4、 全托服务器上执行 fpsc.py ，该文件会读取 cookie.txt 中的cookie信息，利用该cookie读取jqbh.txt，并发起上传请求