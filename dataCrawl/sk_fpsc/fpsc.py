# coding=utf-8

# @title: 触发税控自动上传
# @desc:  读取cookie.txt， 读取jqbh.txt， 触发税控自动上传
# @author: RyanLin
# @date: 2022/06/09

import json
import time
import urllib3


def fpsc_jqbh(cookie, jqbh_data, fpsc_url):
    """ 拼接出每个jqbh对应的curl串，形如：并发起请求
    curl http://httpurl --cookie "user=root;pass=123456 -s -X POST -d $fpsc_data $fpsc_url"""
    curl_str = f'curl {fpsc_url} --cookie {cookie} -s -X POST -d {jqbh_data}'
    # 使用urllib3 模拟http发起请求
    http = urllib3.PoolManager()
    r = http.request('POST', fpsc_url, headers={'Cookie': cookie}, body=jqbh_data)
    print('r.status: ', r.status)
    print('r.data.decode(): ', r.data.decode())


def get_page(cookie, url):
    """ 测试cookie是否有效 """
    http = urllib3.PoolManager()
    r = http.request('POST', url, headers={'Cookie': cookie})
    print('r.status: ', r.status)
    print('r.data.decode(): ', r.data.decode())


def fpsc_all(cookie_file, jqbh_file, fpscurl):
    """ 读取cookie_file 构造 selenium 浏览器的cookie
     遍历jqbh_file，拼接请求"""
    # 读取cookie
    try:
        with open(cookie_file, 'r') as f:
            cookies = json.load(f)
            # 拼接cookie 串
            cookie_str = ';'.join(f"{i['name']}={i['value']}" for i in cookies)
        print('cookie_str: ', cookie_str)

        # 测试：cookie是否有效
        # get_page(cookie_str, 'http://172.28.30.234:9001/SKServer/fwqgl/isHavaFwqxx.do?_=1654778628401')

        with open(jqbh_file, 'r') as f:
            jqbh_list = json.load(f)
        for jqbh in jqbh_list:
            print('jqbh: ', jqbh)
            time.sleep(3)  # 每次上传间隔3秒
            fpsc_jqbh(cookie_str, f"jqbh={jqbh[0]}&url={jqbh[1]}", fpscurl)

    except Exception as e:
        print(e)


if __name__ == '__main__':
    cookie_file = 'cookie.txt'
    jqbh_file = 'jqbh.json'
    fpscurl = 'http://172.28.30.234:9001/SKServer/mxsc/edit.do'
    fpsc_all(cookie_file, jqbh_file, fpscurl)
