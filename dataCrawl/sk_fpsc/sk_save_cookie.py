# coding=utf-8

from selenium import webdriver
import time
import json


def save_cookie(loginURL, cookie_file):
    """ 登录网站，获取cookie，保存到文件中 """
    try:
        browser = webdriver.Chrome()
        browser.get(loginURL)

        # 等20秒登录页面
        time.sleep(20)

        # 构造会话
        # 拿到构造cookie
        cookies = browser.get_cookies()
        # 7pjJvhNRyhw1Tp8WQhPbWSKT71LQ1QRy8wglSKxJZ89Rh9F63ZY9!1369752696
        # 3314370b06af0c232f59e03ba2903cb8

        # 将cookies 写入 cookie_file， 以便下次使用
        with open(cookie_file, 'w') as f:
            json.dump(cookies, f)

    except Exception as e:
        print(e)
    finally:
        # 结束,看结果
        time.sleep(100)


if __name__ == '__main__':
    loginURL = "http://172.28.30.234:9001/SKServer/"
    cookie_file = 'cookie.txt'
    save_cookie(loginURL, cookie_file)