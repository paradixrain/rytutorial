# coding=utf-8
import pandas
# @title: 爬取公司的电话信息
# @desc:
# @author: RyanLin
# @date: 2024/06/27


import requests
from selenium import webdriver
import time
import json
import random
import pandas as pd

loginURL = "https://d.baiwang.com/channelmanage/otherPages/smb-base-sales/businessInfoManage"
Header = {
    'fetch-size': 'same-origin',
    'Origin': 'https://d.baiwang.com',
    'Cookie': 'ZEUSTOKEN=4d25cbef9158af1ff6a3d357675d4738; JSESSIONID=53FCAE90E916F0E790137DD005083614; acw_tc=0bdd34fe17194577015363085e213bb31704e48a465cf99e91e7743e5a5724; tfstk=fAExDX_JH_fmaW7gc-_uIQb7zXXuWoe4USyBjfcD57F8TbXN3rxmXPFIZoc0nmitB7VxSq_ZnfsTwWKDm-146PFLMScfGlmTW7FKjjuG_h3_i7V0gmmmBfi4qrxiisoTCWmOt6jhx-yq0c1ht42m8SoIpCT6sqsSNQEl5eshx-yfpc4CgMXNCpmtsfislq9SFbkXcnNs5b6-CAAX5AN_FTHZNnT6lhwWNADMchG8NihN1X-TWqf3xe92UdpT2Y1ZZ-KvVAzoHdlLHX5COuKEkXwjwh1nB2Qt6brCZaez9rNmU5IdwDaUwl3IXidEHzwKVjmCJUnYESrKckCvmYmrg2asytsbemhxWl3WLUmY5SrturpNL8ibaylEla5reoqnWXuJNswuejgLWSfyfj4LF733qQxnAyP8X2iC4-q3vyAMtXHMkTBJ7FujERTkvEzLW5gqeXXoZF8ZyWk-tTBJ7FujUYhhEUYw74FF.; ssxmod_itna=QqAxRDgiDQ3ewqBPrSDyBWG8D9GjYDB7AP16j740vToGzDAxn40iDtPoTeI8jiUYmPa8FQ0nDthWh0hxaKK7hDLxKKUia35EDCPGnDBIcYh+DYYkDt4DTD34DYDixibhxi5GRD0KDFGkvzMo5Dbxi30xiaDGeDexv8DY5DhxDC0lPDwx0C02hNu6DDBh438nmPY9BHrIHIy=iwiD5D96oDsE26dDHpBfM4Vc7ji7A3fx0kYq0OMp5GhpoDUQHGlO7eKip9rnwO8WpqhAGrCiieoZh9s72FqemTYQDHI745cXDDi2aPaGGDD=; ssxmod_itna2=QqAxRDgiDQ3ewqBPrSDyBWG8D9GjYDB7AP16jDnF3x4DswhDLB0=Y5SjQnB4nRDdRFiTuNqyYFMAqO/8zg5evi6Cmqa8XDtjAnjdsSnD=tsZrt3SjhYB+ABQX5MalSp0k802lBKPLnDbE2=CGaKMCWHk1qxnB91Rl5W8Zu=mOamte+pnYwG3aHTeo7mKlPWVrKrvKvQk=81L3TLW988nFSjcSEqSSoXepEwEZHbnR5Gg/PrT9pujICjtDOjIEQo3KA++pgjt7WzbzSnIo/EmoIrETKbUGPH4SB1vfx1vtNOKxs2UCw74Rz=uPuq3UossRP9TQIrYy2Y5u3tlKGuPuSPVd=431iOxFeQIEo+hC1hm/2P2+T/Ro4oYIqQv+Y=dIzfPC3WY0wU+T+2ramaR0ksfQuhQVRAPpADmF3SKOfo9kTsE1uh+tqTTpAhRm=24Auo/n30gKfmtqiqEDDwEhie+i9cPCYene4YY1enPecgbLi7pE7hlfpImTW3jDvxABDRxHZt7/CHDy7OiDX+bKdBGSIM9xZi48WwG7Oo2htkN=yQyfP38xdixruFgfrDjKDeuwdYqeiqyzTX7T/ZjXr58BqXE=d0=M7D9cDgPpQijH0DOkxM2UYGCqYIzc49ip8YCY2DQB=804q0y7iDD',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0'}
outputFile = '公司清单.csv'


# 需要提前启动edge调试模式：  msedge.exe --remote-debugging-port=9222 --user-data-dir="c:\edgeselenium\"

option = webdriver.EdgeOptions()
# option.add_argument("--no-sandbox")
# option.add_argument("--lang=zh-CN")
option.add_experimental_option("debuggerAddress", "127.0.0.1:9222")

browser = webdriver.Edge(executable_path=r"C:\Program Files\Python312\Scripts\x64\msedgedriver.exe", options=option)
# browser.get(loginURL)

# 进入到订单页面
contractURL = r"https://d.baiwang.com/channelmanage/otherPages/admin-business-config/ChannelOrderingGoods"
# browser.get(contractURL)
# data = browser.page_source
time.sleep(2)
# print('ryan targit html:')
# print(data)

success_file = 'success_get_telephone.txt'
fail_file = 'fail_get_telephone.txt'

# 开始解决payload 的问题
# 直接从F12里面找到payload，最后一个id值不知道是干嘛的，可能是随机值
dataURL = 'https://d.baiwang.com/zeus/cloudec/titlecloud/search'

payloadSTR = '{"key":"武汉市欣鹏飞厨房设备有限责任公司"}'

header = ['unitName',
          'unitTaxNo',
          'unitAddress',
          'unitPhone',
          'bankName',
          'bankNo',
          'phoneNo',
          'email']


# 构造会话
sess = requests.session()

sess.headers.setdefault('authority', 'd.baiwang.com')
sess.headers.setdefault('method', 'POST')
sess.headers.setdefault('path', '/zeus/cloudec/titlecloud/search')
sess.headers.setdefault('scheme', 'https')
sess.headers.setdefault('Accept', 'application/json, text/javascript, */*; q=0.01')
sess.headers.setdefault('Accept-Encoding', 'gzip, deflate, br, zstd')
sess.headers.setdefault('Accept-Language', 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6')
sess.headers.setdefault('Content-Length', '55')
sess.headers.setdefault('Content-Type', 'application/json; charset=UTF-8')
# sess.headers.setdefault('Cookie',
#                         '''ZEUSTOKEN=4d25cbef9158af1ff6a3d357675d4738; JSESSIONID=53FCAE90E916F0E790137DD005083614; acw_tc=0bdd34fe17194577015363085e213bb31704e48a465cf99e91e7743e5a5724; tfstk=fAExDX_JH_fmaW7gc-_uIQb7zXXuWoe4USyBjfcD57F8TbXN3rxmXPFIZoc0nmitB7VxSq_ZnfsTwWKDm-146PFLMScfGlmTW7FKjjuG_h3_i7V0gmmmBfi4qrxiisoTCWmOt6jhx-yq0c1ht42m8SoIpCT6sqsSNQEl5eshx-yfpc4CgMXNCpmtsfislq9SFbkXcnNs5b6-CAAX5AN_FTHZNnT6lhwWNADMchG8NihN1X-TWqf3xe92UdpT2Y1ZZ-KvVAzoHdlLHX5COuKEkXwjwh1nB2Qt6brCZaez9rNmU5IdwDaUwl3IXidEHzwKVjmCJUnYESrKckCvmYmrg2asytsbemhxWl3WLUmY5SrturpNL8ibaylEla5reoqnWXuJNswuejgLWSfyfj4LF733qQxnAyP8X2iC4-q3vyAMtXHMkTBJ7FujERTkvEzLW5gqeXXoZF8ZyWk-tTBJ7FujUYhhEUYw74FF.; ssxmod_itna=QqAxRDgiDQ3ewqBPrSDyBWG8D9GjYDB7AP16j740vToGzDAxn40iDtPoTeI8jiUYmPa8FQ0nDthWh0hxaKK7hDLxKKUia35EDCPGnDBIcYh+DYYkDt4DTD34DYDixibhxi5GRD0KDFGkvzMo5Dbxi30xiaDGeDexv8DY5DhxDC0lPDwx0C02hNu6DDBh438nmPY9BHrIHIy=iwiD5D96oDsE26dDHpBfM4Vc7ji7A3fx0kYq0OMp5GhpoDUQHGlO7eKip9rnwO8WpqhAGrCiieoZh9s72FqemTYQDHI745cXDDi2aPaGGDD=; ssxmod_itna2=QqAxRDgiDQ3ewqBPrSDyBWG8D9GjYDB7AP16jDnF3x4DswhDLB0=Y5SjQnB4nRDdRFiTuNqyYFMAqO/8zg5evi6Cmqa8XDtjAnjdsSnD=tsZrt3SjhYB+ABQX5MalSp0k802lBKPLnDbE2=CGaKMCWHk1qxnB91Rl5W8Zu=mOamte+pnYwG3aHTeo7mKlPWVrKrvKvQk=81L3TLW988nFSjcSEqSSoXepEwEZHbnR5Gg/PrT9pujICjtDOjIEQo3KA++pgjt7WzbzSnIo/EmoIrETKbUGPH4SB1vfx1vtNOKxs2UCw74Rz=uPuq3UossRP9TQIrYy2Y5u3tlKGuPuSPVd=431iOxFeQIEo+hC1hm/2P2+T/Ro4oYIqQv+Y=dIzfPC3WY0wU+T+2ramaR0ksfQuhQVRAPpADmF3SKOfo9kTsE1uh+tqTTpAhRm=24Auo/n30gKfmtqiqEDDwEhie+i9cPCYene4YY1enPecgbLi7pE7hlfpImTW3jDvxABDRxHZt7/CHDy7OiDX+bKdBGSIM9xZi48WwG7Oo2htkN=yQyfP38xdixruFgfrDjKDeuwdYqeiqyzTX7T/ZjXr58BqXE=d0=M7D9cDgPpQijH0DOkxM2UYGCqYIzc49ip8YCY2DQB=804q0y7iDD''')
sess.headers.setdefault('Origin', 'https://d.baiwang.com')
sess.headers.setdefault('Priority', 'u=1, i')
sess.headers.setdefault('Referer', 'https://d.baiwang.com/admin-business-config/ChannelOrderingGoods')
sess.headers.setdefault('Sec-Fetch-Site', 'same-origin')
sess.headers.setdefault('Sec-Ch-Ua-Platform', 'Windows')
sess.headers.setdefault('Sec-Fetch-Dest', 'empty')
sess.headers.setdefault('Sec-Fetch-Mode', 'cors')
sess.headers.setdefault('User-Agent',
                        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0')

# 拿到构造cookie
cookies = browser.get_cookies()
for cookie in cookies:
    sess.cookies.set(cookie['name'], cookie['value'])

company_list = ["武汉市欣鹏飞厨房设备有限责任公司"]
payloadJSON = json.loads(payloadSTR)
company_file = r"C:\Users\linyu\Downloads\联系方式测试数据.xlsx"
df = pandas.read_excel(company_file, sheet_name="Sheet1", usecols="A:C")

for k, company_info in df.iterrows():
    # 修改公司名称
    company_name = company_info['企业名称']
    print("正在处理", k, company_info['序号'], company_name)
    payloadJSON['key'] = company_name

    # 保存参数
    payloadSTR = json.dumps(payloadJSON)

    r = sess.post(dataURL, data=json.dumps(payloadJSON), headers=Header)
    r = r.text
    # print('ryan text:', r)

    # 将所需的结果存到 json 里
    r = json.loads(r)
    success_flag = r['success']
    dataLEN = r['total']
    print('ryan 一共检索到条目数：', success_flag, dataLEN)

    if success_flag and dataLEN > 0:
        # 如果有查得
        # 导出到csv 文件，供以后分析
        try:
            data = r['data']
            first_row = data[0]
            # 取第一行json 对象，存到pandas里
            """
             {
                "unitName": "武汉市欣鹏飞厨房设备有限责任公司",
                "unitTaxNo": "91420106303522420T",
                "unitAddress": "武昌区杨园街鉄机路5、7号橡树湾二期B组团第B-4幢1单元25层4号",
                "unitPhone": "88876173",
                "bankName": "汉口银行黄鹤楼支行",
                "bankNo": "015011000284927",
                "phoneNo": "88876173",
                "email": null
            }
            """
            first_row['序号'] = company_info['序号']
            print(f"{company_name} saved")
            df = pd.DataFrame(first_row, index=[0])
            df.to_csv(outputFile, header=False, mode='a')

            with open(success_file, 'a') as f:
                f.write(f"{company_name}\n")
        except Exception as e:
            with open(fail_file, 'a') as f:
                f.write(f"{company_name}\n")
            print(f"{company_name} error:", e)
        finally:
            time.sleep(0.1)

# 结束,看结果
# time.sleep(10)
