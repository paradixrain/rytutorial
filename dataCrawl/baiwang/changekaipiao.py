# coding=utf-8
import pandas
# @title: 将“开票软件是否可用置为可用”
# @desc:
# @author: RyanLin
# @date: 2024/06/26


import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.edge.service import Service
import time
import os
import json
import random
import pandas as pd

# loginURL = "https://d.baiwang.com/channelmanage/manageLogin"
loginURL = "https://d.baiwang.com/channelmanage/otherPages/smb-base-sales/businessInfoManage"
Header = {
    'fetch-size': 'same-origin',
    'Origin': 'https://d.baiwang.com',
    'Cookie': 'Hm_lvt_951892daa7f3502287d416cf3646da4e=1716774879; Hm_lpvt_951892daa7f3502287d416cf3646da4e=1719221751; acw_tc=0bdd34fe17194099022465728e2122df1c113101eab92877a312fad1cc60a0; acw_sc__v3=667c1cfed8caa80820495a14dda060f87ecf87cb; OAUTH2TOKEN=864d3127-f5a6-4412-8d37-1bec272dec20; ZEUSTOKEN=4e57f05d5b6d31daa4c112cc4bbd1af9; tfstk=fGStBraKBWViu24cfOahnpcfore3qGBZO1WSmIAil6CdaB24QNmMDZCWtGAGIhOfDsdL3RRgQMdfi6_ijhaqcI1GtNmMsN7xk6sLQE4NjmHNv_nmj5zNhmKDc7VuquXZQn-bZaHwGldw39Ob6lznfO-2cWVuquXNQ_BQBzn9cJKBnL0XcK1XAeOe3KG6hcwddBJXcIGxMld4GQoAkF3GbdjD9mixUdL6NuRKcmspBbA5CzojcMp9WixW3tXySZ6yzGXYDD-GewtBHiySkILRlGxCfWZW8EsCOp1gnqJdktsybFkmm_KOMUs6JxiRFhOHvps7n4RhvCffXewo2ExCqUt1-yPMohpRGG5tHm15KTjw8iF-OI7MUH9A08gvMEsP5gjRrjckwK0TpJ3quF9hh2m-7SQpdpppZJAnuq8dapdupJ3quF9epQ2n-qu2J_5..; ssxmod_itna=eu0=DKiIxAxIx7qD5GHjxcQDgCo3ifeAmQD/KDfh4iNDnD8x7YDvAGOmteabNm37RvwQrm78uGeAtDOw04qQ173p4WDCPGnDBIwbaDee=D5xGoDPxDeDADYoUDAqiOD7qDdfhTXtkDbxi3fxiaDGeDeEKODY5DhxDCUAPDwx0CfrRx61DDB10v5mbbPYBh86BCTKvq5bG8D7HpDla3RfgwuFMxg1YLeDAomx0k4q0Oc=vzC=oDUn9z9xAYQ7b4Qi2toWr3AB0Dbn2xrC1ekWGcIwirVAnKzXqKpG2xDDcD5esK4D; ssxmod_itna2=eu0=DKiIxAxIx7qD5GHjxcQDgCo3ifeAeG93xf2OD4GNWExGa73bmOO1zprpT8=M9UOP11DyXx6Dn4fekm2jKRae8orKeOM7mmAHsoiPCIHF69zqA8CmNupy0FzX=GjZs6UhjDN5BBHzMm58Pows587boImx6DBOxpeqn2PtUBwmkgpdLgICSa0pUQDbvpBQ/jWW8Kn=LFffS=DjZWFNGbD=2O8Um6dGyhk17Zu66=lQY6nR865XClKXbD1rm=3GDpa7itfxGSG0Zhpi9g4qOgkcfAPAAAotnPpfnaHB/GWcoBPbDpGV0xrFaX/e04Y8/IlYYzCWcC9ti4TQuqphVDIP22KGKTUrpU2e2W8mRFIKTeIvm+5vWYoqGI4ADgQ3pWeFqMY=7g3vxt5KY1PNPFw0OepbZmA10YtLeNv2tzE19YWuOTPgWLnd=qddR5=8tpOXRYd3xDKqGQGxD2HkAx7ft5D6f1KjZQiATtDDLxD2tGDD',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0'}

option = webdriver.EdgeOptions()
# option.add_argument("--no-sandbox")
# option.add_argument("--lang=zh-CN")
option.add_experimental_option("debuggerAddress","127.0.0.1:9222")

# ryan， modify, 修改驱动地址
browser = webdriver.Edge(executable_path=r"C:\Program Files\Python312\Scripts\x64\msedgedriver.exe", options=option)
browser.get(loginURL)

# 等20秒扫描登录页面
time.sleep(1)
# username = "test"
# password = "2pFl4W6iKk5B"


# 默认会进到首页，进行跳转
# contractURL = "https://d.baiwang.com/channelmanage/otherPages/smb-base-sales/businessInfoManage"
# browser.get(contractURL)
# data = browser.page_source

# time.sleep(5)
print('start:', time.time())

# 等到页面加载完毕
wait = WebDriverWait(browser, 15)
wait.until(EC.presence_of_element_located((By.ID, "incomeInterface")))

main_iframe = browser.find_element(By.ID, "incomeInterface")
browser.switch_to.frame(main_iframe)

# 读取税号
# ryan, modify, 修改原始数据
filename = r'C:\Users\linyu\Downloads\影子软件已开票用户.xlsx'

dtype = {'企业税号': str}
df = pandas.read_excel(filename, sheet_name="Sheet1", dtype=dtype)

success_file = 'successyz.txt'
fail_file = 'failyz.txt'

# ryan, modify, 当需要并发时，修改这里的df的范围 df[0:2000]['企业税号']
for tax_number in df['企业税号']:
    print("current tax number: " + tax_number)

    # tax_number = '91421303MA494AB36C'
    try:
        # 填写税号
        element = browser.find_element(By.ID, "simple-taxNo")
        # element = browser.find_element(By.XPATH, '//*[@id="simple-taxNo"]')
        # print(f"ryan, 找到税号输入框")
        time.sleep(0.2)

        # 填写税号
        element.send_keys(tax_number)
        # print(f"ryan, 填写税号完成")

        # 查询
        query_button = browser.find_element(By.CSS_SELECTOR, ".ant-btn-primary:nth-child(2)")
        query_button.click()
        time.sleep(1)
        # print(f"ryan, 查询完成")

        try:
            # 如果能找到，则点击编辑
            edit_button = browser.find_element(By.LINK_TEXT, "编辑")
            edit_button.click()
            time.sleep(1)
            # print(f"ryan, 点击编辑")

            # 获取状态
            status_button = browser.find_element(By.ID, 'clientUseMark')
            status = status_button.get_attribute("value")
            # print(f"ryan, 当前状态:{status}")

            # 修改状态
            if status == "false":
                status = "true"
                status_button.click()
                time.sleep(0.5)

                save_button = browser.find_element(By.XPATH, '/html/body/div[3]/div/div[2]/div/div[2]/div[2]/div/button[2]')
                # print(f"ryan, 保存")
                save_button.click()
                with open(success_file, 'a') as f:
                    f.write(f"{tax_number}\n")

            else:
                # 不需要修改，则退出
                quit_button = browser.find_element(By.XPATH, '/html/body/div[3]/div/div[2]/div/div[2]/div[2]/div/button[1]')
                quit_button.click()
                # print(f"ryan,不需要保存")
                with open(fail_file, 'a') as f:
                    f.write(f"noneed\t{tax_number}\n")

            # status = browser.find_element(By.ID, 'clientUseMark').get_attribute("value")
            # print(f"ryan, 修改后状态:{status}")
            time.sleep(1)
        except NoSuchElementException:
            print(f"ryan, 没有找到编辑按钮，税号不存在， {tax_number}")
            with open(fail_file, 'a') as f:
                f.write(f"unexist\t{tax_number}\n")
        except Exception as e:
            print("unknown exception", e)
            with open(fail_file, 'a') as f:
                f.write(f"unknown\t{tax_number}\n")

        time.sleep(1)
        # 重置
        reset_button = browser.find_element(By.XPATH, '//*[@id="root"]/section/div/div/div[1]/form/div[2]/div/div/span/button[1]')
        reset_button.click()
        time.sleep(1)
    except ElementClickInterceptedException:
        # 遇到突发意外，重启browser
        with open(fail_file, 'a') as f:
            f.write(f"unknown\t{tax_number}\n")

        browser.get(loginURL)

        # 等到页面加载完毕
        wait = WebDriverWait(browser, 15)
        wait.until(EC.presence_of_element_located((By.ID, "incomeInterface")))

        main_iframe = browser.find_element(By.ID, "incomeInterface")
        browser.switch_to.frame(main_iframe)

        print("重置browser完毕")

# 退出

print('end:', time.time())
# time.sleep(10)
# browser.quit()
