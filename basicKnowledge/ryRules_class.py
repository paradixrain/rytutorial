# coding = utf-8

# @title: 类的默认语法
# @desc: 
# @author: RyanLin
# @date: 2022/01/20


class Fighter(object):  # object 是所有python对象的基类
    def __init__(self, name, health, damage_per_attack):
        self.name = name
        self.health = health
        self.damage_per_attack = damage_per_attack

    def __str__(self):  # 调用print(class)的时候调用此方法ֵ
        return "Fighter({}, {}, {})".format(self.name, self.health, self.damage_per_attack)

    __repr__ = __str__  # 调用repr(class)的时候调用此方法


# classmethod 修饰符对应的函数不需要实例化，不需要 self 参数，
# 但第一个参数需要是表示自身类的 cls 参数，可以来调用类的属性，类的方法，实例化对象等。
# 就是为了调用类的方法和属性
class A(object):
    bar = 1

    @staticmethod
    def func1():
        print('foo')

    @classmethod
    def func2(cls):
        print('func2')
        print(cls.bar)
        cls().func1()  # 调用 foo 方法


class SingleTon(object):
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = super(SingleTon, cls).__new__(cls)
        return cls.instance


s = SingleTon()
print("Object created", s)

# 再次执行实例化，返回的是同一个对象，这是单例模式的特点
s1 = SingleTon()
print("Object created", s1)


# 单例模式的用例之一就是懒汉式实例化。例如，在导入模块的时候，我们可能会无意中创建一个对象，但当时根本用不到它。
# 懒汉式实例化能够确保在实际需要时才创建对象。所以，懒汉式实例化是一种节约资源并仅在需要时才创建它们的方式
class Singleton:
    __instance = None

    def __init__(self):
        if not Singleton.__instance:
            print("__init__ method called")
        else:
            print("Instance already created:", self.getInstance())

    @classmethod
    def getInstance(cls):
        if not cls.__instance:
            cls.__instance = Singleton()
        return cls.__instance


s = Singleton()  # class initialized, but object not created
print("Object created", Singleton.getInstance())  # object created
s1 = Singleton()  # object already created
