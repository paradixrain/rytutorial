# coding=utf-8

import string
a = string.ascii_letters
# spam = "init spam"
print(__package__)

print(__builtins__)

def scope_test():
    print(1, vars())
    def do_local():
        spam = "local spam"  # 此函数定义了另外的一个spam字符串变量，并且生命周期只在此函数内。此处的spam和外层的spam是两个变量，如果写出spam = spam + “local spam” 会报错

    def do_nonlocal():
        nonlocal spam  # 使用外层的spam变量
        spam = "nonlocal spam"
        print(3, vars())
    print(2, vars())
    def do_global():
        global spam
        # print(__name__, __doc__, __file__, )
        spam = "global spam"

    spam = "test spam"
    do_local()
    print("After local assignmane:", spam)
    do_nonlocal()
    print("After nonlocal assignment:", spam)
    do_global()
    print("After global assignment:", spam)
    print('closure', do_global.__closure__)


# scope_test()
# # print("In global scope:", spam)
# print(vars())
# print('closure', scope_test.__closure__)


def outer():
    name = 'kate'
    def inner():
        print("在inner里打印外层函数的变量", name)

    print('inner closure', inner.__closure__)
    return inner


# print('outer closure', outer.__closure__)
# f = outer()  # f = inner
# f()  # 在inner里打印外层函数的变量 kate


#
# if __name__ == '__main__':
#     main()

# a=10
# b=20

# 定义一个变量接收全局命名空间里的变量名、变量值
# global_namespace=locals()
# print(global_namespace)

# 测试for循环的变量范围
def one():
    c=30
    d=40
    # 定义一个变量接收局部命名空间里的变量名、变量值
    local_namespace=locals()
    for x in range(2):
        print(x, locals())
    print(local_namespace)

# 调用one函数
# one()


# 嵌套作用域
# 全局变量的意外
var = "a"
def inner():
    global var
    var = "b"
    print("inner var", var)
inner()
print("outer var", var)
