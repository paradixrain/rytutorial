"""纯虚函数
纯虚函数是一种特殊的虚函数，在许多情况下，在基类中不能对虚函数给出有意义的实现，而把它声明为纯虚函数，
它的实现留给该基类的派生类去做。这就是纯虚函数的作用。
纯虚函数也可以叫抽象函数，一般来说它只有函数名、参数和返回值类型，不需要函数体。
这意味着它没有函数的实现，需要让派生类去实现。



泛型函数
泛型函数就是，你定义函数时候，是万能类型。在调用的时候，只要你把具体的类型传进去就好。



工厂函数
工厂函数看上去有点像函数，实质上他们是类，当你调用它们时，实际上是生成了该类型的一个实例，就像工厂生产货物一样
工厂函数定义了一个外部的函数，这个函数简单的生成并返回一个内嵌的函数，仅仅是返回却不调用，
因此通过调用这个工厂函数，可以得到内嵌函数的一个引用，内嵌函数就是通过调用工厂函数时，运行内部的def语句而创建的。


python中None是一个特殊的常量，“不同的”None的id是一样的。
所以当判断数据结构中的某一直是否为None时，is会更好一些。
（is函数比==要快一些，不用运行查找和比较函数）


装饰器
装饰器函数其实是这样一个接口约束，它必须接受一个callable对象对位参数，然后返回一个callable对象。
在python中一般callable对象都是函数。PS：只要某个对象重载了__call__()函数，那么这个对象就是callable的

装饰器的注意事项
1、不确定的代码执行顺序
最好不要在装饰器函数之外添加逻辑功能，否则装饰器就不受控了
2、使用functools里的@wraps来绑定默认变量，如__name__， __doc__等
3、如果如果原函数有参数，那么闭包函数必需保持参数个数一致，并且将参数传递给原方法

装饰器 分步实现：
https://www.cnblogs.com/fishbiubiu/p/5495345.html

修饰器会增加程序在运行期的开销，而编译型语言没有这种运行期开销。

闭包函数
"""



"""
Here is a recommended learning path for becoming proficient in Python:

Start with the basics: Learn the syntax and fundamental concepts of the language such as variables, data types, control structures, functions, and loops.

Get familiar with built-in data structures: Learn about lists, tuples, dictionaries, sets, and how to manipulate them effectively.

Learn to use functions effectively: Learn how to define and call functions, pass arguments and return values, and use functions to solve problems.

Work with files: Learn how to read from and write to files, and handle exceptions when working with files.

Learn object-oriented programming: Learn about classes, objects, inheritance, and polymorphism.

Work with modules and packages: Learn how to import and use external libraries and modules.

Learn about data analysis: Learn how to use Python libraries such as NumPy and Pandas for data analysis and manipulation.

Learn about web development: Learn how to use Python frameworks such as Django and Flask for web development.

Practice, practice, practice: As with any new skill, the more you practice the better you will become. Try to solve problems and build projects using Python to apply what you have learned.

Stay updated: Python is a constantly evolving language, so make sure to stay updated with the latest features and best practices by reading the official documentation, blogs, and forums.

Remember, learning a programming language takes time and effort, so be patient and persistent in your studies, and you will eventually become proficient in Python.
"""