# coding=utf-8

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution:
    def isBalanced(self, root: TreeNode) -> bool:
        """ 判断二叉树是否是平衡二叉树 """
        def treeDepth(root):
            if not root:
                return 0
            left = treeDepth(root.left)
            right = treeDepth(root.right)
            return max(left, right) + 1

        if not root:
            return True
        left = treeDepth(root.left)
        right = treeDepth(root.right)
        return abs(left - right) <= 1 and self.isBalanced(root.left) and self.isBalanced(root.right)


# 反转二叉树
class Solution1:
    def invertTree(self, root: TreeNode) -> TreeNode:
        if not root:
            return None
        root.left, root.right = self.invertTree(root.right), self.invertTree(root.left)
        return root


A = TreeNode('A')
B = TreeNode('B')
C = TreeNode('C')
D = TreeNode('D')
E = TreeNode('E')
F = TreeNode('F')
G = TreeNode('G')
A.left = B
A.right = C
B.left = D
B.right = E
C.left = F
C.right = G


# 蛇形遍历二叉树，这题没做出来，ryan tod
def serpentine_traversal(tree) -> list:
    def bfs(root):
        depth = [root] if root else []
        while depth:
            yield [node.data for node in depth]
            depth = [child for node in depth
                     for child in (node.left, node.right) if child]
    return [node for i, line in enumerate(bfs(tree), 1) for node in line[::i%2 or -1]]


# 这种方式跟我思路一样
def serpentine_traversal(root) -> list:
    if not root:
        return []
    stack = [root]
    output = []
    depth = 0
    while stack:
        i = len(stack)
        level = []
        for n in stack[:i]:
            if n.left: stack.append(n.left)
            if n.right: stack.append(n.right)
            level.append(n.data)
        output.extend(level[::-1] if depth%2 else level)
        depth += 1
        stack = stack[i:]
    return output
