"""多进程消息传递
全局变量并不能在多进程中间进行消息传递

多线程消息传递
在一个进程内的所有线程共享全局变量，能够在不使用其他方式的前提下完成多线程之间的数据共享。
由于线程可以对全局变量随意修改，这就可能造成多线程之间对全局变量的混乱，需要引入互斥锁

python线程和进程之间最为关注的区别在于，python多线程由于GIL的限制，不能实现真正的并行，但是进程可以实现真正的并行。
GIL是全局解释器锁，即在一个进程中，也就是一个python解释器下，同一时刻只能有一个线程在工作，
所以对于多核cpu来说，单纯的多线程并不能充分的利用cpu资源，这也往往是在python中利用多进程的动机。
由于每个进程是有独立的python解释器的，因此每个进程有自己的全局解释器锁，所以多进程之间不会受GIL的限制，进而可以利用多进程来实现真正的并行，充分利用多核CPU资源。
"""

"""参数默认值的特性有时会很有用处。 如果有个函数的计算过程会比较耗时，有一种常见技巧是将每次函数调用的参数和结果缓存起来，
并在同样的值被再次请求时返回缓存的值。这种技巧被称为“memoize”，实现代码可如下所示："""


# Callers can only provide two parameters and optionally pass _cache by keyword
def expensive(arg1, arg2, *, _cache={}):
    if (arg1, arg2) in _cache:
        return _cache[(arg1, arg2)]

    # Calculate the value
    result = "handle it"  # ... expensive computation ...
    _cache[(arg1, arg2)] = result  # Store result in the cache
    return result
