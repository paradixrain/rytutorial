# coding=utf-8

import oracledb
oracledb.init_oracle_client(lib_dir=r"D:\greenprogram\instantclient-basic-windows.x64-12.2.0.1.0\instantclient_12_2")

DEFAULT_MAIN_USER = "jxstanded2020"
DEFAULT_MAIN_USER_PWD = "jxstanded2020"
DEFAULT_CONNECT_STRING = "oracle1.test.baiwang-inner.com:1521/testdb1"


# 连接数据库
def connect_db(user=DEFAULT_MAIN_USER, password=DEFAULT_MAIN_USER_PWD, connect_string=DEFAULT_CONNECT_STRING):
    try:
        conn = oracledb.connect(user=user, password=password, dsn=connect_string)
        return conn
    except Exception as e:
        print(e)
        raise ConnectionError("数据库连接失败")


# 查询数据
def query_db(conn, sql):
    try:
        cursor = conn.cursor()
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
    except Exception as e:
        print(e)
        return None


if __name__ == '__main__':
    conn = connect_db()
    sql = "select 1 from dual"
    result = query_db(conn, sql)
    print(result)
    conn.close()
    print("连接关闭")
