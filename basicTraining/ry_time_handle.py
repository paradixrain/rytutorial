import datetime
import random

REN_YUAN = ['马丽丽', '何永康', '王强', '王文', '陈丹丹', '孟雪滢', '邹志伟', '娄蒙蒙', '余玲鑫']
REN_YUAN_STATUS = {'马丽丽': 1, '何永康': 1, '王强': 1, '王文': 1, '陈丹丹': 1, '孟雪滢': 1, '邹志伟': 1, '娄蒙蒙': 0, '余玲鑫': 1}


def main():
    # 取系统时间，判断是否为9点
    if (now := datetime.datetime.now()) and now.hour == 10 and now.minute == 18 and 0 <= now.second <= 6:
        try:
            # 将REN_YUAN中的人员打乱顺序
            random.shuffle(REN_YUAN)
            rtn_str = ''
            # 判断今天是否为周末
            if now.weekday() == 5 or now.weekday() == 6:
                # 所有REN_YUAN_STATUS都按0处理
                for index, people in enumerate(REN_YUAN):
                    rtn_str += f"{index + 1}. {people}{now.month}月{now.day}日未到岗，本人及共同居住人均无异常\n"
            else:
                for index, people in enumerate(REN_YUAN):
                    if REN_YUAN_STATUS[people] == 1:
                        status = '正常到岗'
                    else:
                        status = '居家办公'
                    rtn_str += f"{index + 1}. {people}{now.month}月{now.day}日{status}，本人及共同居住人均无异常\n"
            # 将rtn_str最开始的2个字符去掉
            rtn_str = rtn_str[2:]
            print(rtn_str)
        except Exception as err:
            print(err)


if __name__ == '__main__':
    main()
