# coding=utf-8

from abc import ABCMeta, abstractmethod

# 桥接模式 start
class Shape(metaclass=ABCMeta):
    def __init__(self, color):
        self.color = color

    @abstractmethod
    def draw(self):
        pass


class Color(metaclass=ABCMeta):
    @abstractmethod
    def paint(self, shape):
        pass


class Rectangle(Shape):
    name = "长方形"

    def draw(self):
        self.color.paint(self)
        print("draw rectangle")


class Circle(Shape):
    name = "圆形"

    def draw(self):
        self.color.paint(self)
        print("draw circle")


class Red(Color):
    def paint(self, shape):
        print("paint red", shape.name)


class Green(Color):
    def paint(self, shape):
        print("paint green", shape.name)


# Rectangle(Red()).draw()
# Circle(Green()).draw()

# 桥接模式 end


# 单例模式 start
class Singleton(object):
    instance = None

    def __new__(cls, *args, **kwargs):
        print('new')
        if not getattr(cls, 'instance'):
            cls.instance = super().__new__(cls)
        return cls.instance

    def __init__(self, age, name):
        print('init')
        self.age, self.name = age, name


# a = Singleton(18, "ryan")
# b = Singleton(21, "zs")
# print('id of a,b', id(a), id(b), a.__dict__)
# print(a.name)
# a.age = 19   单例模式下，这行报错，还没搞懂为什么， ryantod
# print(b.age)

# 单例模式 end


