# coding=utf-8

# @title: 测试csv
# @desc:  
# @author: RyanLin
# @date: 2022/02/15

import csv
csvIn = "../book/foundations-for-analytics-with-python-master/csv/supplier_data.csv"
# print(csvIn)
csvOut = "csvOut.csv"
myColumn = [0, 3]

with open(csvIn, 'r', newline = '') as cifile:
    with open(csvOut, 'a', newline = '') as cofile:
        csvInReader = csv.reader(cifile)
        csvOutWriteer = csv.writer(cofile)
        for row_list in csvInReader:
            row_list_output = []
            for index_value in myColumn:
                row_list_output.append(row_list[index_value])
            csvOutWriteer.writerow(row_list_output)

