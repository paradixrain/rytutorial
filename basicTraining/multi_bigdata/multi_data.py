import pandas as pd
import queue

excel_length = 50000
counter = 0


# 函数A：读取"a.xlsx"，将每一行数据拼接为一个字符串，插入队列
def function_A(queue):
    df = pd.read_excel("a.xlsx", sheet_name="普票")
    for index, row in df.iterrows():
        data = " ".join([str(cell) for cell in row])
        queue.put(data)


# 函数B：读取队列中的字符串，打印字符串的长度
def function_B(queue):
    global counter
    while not queue.empty() or counter < excel_length:
        data = queue.get()
        print("字符串长度：", data)
        counter += 1


# 主函数
def main():
    # 创建队列
    q = queue.Queue()

    # 启动函数A的线程
    function_A(q)

    # 执行函数B并打印结果
    function_B(q)


if __name__ == "__main__":
    main()
