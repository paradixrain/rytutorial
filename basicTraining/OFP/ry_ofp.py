# coding=utf-8

# 面向函数编程，使用闭包函数
# keyword： 闭包，函数

def nth_power(exponent):
    def exponent_of(base):
        return base ** exponent
    return exponent_of
square = nth_power(2)


#查看 __closure__ 的值
print(square.__closure__)
print(square.__dict__)
print(square.__call__(3))
print(square.__str__())


class describe:
    def __init__(self, description):
        self.description = description
        self.count = 0

    def __call__(self, *args, **kwargs):
        print(*args)
        # if len(*args) == 0:
        #     self.count += 1
        #     return describe.__call__(self, args[1:])'