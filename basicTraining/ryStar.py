# coding=utf-8

args = [[3, 6], [5, 9]]
arg = (3, 6)
# print(*args, *arg)


def args_count(*args, **kwargs):
    # 统计参数个数
    return len(args) + len(kwargs)


ryan = args_count(0, 1, 2, i0=0, i1=1)
print(ryan)
