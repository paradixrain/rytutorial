#! /usr/bin/env python
# -＊- coding: UTF-8-＊-
import threading
import time

num = 0
mlock = threading.Lock()


class MyThread(threading.Thread):
    def run(self):
        global num
        time.sleep(1)

        if mlock.acquire(True):
            num = num + 1
            msg = self.name + ' set num to ' + str(num)
            print(msg)
            mlock.release()


def test():
    for i in range(5):
        t = MyThread()
        t.start()


if __name__ == '__main__':
    test()
