#! /usr/bin/env python
# -＊- coding:utf-8-＊-

import threading
import time
import queue

SHARE_Q = queue.Queue()  # 构造一个不限制大小的队列
WORKER_THREAD_NUM = 3  # 设置线程的个数，此处数值可以自由调整，结合下面的程序完成时间来观察效果


class MyThread(threading.Thread):
    def __init__(self, func):
        super(MyThread, self).__init__()
        self.func = func

    def run(self):
        self.func()


def worker():
    global SHARE_Q
    while not SHARE_Q.empty():
        item = SHARE_Q.get()  # 获得任务
        print("Processing : ", item)
        time.sleep(1)


def main():
    global SHARE_Q
    threads = []
    create_time = time.time()
    # print create_time
    for task in range(500, -1, -1):  # 向队列中放入任务
        SHARE_Q.put(task)
    for i in range(WORKER_THREAD_NUM):
        thread = MyThread(worker)
        thread.start()
        threads.append(thread)
    for thread in threads:
        thread.join()
    end_time = time.time() - create_time
    print("程序总共运行时间为%s" % end_time)


if __name__ == '__main__':
    main()
