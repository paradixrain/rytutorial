# coding = utf-8
import datetime
import os

import pandas
from pandas.errors import ParserError

WORKING_DIR = r"C:\Users\Ryan\git\rytutorial\working"
if not os.path.exists(WORKING_DIR):
    WORKING_DIR = r"D:\work\workspace\git\rytutorial\working"


filename = f"{WORKING_DIR}\\第三批线下核验.xls"

dtype = {"开票日期": str}

df = pandas.read_excel(filename, sheet_name="普票", usecols="D:D")
# df["开票日期"] = pandas.to_datetime(df["开票日期"])

for index, row in df.iterrows():
    # print(type(df.iloc[index]))
    try:
        t = df.iloc[index]["开票日期"]
        # d = '-'.join(t.split('/'))
        # d = pandas.to_date(t)

        tt = datetime.datetime.strftime(t, "%Y-%m-%d")
        print(index, "t=", t, "d=", type(t))
    except ParserError as e:
        print("类型转化错误", e)
    except ValueError as e:
        print(e)
        # print(df.iloc[index])

