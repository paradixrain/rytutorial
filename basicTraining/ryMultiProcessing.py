# coding = utf-8

import multiprocessing
import pandas

# import time

WORKING_DIR = r"C:\Users\Ryan\git\rytutorial\working"
OUT_FILE = r"outprint.txt"


def func(msg):
    params_str = ""
    for key, value in sorted(msg.items()):
        params_str += f"{key}{value}"
    print(multiprocessing.current_process().name + '-' + params_str)

    return f'{params_str}'


if __name__ == "__main__":
    pool = multiprocessing.Pool(processes=2)  # 创建4个进程
    result = []
    df = pandas.read_excel(f'{WORKING_DIR}\\2023年1-7月线下批量查验清单.xlsx', sheet_name="专票")
    for index, row in df.iterrows():
        r = pool.apply_async(func, (row,))
        result.append((row, r))

    pool.close()  # 关闭进程池，表示不能再往进程池中添加进程
    pool.join()  # 等待进程池中的所有进程执行完毕，必须在close()之后调用

    with open(f'{OUT_FILE}', 'w') as f:
        print("开始输出到文件")
        for i in result:
            row, res = i[0], i[1]
            rr = res.get()
            f.write(str(row) + '========' + str(rr)+'\n')

    print("Sub-process(es) done.")
