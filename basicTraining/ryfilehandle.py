# coding = utf-8

MAX_CHAR = 1000


def send2wechat2(base64file):
    with open(base64file, "r") as fr:
        blockcount = 0
        content = fr.read(MAX_CHAR)
        while content != '':
            content = 'content block' + str(blockcount).rjust(20,'0') + content
            print('content block',blockcount,':',content )
            blockcount += 1
            content = fr.read(MAX_CHAR)

if __name__ == '__main__':
    send2wechat2(r'C:\Users\Ryan\git\rytutorial\src\base64_handle\1.zip.txt')