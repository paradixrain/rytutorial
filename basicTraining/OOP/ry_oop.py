# coding=utf-8


# 单例模式
class A:
    instance = None

    def __init__(self, name):
        self.name = name
        print('init')

    def __new__(cls, *args, **kwargs):
        if not getattr(cls, 'instance'):
            print('create a new one', cls.__dict__)
            cls.instance = super().__new__(cls)
        else:
            print('use existing instance', cls.__dict__)
        return cls.instance

    def __del__(self):
        print('del')


# a = A("ryan")
# b = A("brian")
# print(a.name)
# print(b.name)


# 类的多态
class Animal:
    def __init__(self, age: str):
        self.age = age

    def get_age(self):
        return self.age


class Dog(Animal):
    def bark(self):
        return "dog shout"


# b = Animal('18')
# print(b.get_age())
# d = Dog('20')
# print(Dog.__base__, Dog.__bases__)


# 工厂模式
class Shape(object):
    def __init__(object):
        pass

    def draw(self):
        pass


class Triangle(Shape):
    def __init__(self):
        print(" I am a triangle")

    def draw(self):
        print(" I am drawing a triangle")


class Rectangle(Shape):
    def __init__(self):
        print(" I am a rectangle")

    def draw(self):
        print(" I am drawing a rectangle")


class Trapezoid(Shape):
    def __init__(self):
        print(" I am a trapezoid")

    def draw(self):
        print(" I am drawing a trapezoid")


class Diamond(Shape):
    def __init__(self):
        print(" I am a diamond")

    def draw(self):
        print(" I am drawing a diamond")


class ShapeFactory(object):
    shapes = {'triangle': Triangle,
              'rectangle': Rectangle,
              'trapezoid': Trapezoid,
              'diamond': Diamond}

    def __new__(cls, name):
        if name in ShapeFactory.shapes.keys():
            print("creating shape", name)
            return ShapeFactory.shapes[name]()
        else:
            print("creating shape with a new name", name)
            return Shape()
