def http_error(status):
    match status:
        case 400:
            return "Bad request 400"
        case 401:
            return "Unauthorized"
        case 403:
            return "Forbidden"
        case _:
            return "Internal Server Error"


ryan = http_error(401)
print(ryan)
