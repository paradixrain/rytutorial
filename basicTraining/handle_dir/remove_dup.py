# coding=utf-8

# filename: handle_file.py
# authors: RyanLin
# datetime: 2022/6/29
# description: 遍历指定目录下所有文件，删除重复文件


import os
from os.path import join, getsize



def dir_size(path):
    """
    递归调用，记录所有文件的路径和大小，并排序
    """
    for root, dirs, files in os.walk(path):
        for f in files:
            size += os.path.getsize(os.path.join(root, f))
            # print(f)
    return bkmg_fit(size)



if __name__ == '__main__':
    directory = r'C:\Users\Ryan'
    # s = dir_size(directory)
    dir_size1(directory)
    # print('文件夹大小为', s)
