# coding=utf-8

# filename: handle_file.py
# authors: RyanLin
# datetime: 2022/6/29
# description: 指定一个目录，输出这个目录下每个一级目录/文件的大小。目录大小等于内嵌所有文件的递归，包含隐藏文件


import os
from os.path import join, getsize


def bkmg_fit(bytesize: int):
    """
    :param bytesize: number of bytes
    :return: 合适的大小
    """
    if bytesize > 1024 * 1024 * 1024:
        return f"{bytesize // (1024 * 1024 * 1024)} GB"
    elif bytesize > 1024 * 1024:
        return f"{bytesize // (1024 * 1024)} MB"
    elif bytesize > 1024:
        return f"{bytesize // 1024} KB"
    else:
        return f"{bytesize} Bytes"


def dir_size(path, size=0):
    """
    递归调用，统计文件夹大小
    """
    for root, dirs, files in os.walk(path):
        for f in files:
            size += os.path.getsize(os.path.join(root, f))
            # print(f)
    return bkmg_fit(size)


def dir_size1(path):
    print(os.listdir(path))
    son_dir = os.listdir(path)
    for s in son_dir:
        size = dir_size(os.path.join(path, s))
        print(s, size)


if __name__ == '__main__':
    directory = r'C:\Users\Ryan'
    # s = dir_size(directory)
    dir_size1(directory)
    # print('文件夹大小为', s)
