# coding=utf-8

from collections import defaultdict
s = [('yellow', 1), ('blue', 2), ('yellow', 3), ('blue', 4), ('red', 1)]
d = defaultdict(list)
print(d.items())
for k, v in s:
    d[k].append(v)

print(sorted(d.items()))
# sorted(d.items())
# [('blue', [2, 4]), ('red', [1]), ('yellow', [1, 3])]