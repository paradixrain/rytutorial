# coding=utf-8
'''
@filename: ry_license_email_push.py
@title: 注册码到期了，根据excel中的人名，进行告警推送
@desc:  有注册码到期的excel，每个sheet里有人名，此模块用于根据人名精确投递告警信息
@author: RyanLin
@Version: Python 3.10
@date: 2022/03/02
'''

