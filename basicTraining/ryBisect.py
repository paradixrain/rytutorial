'''
@title: ryBisect.py
@desc: 使用二分法查找数值
@author: RyanLin
@date: 2022/03/11
'''

# coding = utf-8

import bisect
import pandas as pd

def main(xz, xzdwS):
    '''
    输入xz薪资和xzdwS薪资档位的pd.Series，返回薪资对应的档位的名字
    原则是：就近就高，如果超过最高档位，则返回最高档位
    '''
    left_pos = bisect.bisect_left(xzdwS, xz)
    left_pos = min(left_pos, xzdwS.size -1)
    # print('left_pos=', left_pos)
    left_pos_name = xzdwS.index[left_pos]
    print(left_pos_name)
    return left_pos_name

if __name__ == '__main__':
    xzdw = pd.DataFrame({'一档': [10], '二档': [20], '三档': [30]})
    xzdwS = pd.Series(xzdw.loc[0])
    # print(xzdwS)
    main(9, xzdwS)
    main(19, xzdwS)
    main(31, xzdwS)