# coding = utf-8
import json

# x = 10
# y = 0
# try:
#     print(x, y)
#     print(x / y)
# except ZeroDivisionError:
#     print('y is zero')
# finally:
#     print('ends1')
#
# try:
#     print('x,y', x, y)
#     print('y/x', y / x)
# except Exception as e:
#     print(e)
# else:
#     print('x/y', x / y)
# finally:
#     print('ends2')

res = {"a": "1", "b": "2"}

try:
    a = res['a']
    print(f"a:{a}")
    c = res['c']
    print(f"a:{c}")
except ValueError:
    print(a, c)
