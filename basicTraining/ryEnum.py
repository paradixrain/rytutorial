# coding = utf-8

personinfo = {'name': 'Jon', 'age': '20', 'hobby': 'football'}

# 不能在访问过程中修改迭代变量
for k, v in personinfo.items():
    print(k, ":", v)
    if k == 'age':
        personinfo['name'] = 'hi'
        personinfo['sex'] = 'female'

print(personinfo)