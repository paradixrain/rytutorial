# coding=utf-8
import inspect


def ryan_read():
    # 文件代码自打印
    with open(globals()['__file__'], 'r', encoding="utf-8") as f:
        print(f.read())


if __name__ == '__main__':
    ryan_read()
    # 已知函数名打印函数代码
    source = inspect.getsource(ryan_read)
    print(source)
