---
name: Announcements
about: Use this if you are sharing something interesting
title: ''
labels: announcement
assignees: ''

---

<!-- Let us know if you are speaking at a conference on python-oracledb, or have a new package or app that uses python-oracledb, or something similarly exciting. -->
