# Python python-oracledb Support

python-oracledb is an Open Source project, so do some searching and reading
before asking questions.

## python-oracledb Installation issues

Read the [Installation instructions](http://python-oracledb.readthedocs.io/en/latest/installation.html)

## SQL and PL/SQL Questions

Ask SQL and PL/SQL questions at [AskTOM](https://asktom.oracle.com/)

Try out SQL and find code snippets on our hosted database
with [LIVE SQL](https://livesql.oracle.com/)

## Database and other Oracle Issues

Ask Database and other Oracle issues at [Oracle
Communities](https://community.oracle.com/hub/).

## python-oracledb Documentation

The python-oracledb documentation
is [here](http://python-oracledb.readthedocs.io/en/latest/)

## Got a python-oracledb question?

Ask at [GitHub](https://github.com/oracle/python-python-oracledb/issues)

When opening a new issue, fill in the template that will be shown.
Include enough information for people to understand your problem.
