Sphinx 4.2 is used to generate the HTML for the python-oracledb documentation.

The generated python-oracledb documentation is at https://python-oracledb.readthedocs.io/

This directory contains the documentation source.  It is written using reST
(re-Structured Text) format source files which are processed using Sphinx and
turned into HTML, PDF or ePub documents. If you wish to build these yourself,
you need to install Sphinx 4.2. Sphinx is available on many Linux distributions as a
pre-built package. You can also install Sphinx 4.2 on all platforms using the Python
package manager "pip".  To install Sphinx 4.2, run the following command:
pip install Sphinx==4.2.0

For more information on Sphinx, please visit this page:

http://www.sphinx-doc.org

Once Sphinx 4.2 is installed, the supplied Makefile can be used to build the
different targets.
