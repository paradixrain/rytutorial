# coding=utf-8

import pandas as pd
import openpyxl
from pathlib import Path


def merge_excel_from_dir(dir_path, output_file, model_file):
    # 以model_file为模板，将目录下的excel文件合并为一个新的excel文件，并保存为output_file
    # model_file 为只含有表头不含数据的excel文件，model_file不能存放于dir_path下
    # 所有的excel文件必须含有相同的表头，否则可能会报错
    # 目录下所有excel文件的同sheet_name的sheet页会合并为output_file的一个sheet
    # 只合并在MERGE_SHEET_LIST中指定的sheet
    merge_sheet_list = [
        '1-其他应收款',
        '2-其他应付款',
        '3-应付赔付款',
        '4-预付赔款'
    ]

    # 获取dir_path下所有excel文件列表
    excel_files = list(dir_path.glob('*.xlsx'))

    # 复制model_file为output_file，目的是为了保留model_file的表头
    output_wb = openpyxl.load_workbook(model_file)
    output_wb.save(output_file)

    # 遍历merge_sheet_list中的sheet，将dir_path下的excel文件中的sheet合并到output_file中
    for sheet in merge_sheet_list:
        print('正在合并sheet:', sheet)
        # 获取output_file中的sheet
        output_sheet = output_wb[sheet]
        print('output_sheet:', output_sheet)

        # 遍历dir_path下的excel文件
        for excel_file in excel_files:
            print('正在合并文件:', excel_file)
            # 获取model_file中的sheet的行数，因为openpyxl的行数不准，所以还是用pandas
            header_row = pd.read_excel(model_file, sheet_name=sheet).shape[0]

            # 获取excel_file中的sheet
            excel_sheet = pd.read_excel(excel_file, sheet_name=sheet, header=header_row)

            # 将excel_sheet中的数据合并到output_sheet中
            for row in excel_sheet.values.tolist():
                output_sheet.append(row)

        # 保存output_file
        output_wb.save(output_file)


if __name__ == '__main__':
    merge_excel_from_dir(Path('./data'), Path('./merge_data.xlsx'), Path('./model.xlsx'))
    print('合并完成')