# def lovefunc( flower1, flower2):
#     ''' Opposites Attract '''
#     return ((flower1 - flower2) % 2 ) != 0
# 
# def score(dice):
#     ''' Greed is Good '''
#     score = int(dice.count(1)/3)*1000 + int(dice.count(6)/3)*600 + int(dice.count(5)/3)*500 + int(dice.count(4)/3)*400 + int(dice.count(3)/3)*300 + int(dice.count(2)/3)*200
#     score = score + 100*(dice.count(1) % 3) + 50*(dice.count(5) % 3)
#     return int(score)
# 
# def increment_string(strng):
#     ''' String incrementer '''
#     def replacestr(m):
#         orglen=len(str(m.group(0)))
#         rtnstr=str(int(m.group(0))+1)
#         if( len(rtnstr) < orglen ):
#             return '0'*(orglen-len(rtnstr))+rtnstr
#         else:
#             return rtnstr
#     import re
#     strngout=re.sub('([0-9]+)$', replacestr, strng)
#     if strngout == strng:
#         return strng+"1"
#     else:
#         return strngout
# 
# def increment_string2(strng):
#     ''' String incrementer '''
#     head = strng.rstrip('0123456789')
#     tail = strng[len(head):]
#     if tail == "": return strng+"1"
#     return head + str(int(tail) + 1).zfill(len(tail))
# 
# def isPP(n):
#     ''' What's a Perfect Power anyway? '''
#     import math
#     for i in range(2,n+1):
#         for j in range(2,int(math.sqrt(n)),1):
#             if ( i % j == 0):
#                 return [i,j]
#     return None
# 
# def minNum(n):
#     import math
#     if n<=2: return n
#     for i in range(2, math.isqrt(n)+1):
#         if n % i == 0:
#             return i
# 
# def usdcny(usd):
#     return "{0:.2f}".format(usd*6.75)+" Chinese Yuan"
# 
# def kata_13_december(lst): 
#     for i in range(len(lst)-1,-1,-1):
#         if lst[i]%2==0: 
#             lst.pop(i)
#     return lst
# 
# def isDigit(string):
#     print(string)
#     dspace = string.rstrip().lstrip()
#     if dspace=='' or dspace == None: return False
#     dint = dspace.isdigit()
#     f = dspace.split('.')
#     if dint:
#         return True
#     if len(f)<=2 and len(f)>=1:
#         if f[0][0]=="-":
#             f[0]=f[0][1:]
#         for fi in f:
#             if not fi.isdigit():
#                 return False
#         return True
#     return False
# 
# def isDigit2(string):
#     try:
#         float(string)
#         return True
#     except:
#         return False
# 
# def sum_str(a, b):
#     if a!="":
#         if b!="":
#             return int(a)+int(b)
#         return int(a)
#     if a=="":
#         if b!="":
#             return int(b)
#         return 0
# 
# def sum_str(a, b):
#     return str(int(a or 0)+int(b or 0))
# 
# def human_years_cat_years_dog_years(human_years):
#     return [human_years,
#             15+(human_years>0)*9+(human_years>1)*(human_years-2)*4,
#             15+(human_years>0)*9+(human_years>1)*(human_years-2)*5]
# 
# 
# def get_drink_by_profession(param):
#     dictd={"Jabroni":"Patron Tequila",
#            "School Counselor":"Anything with Alcohol",
#            "Programmer":"Hipster Craft Beer",
#            "Bike Gang Member":"Moonshine",
#            "Politician":"Your tax dollars",
#            "Rapper":"Cristal"}
#     import string
#     cdict=string.capwords(param)
#     print(cdict)
#     if cdict not in dictd.keys(): 
#         return "Beer"
#     else:
#         return dictd[cdict]
# 
# def truthy(): 
#   print("True")
#   
# def falsey(): 
#   print("False")
# 
# def _if(bool, func1, func2):
#     if bool:
#         func1()
#     else:
#         func2()
# 
# def merge_arrays(first, second): 
#     return list(set(first.extend(second)))
# 
# 
# def HQ9(code):
#     if code[0]=='H':
#         return 'Hello World!'
#     elif code[0]=='Q':
#         return code
#     elif code[0]=='9':
#         outstr=""
#         for i in range(99,2,-1):
#             outstr+="{} bottles of beer on the wall, {} bottles of beer.\nTake one down and pass it around, {} bottles of beer on the wall.\n".format(i,i,i-1)
#         outstr+="2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n1 bottle of beer on the wall, 1 bottle of beer.\nTake one down and pass it around, no more bottles of beer on the wall.\nNo more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall."
#         return outstr
# 
# 
# def round_it(n):
#     from math import ceil
#     dot=str(n).index('.')
#     if dot<len(str(n))-dot-1:
#         return ceil(n)
#     if dot>len(str(n))-dot-1:
#         return int(n)
#     return round(n)
# 
# def cal(s, ops):
#     t=True
#     f=False
#     cals=''
#     for i in range(len(ops)):
#         cals=cals+s[i]+ops[i]
#     cals=cals+s[-1]
#     print('cals=', cals)
#     return eval(cals)
# 
# def solve(s,ops):
#     if len(ops)==1:
#         return cals(s,ops)
#     pass
#     return solve(s1, ops)+cal(s,ops)
# 
# def remove_consecutive_duplicates(s):
#     l = s.split()
#     tmp = l[0]
#     lcp = []
#     lcp.append(tmp)
#     for i in range(1,len(l)):
#         if tmp == l[i]:
#             pass
#         else:
#             lcp.append(l[i])
#             tmp = l[i]
#     return ' '.join(lcp)
# 
# 
# def box_capacity(length, width, height):
#     # 1 feet = 12 inch
#     return int(length*12/16) * int(width*12/16) * int(height*12/16)
# 
# def remove(s):
#     return s.replace('!','') + '!'*s.count('!')
# 
# def swap(st):
#     return st.translate(str.maketrans('aeiou','AEIOU'))
# 
# def explode(s):
#     outstr = ''
#     for i in s:
#         outstr += int(i) * i
#     return outstr
# 
# def compare(s1,s2):
#     c1, c2 = -1, -1
#     if( s1 == None or not s1.isalpha()):
#         c1 = 0
#     if( s2 == None or not s2.isalpha()):
#         c2 = 0
#     if c1 == 0 or c2 == 0:
#         return c1 == c2
#     else:
#         return sum(ord(i) for i in s1.upper() ) == sum(ord(j) for j in s2.upper())
#     
# def paul(x):
#     dictr={'kata':5, 'Petes kata':10, 'life':0, 'eating':1}
#     val = sum(dictr.get(i,0) for i in x)
#     if val < 40:
#         return 'Super happy!'
#     elif val < 70:
#         return 'Happy!'
#     elif val < 100:
#         return 'Sad!'
#     else:
#         return 'Miserable!'
# 
# PTS  = {'kata':5, 'Petes kata':10, 'life':0, 'eating':1}
# OUTS = ((100,'Miserable!'), (70,'Sad!'), (40,'Happy!'), (0,'Super happy!'))
# 
# def paul2(lst):
#     v = sum(map(PTS.__getitem__, lst))
#     return next(s for limit,s in OUTS if v>=limit)
# 
# def triangular(n):
#     return n*(n+1)//2 if n>=0 else 0
#
# s1 = triangular(1951242374880)
# s2 = triangular(7764414977763)
# print(s1,s2)
# 1903673402764646902923264 30143069973459086407106560
# 1903673402764646848694640 30143069973459086099730966
# 3807346805529293697389280 60286139946918172199461932
from ctypes.test.test_pickling import name
from _ast import Name

# class Node():
#     def __init__(self, data, next = None):
#         self.data = data
#         self.next = next
#
# def stringify(node):
#     if node == None:
#         return 'None'
#     outStr = ''
#     nodeNext = node
#     while True:
#         outStr += str(nodeNext.data) + ' -> '
#         nodeNext = nodeNext.next
#         if nodeNext == None:
#             break
#     outStr += 'None'
#     return outStr
#
# def stringify2(node):
#     result = ''
#     while node:
#         result += str(node.data) + ' -> '
#         node = node.next
#     return result + 'None'

# def prev_mult_of_three(n):
#     while n%3 != 0 and n>0:
#         n = n//10
#     if n == 0:
#         return None
#     return n

# def collision(x1, y1, radius1, x2, y2, radius2): 
#     return radius1 + radius2 > ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5 

# class Song:
#     def __init__(self, title, artist):
#         self.title = title
#         self.artist = artist
#         self.todayheard = set()
#
#     def how_many(self, newheardpeople):
#         nhp = set(i.lower() for i in newheardpeople)
#         len1 = len(self.todayheard)
#         self.todayheard = self.todayheard.union(nhp)
#         len2 = len(self.todayheard)
#         return len2 - len1
#
# q = Song('Mount Moose', 'The Snazzy Moose')
# p = q.how_many([])
# print(p)
# p = q.how_many(['John', 'Fred', 'Bob', 'Carl', 'RyAn'])
# print(p)
# p = q.how_many(['John', 'Fred', 'Bob', 'Carl', 'rain'])



# class List(object):
#     def count_spec_digits(self, integers_list, digits_list):
#         s=''.join(str(integers_list))
#         print('s:', s)
#         rtnList = []
#         for i in digits_list:
#             rtnList.append((int(i), s.count(str(i))))
#         return rtnList
# integers_list =  [1, 1, 2 ,3 ,1 ,2 ,3 ,4]
# digits_list = [1, 3]
# l = List()
# s = l.count_spec_digits(integers_list, digits_list)


# class List:
#     def remove_(self, integer_list, values_list):
#         return [i for i in integer_list if i not in values_list]
    
# integer_list =  [1, 1, 2 ,3 ,1 ,2 ,3 ,4]
# values_list = [1, 3]
# l=List()
# s=l.remove_(integer_list, values_list)


# class Person():
#
#     def __init__(self, first_name, last_name, ages):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.ages = ages
#
#     @property
#     def full_name(self):
#         return '{} {}'.format(self.first_name, self.last_name)
#
#     @property
#     def age(self):
#         return self.ages
#
# matz = Person('Yukihiro', 'Matsumoto', 47)
# print(matz.full_name, 'Yukihiro Matsumoto')
# print(matz.ages, 47)


# class Dictionary():
#     def __init__(self):
#         self.dictionary = {}
#
#     def newentry(self, word, definition):
#         self.dictionary[word] = definition
#
#     def look(self, key):
#         return self.dictionary.get(key, "Can't find entry for {}".format(key))
#
# d = Dictionary()
#
# d.newentry("Apple", "A fruit")
# print(d.look("Apple"), "A fruit")
# print(d.look("Pie"), "A Pie")


# class User(object):
#     def __init__(self, name, balance, checking_account):
#         self.name = name
#         self.balance = balance
#         self.checking_account = checking_account
#
#     def withdraw(self, money):
#         if self.balance < money or money < 0:
#             raise ValueError
#         self.balance -= money
#         return '{} has {}'.format(self.name, self.balance)
#
#     def check(self, people, money):
#         if people.balance < money or people.checking_account == False:
#             raise ValueError
#         people.balance -= money
#         self.balance += money
#         return '{} has {} and {} has {}'.format(self.name, self.balance, people.name, people.balance)
#
#     def add_cash(self, money):
#         self.balance += int(money)
#         return '{} has {}'.format(self.name, self.balance)
#
#
# Jeff = User('Jeff', 70, True)
# Joe = User('Joe', 70, True)
#
# s1 = Jeff.withdraw(2) # Returns 'Jeff has 68.'
# s2 = Joe.check(Jeff, 50) # Returns 'Joe has 120 and Jeff has 18.'
# # s3 = Jeff.check(Joe, 80) # Raises a ValueError
# s4 = Joe.checking_account # Enables checking for Joe
# s5 = Jeff.check(Joe, 80) # Returns 'Jeff has 98 and Joe has 40'
# # s6 = Joe.check(Jeff, 100) # Raises a ValueError
# s7 = Jeff.add_cash(20.00) # Returns 'Jeff has 118.'
# print(s1, s2,  s4, s5 , s7)


# class Quark(object):
#     def __init__(self, color, flavor):
#         self.color = ""
#         self.flavor = ""
#         if color in ("red", "blue", "green"):
#             self.color = color
#         if flavor in ('up', 'down', 'strange', 'charm', 'top', 'bottom'):
#             self.flavor = flavor
#         self.baryon_number = 1/3
#
#     def interact(self, q2):
#         if isinstance(q2, Quark):
#             self.color, q2.color = q2.color, self.color
#
#
# q1 = Quark("red", "up")
# print(q1.color, q1.flavor)
# q2 = Quark("blue", "strange")
# print(q2.color, q2.baryon_number)
# q1.interact(q2)
# print(q1.color, q2.color)
# if isinstance(q2, Quark):
#     print('yes')


# class List:
#     def __init__(self,type):
#         self.type=type
#         self.items=[]
#         self.count=0
#
#     def add(self,item):
#         if type(item)!=self.type:
#             item_type="str" if self.type==str else "int" if self.type==int else "float"
#             return "This item is not of type: %s" %(item_type)
#         self.items.append(item)
#         self.count += 1
#         return self
#
# my_list=List(str)
#
# print(my_list.add('Hello').count)
# print(my_list.add(5))
# print(my_list.add(' ').add('World!').count)


# class Fighter(object):
#     def __init__(self, name, health, damage_per_attack):
#         self.name = name
#         self.health = health
#         self.damage_per_attack = damage_per_attack
#
#     def __str__(self):
#         return "Fighter({}, {}, {})".format(self.name, self.health, self.damage_per_attack)
#     __repr__=__str__
#
# def declare_winner(fighter1, fighter2, first_attacker):
#     if fighter1.health // fighter2.damage_per_attack < fighter2.health // fighter1.damage_per_attack:
#         return fighter2.name
#     if fighter1.health // fighter2.damage_per_attack == fighter2.health // fighter1.damage_per_attack:
#         if fighter1.health % fighter2.damage_per_attack == 0:
#             if fighter2.health % fighter1.damage_per_attack == 0:
#                 return first_attacker
#             return fighter2.name
#         return fighter1.name
#     return fighter1.name
#
# s = declare_winner(Fighter("Lui", 334, 59), Fighter("Bostin", 341, 64), "Lui")


# class PokeScan:
#     def __init__(self, name, level, pkmntype):
#         self.name = name
#         self.level = level
#         self.pkmntype = pkmntype
#
#     def info(self):
#         dictpokeman = {"water": "wet", "fire": "fiery", "grass": "grassy"}
#         desc = "weak" if self.level <=20 else "fair" if self.level<=50 else "strong"
#         return '{}, a {} and {} Pokemon.'.format(self.name, dictpokeman.get(self.pkmntype), desc)


# from string import ascii_letters, digits, ascii_uppercase
# class MyClass:
#     pass
#
# def class_name_changer(cls, new_name):
#     print(new_name)
#     if new_name[0] not in ascii_uppercase:
#         raise ValueError
#     for i in new_name:
#         if i not in ascii_letters and i!='*' and i not in digits:
#             raise ValueError
#     cls.__name__ = new_name
#
# myObject = MyClass();
# s1 = MyClass.__name__
#
# class_name_changer(MyClass, "UsefulClass");
# s2 = MyClass.__name__
#
# print(s1, s2)

# def class_name_changer2(cls, new_name):
#     assert new_name[0].isupper() and new_name.isalnum()
#     cls.__name__ = new_name


# class ReNameAbleClass(object):
#     @classmethod
#     def change_class_name(cls, new_name):
#         assert new_name[0].isupper() and new_name.isalnum()
#         cls.__name__ = new_name
#
#     @classmethod
#     def __str__(cls):
#         return "Class name is: {}".format(cls.__name__)
#
# class MyClass(ReNameAbleClass):
#     pass
#
# myObject = MyClass()
# print(str(myObject))
#
# myObject.change_class_name("UsefulClass")
# print(str(myObject))


# def get_even_numbers(arr):
#     return list(filter(lambda x: x%2 == 0, arr))

# def highest_value(a, b):
#     return a if sum(ord(i) for i in a) >= sum(ord(i) for i in b) else b
#
# def draw_a_cross(n):
#     if n<3:
#         return "Not possible to draw cross for grids less than 3x3!"
#     if n%2 == 0 :
#         return "Centered cross not possible!"
#     x = []
#     for i in range(n):
#         if i< n//2:
#             left = ' '*i + 'x' + ' '*int((n-2-2*i)//2)
#             mid = ' '
#             right = left[::-1]
#             l = left + mid + right
#         elif i == n//2:
#             l = ' '*i + 'x' + ' '*i
#         else:
#             left = ' '*(n-1-i) + 'x' + ' '*int(abs(n-2*i)//2)
#             mid = ' '
#             right = left[::-1]
#             l = left + mid + right
#         print(l)
#         x.append(l+'\n')
#     x =''.join(x)
#     return x[:-1]
#
# def draw_a_cross2(n: int) -> str:
#     if n < 3:
#         return "Not possible to draw cross for grids less than 3x3!"
#
#     if n % 2 == 0:
#         return "Centered cross not possible!"
#
#     lines = [('x' + ' ' * i + 'x').center(n) for i in range(n - 2, 0, -2)]
#     return '\n'.join(lines + ['x'.center(n)] + lines[::-1])

# def convert(number):
#     s = ''
#     for i in zip(number[::2],number[1::2]):
#         s += chr(int(i[0] + i[1]))
#     return ''.join(s)
#
# def convert2(number):
#     return ''.join(chr(int(number[a:a + 2])) for a in range(0, len(number), 2))


# def pyramid(n):
#     return '\n'.join("/{}\\".format(" _"[r==n-1] * r*2).center(2*n).rstrip() for r in range(n)) + '\n'


def solution(str):
    return chr(sum(ord(i.upper()) for i in str) // len(str))

s = solution("abc")



print(s)

