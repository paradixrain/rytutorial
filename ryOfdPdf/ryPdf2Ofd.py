import fitz
import pymupdf


# def ofd_to_pdf(file_path):
#     doc = fitz.open(file_path)
#     pdf_bytes = doc.convert_to_pdf()
#     with open(file_path[:-4] + ".pdf", "wb") as f:
#         f.write(pdf_bytes)
#
#
# def pdf_to_ofd(file_path):
#     doc = fitz.open(file_path)
#     ofd_bytes = doc.
#     with open(file_path[:-4] + ".ofd", "wb") as f:
#         f.write(ofd_bytes)


def replace_text_in_pdf(input_pdf_path, output_pdf_path, old_text, new_text):
    # 打开PDF文件
    doc = fitz.open(input_pdf_path)

    page = doc[0]
    p = pymupdf.Point(306.97, 540.26)
    text = "2"
    page.insert_text(p, text, fontname="helv", fontsize=9, rotate=0)


    # 保存修改后的PDF文件
    doc.save(output_pdf_path)
    doc.close()


if __name__ == '__main__':
    input_pdf = r'C:\Users\linyu\Downloads\滴滴出行行程报销单B.pdf'
    output_pdf = r'C:\Users\linyu\Downloads\滴滴出行行程报销单B_modify.pdf'
    old_text = r"共2笔行程"
    new_text = r"共1笔行程"
    replace_text_in_pdf(input_pdf, output_pdf, old_text, new_text)
