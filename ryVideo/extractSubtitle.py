from moviepy.editor import VideoFileClip
from google.cloud import speech_v1p1beta1 as speech

# Extract audio from the video
video_path = r'C:\迅雷下载\video\202405191214_331760AA.MP4'
audio_path = r'C:\迅雷下载\video\202405191214_331760AA.wav'

video = VideoFileClip(video_path)
audio = video.audio
audio.write_audiofile(audio_path)

# Transcribe the audio using Google Cloud Speech-to-Text API
client = speech.SpeechClient()
config = {
    "language_code": "zh-CN"
}

with open(audio_path, 'rb') as audio_file:
    content = audio_file.read()

audio = speech.RecognitionAudio(content=content)
response = client.recognize(config=config, audio=audio)

transcript = ''
for result in response.results:
    transcript += result.alternatives[0].transcript

# Save the transcribed text into a text file
text_file_path = 'output_text.txt'
with open(text_file_path, 'w') as text_file:
    text_file.write(transcript)

print('Transcription complete. Text saved to', text_file_path)