import time

from faker import Faker
import csv

# fake = Faker()
fake = Faker('zh_CN')

# Generate 1 million rows of data
num_rows = 10000
duplicate_percentage = 10  # 10% duplication rate

starttime = time.time()

# Specify the file name
file_name = "test_data1.csv"

# Generate data and write to the file
with open(file_name, 'w', newline='', encoding="gbk") as file:
    writer = csv.writer(file)
    writer.writerow(["公司名称", "地址", "电话"])

    for _ in range(num_rows):
        company_name = fake.company()
        company_address = fake.address()
        company_phone = fake.phone_number()

        writer.writerow([company_name, company_address, company_phone])

print(f"Test data file with {num_rows} rows generated successfully: {file_name}")

print(time.time()-starttime)
