import time

import pandas as pd

# Read the data from test_data.csv
data = pd.read_csv('test_data1.csv', encoding="gbk")
starttime = time.time()

# Remove duplicate company names
unique_company_names = data['公司名称'].drop_duplicates()

# Save the unique company names to company.csv
unique_company_names.to_csv('company.csv', index=False, header=['Company Name'])

print("Unique company names saved to company.csv")

print(time.time()-starttime)
