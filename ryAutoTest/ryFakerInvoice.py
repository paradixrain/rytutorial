import csv
import random

# 设置生成数据的数量
num_records = 5000  # 你可以根据需要修改这个数字

# 创建CSV文件并写入数据
with open('data.csv', mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['金额', '税率'])  # 写入表头

    for _ in range(num_records):
        amount = round(random.uniform(10, 30000), 2)  # 生成10到1000之间的随机金额
        tax_rate = random.choice([0.06, 0.13])  # 随机选择税率
        writer.writerow([amount, tax_rate])  # 写入金额和税率

print('CSV文件已生成。')