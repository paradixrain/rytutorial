import pandas as pd
import os  # 导入os模块以处理文件路径


def fill_in_md(template, row):
    # 替换模板中的占位符
    for key, value in row.items():
        template = template.replace(f'{{{key}}}', str(value) if value is not None else '')

        # 处理附件列列
        if '附件' in row:
            try:
                image_urls = row['附件'].split('\n')
                attachmentURL = '\n'.join([f'![image]({url})' for url in image_urls])
                template = template.replace('{attachmentURL}', attachmentURL)
            except AttributeError:
                print("转换报错", row['附件'])

    return template

def generateSingleMRD(file_path, filter_version):
    # 使用pandas读取Excel文件
    df = pd.read_excel(file_path)

    # 筛选出"修复版本"列等于filter_version的行，并且"需求ID（林宇维护）"不为空
    filtered_rows = df[(df['修复版本(研发维护)'] == filter_version) &
                       (df['需求ID（林宇维护）'].notna()) &
                       (df['状态'] != '已驳回')]

    for index, row in filtered_rows.iterrows():
        row_dict = {
            '菜单名称': row['菜单名称'],
            '需求编号': row['需求ID（林宇维护）'],
            '简略描述': row['简略描述'],
            '需求类别': row['类型'],
            '详细描述': row['详细描述'],
            '研发答复': row['修改说明(研发填写)'],
            '附件': row['附件']
        }

        print(row_dict)


        # 读取模板文件
        with open('单个需求模板.md', 'r', encoding='utf-8') as template_file:
            template = template_file.read()
            filled_md = fill_in_md(template, row_dict)

            # 使用需求编号作为文件名保存新的md文件
            output_file_name = f'{row_dict["需求编号"]}{row_dict["简略描述"]}.md'
            with open(f'{filter_version}\\{output_file_name}', 'w', encoding='utf-8') as output_file:
                output_file.write(filled_md)  # 保存填充后的内容
            print(f'已保存: {output_file_name}')  # 打印保存的文件名


def merge_single_mrd(module_template, single_mrd_dir, dicts, output_file_name):
    '''
    将每个独立需求文档合并，需要替换一次序号
    '''
    with open(output_file_name, 'w', encoding='utf-8') as output_file:
        with open(module_template, 'r', encoding='utf-8') as module_file:
            module_file_content = module_file.read()
            for key, value in dicts.items():
                module_file_content = module_file_content.replace(f'{{{key}}}', str(value) if value is not None else '')
                print(f'{{{key}}}', str(value))
            output_file.write(module_file_content)

        serial_number = 1
        for single_mrd in os.listdir(single_mrd_dir):
            with open(f"{single_mrd_dir}\\{single_mrd}", 'r', encoding='utf-8') as singlemrd_file:
                if single_mrd.endswith('.md'):
                    single_mrd_content = singlemrd_file.read()

                    # 序号
                    single_mrd_content = single_mrd_content.replace("{序号}", f"{serial_number}.")
                    print("文件内容", single_mrd_content)
                    serial_number += 1
                    output_file.write(single_mrd_content)  # 保存填充后的内容
        print(f'已保存需求说明书: {output_file_name}')  # 打印保存的文件名


# if __name__ == "__main__":
#     excel_file_name = '项目问题管理 增值税4.0-项目反馈跟踪记录.xlsx'
#     filter_version = 'V4.3.1'
#     if not os.path.exists(filter_version):
#         os.makedirs(filter_version)
#     generateSingleMRD(excel_file_name, filter_version)
#     dicts={"version": filter_version,
#            "currentDate": "2025-01-13",
#            "pubishDate": "2025-01-20"}
#     outputfile = f"需求说明书{filter_version}.md"
#     merge_single_mrd("需求说明书模板.md", filter_version, dicts, outputfile)

if __name__ == "__main__":
    excel_file_name = '项目问题管理 增值税4.0-项目反馈跟踪记录.xlsx'
    filter_versions = ['V4.3.1', 'V4.4.0', 'V4.4.1', 'V4.4.2', 'V4.4.5', 'V4.5.0']
    publish_dates = {
        'V4.3.1': "2025-01-20",
        'V4.4.0': "2025-01-21",
        'V4.4.1': "2025-01-22",
        'V4.4.2': "2025-01-23",
        'V4.4.5': "2025-01-24",
        'V4.5.0': "2025-01-25"
    }

    for filter_version in filter_versions:
        if not os.path.exists(filter_version):
            os.makedirs(filter_version)
        generateSingleMRD(excel_file_name, filter_version)

        dicts = {
            "version": filter_version,
            "currentDate": "2025-01-13",
            "pubishDate": publish_dates[filter_version]  # 根据filter_version获取对应的publishDate
        }
        outputfile = f"需求说明书{filter_version}.md"
        merge_single_mrd("需求说明书模板.md", filter_version, dicts, outputfile)