import logging

import requests
import os
import datetime
import time
import pandas
import hashlib
import json
import multiprocessing
import re
import redis

"""
对票据核验程序进行改造，只提取需要的列，通过pickCol参数来选择
将存储到redis的数据进行读取
"""

logging.getLogger().setLevel(logging.DEBUG)



BASE_URL = r"http://210.12.38.194:9080/openapi/open/api?"
METHOD = "winLending.finance.assets.ocrInvoiceCheckv2"
APPKEY = "1001260"
VERSION = "3.0"
FORMAT = "json"
TYPE = "SYNC"
USERNAME = "46583014"
PASSWORD = "qwer1234"
CLIENT_ID = "1001260"
CLIENT_SECRET = "78144976-cd59-490e-abc9-104810dd7232"
TAG = "2024年6月"
TOKEN = "4c2564e7-9cd6-4e8a-a529-ce86fdb42339"

PATHS = [r"C:\Users\Ryan\git\rytutorial\working",
         r"D:\work\workspace\git\rytutorial\working",
         r"D:\Pycharm\rytutorial\working"]

WORKING_DIR = r"C:\Users\Ryan\git\rytutorial\working"
if os.path.exists(PATHS[0]):
    WORKING_DIR = PATHS[0]
elif os.path.exists(PATHS[1]):
    WORKING_DIR = PATHS[1]
else:
    WORKING_DIR = PATHS[2]

DATA_DIR = f"{WORKING_DIR}\\{TAG}"
if not os.path.exists(DATA_DIR):
    os.makedirs(DATA_DIR)

CONCURRENCY = 4

PICK_COLS = ['subMessage', 'totalAmount', 'purchaserName', 'salesName', 'salesAddressPhone', 'state']


# 从JSON字符串中提取特定值的函数
def extract_value_from_json(json_str, key):
    pattern = rf'"{key}":\s*"([^"]*)"'
    match = re.search(pattern, json_str)
    if match:
        return match.group(1)
    else:
        return "NULL"


def main():
    # redis connection， 公司
    ryRedis = redis.Redis(
        host='redis-pool.test.baiwang-inner.com',
        port=6379,
        password='Baiwang*58')

    # redis connection， 本机docker
    # ryRedis = redis.Redis(
    #     host='localhost',
    #     port=6379,
    #     password='redis2024')

    filename = r"线下批量查验清单（6.19）.xlsx"

    dtype = {'发票代码': str,
             '发票号码': str,
             '校验码': str}

    FPZT = {"0": "正常", "2": "已作废", "3": "已红冲"}
    STATE_DICT = str.maketrans(FPZT)

    # 读取excel中所有key，会写到csv中，为了保证回写效率，只使用单进程
    # 数电
    df = pandas.read_excel(filename, sheet_name="数电", usecols="A:K", dtype=dtype, header=0)
    for index, row in df.iterrows():
        json_data = ryRedis.get(f"SD{row[0]}")
        json_data = json_data.decode()
        print(index, f"SD{row[0]}")
        logging.info(json_data)
        df.at[index, '无法获取发票记录原因'] = extract_value_from_json(json_data, "subMessage")
        df.at[index, '价税合计金额'] = extract_value_from_json(json_data, "amountTax")
        df.at[index, '购买方名称'] = extract_value_from_json(json_data, "purchaserName")
        df.at[index, '销售方名称'] = extract_value_from_json(json_data, "salesName")
        df.at[index, '销售方地址电话'] = extract_value_from_json(json_data, "salesAddressPhone")
        df.at[index, '发票状态'] = extract_value_from_json(json_data, "state").translate(STATE_DICT)

    # 将 df 保存，输出为excel
    output_file = f"{WORKING_DIR}\\{TAG}_SD.xlsx"
    df.to_excel(output_file, sheet_name="数电")

    # 普票
    df = pandas.read_excel(filename, sheet_name="普票", usecols="A:K", dtype=dtype, header=0)
    for index, row in df.iterrows():
        json_data = ryRedis.get(f"PP{row[0]}")
        json_data = json_data.decode()
        print(index, f"PP{row[0]}")
        logging.info(json_data)
        df.at[index, '无法获取发票记录原因'] = extract_value_from_json(json_data, "subMessage")
        df.at[index, '价税合计金额'] = extract_value_from_json(json_data, "amountTax")
        df.at[index, '购买方名称'] = extract_value_from_json(json_data, "purchaserName")
        df.at[index, '销售方名称'] = extract_value_from_json(json_data, "salesName")
        df.at[index, '销售方地址电话'] = extract_value_from_json(json_data, "salesAddressPhone")
        df.at[index, '发票状态'] = extract_value_from_json(json_data, "state").translate(STATE_DICT)

    # 将 df 保存，输出为excel
    output_file = F"{WORKING_DIR}\\{TAG}_PP.xlsx"
    df.to_excel(output_file, sheet_name="普票")


if __name__ == '__main__':
    print('start at:', datetime.datetime.now())
    main()
    print('end at:', datetime.datetime.now())
