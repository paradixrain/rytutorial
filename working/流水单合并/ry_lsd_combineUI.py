# coding = utf-8

# @filename: ry_lsd_combineUI.py
# @desc: 
# @author: PC\RyanLin
# @version: Python 3.12
# @date: 2024/9/26

# coding = utf-8
import pandas as pd
import tkinter as tk
from tkinter import filedialog, messagebox


def merge(df, diff, limit):
    """
    将orgin_df 进行合并，输出到target_df并返回

    参数:
    df: DataFrame, 包含至少包含'id'和'金额'两列。
    diff: 合并行误差范围
    limit: 合并行金额
    返回:
    target: DataFrame
    """

    # debug
    if df.empty:
        print("DataFrame is empty")

    # 将target_df排序，使用降序，尽可能匹配大值
    df = df.sort_values(by='金额', ascending=False)

    # 定义出参，初始为空
    target_df = pd.DataFrame(columns=df.columns)

    # 定义一个对象存储df的第一列id
    id_array = df['id'].tolist()

    # 使用pop方式遍历id_array
    while id_array:
        # 获取数组第一个元素对应的id——也就是最大值
        current_id = id_array.pop(0)
        print("处理中…", current_id)

        # 获取current每个值
        je = df[df['id'] == current_id]['金额'].iloc[0]
        sl = df[df['id'] == current_id]['税率'].iloc[0]
        se = df[df['id'] == current_id]['税额'].iloc[0]
        ysse = df[df['id'] == current_id]['原始税额'].iloc[0]
        ce = df[df['id'] == current_id]['差额'].iloc[0]

        len_id_array = len(id_array)

        # 找到最接近 limit - je 的行号
        # 设置条件
        condition = df['金额'] <= limit - je

        # 获取所有满足条件的行的索引
        indexes = df.index[condition].tolist()

        for loc in indexes:
            # 获取current loc的每个值
            line = df.iloc[loc]
            temp_id = line['id']
            temp_je = line['金额']
            temp_se = line['税额']
            temp_ysse = line['原始税额']
            temp_ce = line['差额']
            if temp_je + je < limit and temp_ce + ce < diff and temp_id in id_array:
                # 满足合并条件
                new_id = f"{current_id}_{temp_id}"
                print("满足合并条件", new_id)
                je += temp_je
                se += temp_se
                ysse += temp_ysse
                ce += temp_ce
                target_df.loc[len(target_df)] = [new_id, je, sl, se, ysse, ce]
                id_array.remove(temp_id)
                break
        else:
            # 没有符合条件的，则入库当前行
            target_df.loc[len(target_df)] = [current_id, je, sl, se, ysse, ce]

    return target_df


def loop_merge(df, diff, limit):
    # 循环合并，直到行数不减少
    while True:
        origin_len = len(df)
        df = merge(df, diff, limit)
        target_len = len(df)
        print("loop, origin, target：", origin_len, target_len)

        if origin_len == target_len:
            break
    return df


def process_tax_data(df):
    """
    读取Excel文件，计算并添加新的列：计算所得税额和差异额。
    参数:
    df: 原始df。
    返回:
    DataFrame, 包含新增列的DataFrame。
    """

    # 将 id 列设置为行索引，排序
    df = df.sort_values(by='金额')

    # 计算“计算所得税额”
    df['原始税额'] = df['金额'] * df['税率']

    # 计算“差异额”
    df['差额'] = df['税额'] - df['原始税额']

    # 将新列保留四位小数
    df['原始税额'] = df['原始税额'].round(4)
    df['差额'] = df['差额'].round(4)

    return df

def run_merge(max_diff, max_limit, file_path):
    try:
        dtype = {'id': str}
        df = pd.read_excel(file_path, dtype=dtype)
        df = process_tax_data(df)
        print("process_tax_data 执行结束")
        df = loop_merge(df, max_diff, max_limit)
        # 保存合并后的数据到新的Excel文件
        df.to_excel('combine_data.xlsx', index=False)
        messagebox.showinfo('成功', '数据合并完成，已保存到 combine_data.xlsx')
    except Exception as e:
        messagebox.showerror('错误', str(e))

def select_file():
    file_path = filedialog.askopenfilename(filetypes=[("Excel files", "*.xlsx;*.xls")])
    if file_path:
        file_entry.delete(0, tk.END)
        file_entry.insert(0, file_path)

def start_process():
    try:
        max_diff = float(diff_entry.get())
        max_limit = float(limit_entry.get())
        file_path = file_entry.get()
        run_merge(max_diff, max_limit, file_path)
    except ValueError:
        messagebox.showerror('输入错误', '请确保输入的最大差异和最大限额为数字')

# 创建主窗口
root = tk.Tk()
root.title("数据合并工具")

# 文件选择部分
file_label = tk.Label(root, text="选择流水单Excel文件:")
file_label.pack(pady=5)

file_entry = tk.Entry(root, width=50)
file_entry.pack(pady=5)

file_button = tk.Button(root, text="浏览", command=select_file)
file_button.pack(pady=5)

# max_diff 输入部分
diff_label = tk.Label(root, text="输入单张发票允许最大尾差:")
diff_label.pack(pady=5)

diff_entry = tk.Entry(root)
diff_entry.insert(0, "0.005")  # 默认值
diff_entry.pack(pady=5)

# max_limit 输入部分
limit_label = tk.Label(root, text="输入单张发票限额:")
limit_label.pack(pady=5)

limit_entry = tk.Entry(root)
limit_entry.pack(pady=5)

# 开始处理按钮
start_button = tk.Button(root, text="开始合并", command=start_process)
start_button.pack(pady=20)

# 启动主循环
root.mainloop()