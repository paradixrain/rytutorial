import pandas as pd

# 读取Excel文件
df = pd.read_excel('data1.xlsx', sheet_name='data')

# 按“金额”降序排列
df_sorted = df.sort_values(by='金额', ascending=False)
df_sorted['分组序号'] = ''

# 设置SINGLE_LIMIT
SINGLE_LIMIT = 40000

# 新建一个空的DataFrame，包含所有原始列
new_df = pd.DataFrame(columns=df_sorted.columns)

matched_list = []
# 使用双层循环处理数据
for i in range(len(df_sorted)):
    print(i)
    if i in matched_list:
        print("pass")
        continue
    x = df_sorted.iloc[i]['与原始税额的差额']
    current_amount = df_sorted.iloc[i]['金额']

    # 开始内层循环
    for j in range(i + 1, len(df_sorted)):
        if j in matched_list:
            continue
        y = df_sorted.iloc[j]['与原始税额的差额']
        next_amount = df_sorted.iloc[j]['金额']

        # 检查条件：差额之和为0，且金额之和小于SINGLE_LIMIT
        if x + y == 0 and current_amount + next_amount < SINGLE_LIMIT:
            # 将两行相加（除了“税率”）并追加到new_df
            new_row = df_sorted.iloc[i].to_dict()
            new_row['分组序号'] = f"{i}_{j}"
            new_row['金额'] += df_sorted.iloc[j]['金额']
            new_row['税额'] += df_sorted.iloc[j]['税额']
            new_row['原始税额'] += df_sorted.iloc[j]['原始税额']
            new_row['与原始税额的差额'] += df_sorted.iloc[j]['与原始税额的差额']
            new_df = pd.concat([new_df, pd.DataFrame([new_row])], ignore_index=True)
            matched_list.append(j)
            break  # 找到匹配项后退出内层循环
    else:
        # 如果没有找到匹配项，则将当前行追加到new_df
        new_row = df_sorted.iloc[i].to_dict()
        new_row['分组序号'] = f"{i}"
        new_df = pd.concat([new_df, pd.DataFrame([new_row])], ignore_index=True)

# 将处理后的DataFrame保存到新的Excel文件
new_df.to_excel('updated_data_with_group_ids.xlsx', index=False)
