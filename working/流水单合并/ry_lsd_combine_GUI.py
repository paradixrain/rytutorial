# coding = utf-8
import time
import pandas as pd
import tkinter as tk
from tkinter import filedialog, messagebox

# Function to merge DataFrame based on the provided rules
def merge(df, diff, limit):
    if df.empty:
        print("DataFrame is empty")
    df = df.sort_values(by='金额', ascending=False)
    target_df = pd.DataFrame(columns=df.columns)
    id_array = df['id'].tolist()

    while id_array:
        current_id = id_array.pop(0)
        print("Processing...", current_id)
        je = df[df['id'] == current_id]['金额'].iloc[0]
        sl = df[df['id'] == current_id]['税率'].iloc[0]
        se = df[df['id'] == current_id]['税额'].iloc[0]
        ysse = df[df['id'] == current_id]['原始税额'].iloc[0]
        ce = df[df['id'] == current_id]['差额'].iloc[0]

        len_id_array = len(id_array)
        for loc in range(len_id_array):
            temp_id = id_array[loc]
            temp_je = df[df['id'] == temp_id]['金额'].iloc[0]
            temp_se = df[df['id'] == temp_id]['税额'].iloc[0]
            temp_ysse = df[df['id'] == temp_id]['原始税额'].iloc[0]
            temp_ce = df[df['id'] == temp_id]['差额'].iloc[0]

            if temp_je + je < limit and temp_ce + ce < diff:
                new_id = f"{current_id}_{temp_id}"
                print("Merge condition met", new_id)
                je += temp_je
                se += temp_se
                ysse += temp_ysse
                ce += temp_ce
                print(target_df)
                target_df.loc[len(target_df)] = [new_id, je, sl, se, ysse, ce]
                id_array.pop(loc)
                break
        else:
            target_df.loc[len(target_df)] = [current_id, je, sl, se, ysse, ce]
    return target_df

def loop_merge(df, diff, limit):
    while True:
        origin_len = len(df)
        df = merge(df, diff, limit)
        target_len = len(df)
        print("Loop, origin, target:", origin_len, target_len)
        if origin_len == target_len:
            break
    return df

def process_tax_data(df):
    df = df.sort_values(by='金额')
    df['原始税额'] = df['金额'] * df['税率']
    df['差额'] = df['税额'] - df['原始税额']
    df['原始税额'] = df['原始税额'].round(4)
    df['差额'] = df['差额'].round(4)
    return df

def run_merge():
    file_path = filedialog.askopenfilename(title="Select Excel File", filetypes=[("Excel files", "*.xlsx")])
    if not file_path:
        return

    try:
        max_diff = float(diff_entry.get())
        max_limit = float(limit_entry.get())
    except ValueError:
        messagebox.showerror("Input Error", "Please enter valid numbers for max_diff and max_limit.")
        return

    dtype = {'id': str}
    df = pd.read_excel(file_path, dtype=dtype)
    df = process_tax_data(df)
    print("process_tax_data executed")
    df = loop_merge(df, max_diff, max_limit)

    output_file = 'combine_data.xlsx'
    df.to_excel(output_file, index=False)
    messagebox.showinfo("Success", f'Data merged successfully and saved to {output_file}')

def create_gui():
    global diff_entry, limit_entry

    root = tk.Tk()
    root.title("Data Merger")

    tk.Label(root, text="Max Difference:").grid(row=0, column=0, padx=10, pady=10)
    diff_entry = tk.Entry(root)
    diff_entry.grid(row=0, column=1, padx=10, pady=10)

    tk.Label(root, text="Max Limit:").grid(row=1, column=0, padx=10, pady=10)
    limit_entry = tk.Entry(root)
    limit_entry.grid(row=1, column=1, padx=10, pady=10)

    merge_button = tk.Button(root, text="Select Excel File and Merge", command=run_merge)
    merge_button.grid(row=2, columnspan=2, padx=10, pady=10)

    root.mainloop()

if __name__ == '__main__':
    create_gui()