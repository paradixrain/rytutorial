# coding = utf-8
import time

# @filename: ry_lsd_combine.py
# @desc: 
# @author: PC\RyanLin
# @version: Python 3.12
# @date: 2024/9/26

import pandas as pd


# 设置合并规则的参数
max_diff = 0.005
max_limit = 40000  # 假设的限额，您可以根据需要调整


def merge(df, diff, limit):
    """
    将orgin_df 进行合并，输出到target_df并返回

    参数:
    df: DataFrame, 包含至少包含'id'和'金额'两列。
    diff: 合并行误差范围
    limit: 合并行金额
    返回:
    target: DataFrame
    """

    # debug
    if df.empty:
        print("DataFrame is empty")

    # 将target_df排序，使用降序，尽可能匹配大值
    df = df.sort_values(by='金额', ascending=False)

    # 定义出参，初始为空
    target_df = pd.DataFrame(columns=df.columns)

    # 定义一个对象存储df的第一列id
    id_array = df['id'].tolist()

    # 使用pop方式遍历id_array
    while id_array:
        # 获取数组第一个元素对应的id——也就是最大值
        current_id = id_array.pop(0)
        print("处理中…", current_id)

        # 获取current每个值
        je = df[df['id'] == current_id]['金额'].iloc[0]
        sl = df[df['id'] == current_id]['税率'].iloc[0]
        se = df[df['id'] == current_id]['税额'].iloc[0]
        ysse = df[df['id'] == current_id]['原始税额'].iloc[0]
        ce = df[df['id'] == current_id]['差额'].iloc[0]

        len_id_array = len(id_array)

        for loc in range(0, len_id_array):
            # 获取current每个值
            temp_id = id_array[loc]
            temp_je = df[df['id'] == temp_id]['金额'].iloc[0]
            temp_se = df[df['id'] == temp_id]['税额'].iloc[0]
            temp_ysse = df[df['id'] == temp_id]['原始税额'].iloc[0]
            temp_ce = df[df['id'] == temp_id]['差额'].iloc[0]
            if temp_je + je < limit and temp_ce + ce < diff:
                # 满足合并条件
                new_id = f"{current_id}_{temp_id}"
                print("满足合并条件", new_id)
                je += temp_je
                se += temp_se
                ysse += temp_ysse
                ce += temp_ce
                target_df.loc[len(target_df)] = [new_id, je, sl, se, ysse, ce]
                id_array.pop(loc)
                break
        else:
            # 没有符合条件的，则入库当前行
            target_df.loc[len(target_df)] = [current_id, je, sl, se, ysse, ce]

    return target_df


def loop_merge(df, diff, limit):
    # 循环合并，直到行数不减少
    while True:
        origin_len = len(df)
        df = merge(df, diff, limit)
        target_len = len(df)
        print("loop, origin, target：", origin_len, target_len)

        if origin_len == target_len:
            break
    return df


def process_tax_data(df):
    """
    读取Excel文件，计算并添加新的列：计算所得税额和差异额。
    参数:
    df: 原始df。
    返回:
    DataFrame, 包含新增列的DataFrame。
    """

    # 将 id 列设置为行索引，排序
    df = df.sort_values(by='金额')

    # 计算“计算所得税额”
    df['原始税额'] = df['金额'] * df['税率']

    # 计算“差异额”
    df['差额'] = df['税额'] - df['原始税额']

    # 将新列保留四位小数
    df['原始税额'] = df['原始税额'].round(4)
    df['差额'] = df['差额'].round(4)

    return df


def main():
    # 读取excel文件，要求文件中只有相同税率， 第一列是id或者流水单（唯一），

    # 加载数据
    dtype = {'id': str}
    file_path = 'data1.xlsx'
    df = pd.read_excel(file_path, dtype=dtype)
    df = process_tax_data(df)
    print("process_tax_data 执行结束")

    df = loop_merge(df, max_diff, max_limit)
    # 保存合并后的数据到新的Excel文件
    df.to_excel('combine_data.xlsx', index=False)
    print('数据合并完成，已保存到 combine_data.xlsx')


if __name__ == '__main__':
    start_time = time.time()
    print(start_time)
    main()
    end_time = time.time()
    print(end_time - start_time)
