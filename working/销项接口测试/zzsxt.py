import sys

import requests

# 设置请求的URL和请求体
url = "http://172.28.30.86/kpxtService"
headers = {
    "Content-Type": "text/xml"
}

prompt = "EDCX"
# Check if a command-line argument is provided
if len(sys.argv) > 1:
    prompt = sys.argv[1]
else:
    pass  # Default EDCX if no argument is provided

INPUT = {
    "EDCX": r"./接口报文/FLGL.SD.EDCX.xml",
    "LSDXX": r"./接口报文/FPXT.SDCJ.LSDXX.xml",
    "HZQRDCX": r"./接口报文/FPXT.SD.HZQRDCX.xml",
    "SDKJ": r"./接口报文FPXT.SD.KJ.xml"
}

with open(INPUT.get(prompt), "r", encoding="utf-8") as file:
    payload = file.read().encode("utf-8")

# 发送POST请求
response = requests.post(url, data=payload, headers=headers)

# 检查响应
if response.status_code == 200:
    print("Request successful!")
    rtn_text = response.text
    rtn_text = rtn_text.replace("&lt;", "<").replace("&gt;", ">")
    print(rtn_text)
else:
    print("Request failed. Status code:", response.status_code)
