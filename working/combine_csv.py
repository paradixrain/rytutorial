import pandas as pd
import glob
import os
import csv


def merge_csv_files(input_dir, merged_csv_file):
    """Merges all CSV files in the given directory into a single CSV file.

    Args:
      input_dir: The directory containing the CSV files to be merged.
      merged_csv_file: output CSV file to be merged

    Returns:
      The path to the merged CSV file.
    """

    # Get the list of CSV files in the input directory.
    csv_files = []
    df = pd.DataFrame()

    for root, dirs, files in os.walk(input_dir):
        # Iterate over the files in the current folder
        for file in files:
            if file.endswith(".csv"):
                # Get the full path of the file
                file_path = os.path.join(root, file)
                # Append the file path to the list
                csv_files.append(file_path)
                df1 = pd.read_csv(file_path, encoding='utf-8')
                df = pd.concat([df, df1])

    df.to_csv(merged_csv_file)
    return merged_csv_file


if __name__ == "__main__":
    merge_csv_files(r"C:\Users\Ryan\Downloads\messages_package", r"\merged_data.csv")
