# coding = utf-8
import pathlib
# @filename: split_sql_file.py
# @desc: 把一个大的sql文件拆分成若干个sql文件，并区分ddl和dml，方便以后使用程序进行sql的ddl和dml的执行
# @author: PC\RyanLin
# @version: Python 3.12
# @date: 2024/8/5

import re
import os
import shutil


def split_sql_file(input_file, output_dir):
    # Create a directory to save the output files
    os.makedirs(output_dir, exist_ok=True)

    with open(input_file, 'r', encoding='utf-8') as file:
        sql_content = file.read()

    # Split the SQL content by semicolon
    statements = sql_content.split(';')

    # 每个sql写入
    for k, statement in enumerate(statements):
        # Remove leading and trailing whitespace
        statement = statement.strip()
        with open(f"{output_dir}\\{k:05}.sql", 'w', encoding='utf-8') as file:
            file.write(statement)


def combine_ddl_dml(input_dir, output_dir):
    """
    将sql语句分成ddl和dml两个大文件
    :param input_dir:
    :param output_dir:
    :return:
    """
    os.makedirs(output_dir, exist_ok=True)
    k = 0

    for filename in os.listdir(input_dir):
        k += 1
        file_path = os.path.join(input_dir, filename)
        with open(file_path, 'r', encoding='utf-8') as f_in:
            statement = f_in.read()

        print(f"{k}, {filename} processing")
        ddl_patterns = {
            "create_table": r'CREATE TABLE (\w+)',
            "create_table_quoted": r'CREATE TABLE "(\w+)',
            "alter_table": r'ALTER TABLE (\w+)',
            "alter_table_quoted": r'ALTER TABLE "(\w+)',
            "drop_table": r'DROP TABLE (\w+)',
            "comment_column": r'COMMENT ON COLUMN (\w+)',
            "comment_column_quoted": r'COMMENT ON COLUMN "(\w+)',
            "comment_table": r'COMMENT ON TABLE (\w+)',
            "comment_table_quoted": r'COMMENT ON TABLE "(\w+)',
            "index_table_unique": r'create unique index (\w+) on (\w+)',
            "index_table": r'create index (\w+) on (\w+)',
            "index_table_quoted": r'create index "(\w+)" on "(\w+)"',
            "index_drop": r'drop index (\w+)',
            "view": r'CREATE OR REPLACE VIEW (\w+)'
        }
        dml_patterns = {
            "insert_table": r'INSERT INTO',
            "delete_table": r'DELETE FROM',
            "update_table": r'UPDATE'
        }

        # Create a dictionary to store the matches
        ddl_matches = {key: re.search(pattern, statement, re.IGNORECASE) for key, pattern in ddl_patterns.items()}
        dml_matches = {key: re.search(pattern, statement, re.IGNORECASE) for key, pattern in dml_patterns.items()}

        output_file = 'unhandled.sql'  # Default value if no match is found

        # Check for ddl matches and assign table_name accordingly
        for key, match in ddl_matches.items():
            if match:
                output_file = f"{output_dir}/ddl.sql"
                break  # Exit the loop once a match is found

        # Check for dml matches and assign table_name accordingly
        for key, match in dml_matches.items():
            if match:
                output_file = f"{output_dir}/dml.sql"
                break  # Exit the loop once a match is found
        print(output_file)

        with open(output_file, 'a', encoding='utf-8') as f_out:
            f_out.write(statement)
            f_out.write(';\n\n')

    # 在dml文件后面追加 commit；
    dml_file = f"{output_dir}/dml.sql"
    if pathlib.Path(dml_file).exists():
        with open(dml_file, 'a', encoding='utf-8') as f_dml:
            f_dml.write('commit;\n')


def combine_sql_file(input_dir, output_dir):
    """读取input下所有sql文件，将对同一个表的操作，合并在一起"""
    os.makedirs(output_dir, exist_ok=True)
    k = 0
    table_set = set()
    for filename in os.listdir(input_dir):
        k += 1
        file_path = os.path.join(input_dir, filename)
        with open(file_path, 'r', encoding='utf-8') as f_in:
            statement = f_in.read()

        print(f"{k}, {filename} processing")
        patterns = {
            "create_table": r'CREATE TABLE (\w+)',
            "create_table_quoted": r'CREATE TABLE "(\w+)',
            "alter_table": r'ALTER TABLE (\w+)',
            "alter_table_quoted": r'ALTER TABLE "(\w+)',
            "insert_table": r'INSERT INTO (\w+)',
            "insert_table_quoted": r'INSERT INTO "(\w+)',
            "delete_table": r'DELETE FROM (\w+)',
            "update_table": r'UPDATE (\w+)',
            "drop_table": r'DROP TABLE (\w+)',
            "comment_column": r'COMMENT ON COLUMN (\w+)',
            "comment_column_quoted": r'COMMENT ON COLUMN "(\w+)',
            "comment_table": r'COMMENT ON TABLE (\w+)',
            "comment_table_quoted": r'COMMENT ON TABLE "(\w+)',
            "index_table_unique": r'create unique index (\w+) on (\w+)',
            "index_table": r'create index (\w+) on (\w+)',
            "index_table_quoted": r'create index "(\w+)" on "(\w+)"',
            "index_drop": r'drop index (\w+)',
            "view": r'CREATE OR REPLACE VIEW (\w+)',
        }

        # Create a dictionary to store the matches
        sql_matches = {key: re.search(pattern, statement, re.IGNORECASE) for key, pattern in patterns.items()}

        table_name = 'unhandled'  # Default value if no match is found

        # Check for matches and assign table_name accordingly
        for key, match in sql_matches.items():
            if match:
                if 'index_table' in key:  # For index patterns, we want the second capturing group
                    table_name = match.group(2)
                else:  # For all other patterns, we want the first capturing group
                    table_name = match.group(1)
                break  # Exit the loop once a match is found

        print(table_name)

        if table_name not in table_set and table_name != f'unhandled.sql':
            table_set.add(table_name)

        output_file = f'{output_dir}\\{table_name}.sql'
        with open(output_file, 'a', encoding='utf-8') as f_out:
            if pathlib.Path(output_file).exists():
                f_out.write(';\n\n')
            f_out.write(statement)

    # 删除output_dir文件夹
    shutil.rmtree(output_dir)


def clean_file(input_file, output_file):
    """ # 对输入sql先进行简单清理
        # 1。替换2个连续的空格为1个空格
        # “ON”，如果在行首，则删除到行尾"""
    with open(input_file, 'r', encoding='utf-8') as file:
        lines = file.readlines()

    merged_lines = []
    for line in lines:
        line = line.rstrip()  # Remove trailing whitespace
        if line.startswith("commit") or line.startswith("COMMIT"):
            continue
        elif line.startswith("ON "):
            if merged_lines:  # Ensure there is a previous line to merge with
                merged_lines[-1] += ' ' + line  # Append the "ON" line to the last line
        else:
            merged_lines.append(line.replace("  ", " "))  # Add the current line

    # Write the merged lines to the output file
    with open(output_file, 'w', encoding='utf-8') as file:
        for merged_line in merged_lines:
            file.write(merged_line + '\n')


def main():
    # input_file = r'2.1.6脚本/0.初始化脚本/01_sklq_oracle_gbk.sql'

    sql_list = [r'2.1.6脚本/0.初始化脚本/01_sklq_oracle_gbk.sql',
                r'2.1.6脚本/0.初始化脚本/02 全托增值税初始化脚本.sql',
                r'2.1.6脚本/0.初始化脚本/03 quantuo.sql',
                r'2.1.6脚本/1.数电脚本/2-ddl_for_update.sql',
                r'2.1.6脚本/1.数电脚本/3-dml_for_update.sql',
                r'2.1.6脚本/2.增值税数电脚本/2-ddl_for_update.sql',
                r'2.1.6脚本/2.增值税数电脚本/3-dml_for_update.sql',
                r'2.1.6脚本/3.增值税2.1.1脚本/2-ddl_for_update.sql',
                r'2.1.6脚本/3.增值税2.1.1脚本/2-ddl_for_update - 版式.sql',
                r'2.1.6脚本/3.增值税2.1.1脚本/3-dml_for_update.sql',
                r'2.1.6脚本/3.增值税2.1.1脚本/3-dml_for_update -版式.sql',
                r'2.1.6脚本/3.增值税2.1.1脚本/5-重建税控定时任务脚本(本地部署版式时需要执行).sql'
                ]

    for input_file in sql_list:
        path, filename = os.path.split(input_file)
        clean_input_file = os.path.join(path, f"clean_{filename}")
        clean_file(input_file, clean_input_file)
        dir_name = filename.rstrip(".sql")
        sqlit_output_dir = os.path.join(path, f"{dir_name}/output_dir")
        combine_output_dir = os.path.join(path, f"{dir_name}/combine_dir")

        # 按分號拆分；然後再按表合併
        split_sql_file(clean_input_file, sqlit_output_dir)
        # combine_sql_file(sqlit_output_dir, combine_output_dir)
        combine_ddl_dml(sqlit_output_dir, combine_output_dir)
        # 刪除clean文件
        os.remove(clean_input_file)


if __name__ == "__main__":
    main()
