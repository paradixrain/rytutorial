import hashlib


def calculate_md5(file_path):
    md5 = hashlib.md5()
    with open(file_path, "rb") as file:
        chunk = file.read()
        md5.update(chunk)
    return md5.hexdigest()


if __name__ == "__main__":
    file = r"C:\Users\linyu\Downloads\增值税销项v2.1.5-hotfix20240712\增值税销项2.1.5-hotfix20240712\KPServer.war"
    md5_value = calculate_md5(file)
    print(md5_value)
