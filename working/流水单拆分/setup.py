import sys

from cx_Freeze import setup, Executable

# 定义应用程序的元数据
base = None

# Windows平台的选项
if sys.platform == "win32":
    base = "Win32GUI"

# 创建 Executable对象列表
executables = [
    Executable("ry_lsd_split_GUI.py",
               base=base,
               target_name=r"ry_lsd_split_GUI.exe")
]

# 运行 setup 函数
setup(
    name="lsdsplit",
    version="0.1",
    build_exe=r"C:\Users\linyu\PycharmProjects\rytutorial\working",
    description="根据流水单最大限额进行拆分",
    executables=executables
)