# coding = utf-8

# @filename: split_test.py
# @desc: 给你一个单价d（6位小数），一个数量n（6位小数），一个金额j（2位小数）；
# 如何找到一个小于j的最大值x（2位小数），且他能够被d整除，且商不超过6位小数
# @author: PC\RyanLin
# @version: Python 3.12
# @date: 2024/9/20
from decimal import Decimal, ROUND_DOWN


def find_max_divisible_value_optimized(d, j):
    # 计算可能的最大商
    max_quotient = j / d
    max_quotient = max_quotient.quantize(Decimal('0.000001'), rounding=ROUND_DOWN)
    print(max_quotient)

    # 从可能的最大商开始向下寻找
    quotient = max_quotient
    while quotient > max_quotient/2:
        print(quotient)
        candidate = quotient * d
        if candidate <= j:
            return float(candidate)  # 找到符合条件的值
        quotient = quotient - 0.01

    return None  # 如果没有找到符合条件的值，返回None


# 示例
d = Decimal('3060701.98')  # 单价，6位小数
j = Decimal('100000.00')  # 金额，2位小数

result = find_max_divisible_value_optimized(d, j)
if result:
    print(f"找到的最大值 x: {result}")
else:
    print("没有找到符合条件的值")
