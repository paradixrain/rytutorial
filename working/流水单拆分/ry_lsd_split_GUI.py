# coding = utf-8
from datetime import datetime
# @filename: ry_lsd_split_GUI.py
# @desc:
# @author: PC\RyanLin
# @version: Python 3.12
# @date: 2024/9/21
import pandas as pd
from decimal import Decimal, ROUND_HALF_UP
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import math

def split_row(data_row, limit, max_error, update_status):
    unit_price = Decimal(data_row['dj-单价']).quantize(Decimal('1.000000'), rounding=ROUND_HALF_UP)
    quantity = Decimal(data_row['spsl-商品数量']).quantize(Decimal('1.000000'), rounding=ROUND_HALF_UP)
    je = Decimal(data_row['je-金额']).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
    se = Decimal(data_row['se-税额']).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
    calc_je = unit_price * quantity
    tax_rate = Decimal(data_row['sl-税率']).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
    calc_tax = je * tax_rate
    if abs(je - calc_je) > Decimal('0.01'):
        print(f"传入的数量 * 单价 不等于 金额 ： {quantity} * {unit_price} != {je}， {calc_je}")
        return False
    if abs(se - calc_tax) > Decimal('0.01'):
        print(f"传入的 金额 * 税率 不等于 税额 ： {je} * {tax_rate} != {se}, {calc_tax}")
        return False
    single_amount = (limit / unit_price).quantize(Decimal('1.000000'), rounding=ROUND_HALF_UP) + Decimal('0.000001')
    count = 0
    while single_amount > (limit / unit_price) / 2 and count < 100000:
        single_amount -= Decimal('0.000001')
        count += 1
        single_je = (single_amount * unit_price).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
        single_tax_min = (single_je * tax_rate).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP) - Decimal('0.06')
        single_tax_max = (single_je * tax_rate).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP) + Decimal('0.06')
        single_tax = single_tax_max

        while single_tax >= single_tax_min:
            # Update the status bar with the current single_tax, single_amount, and single_je values
            update_status(single_tax, single_amount, single_je)

            line_count = math.floor(je / single_je)
            last_line_je = (je - single_je * line_count).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            last_line_se = (se - single_tax * line_count).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            last_line_amount = quantity - single_amount * line_count
            last_line_je_calc = (unit_price * last_line_amount).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            diff_je = abs(last_line_je - last_line_je_calc).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            last_line_se_calc = (last_line_je_calc * tax_rate).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            diff_se = abs(last_line_se - last_line_se_calc).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            if diff_se < Decimal(f'{max_error}') and diff_je < Decimal(f'{max_error}'):
                temp_rows = []
                serial_number = 0
                number_length = line_count // 10 + 1
                new_row = data_row.copy()
                new_row['spsl-商品数量'] = single_amount
                new_row['je-金额'] = single_je
                new_row['se-税额'] = single_tax
                new_row['lsdbh-流水单编号'] = f"{data_row['lsdbh-流水单编号']}_{str(serial_number).zfill(number_length)}"
                for _ in range(line_count):
                    temp_rows.append(new_row.copy())
                    serial_number += 1
                    temp_rows[-1]['lsdbh-流水单编号'] = f"{data_row['lsdbh-流水单编号']}_{str(serial_number).zfill(number_length)}"
                last_row = data_row.copy()
                last_row['spsl-商品数量'] = quantity - single_amount * line_count
                last_row['je-金额'] = last_line_je
                last_row['se-税额'] = last_line_se
                last_row['lsdbh-流水单编号'] = f"{data_row['lsdbh-流水单编号']}_{str(serial_number).zfill(number_length)}"
                temp_rows.append(last_row)
                print(f"可拆分成{line_count}行，每行数量{single_amount}，金额{single_je}，税额{single_tax}，"
                      f"以及一个尾差行，数量{last_row['spsl-商品数量']}，金额{last_line_je}， 税额{last_line_se}")
                return temp_rows
            single_tax -= Decimal('0.01')
    print("没有找到合适的结果")
    return None

def main(file_name, limit, max_error, update_status):
    df = pd.read_excel(file_name)
    split_data = []
    for index, row in df.iterrows():
        split_rows = split_row(row, limit, max_error, update_status)
        if split_rows:
            split_data.extend(split_rows)
    if split_data:
        result_df = pd.DataFrame(split_data)
        formatted_time = datetime.now().strftime('%Y%m%d%H%M%S')
        result_df.to_excel(f"拆分流水单{formatted_time}.xls", index=False, engine='openpyxl')
        print("拆分完成，数据已保存到 拆分流水单.xls")
        return True
    else:
        print("没有找到最精确的拆分项，请手动拆分")
        return False

def select_file():
    file_path = filedialog.askopenfilename(filetypes=[("Excel files", "*.xls")])
    if file_path:
        entry_file.delete(0, tk.END)
        entry_file.insert(0, file_path)

def start_split():
    file_name = entry_file.get()
    limit = Decimal(entry_limit.get())
    max_error = Decimal(entry_error.get())
    split_data_success = main(file_name, Decimal(str(limit)), max_error, update_status)
    if split_data_success:
        messagebox.showinfo("成功", "拆分完成，数据已保存到 拆分流水单.xls")
    else:
        messagebox.showerror("错误", "没有找到最精确的拆分项，请手动拆分，或逐步增大最大允许误差（建议0.005~0.009）")

def update_status(single_tax, single_amount, single_je):
    # Update the status bar with the current values of single_tax, single_amount, and single_je
    status_var.set(f"每行金额：{single_je}, 税额：{single_tax}, 数量: {single_je:.6f}")

# GUI setup
root = tk.Tk()
root.title("流水单拆分工具")
tk.Label(root, text="文件名:").grid(row=0, column=0)
entry_file = tk.Entry(root, width=50)
entry_file.grid(row=0, column=1)
button_file = tk.Button(root, text="选择文件", command=select_file)
button_file.grid(row=0, column=2)
tk.Label(root, text="单张发票限额:").grid(row=1, column=0)
entry_limit = tk.Entry(root, width=50)
entry_limit.grid(row=1, column=1)
tk.Label(root, text="最大允许误差:").grid(row=2, column=0)
entry_error = tk.Entry(root, width=50)
entry_error.insert(0, "0.005")
entry_error.grid(row=2, column=1)
button_start = tk.Button(root, text="开始拆分", command=start_split)
button_start.grid(row=3, column=1)

# Create a StringVar to hold the status message
status_var = tk.StringVar()
status_var.set("Current Single Tax: 0.00, Single Amount: 0.000000, Single JE: 0.00")  # Initial status
status_bar = tk.Label(root, textvariable=status_var, bd=1, relief=tk.SUNKEN, anchor=tk.W)
status_bar.grid(row=4, column=0, columnspan=3, sticky='we')

root.mainloop()