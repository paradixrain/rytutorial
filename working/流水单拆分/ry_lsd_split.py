# coding = utf-8
import argparse
import math

# @filename: ry_lsd_split.py
# @desc: 我想要写一个python的程序，实现流水单拆分的功能,
# 差异是，这个程序强制满足 单价*数量=金额，之前的版本允许1分钱误差
# 1、程序有文件读取功能，用户需要输入文件名，文件的内容和格式会同我上传的这个《流水单导入模板-正数.xls》一样；这个文件只会有2行，第一行是表头，第二行是数据；
# 2、用户需要输入一个“单张发票限额”的数值，这个数为2位小数
# 3、数据中的“dj-单价” 和 “spsl-商品数量”都是6位小数
# 4、在四舍五入的规则下，“dj-单价” * “spsl-商品数量” = “je-金额”， “je-金额” * “sl-税率” = “se-税额”；
# 5、程序需要实现：将数据行拆分为多行，拆分后的行每行的“je-金额”之和四舍五入后等于原始行的“je-金额”，每行的“spsl-商品数量” 之和四舍五入后，等于原始行的“spsl-商品数量” ，其他所有列都不变
# @author: PC\RyanLin
# @version: Python 3.12
# @date: 2024/9/20

import pandas as pd
from decimal import Decimal, ROUND_HALF_UP


def split_row(data_row, limit):
    unit_price = Decimal(data_row['dj-单价']).quantize(Decimal('1.000000'), rounding=ROUND_HALF_UP)
    quantity = Decimal(data_row['spsl-商品数量']).quantize(Decimal('1.000000'), rounding=ROUND_HALF_UP)
    je = Decimal(data_row['je-金额']).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
    se = Decimal(data_row['se-税额']).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
    calc_je = unit_price * quantity
    tax_rate = Decimal(data_row['sl-税率']).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
    calc_tax = je * tax_rate

    if abs(je - calc_je) > Decimal('0.01'):
        print(f"传入的数量 * 单价 不等于 金额 ： {quantity} * {unit_price} != {je}， {calc_je}")
        return False
    if abs(se - calc_tax) > Decimal('0.01'):
        print(f"传入的 金额 * 税率 不等于 税额 ： {je} * {tax_rate} != {se}, {calc_tax}")
        return False

    # 计算需要拆分的最小行数，如果无法得到满意结果，则单行数量-0.000001，如果减少了单行限额的一半，还未得到结果，则说明无解
    single_amount = (limit / unit_price).quantize(Decimal('1.000000'), rounding=ROUND_HALF_UP) + Decimal('0.000001')
    count = 0
    while single_amount > (limit / unit_price) / 2 and count < 100000:
        # 一分一分的进行测算来得到结果集，拆分后每一行的税额可以有1分钱误差，但是金额总数和税额总数需要严格相等，否则做账麻烦
        single_amount -= Decimal('0.000001')
        count += 1
        single_je = (single_amount * unit_price).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)

        if abs(single_amount * unit_price - single_je) > 0.01:
            # print(f"尝试第 {count} 次，单行金额{single_je}，单行数量：{single_amount} 失败")
            continue

        single_tax_min = (single_je * tax_rate).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)-Decimal('0.06')
        single_tax_max = (single_je * tax_rate).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)+Decimal('0.06')
        single_tax = single_tax_max

        while single_tax >= single_tax_min:
            # 用税额来调整误差
            line_count = math.floor(je / single_je)
            last_line_je = (je - single_je * line_count).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            last_line_se = (se - single_tax * line_count).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            last_line_amount = quantity - single_amount * line_count
            last_line_je_calc = (unit_price * last_line_amount).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            diff_je = abs(last_line_je - last_line_je_calc).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            last_line_se_calc = (last_line_je_calc * tax_rate).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            diff_se = abs(last_line_se - last_line_se_calc).quantize(Decimal('1.00'), rounding=ROUND_HALF_UP)
            # print(f"尝试第 {count} 次： 数量：{single_amount}，金额：{single_je}，税额：{single_tax}，行数：{line_count}，"
            #       f"倒减金额：{last_line_je}， 倒减税额：{last_line_se}，\n"
            #       f"税额尾差为{diff_se}={last_line_se}-{last_line_se_calc}\n"
            #       f"金额尾差为{diff_je}={last_line_je}-{last_line_je_calc}")

            if diff_se < 0.005 and diff_je < 0.005:
                temp_rows = []
                new_row = data_row.copy()
                new_row['spsl-商品数量'] = single_amount
                new_row['je-金额'] = single_je
                new_row['se-税额'] = single_tax
                for _ in range(line_count):
                    temp_rows.append(new_row)
                last_row = data_row.copy()
                last_row['spsl-商品数量'] = quantity - single_amount * line_count
                last_row['je-金额'] = last_line_je
                last_row['se-税额'] = last_line_se
                temp_rows.append(last_row)
                return temp_rows

            single_tax -= Decimal('0.01')
    print("没有找到合适的结果")


def main(file_name, limit):
    df = pd.read_excel(file_name, sheet_name="sheet1")

    split_data = []
    for index, row in df.iterrows():
        split_rows = split_row(row, limit)
        if split_rows:
            split_data.extend(split_rows)

    if split_data:
        result_df = pd.DataFrame(split_data)
        result_df.to_excel("金额不变拆分流水单.xls", index=False, engine='openpyxl')
        print("拆分完成，数据已保存到 '金额不变拆分流水单.xls'")
    else:
        print("没有找到最精确的拆分项，请手动拆分")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Split data based on invoice limit.')
    parser.add_argument('file_name', type=str, help='流水单文件名， 只能读取 xls ')
    parser.add_argument('limit', type=float, help='单张发票限额')
    args = parser.parse_args()
    main(args.file_name, Decimal(str(args.limit)))
