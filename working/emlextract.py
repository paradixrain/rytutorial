import os
import email


def extract_attachments(input_dir):
    """Extracts attachments from all .eml files in the given directory.

    Args:
      input_dir: The directory to search for .eml files.

    Returns:
      A list of paths to the extracted attachments.
    """
    # Get a list of all .eml files in the input directory.
    eml_files = [
        os.path.join(input_dir, f) for f in os.listdir(input_dir) if f.endswith(".eml")
    ]
    # print(eml_files)

    # Extract the attachments from each .eml file.
    attachments = []
    for eml_file in eml_files:
        with open(eml_file, "rb") as f:
            email_message = email.message_from_bytes(f.read())
            for attachment in email_message.get_payload():
                # print(attachment)
                attachment_file = attachment.get_filename()
                if attachment_file:
                    attachment_path = os.path.join(input_dir, attachment.get_filename())
                    with open(attachment_path, "wb") as f2:
                        f2.write(attachment.get_payload(decode=True))
                    attachments.append(attachment_path)

    return attachments


if __name__ == "__main__":
    extract_attachments(r"C:\Users\Ryan\Downloads\messages_package")
