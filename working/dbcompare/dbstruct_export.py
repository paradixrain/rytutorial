import cx_Oracle

def read_user_tables(username, password, host, port, service_name):
    dsn = cx_Oracle.makedsn(host, port, service_name)
    connection = cx_Oracle.connect(username, password, dsn)

    cursor = connection.cursor()
    cursor.execute("SELECT table_name FROM user_tables")

    tables = [row[0] for row in cursor.fetchall()]

    cursor.close()
    connection.close()

    return tables

# Usage example
username = "your_username"
password = "your_password"
host = "your_host"
port = "your_port"
service_name = "your_service_name"

tables = read_user_tables(username, password, host, port, service_name)
print(tables)