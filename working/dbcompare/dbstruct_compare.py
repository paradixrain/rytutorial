# coding = utf-8

# @title: dbstruct_compare
# @desc:
"""
使用方式：
1、将 export_all.sh 和 export_all.sql传到有sqlplus的服务器上，执行sh以后，会生成以下文件
    如果不能上传执行 export_all.sql ，可以把sql文件中的查询语句拷贝出来在pl/sql中执行，生成以下文件
    user_col.csv
    user_constraint.csv
    user_indexes.csv
    user_procedure.csv
    user_sequence.csv
    user_tab.csv
    user_trigger.csv
    user_views.csv
2、将生成的文件放到本地夹 ${dir_path_user}进行比对。执行当前程序，将 ${dir_path_model} 版本与生成的文件夹进行比对，输出差异
3、运行本程序，会生成一组html文件，挨个点击查看
"""

from difflib import HtmlDiff
import os

USER_TAB_HEADER = {"TABLE_NAME", "COMMENTS"}
USER_COL_HEADER = {"TABLE_NAME", "COLUMN_NAME", "DATA_TYPE", "DATA_LENGTH", "NULLABLE", "COMMENTS"}
USER_CONSTRAINT_HEADER = {"TABLE_NAME", "CONSTRAINT_TYPE", "CONSTRAINT_NAME", "INDEX_NAME"}
USER_VIEW_HEADER = {"VIEW_NAME", "TEXT_LENGTH"}
USER_SEQUENCE_HEADER = {"SEQUENCE_NAME"}
USER_PROCEDURE_HEADER = {"OBJECT_NAME", "PROCEDURE_NAME", "OBJECT_TYPE"}
USER_INDEX_HEADER = {"TABLE_NAME", "TABLE_TYPE", "INDEX_NAME", "UNIQUENESS"}
USER_TRIGGER_HEADER = {"TRIGGER_NAME"}

COMPARE_LIST = {"TAB": "user_col.csv",
                "COL": "user_tab.csv",
                "CONSTRAINT": "user_constraint.csv",
                "VIEW": "user_views.csv",
                "SEQUENCE": "user_sequence.csv",
                "PROCEDURE": "user_procedure.csv",
                "INDEX": "user_indexes.csv",
                "TRIGGER": "user_trigger.csv"
                }


def ddl_compare(file_path_1, file_path_2):
    """
    比较两个文件的差异
    :param file_path_1:
    :param file_path_2:
    :return:
    """
    with open(file_path_1, 'r', encoding='utf-8') as file1, open(file_path_2, 'r', encoding='utf-8') as file2:
        diff = HtmlDiff().make_file(file1.readlines(), file2.readlines(), "model", "user", context=True)
        return diff


if __name__ == "__main__":
    dir_path_model = r"D:\Pycharm\rytutorial\working\dbcompare\csv_2.1.5"  # 模板路径
    dir_path_user = r"D:\Pycharm\rytutorial\working\dbcompare\csv_bx"  # 用户环境路径
    dir_path_out = r"D:\Pycharm\rytutorial\working\dbcompare\out"  # 结果输出
    if not os.path.exists(dir_path_out):
        os.makedirs(dir_path_out)

    compare_file_list = ["TAB", "COL", "CONSTRAINT", "VIEW", "SEQUENCE", "PROCEDURE", "INDEX", "TRIGGER"]

    for compare_item in compare_file_list:
        html_file = ddl_compare(os.path.join(dir_path_model, COMPARE_LIST[compare_item]),
                                os.path.join(dir_path_user, COMPARE_LIST[compare_item]))

        output_file = os.path.join(dir_path_out, f"{compare_item}.html")
        with open(output_file, 'w', encoding="utf-8") as f:
            f.write(html_file)

    print(f"比对结束…请去{dir_path_out}中查看比对结果")
