import os

import pandas
import pandas as pd

filename = r'票据核验/第三批线下核验.xls'
TAG = "2024"

WORKING_DIR = r"C:\Users\Ryan\git\rytutorial\working"
if not os.path.exists(WORKING_DIR):
    WORKING_DIR = r"D:\work\workspace\git\rytutorial\working"


DATA_DIR = f"{WORKING_DIR}\\{TAG}"
if not os.path.exists(DATA_DIR):
    os.makedirs(DATA_DIR)


sheets = ['卷联', '数电', '普票']

dtype = {'发票代码': str,
         '发票号码': str,
         '开票日期': str,
         '校验码': str}

for sheet in sheets:
    df = pandas.read_excel(filename, sheet_name=sheet, dtype=dtype)

    # 将数据保存为 CSV 文件
    df.to_csv(f'{DATA_DIR}\\{sheet}.csv', index=False)
