import requests
import os
import datetime
import time
import hashlib
import json

"""
{
  "method": "winLending.finance.assets.invoiceCheckv2",
  "requestId": "60dfa7e1-3577-48c9-bfee-271fffa5a95d",
  "response": {
    "totalTax": "275.15",
    "amountTaxCn": "肆仟捌佰陆拾壹圆整",
    "salesBank": "中国工商银行股份有限公司上海市分行营业部 1001244319023113224",
    "salesPhone": "",
    "record": {
      "firstSign": "1",
      "checkTotal": "1",
      "checkOther": "0",
      "checkOwn": "1",
      "monitorOther": "0",
      "monitorOwn": "0",
      "taxDepartmentCount": "1",
      "monitorTotal": "0"
    },
    "invoiceNumber": "24312000000191239349",
    "invoiceType": "电子发票(增值税专用发票)",
    "purchaserAddressPhone": "",
    "state": "0",
    "purchaserBank": "",
    "machineCode": "",
    "cardInfo": {
      "engineNo": "",
      "vehicleNo": "",
      "originPlace": "",
      "passengersLimited": "",
      "idCardNo": "",
      "brandModel": "",
      "tonnage": "",
      "importCertificateNo": "",
      "certificateNo": "",
      "inspectionListNo": "",
      "vehicleType": "",
      "paymentVoucherNo": ""
    },
    "salesTaxNo": "91310000059392336G",
    "invoiceCode": "",
    "purchaserName": "索创物流（上海）有限公司",
    "salesAddress": "",
    "checkCode": "24312000000191239349",
    "billingDate": "2024-06-27",
    "totalAmount": "4585.85",
    "salesAddressPhone": "上海市浦东新区世纪大道100号上海环球金融中心34楼T30室 13761533075",
    "salesName": "三井住友海上火灾保险（中国）有限公司上海营业部",
    "purchaserTaxNo": "913100007970335544",
    "invoiceTypeCode": "31",
    "items": [
      {
        "unitPrice": "4585.8500000",
        "taxRate": "0.06",
        "unit": "单",
        "amount": "4585.85",
        "specificationModel": "",
        "quantity": "1",
        "tax": "275.15",
        "commodityName": "*保险服务*机动车交通事故责任强制保险费"
      }
    
    "remarks": "保单号:123100003312024000126等 批单号:等 经办人:倪晨茜 代收车船税:43.12元,滞纳金: 0.00元 (税款属期:202401-202412) 合计:4904.12元",
    "amountTax": "4861.00"
  }
}
"""


BASE_URL = r"http://210.12.38.194:9080/openapi/open/api?"
METHOD = "winLending.finance.assets.ocrInvoiceCheckv2"
APPKEY = "1001260"
VERSION = "3.0"
FORMAT = "json"
TYPE = "SYNC"
USERNAME = "46583014"
PASSWORD = "qwer1234"
CLIENT_ID = "1001260"
CLIENT_SECRET = "78144976-cd59-490e-abc9-104810dd7232"

TAG = "2024"
TOKEN = "4c2564e7-9cd6-4e8a-a529-ce86fdb42339"

WORKING_DIR = r"C:\Users\Ryan\git\rytutorial\working"
if not os.path.exists(WORKING_DIR):
    WORKING_DIR = r"D:\work\workspace\git\rytutorial\working"

DATA_DIR = f"{WORKING_DIR}\\{TAG}"
if not os.path.exists(DATA_DIR):
    os.makedirs(DATA_DIR)

CONCURRENCY = 4


def get_token():
    # Set the base URL for the API.
    global TOKEN
    base_url = f"{BASE_URL}method=oauth.token&client_id={CLIENT_ID}&grant_type=password&" \
               f"version=3.0&timestamp={str(int(time.time() * 1000))}"

    # Set the request parameters.
    params = {
        "username": f"{USERNAME}",
        "password": F"{PASSWORD}",
        "client_secret": f"{CLIENT_SECRET}"
    }

    # 获取token
    # Make the request.
    response = requests.post(base_url, data=params, timeout=5)
    # print(response.text)

    # Check the response status code.
    if response.status_code == 200:
        # The request was successful.
        # Get the access token from the response.
        access_token = response.json()["response"]["access_token"]
        # Print the access token.
        # print("access_token", access_token)
        return access_token
    else:
        # The request failed.
        # Print the error message.
        print("request failed")
        return False


def sign(params):
    md5_hash = hashlib.md5(params.encode("utf-8")).hexdigest()
    return md5_hash.upper()


def get_four_check_data(access_token, four_check_params):
    # 四要素核验，返回json格式的text
    """
    :param access_token:  Token
    :param four_check_params: {'发票代码': 4200193130, '发票号码': 5138803, '开票日期': Timestamp('2021-06-11 00:00:00'), '不含税金额': 1132.08}
    :return: response text
    """


    billing_date = f'{four_check_params.get("开票日期", "")}'
    print("1 billing_date,", billing_date)
    try:
        billing_date = billing_date.split(' ')[0]
    except AttributeError:
        # 如果不是日期，而是文本
        error_message = """{"method":"winLending.finance.assets.invoiceCheckv2","requestId":"0","errorResponse":{
            "code":0, "subCode":"0","message":"自定义错误","subMessage":"日期格式错误"}} """
        error_message = f'{error_message},{four_check_params.get("发票号码", "")},{four_check_params.get("开票日期", "")}'
        print("error_message", error_message)
    finally:
        print("9 billing_date,", billing_date)
        pass

    global TOKEN
    tsp = int(time.time() * 1000)
    params = {'method': r'winLending.finance.assets.invoiceCheckv2',
              'appKey': f'{APPKEY}',
              'token': f'{access_token}',
              'version': f"{VERSION}", 'format': f"{FORMAT}",
              "type": "sync", 'timestamp': f'{tsp}'}
    params_str = ""

    for key, value in sorted(params.items()):
        params_str += f"{key}{value}"

    # 在这里读取来自入参的四要素
    # 根据票种，构造对应的body。全电不需要invoiceCode，专票不需要checkCode，全电发票传入价税合计，其他传入不含税金额

    try:
        body = {'orgNo': '4e9ca5f95882545623ad',
                'invoice': {
                    'invoiceCode': f'{four_check_params.get("发票代码", "")}',
                    'invoiceNumber': f'{four_check_params.get("发票号码", "")}',
                    'billingDate': f'{four_check_params.get("开票日期","")}',
                    'checkCode': f'{four_check_params.get("校验码", "")}',
                    'totalAmount': f'{four_check_params.get("金额", "")}'}
                }
    except AttributeError as e:
        print(e)
        return """{"method":"winLending.finance.assets.invoiceCheckv2","requestId":"0","errorResponse":{"code":0,
        "subCode":"0","message":"自定义错误","subMessage":"日期转化错误"}} """ + \
               f'{four_check_params.get("发票号码", "")},{four_check_params.get("开票日期", "")}'

    bb = json.dumps(body)
    print(f"nb={body['invoice']['invoiceNumber']}")
    bb = bb.replace('\n', '').replace('\r', '').replace('\t', '')
    params_str = f'{CLIENT_SECRET}{params_str}{bb}{CLIENT_SECRET}'
    # print("params_str=", f'{bb}')

    signature = sign(params_str)
    # print("signature=", signature)

    # Set the base URL for the API.
    base_url = f"http://210.12.38.194:9080/openapi/open/api?method={params['method']}&" \
               f"appKey={params['appKey']}&token={access_token}&" \
               f"timestamp={params['timestamp']}&" \
               f"format=json&version=3.0&type=sync&sign={signature}"
    # print("base_url=", base_url)

    response = requests.post(base_url, data=json.dumps(body))
    print("response", response.text, four_check_params)
    return response.text, four_check_params



def main():
    global TOKEN
    TOKEN = get_token()
    print("Access token:", TOKEN)

    row_dict = {
        "序号": 1,
        "发票代码": "",
        "发票号码": "24329216805000000072",
        "开票日期": "2024-11-01",
        "校验码": "",
        "金额": 2.50
    }

    r = get_four_check_data(TOKEN, row_dict)
    print(r)


if __name__ == '__main__':
    print(datetime.datetime.now())
    main()
    print(datetime.datetime.now())
