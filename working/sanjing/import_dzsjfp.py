# coding=utf-8
import datetime
import uuid
import pandas


def fpgl_sd_dml(df, dml_file):
    sd_fphm_list = {}  # 发票主数据清单

    for index, fpmx in df.iterrows():
        # 先生成dml语句，每一行都是一个 fpgl_fpmx中的一行
        # print(fpmx, type(fpmx))

        id = uuid.uuid4().hex[:32]
        fpid = uuid.uuid4().hex[:32]
        fphm = fpmx['数电票号码']

        xmmc = fpmx['货物或应税劳务名称']
        ggxh = fpmx['规格型号']
        dw = fpmx['单位']
        sl = fpmx['数量']
        dj = fpmx['单价']
        je = fpmx['金额']
        slv = fpmx['税率']
        try:
            s = slv.split('%')
            slv = round(s[0] / 100, 2)
        except Exception:
            slv = 0.00
        se = fpmx['税额']
        hsje = fpmx['价税合计']
        sphfwssflhbbm = fpmx['税收分类编码']
        zzstsgl = fpmx['特定业务类型']
        org_id = '0000000000000000000000000000000'
        tenant_id = '0000000000000000000000000000000'

        insert_fpmx = """insert into fpgl_sd_fpmx (ID, FPID, XMMC, GGXH, DW, SL, DJ, JE, SLV, SE, HSJE, SPHFWSSFLHBBM, ZZSTSGL, ORG_ID, TENANT_ID ) VALUES """

        # insert fpgl_sd_fpxx
        if fphm not in sd_fphm_list.keys():
            # 主数据未添加的情况
            fpqqlsh = uuid.uuid4().hex[:32]
            if fpmx['是否正数发票'] == '是':
                lzfpbz = 0
            else:
                lzfpbz = 1
            if fpmx['发票票种'] == '数电票（普通发票）':
                fppz = "02"
            else:
                fppz = "01"
            xsfnsrsbh = fpmx['销方识别号']
            xsfmc = fpmx['销方名称']
            gmfnsrsbh = fpmx['购方识别号']
            gmfmc = fpmx['购买方名称']
            kpr = fpmx['开票人']
            kprq = fpmx['开票日期']
            filtered_rows = df[df['数电票号码'] == fphm]
            sum_hjje = filtered_rows['金额'].sum()
            sum_hjse = filtered_rows['税额'].sum()
            sum_jshj = filtered_rows['价税合计'].sum()

            # 如果不存在则插入fpgl_sd_fpxx表
            insert_fpxx = """insert into fpgl_sd_fpxx(ID, FPQQLSH, LZFPBZ, FPPZ, XSFNSRSBH, XSFMC, GMFNSRSBH, GMFMC, HJJE, HJSE, JSHJ, KPR, KPRQ, FPHM, ORG_ID, TENANT_ID) VALUES """
            insert_fpxx_sql = f"{insert_fpxx} ('{fpid}', '{fpqqlsh}', '{lzfpbz}', '{fppz}', '{xsfnsrsbh}', '{xsfmc}', '{gmfnsrsbh}', '{gmfmc}', '{sum_hjje}', '{sum_hjse}', '{sum_jshj}', '{kpr}', TO_DATE('{kprq}', 'YYYY-MM-DD HH24:MI:SS'), '{fphm}', '{org_id}', '{tenant_id}' );"
            with open(dml_file, 'a', encoding="utf-8") as f_in:
                f_in.write(insert_fpxx_sql + '\n')
                sd_fphm_list[fphm] = fpid
        else:
            # 如果已经存在，则找到发票号码对应的fpid，则不插入fpgl_sd_fpxx表
            fpid = sd_fphm_list[fphm]

        insert_fpmx_sql = f"{insert_fpmx}('{id}', '{fpid}', '{xmmc}', '{ggxh}', '{dw}', {sl}, {dj}, {je}, {slv}, {se}, {hsje}, '{sphfwssflhbbm}', '{zzstsgl}', '{org_id}', '{tenant_id}');"

        # insert fpgl_sd_fpmx
        with open(dml_file, 'a', encoding="utf-8") as f_in:
            f_in.write(insert_fpmx_sql + '\n')
            sd_fphm_list[fphm] = fpid

    print(sd_fphm_list)


def main():
    dzsj_file = r"渤海202409271415.xlsx"
    now = datetime.datetime.now()
    formatted_time = now.strftime("%Y%m%d%H%M%S")
    dml = f"{dzsj_file.split('.')[0]}_dml_file_{formatted_time}.sql"
    sheet = "信息汇总表"

    dtype = {'数电票号码': str,
             '销售方识别号': str,
             '购买方识别号': str,
             '税收分类编码': str
             }

    df = pandas.read_excel(dzsj_file, sheet_name=sheet, dtype=dtype)
    df = df.fillna('')

    fpgl_sd_dml(df, dml)
    with open(dml, 'a', encoding="utf-8") as f_in:
        f_in.write("COMMIT;\n")


if __name__ == "__main__":
    main()
