import json
import os

import pandas
import requests
import time
import uuid
import base64
import hashlib

BASE_URL = r"http://210.12.38.194:9080/openapi/open/api?"
METHOD = "winLending.finance.assets.ocrInvoiceCheckv2"
APPKEY = "1001260"
VERSION = "3.0"
FORMAT = "json"
TYPE = "SYNC"
USERNAME = "46583014"
PASSWORD = "qwer1234"
CLIENT_ID = "1001260"
CLIENT_SECRET = "78144976-cd59-490e-abc9-104810dd7232"


def get_token():
    # Set the base URL for the API.
    global TOKEN
    base_url = f"{BASE_URL}method=oauth.token&client_id={CLIENT_ID}&grant_type=password&" \
               f"version=3.0&timestamp={str(int(time.time() * 1000))}"

    # Set the request parameters.
    params = {
        "username": f"{USERNAME}",
        "password": F"{PASSWORD}",
        "client_secret": f"{CLIENT_SECRET}"
    }

    # 获取token
    # Make the request.
    response = requests.post(base_url, data=params, timeout=5)
    # print(response.text)

    # Check the response status code.
    if response.status_code == 200:
        # The request was successful.
        # Get the access token from the response.
        access_token = response.json()["response"]["access_token"]
        # Print the access token.
        # print("access_token", access_token)
        return access_token
    else:
        # The request failed.
        # Print the error message.
        print("request failed")
        return False


def sign(params):
    md5_hash = hashlib.md5(params.encode("utf-8")).hexdigest()
    return md5_hash.upper()


def get_all_file_paths(folder_path):
    file_paths = []
    for root, dirs, files in os.walk(folder_path):
        # Iterate over the files in the current folder
        for file in files:
            # Get the full path of the file
            file_path = os.path.join(root, file)
            # Append the file path to the list
            file_paths.append(file_path)
    return file_paths


def get_ocr_data(access_token, filename):
    tsp = int(time.time() * 1000)
    params = {'method': f'{METHOD}',
              'appKey': f'{APPKEY}',
              'token': f'{access_token}',
              'version': f"{VERSION}", 'format': f"{FORMAT}",
              "type": "sync", 'timestamp': f'{tsp}'}
    params_str = ""
    appSecret = "78144976-cd59-490e-abc9-104810dd7232"
    request_id = f'{uuid.uuid4().hex}'
    for key, value in sorted(params.items()):
        # print("{}={}".format(key, value))
        params_str += f"{key}{value}"

    body = {'orgNo': '4e9ca5f95882545623ad', 'requestId': f'{request_id}'}

    with open(filename, "rb") as fr:
        s = base64.b64encode(fr.read()).decode('utf-8')
        body['invoiceImgData'] = s

    bb = json.dumps(body)
    bb = bb.replace('\n', '').replace('\r', '').replace('\t', '')
    params_str = f'{appSecret}{params_str}{bb}{appSecret}'
    # print(params_str)

    signature = sign(params_str)
    # print("signature=", signature)

    # Set the base URL for the API.
    base_url = f"http://210.12.38.194:9080/openapi/open/api?method={params['method']}&" \
               f"appKey={params['appKey']}&token={access_token}&" \
               f"timestamp={params['timestamp']}&" \
               f"format=json&version=3.0&type=sync&sign={signature}"
    # print("base_url=", base_url)

    response = requests.post(base_url, data=json.dumps(body))
    print("response=", response.text)

    return response.text


def save_csv(filename, jsonstring):
    orgin_filename = filename
    csv_filename = os.path.join(os.path.dirname(filename), 'data.csv')

    # print("csv_filename", csv_filename)
    row = json.loads(jsonstring)["response"]
    record = row["record"]
    del row["record"]
    # print("del row")
    row["items"] = json.dumps(row["items"])
    # print("json.dumps")
    df_row = pandas.DataFrame(row, index=[0])
    # print("items", row["items"])

    df_record = pandas.DataFrame(record, index=[0])
    df_row.insert(0, "filename", orgin_filename)
    df_record.insert(0, "filename", orgin_filename)
    df_row.set_index("filename")
    df_record.set_index("filename")
    df = pandas.concat([df_row, df_record], axis=1)

    if os.path.isfile(csv_filename):
        df.to_csv(csv_filename, index=False, mode='a', header=False)
    else:
        df.to_csv(csv_filename, index=False, mode='w')
    print(filename, "saved")
    return True


def main():
    access_token = get_token()
    print("access_token", access_token)

    params = {'method': "winLending.finance.assets.ocrInvoiceCheckv2", 'appKey': "1001260",
              'token': f'{access_token}',
              'version': "3.0", 'format': "json", "type": "sync"}

    # 遍历文件夹下所有文件
    dir_path = r"C:\Users\PC\Desktop\家电发票测试样本"
    files = get_all_file_paths(dir_path)

    # 遍历所有文件清单
    for file in files:
        try:
            params_str = get_ocr_data(access_token, file.replace("\n", ""))
            save_csv(file, params_str)
            print("Success saving", file)
            with open("success.list.txt", "a") as success_file:
                success_file.write(f"{file}")
        except Exception as e:
            print(e)
            print("Error saving", file)
            with open("error.list.txt", "a") as error_file:
                error_file.write(f"{file}\n")

    # params_str = get_ocr_data(params, access_token, r"APFI-05540322-1.jpg")
    # print(params_str)


if __name__ == '__main__':
    main()
