import requests
import os
import datetime
import time
import hashlib
import json
import pandas as pd

# 调用百望云的单张查验，查验二手车
"""
{
    "purchaserPhone": "15597157811",
    "totalPrice": "5000.00",
    "brandModel": "雪佛兰牌SGM7166ATC",
    "purchaserAddress": "西宁市城北区祁连路904号1栋212室",
    "auctionPhone": "",
    "usedCarBank": "中国农业银行股份有限公司西宁城东经济技术开发区支行 28012001040001780",
    "usedCarTaxNo": "916301210916481947",
    "salesPhone": "17797143138",
    "record": {
        "firstSign": "1",
        "checkTotal": "1",
        "checkOwn": "1",
        "checkOther": "0",
        "monitorTotal": "0",
        "monitorOwn": "0",
        "monitorOther": "0",
        "taxDepartmentCount": "4"
    },
    "invoiceNumber": "00109197",
    "invoiceType": "二手车发票",
    "state": "0",
    "vehicleType": "小型轿车",
    "auctionBank": "",
    "machineCode": "",
    "vehiclePlaceName": "西宁市车辆管理所",
    "usedCarName": "青海华业二手车交易市场有限公司",
    "salesTaxNo": "630105196601051316",
    "auctionName": "",
    "auctionAddress": "",
    "invoiceCode": "063002300117",
    "purchaserName": "王小倩",
    "salesAddress": "西宁市城北区祁连路904号2号楼4单元451室",
    "carNumber": "青AWJ819",
    "billingDate": "2024-10-10",
    "vehicleNo": "LSGPC52U4EF181180",
    "salesName": "王军",
    "purchaserTaxNo": "632124198711252047",
    "registrationNumber": "630000778360",
    "invoiceTypeCode": "006",
    "auctionTaxNo": "",
    "usedCarPhone": "13997240919",
    "usedCarAddress": "青海省西宁市城中区同安路109号办公楼1楼1号",
    "totalPriceCn": "伍仟圆整",
    "remarks": ""
}
"""

BASE_URL = r"https://sandbox.yinshuitong.com/api?"
METHOD = "winLending.finance.assets.ocrInvoiceCheckv2"
APPKEY = "1001260"
VERSION = "3.0"
FORMAT = "json"
TYPE = "sync"
USERNAME = "46583014"
PASSWORD = "qwer1234"
CLIENT_ID = "1001260"
CLIENT_SECRET = "78144976-cd59-490e-abc9-104810dd7232"

TAG = "2024"
TOKEN = "4c2564e7-9cd6-4e8a-a529-ce86fdb42339"


WORKING_DIR = r"C:\Users\Ryan\git\rytutorial\working"
if not os.path.exists(WORKING_DIR):
    WORKING_DIR = r"D:\work\workspace\git\rytutorial\working"

DATA_DIR = f"{WORKING_DIR}\\{TAG}"
if not os.path.exists(DATA_DIR):
    os.makedirs(DATA_DIR)

CONCURRENCY = 4


def get_token():
    # Set the base URL for the API.
    global TOKEN
    base_url = f"{BASE_URL}method=baiwang.oauth.token&client_id={CLIENT_ID}&grant_type=password&" \
               f"version=3.0&timestamp={str(int(time.time() * 1000))}"

    # Set the request parameters.
    params = {
        "username": f"{USERNAME}",
        "password": F"{PASSWORD}",
        "client_secret": f"{CLIENT_SECRET}"
    }

    # 获取token
    # Make the request.
    response = requests.post(base_url, data=params, timeout=5)
    print(response.text)

    # Check the response status code.
    if response.status_code == 200:
        # The request was successful.
        # Get the access token from the response.
        access_token = response.json()["response"]["access_token"]
        # Print the access token.
        # print("access_token", access_token)
        return access_token
    else:
        # The request failed.
        # Print the error message.
        print("request failed")
        return False


def sign(params):
    md5_hash = hashlib.md5(params.encode("utf-8")).hexdigest()
    return md5_hash.upper()


def get_four_check_data(access_token, four_check_params):
    # 四要素核验，返回json格式的text
    """
    :param access_token:  Token
    :param four_check_params: {'发票代码': 4200193130, '发票号码': 5138803, '开票日期': Timestamp('2021-06-11 00:00:00'), '不含税金额': 1132.08}
    :return: response text
    """

    billing_date = f'{four_check_params.get("开票日期", "")}'
    print("1 billing_date,", billing_date)
    try:
        billing_date = billing_date.split(' ')[0]
    except AttributeError:
        # 如果不是日期，而是文本
        error_message = """{"method":"winLending.finance.assets.invoiceCheckv2","requestId":"0","errorResponse":{
            "code":0, "subCode":"0","message":"自定义错误","subMessage":"日期格式错误"}} """
        error_message = f'{error_message},{four_check_params.get("发票号码", "")},{four_check_params.get("开票日期", "")}'
        print("error_message", error_message)
    finally:
        print("9 billing_date,", billing_date)
        pass

    global TOKEN
    tsp = int(time.time() * 1000)
    params = {'method': r'winLending.finance.assets.invoiceCheckv2',
              'appKey': f'{APPKEY}',
              'token': f'{access_token}',
              'version': f"{VERSION}", 'format': f"{FORMAT}",
              "type": "sync", 'timestamp': f'{tsp}'}
    params_str = ""

    for key, value in sorted(params.items()):
        params_str += f"{key}{value}"

    # 在这里读取来自入参的四要素
    # 根据票种，构造对应的body。全电不需要invoiceCode，专票不需要checkCode，全电发票传入价税合计，其他传入不含税金额

    try:
        body = {'orgNo': '4e9ca5f95882545623ad',
                'invoice': {
                    'invoiceCode': f'{four_check_params.get("发票代码", "")}',
                    'invoiceNumber': f'{four_check_params.get("发票号码", "")}',
                    'billingDate': f'{four_check_params.get("开票日期", "")}',
                    'checkCode': f'{four_check_params.get("校验码", "")}',
                    'totalAmount': f'{four_check_params.get("金额", "")}'}
                }
    except AttributeError as e:
        print(e)
        return """{"method":"winLending.finance.assets.invoiceCheckv2","requestId":"0","errorResponse":{"code":0,
        "subCode":"0","message":"自定义错误","subMessage":"日期转化错误"}} """ + \
            f'{four_check_params.get("发票号码", "")},{four_check_params.get("开票日期", "")}'

    bb = json.dumps(body)
    print(f"nb={body['invoice']['invoiceNumber']}")
    bb = bb.replace('\n', '').replace('\r', '').replace('\t', '')
    params_str = f'{CLIENT_SECRET}{params_str}{bb}{CLIENT_SECRET}'
    print("params_str=", f'{params_str}')

    signature = sign(params_str)
    print("signature=", signature)

    # Set the base URL for the API.
    base_url = f"https://sandbox.yinshuitong.com/api?method={params['method']}&" \
               f"appKey={params['appKey']}&token={access_token}&" \
               f"timestamp={params['timestamp']}&" \
               f"format=json&version=3.0&type=sync&sign={signature}"
    print("base_url=", base_url)

    response = requests.post(base_url, data=json.dumps(body))
    print("response", response.text, four_check_params)
    return response.text


def process_row(row):
    global TOKEN
    row_dict = create_row_dict(row)
    result = get_four_check_data(TOKEN, row_dict)
    print("result=", result)

    response = json.loads(result)
    try:
        response = response['response']
        print(response.get('purchaserPhone'))
        # Add new columns to the DataFrame
        new_columns = {
            'purchaserPhone': response.get('purchaserPhone', ''),
            'totalPrice': response.get('totalPrice', ''),
            'brandModel': response.get('brandModel', ''),
            'purchaserAddress': response.get('purchaserAddress', ''),
            'auctionPhone': response.get('auctionPhone', ''),
            'usedCarBank': response.get('usedCarBank', ''),
            'usedCarTaxNo': response.get('usedCarTaxNo', ''),
            'salesPhone': response.get('salesPhone', ''),
            'invoiceNumber': response.get('invoiceNumber', ''),
            'invoiceType': response.get('invoiceType', ''),
            'state': response.get('state', ''),
            'vehicleType': response.get('vehicleType', ''),
            'auctionBank': response.get('auctionBank', ''),
            'machineCode': response.get('machineCode', ''),
            'vehiclePlaceName': response.get('vehiclePlaceName', ''),
            'usedCarName': response.get('usedCarName', ''),
            'salesTaxNo': response.get('salesTaxNo', ''),
            'auctionName': response.get('auctionName', ''),
            'auctionAddress': response.get('auctionAddress', ''),
            'invoiceCode': response.get('invoiceCode', ''),
            'purchaserName': response.get('purchaserName', ''),
            'salesAddress': response.get('salesAddress', ''),
            'carNumber': response.get('carNumber', ''),
            'billingDate': response.get('billingDate', ''),
            'vehicleNo': response.get('vehicleNo', ''),
            'salesName': response.get('salesName', ''),
            'purchaserTaxNo': response.get('purchaserTaxNo', ''),
            'registrationNumber': response.get('registrationNumber', ''),
            'invoiceTypeCode': response.get('invoiceTypeCode', ''),
            'auctionTaxNo': response.get('auctionTaxNo', ''),
            'usedCarPhone': response.get('usedCarPhone', ''),
            'usedCarAddress': response.get('usedCarAddress', ''),
            'totalPriceCn': response.get('totalPriceCn', ''),
            'remarks': response.get('remarks', '')
        }


        # Add record fields
        if 'record' in response:
            record = response['record']
            new_columns.update({
                'firstSign': record.get('firstSign', ''),
                'checkTotal': record.get('checkTotal', ''),
                'checkOwn': record.get('checkOwn', ''),
                'checkOther': record.get('checkOther', ''),
                'monitorTotal': record.get('monitorTotal', ''),
                'monitorOwn': record.get('monitorOwn', ''),
                'monitorOther': record.get('monitorOther', ''),
            })

        # Update the row with new columns
        for key, value in new_columns.items():
            row[key] = value
            print("key, value=", key, value)

        print("返回数据 row:", row)
    except Exception as e:
        print("解析失败，错误：")
        print(e)
    return row



def create_row_dict(row):
    return {
        '发票代码': row['发票代码'],
        '发票号码': row['发票号码'],
        # 'invoice_date': row['开票日期'].strftime('%Y-%m-%d'),  # Assuming the date is in datetime format
        '开票日期': row['开票日期'],  # Assuming the date is in datetime format
        '金额': float(row['金额'])
    }


def main():
    global TOKEN
    TOKEN = get_token()
    print("Access token:", TOKEN)

    row_dict = {
        "发票代码": "111002222011",
        "发票号码": "00245086",
        "开票日期": "2024-08-24",
        "校验码": "",
        "金额": 91858.41
    }

    dtype = {'发票代码': str,
             '发票号码': str,
             '开票日期': str}

    df = pd.read_excel('excel_data/二手车发票测试样本.xlsx', dtype=dtype)
    # r = get_four_check_data(TOKEN, row_dict)
    # print(r)
    df = df.apply(process_row, axis=1)
    df.to_excel('excel_data/二手车发票测试样本_结果.xlsx', index=False)
    print("处理完成，结果已写入 '二手车发票测试样本_结果.xlsx'")


if __name__ == '__main__':
    print(datetime.datetime.now())
    main()
    print(datetime.datetime.now())
