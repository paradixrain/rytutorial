import requests
import os
import datetime
import time
import pandas
import hashlib
import json
import multiprocessing
import openpyxl

BASE_URL = r"http://210.12.38.194:9080/openapi/open/api?"
METHOD = "winLending.finance.assets.ocrInvoiceCheckv2"
APPKEY = "1001260"
VERSION = "3.0"
FORMAT = "json"
TYPE = "SYNC"
USERNAME = "46583014"
PASSWORD = "qwer1234"
CLIENT_ID = "1001260"
CLIENT_SECRET = "78144976-cd59-490e-abc9-104810dd7232"

TAG = "2024"
TOKEN = "4c2564e7-9cd6-4e8a-a529-ce86fdb42339"

WORKING_DIR = r"C:\Users\Ryan\git\rytutorial\working"
if not os.path.exists(WORKING_DIR):
    WORKING_DIR = r"D:\work\workspace\git\rytutorial\working"

DATA_DIR = f"{WORKING_DIR}\\{TAG}"
if not os.path.exists(DATA_DIR):
    os.makedirs(DATA_DIR)

CONCURRENCY = 4


def get_token():
    # Set the base URL for the API.
    global TOKEN
    base_url = f"{BASE_URL}method=oauth.token&client_id={CLIENT_ID}&grant_type=password&" \
               f"version=3.0&timestamp={str(int(time.time() * 1000))}"

    # Set the request parameters.
    params = {
        "username": f"{USERNAME}",
        "password": F"{PASSWORD}",
        "client_secret": f"{CLIENT_SECRET}"
    }

    # 获取token
    # Make the request.
    response = requests.post(base_url, data=params, timeout=5)
    # print(response.text)

    # Check the response status code.
    if response.status_code == 200:
        # The request was successful.
        # Get the access token from the response.
        access_token = response.json()["response"]["access_token"]
        # Print the access token.
        # print("access_token", access_token)
        return access_token
    else:
        # The request failed.
        # Print the error message.
        print("request failed")
        return False


def sign(params):
    md5_hash = hashlib.md5(params.encode("utf-8")).hexdigest()
    return md5_hash.upper()


def get_four_check_data(access_token, four_check_params):
    # 四要素核验，返回json格式的text
    """
    :param access_token:  Token
    :param four_check_params: {'发票代码': 4200193130, '发票号码': 5138803, '开票日期': Timestamp('2021-06-11 00:00:00'), '不含税金额': 1132.08}
    :return: response text
    """


    # 时间检查，可以适配 , todo
    billing_date = f'{four_check_params.get("开票日期", "")}'
    print("1 billing_date,", billing_date)
    try:
        billing_date = billing_date.split(' ')[0]
    except AttributeError:
        # 如果不是日期，而是文本
        error_message = """{"method":"winLending.finance.assets.invoiceCheckv2","requestId":"0","errorResponse":{
            "code":0, "subCode":"0","message":"自定义错误","subMessage":"日期格式错误"}} """
        error_message = f'{error_message},{four_check_params.get("发票号码", "")},{four_check_params.get("开票日期", "")}'
        print("error_message", error_message)
    finally:
        print("9 billing_date,", billing_date)
        pass

    global TOKEN
    tsp = int(time.time() * 1000)
    params = {'method': r'winLending.finance.assets.invoiceCheckv2',
              'appKey': f'{APPKEY}',
              'token': f'{access_token}',
              'version': f"{VERSION}", 'format': f"{FORMAT}",
              "type": "sync", 'timestamp': f'{tsp}'}
    params_str = ""

    for key, value in sorted(params.items()):
        params_str += f"{key}{value}"

    # 在这里读取来自入参的四要素
    # 根据票种，构造对应的body。全电不需要invoiceCode，专票不需要checkCode，全电发票传入价税合计，其他传入不含税金额

    try:
        body = {'orgNo': '4e9ca5f95882545623ad',
                'invoice': {
                    'invoiceCode': f'{four_check_params.get("发票代码", "")}',
                    'invoiceNumber': f'{four_check_params.get("发票号码", "")}',
                    'billingDate': f'{four_check_params.get("开票日期", "").strftime("%Y/%m/%d")}',
                    'checkCode': f'{four_check_params.get("校验码", "")}',
                    'totalAmount': f'{four_check_params.get("金额", "")}'}
                }
    except AttributeError as e:
        print(e)
        return """{"method":"winLending.finance.assets.invoiceCheckv2","requestId":"0","errorResponse":{"code":0,
        "subCode":"0","message":"自定义错误","subMessage":"日期转化错误"}} """ + \
               f'{four_check_params.get("发票号码", "")},{four_check_params.get("开票日期", "")}'

    bb = json.dumps(body)
    print(f"nb={body['invoice']['invoiceNumber']}")
    bb = bb.replace('\n', '').replace('\r', '').replace('\t', '')
    params_str = f'{CLIENT_SECRET}{params_str}{bb}{CLIENT_SECRET}'
    # print("params_str=", f'{bb}')

    signature = sign(params_str)
    # print("signature=", signature)

    # Set the base URL for the API.
    base_url = f"http://210.12.38.194:9080/openapi/open/api?method={params['method']}&" \
               f"appKey={params['appKey']}&token={access_token}&" \
               f"timestamp={params['timestamp']}&" \
               f"format=json&version=3.0&type=sync&sign={signature}"
    # print("base_url=", base_url)

    response = requests.post(base_url, data=json.dumps(body))
    print("response", response.text, four_check_params)
    return response.text, four_check_params


# def save_csv_four_check(filename, jsonstring):
#     # 将json版本的text进行解析和存储，会根据票种进行分类存储，只存储必要字段
#     # 会尝试修复 "name":"na"me"，这种异常情况，方法：使用正则表达式
#     try:
#         row = json.loads(jsonstring)
#     except Exception as e:
#         print(f"解析异常:{jsonstring}")
#         return False
#     row = row["response"]
#
#     # 处理 "record节点"
#     record = row["record"]
#     del row["record"]
#
#     # 处理 "cardinfo节点"
#     try:
#         cardinfo = row["cardInfo"]
#         del row["cardInfo"]
#     except KeyError:
#         pass
#
#     # 处理 "cardinfo节点"
#     try:
#         cardinfo = row["cardinfo"]
#         del row["cardinfo"]
#     except KeyError:
#         pass
#
#     # 处理 发票明细行节点
#     try:
#         items_row = row["items"]
#         del row["items"]
#     except KeyError:
#         print("items not exist")
#
#     # 处理完以上3个节点后，row只有一级，没有嵌套
#
#     df_row = pandas.DataFrame(row, index=[0])
#     df_record = pandas.DataFrame(record, index=[0])
#
#     # 调整列位置
#     inv_code = df_row.pop("invoiceCode")
#     inv_number = df_row.pop("invoiceNumber")
#     inv_date = df_row.pop("billingDate")
#     inv_amount = df_row.pop("totalAmount")
#
#     df_row.insert(0, "totalAmount", inv_amount)
#     df_row.insert(0, "billingDate", inv_date)
#     df_row.insert(0, "invoiceNumber", inv_number)
#     df_row.insert(0, "invoiceCode", inv_code)
#
#     df_record.insert(0, "invoiceNumber", inv_number)
#     df_record.insert(0, "invoiceCode", inv_code)
#
#     df_row.set_index(["invoiceCode", "invoiceNumber"])
#     df_record.set_index(["invoiceCode", "invoiceNumber"])
#
#     df = pandas.concat([df_row, df_record], axis=1)
#
#     # 原始数据中存在普票list里有025的场景，如果票据类型是025
#     # 此处还需要增加处理逻辑，根据发票类型进行存储，将filename替换为票种名字
#     invoice_type = df_row["invoiceTypeCode"]
#     if str(invoice_type) == "025":
#         filename.replace("普票", "025")
#
#     if os.path.isfile(filename):
#         df.to_csv(filename, index=False, mode='a', header=False, encoding='utf-8')
#     else:
#         df.to_csv(filename, index=False, mode='w', encoding='utf-8')
#     return True
#
#
# def loop_data(df, sheet, rows_start, rows_end):
#     global TOKEN, DATA_DIR, TAG
#     # 获取并发时的token，否则第一行会失败
#     TOKEN = get_token()
#     for index, row in df.iloc[rows_start:rows_end].iterrows():
#         # print(index, sheet, row[0], row[1])
#         try:
#             row_dict = row.to_dict()
#             check_result = get_four_check_data(TOKEN, row_dict)
#             # print("Check result:", check_result)
#
#             save_suc = save_csv_four_check(f"{DATA_DIR}\\{TAG}_{sheet}_data.csv", check_result)
#             with open(f"{DATA_DIR}\\{TAG}_{sheet}_data_success.list.txt", "a", encoding="utf-8") as success_file:
#                 row_str = ",".join(f"{v}" for k, v in row_dict.items())
#                 success_file.write(f"{row_str}\n")
#         except Exception as e:
#             error_message = "unknown error"
#             try:
#                 error_message = json.dumps(json.loads(check_result)["errorResponse"]["subMessage"], ensure_ascii=False)
#                 # print(f"error_message{error_message}")
#                 code = json.dumps(json.loads(check_result)["errorResponse"]["code"], ensure_ascii=False)
#                 if str(code) == "100002":
#                     # 如果失败，就重新获取token
#                     print("token refresh")
#                     TOKEN = get_token()
#             except KeyError:
#                 print("KeyError, errorResponse_subMessage not found")
#             finally:
#                 with open(f"{DATA_DIR}\\{TAG}_{sheet}_data_error.list.txt", "a", encoding="utf-8") as error_file:
#                     row_str = ",".join(f"{v}" for k, v in row_dict.items())
#                     error_file.write(f"{row_str},{error_message}\n")
#                 print("error found: ", f"{row_str},{error_message}")
#         finally:
#             if not save_suc:
#                 with open(f"{DATA_DIR}\\{TAG}_{sheet}_data_error.list.txt", "a", encoding="utf-8") as error_file:
#                     row_str = ",".join(f"{v}" for k, v in row_dict.items())
#                     error_file.write(f"{row_str},查得但解析异常\n")


def main():
    global TOKEN
    TOKEN = get_token()
    print("Access token:", TOKEN)

    # 遍历文件中所有的四要素
    filename = r"线下批量查验清单（含上线前（2024年1-2月）及上线后未调阅）.xlsx"
    sheets = ['数电']
    # sheets = ['专票', '卷联', '数电', '普票']

    wb = openpyxl.load_workbook(filename)
    for sheet in sheets:
        pool = multiprocessing.Pool(processes=CONCURRENCY)  # 创建并发个进程
        reqs = []

        cur_sheet = wb[sheet]
        for row in cur_sheet.rows:
            if sheet == "数电":
                row_dict = {
                    "序号": row[0].value,
                    "发票代码": "",
                    "发票号码": row[1].value,
                    "开票日期": row[2].value,
                    "校验码": "",
                    "金额": row[3].value
                }
            else:
                row_dict = {
                    "序号": row[0].value,
                    "发票代码": row[1].value,
                    "发票号码": row[2].value,
                    "开票日期": row[3].value,
                    "校验码": row[4].value,
                    "金额": ""
                }
            try:
                r = pool.apply_async(get_four_check_data, (TOKEN, row_dict,))
                reqs.append(r)
            except Exception as e:
                print(e)

        pool.close()
        pool.join()

        for i in reqs:
            i.get()


if __name__ == '__main__':
    print(datetime.datetime.now())
    main()
    print(datetime.datetime.now())
