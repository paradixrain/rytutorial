import logging

import requests
import os
import datetime
import time
import pandas
import hashlib
import json
import multiprocessing
import re
import redis

"""
对票据核验程序进行改造，只提取需要的列，通过pickCol参数来选择
注意： 读取的excel或者csv列需要满足，以下的列名称
    'invoiceCode': f'{four_check_params.get("发票代码", "")}',
    'invoiceNumber': f'{four_check_params.get("发票号码", "")}',
    'billingDate': f'{four_check_params.get("开票日期", "").strftime("%Y-%m-%d")}',
    'checkCode': f'{four_check_params.get("校验码后六位", "")}',
    'totalAmount': f'{four_check_params.get("金额", "")}'}
"""

BASE_URL = r"http://210.12.38.194:9080/openapi/open/api?"
METHOD = "winLending.finance.assets.ocrInvoiceCheckv2"
APPKEY = "1001260"
VERSION = "3.0"
FORMAT = "json"
TYPE = "SYNC"
USERNAME = "46583014"
PASSWORD = "qwer1234"
CLIENT_ID = "1001260"
CLIENT_SECRET = "78144976-cd59-490e-abc9-104810dd7232"
TAG = "2024年5月"
TOKEN = "4c2564e7-9cd6-4e8a-a529-ce86fdb42339"

PATHS = [r"C:\Users\Ryan\git\rytutorial\working",
         r"D:\work\workspace\git\rytutorial\working",
         r"D:\Pycharm\rytutorial\working"]

WORKING_DIR = r"C:\Users\Ryan\git\rytutorial\working"
if os.path.exists(PATHS[0]):
    WORKING_DIR = PATHS[0]
elif os.path.exists(PATHS[1]):
    WORKING_DIR = PATHS[1]
else:
    WORKING_DIR = PATHS[2]

DATA_DIR = f"{WORKING_DIR}\\{TAG}"
if not os.path.exists(DATA_DIR):
    os.makedirs(DATA_DIR)

CONCURRENCY = 2

PICK_COLS = ['subMessage', 'totalAmount', 'purchaserName', 'salesName', 'salesAddressPhone', 'state']

STATE_DICT = {"0": "正常", "1": "已作废", "2": "已红冲"}


def get_token():
    # Set the base URL for the API.
    global TOKEN
    base_url = f"{BASE_URL}method=oauth.token&client_id={CLIENT_ID}&grant_type=password&" \
               f"version=3.0&timestamp={str(int(time.time() * 1000))}"

    # Set the request parameters.
    params = {
        "username": f"{USERNAME}",
        "password": F"{PASSWORD}",
        "client_secret": f"{CLIENT_SECRET}"
    }

    # 获取token
    # Make the request.
    response = requests.post(base_url, data=params, timeout=5)
    # print(response.text)

    # Check the response status code.
    if response.status_code == 200:
        # The request was successful.
        # Get the access token from the response.
        access_token = response.json()["response"]["access_token"]
        # Print the access token.
        # print("access_token", access_token)
        return access_token
    else:
        # The request failed.
        # Print the error message.
        print("request failed")
        return False


def sign(params):
    md5_hash = hashlib.md5(params.encode("utf-8")).hexdigest()
    return md5_hash.upper()


def get_four_check_data(access_token, four_check_params):
    # 四要素核验
    """
    :param access_token:  Token
    :param four_check_params: {'发票代码': 4200193130, '发票号码': 5138803, '开票日期': Timestamp('2021-06-11 00:00:00'), '不含税金额': 1132.08}
    :return: response object
    """
    global TOKEN
    tsp = int(time.time() * 1000)
    params = {'method': r'winLending.finance.assets.invoiceCheckv2',
              'appKey': f'{APPKEY}',
              'token': f'{access_token}',
              'version': f"{VERSION}", 'format': f"{FORMAT}",
              "type": "sync", 'timestamp': f'{tsp}'}
    params_str = ""

    for key, value in sorted(params.items()):
        params_str += f"{key}{value}"

    # 在这里读取来自入参的四要素
    # 根据票种，构造对应的body。全电不需要invoiceCode，专票不需要checkCode，全电发票传入价税合计，其他传入不含税金额

    try:
        body = {'orgNo': '4e9ca5f95882545623ad',
                'invoice': {
                    'invoiceCode': f'{four_check_params.get("发票代码", "")}',
                    'invoiceNumber': f'{four_check_params.get("发票号码", "")}',
                    'billingDate': f'{four_check_params.get("开票日期", "").strftime("%Y-%m-%d")}',
                    'checkCode': f'{four_check_params.get("校验码后六位", "")}',
                    'totalAmount': f'{four_check_params.get("金额", "")}'}
                }
    except AttributeError as e:
        print(e)
        return """{"method":"winLending.finance.assets.invoiceCheckv2","requestId":"0","errorResponse":{"code":0,
        "subCode":"0","message":"自定义错误","subMessage":"日期转化错误"}} """ + \
            f'{four_check_params.get("发票号码", "")},{four_check_params.get("开票日期", "")}'

    bb = json.dumps(body)
    # print(f"nb={body['invoice']['invoiceNumber']}")
    bb = bb.replace('\n', '').replace('\r', '').replace('\t', '')
    params_str = f'{CLIENT_SECRET}{params_str}{bb}{CLIENT_SECRET}'
    # print("params_str=", f'{bb}')

    signature = sign(params_str)
    print(f"四要素报文{bb}")
    # print("signature=", signature)

    # Set the base URL for the API.
    base_url = f"http://210.12.38.194:9080/openapi/open/api?method={params['method']}&" \
               f"appKey={params['appKey']}&token={access_token}&" \
               f"timestamp={params['timestamp']}&" \
               f"format=json&version=3.0&type=sync&sign={signature}"
    # print("base_url=", base_url)

    response = requests.post(base_url, data=json.dumps(body))
    return response.text


def loop_data(token, df, sheet, rows_start, rows_end):
    # 循环数据，并将所有数据存到redis

    # redis connection， 公司
    ryRedis = redis.Redis(
        host='redis-pool.test.baiwang-inner.com',
        port=6379,
        password='Baiwang*58')

    # redis connection， 本机docker
    # ryRedis = redis.Redis(
    #     host='localhost',
    #     port=6379,
    #     password='redis2024')

    for index, row in df.iloc[rows_start:rows_end].iterrows():
        try:
            row_dict = row.to_dict()
            check_result = get_four_check_data(token, row_dict)
            print("Check result", f"{sheet}{row['序号']}")

            # 将结果存到redis中，注意，存储的是bytes流，中文需要decode
            ryRedis.set(f"{sheet}{row['序号']}", check_result)
        except Exception as e:
            print(e)


def main():
    TOKEN = get_token()
    print("Access token", TOKEN)
    time.sleep(2)

    # 遍历文件中所有的四要素
    filename = r"线下批量查验清单（6.19）.xlsx"
    # sheets = {'专票': 'ZP', '卷联': 'JL', '数电': 'SD', '普票': 'PP'}
    sheets = {'普票': 'PP'}


    # 数电
    dtype = {'发票代码': str,
             '发票号码': str,
             '校验码后六位': str}

    for sheet in sheets.keys():
        df = pandas.read_excel(filename, sheet_name=sheet, usecols="A:E", dtype=dtype)
        # 这里要提前针对excel的日期列进行筛选，保证没有异常时间，可以在excel中将日期拆分成year,month,day，检查里面有没有异常值0,1970等
        # 要保证开票日期这列没有异常值
        # 删除行、列 最末尾的空行空列
        df['开票日期'] = pandas.to_datetime(df['开票日期'])

        row, col = df.shape

        pool = multiprocessing.Pool(processes=CONCURRENCY)  # 创建并发个进程
        reqs = []

        for i in range(CONCURRENCY + 1):
            # 将所有行，拆分到 CONCURRENCY 个块中，每个块中的请求书为 总数/CONCURRENCY
            start = row * i // CONCURRENCY
            end = row * (i + 1) // CONCURRENCY
            r = pool.apply_async(loop_data, (TOKEN, df, sheets[sheet], start, end,))
            reqs.append(r)

        pool.close()  # 关闭进程池，表示不能再往进程池中添加进程
        pool.join()  # 等待进程池中的所有进程执行完毕，必须在close()之后调用

        for i in reqs:
            # 开始执行数据查询
            i.get()


if __name__ == '__main__':
    print('start at:', datetime.datetime.now())
    main()
    print('end at:', datetime.datetime.now())
