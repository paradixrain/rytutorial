import redis
import pandas as pd
import time

HOST = 'redis-17463.c292.ap-southeast-1-1.ec2.cloud.redislabs.com',
PORT = 17463
PASSWORD = 'xM3NeovAp8EkyT96oCNFFYEPAvBTYJaN'


def redis_connect(host, port, password):
    try:
        r = redis.Redis(
            host='redis-17463.c292.ap-southeast-1-1.ec2.cloud.redislabs.com',
            port=17463,
            password='xM3NeovAp8EkyT96oCNFFYEPAvBTYJaN',
            decode_responses=True)
        return r
    except redis.exceptions.ConnectionError:
        print("failed to connect to Redis Cloud Server")
        return None


# 创建一个订阅者
def sub():
    r = redis_connect(HOST, PORT, PASSWORD)
    while True:
        try:
            messages = r.xread({'excel_queue': '0'}, block=0)  # Block until a new message arrives
            for stream, message_list in messages:
                for message_id, message_data in message_list:
                    # Process the message
                    print(f"Received message: {message_id} : {message_data}")
                    # Acknowledge the message by removing it from the stream
                    r.xdel('excel_queue', message_id)
        except redis.exceptions.ConnectionError:
            print("Redis Connection Lost, Reconnecting...")
            r = redis_connect(HOST, PORT, PASSWORD)


if __name__ == "__main__":
    sub()
