import redis
import pandas as pd
import time

r = redis.Redis(
    host='redis-17463.c292.ap-southeast-1-1.ec2.cloud.redislabs.com',
    port=17463,
    password='xM3NeovAp8EkyT96oCNFFYEPAvBTYJaN',
    decode_responses=True)

file = r'../basicTraining/multi_bigdata/a.xlsx'


def producer():
    df = pd.read_excel(file, sheet_name='普票', usecols="B:E")
    for index, line in df.iterrows():
        # print(index, line, type(line))
        line = line.to_dict()
        line["开票日期"] = f'{line["开票日期"]}'
        each_line = {
            'index': index,
            'line': f'{line.get("发票代码")} {line.get("发票号码")}'
        }
        r.xadd("excel_queue", line)
        print(f"index {index} sent {line.get('发票号码')}")
        time.sleep(1)


if __name__ == "__main__":
    producer()
