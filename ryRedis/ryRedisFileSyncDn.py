# coding = utf-8

"""
@filename: ryRedisFileSyncDn.py
@desc: 把redis中的文件下载到本地对应目录，主要是少量文件传输
需要配合ryRedisFileSyncUp.py
Redis中用两组key值存储：
第一组： key = "PATH" + serial number, value = 文件路径 + 文件名
第二组： key = "CONTENT" + serial number，文件二进制
@author: RyanLin
@Version: Python 3.12
@date: 2024年8月21日
"""

import redis
import os
from pathlib import Path


r = redis.Redis(
    host='PF46ML16',
    port=6379,
    password='ryredis2024')


# 连接到Redis
# 路径、文件名中有中文会有问题
download_path = r"C:\Users\linyu\Downloads\rsync"

# 新建文件夹
for dir in r.keys('DIR*'):
    dir_path = r.get(dir).decode('utf-8')
    dir_path = os.path.join(download_path, dir_path)
    dir_path = Path(dir_path)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

for key in r.keys('PATH*'):
    contentID = f'CONTENT{key[4:].decode('utf-8')}'
    file_path = os.path.join(download_path, r.get(key).decode('utf-8'))
    file_content = r.get(contentID)

    with open(file_path, 'wb') as f:
        f.write(file_content)

print("Files stored successfully.")
