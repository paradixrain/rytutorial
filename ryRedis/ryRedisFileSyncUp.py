# coding = utf-8

"""
@filename: ryRedisFileSyncUp.py
@desc: 把指定文件夹下的文件upload到redis中，主要是少量文件传输
需要配合ryRedisFileSyncDn.py
建议使用之前先flush所有的redis键对，保持干净
用两组key值存储：
第一组： key = "PATH" + serial number, value = 文件路径 + 文件名
第二组： key = "CONTENT" + serial number，文件二进制
@author: RyanLin
@Version: Python 3.12
@date: 2024年8月21日
"""
import shutil

import redis
import os
import random
import string


# r = redis.Redis(
#     host='redis-16785.c54.ap-northeast-1-2.ec2.cloud.redislabs.com',
#     port=16785,
#     password='jnanN0vKtDe9vOaEmDRUr6l4O6oUSZ6Z')


r = redis.Redis(
    host='127.0.0.1',
    port=6379,
    password='ryredis2024')


def generate_random_string(length):
    letters = string.ascii_letters + string.digits
    random_string = ''.join(random.choice(letters) for _ in range(length))
    return random_string


# 连接到Redis
# 路径、文件名中有中文会有问题
upload_path = r"D:\Working\single"

serial_number = 0
# Iterate through files in the directory
for root, dirs, files in os.walk(upload_path):
    for tempdir in dirs:
        # 存储路径
        serial_number += 1
        dir_path = os.path.relpath(os.path.join(root, tempdir), upload_path).encode('utf-8')
        print("dir", dir_path)
        r.set(f'DIR{serial_number}', dir_path)
    for file in files:
        serial_number += 1
        file_path = os.path.join(root, file)
        file_path_name = os.path.join(root, file).encode('utf-8')
        file_path_rel = os.path.relpath(os.path.join(root, file), upload_path).encode('utf-8')

        # Generate a random string for the new file name
        random_file_name = generate_random_string(15) + os.path.splitext(file)[1]
        new_file_path = os.path.join(root, random_file_name)

        # Copy the original file to the new file with the random name
        shutil.copyfile(file_path, new_file_path)

        with open(new_file_path, 'rb') as f:
            content = f.read()
            # 把文件名和文件内容分别存储，用serial_number来关联
            r.set(f'PATH{serial_number}', file_path_rel)
            r.set(f'CONTENT{serial_number}', content)

        os.remove(new_file_path)
print("Files stored in Redis successfully.")
