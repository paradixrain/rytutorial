# coding=utf-8

# @title: functools --- 高阶函数和可调用对象上的操作
# @desc: functools 模块应用于高阶函数，即参数或（和）返回值为其他函数的函数。
#        通常来说，此模块的功能适用于所有可调用对象。
# @author: RyanLin
# @date: 2022/01/30

# @functools.cache(user_function)    
#    因为它不需要移出旧值，所以比带有大小限制的 lru_cache() 更小更快。
#    连续计算的时候，直接查询每个步骤的缓存，所以快很多

import functools

# @functools.cache
# def factorial(n):
#     return n * factorial(n-1) if n else 1
# print(factorial(5))
# print(factorial(4))         # 秒出结果，不用再计算一次，因为都在缓存里了
#
# def factorial2(n):
#     return n * factorial2(n-1) if n else 1
# print(factorial2(5))


# 将一个类方法转换为特征属性，一次性计算该特征属性的值，然后将其缓存为实例生命周期内的普通属性。
# @functools.cached_property(func)
# class DataSet:
#     def __init__(self, sequence_of_numbers):
#         self._data = tuple(sequence_of_numbers)
#
#     @cached_property
#     def stdev(self):
#         return statistics.stdev(self._data)
#
#
# # functools.cmp_to_key(func)
# @functools.lru_cache(user_function)
# @functools.lru_cache(maxsize=128, typed=False)
# 一个为函数提供缓存功能的装饰器，缓存 maxsize 组传入参数，在下次以相同参数调用时直接返回上一次的结果。
# 用以节约高开销或I/O函数的调用时间。

# @lru_cache(maxsize=32)
# def get_pep(num):
#     'Retrieve text of a Python Enhancement Proposal'
#     resource = 'https://www.python.org/dev/peps/pep-%04d/' % num
#     try:
#         with urllib.request.urlopen(resource) as s:
#             return s.read()
#     except urllib.error.HTTPError:
#         return 'Not Found'
#
# for n in 8, 290, 308, 320, 8, 218, 320, 279, 289, 320, 9991:
#     pep = get_pep(n)
#     print(n, len(pep))
#
# get_pep.cache_info()
# #CacheInfo(hits=3, misses=8, maxsize=32, currsize=8)

# @functools.total_ordering


# 功能
# functools.partial(func, /, *args, **keywords)
# class functools.partialmethod(func, /, *args, **keywords)
# class functools.partialmethod(func, /, *args, **keywords)
# functools.reduce(function, iterable[, initializer])
# @functools.singledispatch
# class functools.singledispatchmethod(func)
#
# @fun.register(float)
# @fun.register(Decimal)
# def fun_num(arg, verbose=False):
#     if verbose:
#         print("Half of your number:", end=" ")
#     print(arg / 2)
# >>> fun_num is fun
# False
# 泛型函数就是，你定义函数时候，是万能类型。
# 在调用的时候，只要你把具体的类型传进去就好。好处呢，就是代码的复用，减少代码量。



# functools.update_wrapper(wrapper, wrapped, assigned=WRAPPER_ASSIGNMENTS, updated=WRAPPER_UPDATES)
# @functools.wraps(wrapped, assigned=WRAPPER_ASSIGNMENTS, updated=WRAPPER_UPDATES)


from functools import reduce
a = [1, 2]
b = reduce(lambda x, y: x * y, a[:1] + a[1:])
print(b)
