# coding=utf-8

# @title: bisect --- 数组二分查找算法
# @desc: 这个模块对有序列表提供了支持，使得他们可以在插入新数据仍然保持有序。
#        对于长列表，如果其包含元素的比较操作十分昂贵的话，这可以是对更常见方法的改进。
#        这个模块叫做 bisect 因为其使用了基本的二分（bisection）算法
# @author: RyanLin
# @date: 2022/01/20

# 方法
# bisect.bisect_left(a, x, lo=0, hi=len(a), *, key=None)
# bisect.bisect_right(a, x, lo=0, hi=len(a), *, key=None)
# bisect.bisect(a, x, lo=0, hi=len(a))
# bisect.insort_left(a, x, lo=0, hi=len(a), *, key=None)
# bisect.insort_right(a, x, lo=0, hi=len(a), *, key=None)
# bisect.insort(a, x, lo=0, hi=len(a))

# 性能说明
# 当使用 bisect() 和 insort() 编写时间敏感的代码时，请记住以下概念。
# 二分法对于搜索一定范围的值是很高效的。 对于定位特定的值，则字典的性能更好。
# insort() 函数的时间复杂度为 O(n) 因为对数时间的搜索步骤被线性时间的插入步骤所主导。
# 这些搜索函数都是无状态的并且会在它们被使用后丢弃键函数的结果。 
# 因此，如果在一个循环中使用搜索函数，则键函数可能会在同一个数据元素上被反复调用。 如果键函数速度不快，请考虑用 functools.cache() 来包装它以避免重复计算。

# 用于范围数字查询
# import bisect
# def grade(score, breakpoints=[60, 70, 80, 90], grades='FDCBA'):
#     i = bisect.bisect(breakpoints, score)
#     return grades[i]
#
# s = [grade(score) for score in [33, 99, 77, 70, 89, 90, 100]]
# print(s)
# # ['F', 'A', 'C', 'C', 'B', 'A', 'A']


import bisect

hour = [-1, 13, 17, -2, 9, -3, 100]


def grade(h, breakpoints=[0, 15], grades=['未拨打', '未超时', '超时']):
    i = bisect.bisect(breakpoints, h)
    return grades[i]


s = [grade(i) for i in hour]
print(s)
