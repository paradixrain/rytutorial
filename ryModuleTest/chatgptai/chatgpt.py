# import os
from openai import OpenAI

import os

# 设置环境变量
os.environ["OPENAI_API_KEY"] = r"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"

# 获取环境变量的值
value = os.environ["OPENAI_API_KEY"]

# OPENAI_API_KEY = r"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"

prompt = """
    我想要写一个挡板服务，计划用spring boot实现。挡板服务可以针对很多请求进行响应，首先请帮我实现第一个挡板服务
    请求址url为http://localhost:19091/access/v2/invoke/202007/CXNSRJBXX，请求参数Param:{nsrsbh=91440105755527093N}的时候
    可以返回
    {"returnmsg":"成功","returncode":"00","jcxx":{"xdpsdqybz":"Y","ssdabh":"10124401000000782220","fddbrxm":"罗国聪","zgswskfjmc":"国家税务总局广州市海珠区税务局赤岗税务所","yhzh":"3602088709200000636","nsrmc":"广州市海珠区雅坞互动设计室","yxqq":"2023-11-01","hydm":"6599","zgswskfjdm":"14401050041","yxqz":"9999-12-31","nsrsbh":"91440105755527093N","yhyywddm":"102440100016","khhmc":"中国工商银>行股份有限公司广州大道支行","scjydz":"广州市海珠区怡乐路怡乐四巷22号904房","djxh":"10114401000130918307","nsrztdm":"03","djzclxdm":"490","nsrlx":"1","qxjswjgdm":"14401050000"},"xfsnsrlxGrid":[],"qyhyxzGrid":[{"yxqz":"2025-08-26","qyhyxzdm":"18","yxqq":"2023-08-26"},{"yxqz":"2025-08-26","qyhyxzdm":"41","yxqq":"2023-08-26"},{"yxqz":"2099-12-31","qyhyxzdm":"11","yxqq":"2023-06-20"},{"yxqz":"2099-12-31","qyhyxzdm":"10","yxqq":"2023-06-20"},{"yxqz":"2099-12-31","qyhyxzdm":"36","yxqq":"2023-06-20"},{"yxqz":"2099-12-31","qyhyxzdm":"17","yxqq":"2023-06-20"},{"yxqz":"2099-12-31","qyhyxzdm":"37","yxqq":"2023-06-20"},{"yxqz":"2025-08-26","qyhyxzdm":"35","yxqq":"2023-08-26"},{"yxqz":"2025-08-26","qyhyxzdm":"42","yxqq":"2023-08-26"},{"yxqz":"2099-12-31","qyhyxzdm":"A1","yxqq":"2023-07-01"},{"yxqz":"2099-12-31","qyhyxzdm":"50","yxqq":"2023-07-01"}]}
    """

client = OpenAI()

completion = client.chat.completions.create(
    model="gpt-4o-mini",
    messages=[
        {"role": "system", "content": "You are a helpful assistant."},
        {
            "role": "user",
            "content": "Write a haiku about recursion in programming."
        }
    ]
)

print(completion.choices[0].message)
