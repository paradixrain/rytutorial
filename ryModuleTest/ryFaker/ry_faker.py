# coding=utf-8

from faker import Faker

output_file = "../../../openai-quickstart-python/address_phone_recognize/address_phone.txt"
fake = Faker(locale='zh_CN')

with open(output_file, "a", encoding="utf-8") as f:
    for i in range(10000):
        name = fake.name()
        address = fake.address().split()[0]
        if i % 5 != 0:
            phone = f"{fake.phonenumber_prefix()}-{fake.phone_number()[:8]}"
        else:
            phone = f"{fake.phone_number()[:8]}"
        if i % 8 == 0:
            f.writelines(f"{address}{phone}\n")
        else:
            f.writelines(f"{address} {phone}\n")
