# coding=utf-8

# @title: copy --- 浅层 (shallow) 和深层 (deep) 复制操作
# @desc: Python 的赋值语句不复制对象，而是创建目标和对象的绑定关系。
#         对于自身可变，或包含可变项的集合，有时要生成副本用于改变操作，而不必改变原始对象。本模块提供了通用的浅层复制和深层复制操作，
# @author: RyanLin
# @date: 2022/01/22

# copy.copy(x)    #返回 x 的浅层复制。
# copy.deepcopy(x[, memo])    #返回 x 的深层复制。
# exception copy.Error        #针对模块特定错误引发。

# 浅层复制 构造一个新的复合对象，然后（在尽可能的范围内）将原始对象中找到的对象的 引用 插入其中。
# 深层复制 构造一个新的复合对象，然后，递归地将在原始对象里找到的对象的 副本 插入其中。

import copy
# 方法一： 直接赋值,默认浅拷贝传递对象的引用而已, 原始列表改变，被赋值的b也会做相同的改变
a = [1, 2, 3, ["a", "b"]]
b = a
print(b)
a.append(5)
print(a, id(a), b, id(b))

# 方法二：浅拷贝，只拷贝了外层对象，没有拷贝子对象，所以外层的数据不变，子对象会改变
c = copy.copy(a)
a.append(6)
print('test3:', a, id(c), c)
a[3].append('c')
print('test4:', a, c)

# 方法三：深拷贝，包含对象里面的自对象的拷贝，所以原始对象的改变不会造成深拷贝里任何子元素的改变
d = copy.deepcopy(a)
a.append(7)
print('test5:', a, d, id(d))
a[3].append('d')
print('test6:', a, d)       # 拷贝后新增的'd',没有被添加到d里
