# coding = utf-8

# data = bytearray(b'abcefg')
# v = memoryview(data)
# v[0] = ord(b'z')
# print(data)
# v[1:4] = b'123'
# print(data)
# # v[2:3] = b'spam'
# v[2:6] = b'spam'
# print(data)

s = 'abcdefg'
bs = bytearray(s.encode())
print('bs', bs)
v = memoryview(bs)
print(v)
print(s)