# coding=utf-8

# @title: log——日志带配置文件
# @desc: 
# @author: RyanLin
# @date: 2022/02/11

import logging
import logging.config

logging.config.fileConfig('logging.conf')

# create logger
logger = logging.getLogger('simpleExample')

# 'application' code
logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
logger.critical('critical message')