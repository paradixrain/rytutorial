# coding=utf-9

# 1.  __str__ 用来控制类的print输出时候的默认方法

# >>> class Point(namedtuple('Point', ['x', 'y'])):
# ...     __slots__ = ()
# ...     @property
# ...     def hypot(self):
# ...         return (self.x ** 2 + self.y ** 2) ** 0.5
# ...     def __str__(self):
# ...         return 'Point: x=%6.3f  y=%6.3f  hypot=%6.3f' % (self.x, self.y, self.hypot)
# 
# >>> for p in Point(3, 4), Point(14, 5/7):
# ...     print(p)
# Point: x= 3.000  y= 4.000  hypot= 5.000
# Point: x=14.000  y= 0.714  hypot=14.018


# 2.   __doc__ 文档字符串， 默认会取 类下方一行的''' 内容 ''' 中的 内容，但是也可以自定义，通过直接赋值给 __doc__ 属性:
# 
# Book = namedtuple('Book', ['id', 'title', 'authors'])
# Book.__doc__ += ': Hardcover book in active collection'
# Book.id.__doc__ = '13-digit ISBN'
# Book.title.__doc__ = 'Title of first printing'
# Book.authors.__doc__ = 'List of authors sorted by last name'
# 在 3.5 版更改: 文档字符串属性变成可写。