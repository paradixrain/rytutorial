# coding = utf-8
# @title: json 
# @desc: JSON (JavaScript Object Notation)，由 RFC 7159 (which obsoletes RFC 4627) 和 ECMA-404 指定，
#     是一个受 JavaScript 的对象字面量语法启发的轻量级数据交换格式，尽管它不仅仅是一个严格意义上的 JavaScript 的字集 1。
# @author: RyanLin
# @date: 2022/02/27

import json
Header = {'User-Agent' :'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'}

print(Header['User-Agent'])
# with open('jsontmp.txt', 'w') as jfp:
    # j = json.dump(Header, jfp)


payloadSTR = '''
{
    "jsonrpc": "2.0", 
    "method": "call", 
    "params": {
        "model": "business.payment", 
        "domain": [ ], 
        "fields": [
            "business_code", 
            "customer_partner_id_string", 
            "collection_date", 
            "collection_amount", 
            "implement_salesmen_user_id", 
            "login_state", 
            "note", 
            "write_off_state"
        ], 
        "limit": 80, 
        "offset": 80, 
        "sort": "", 
        "context": {
            "lang": "zh_CN", 
            "tz": "Asia/Shanghai", 
            "uid": 36, 
            "params": {
                "action": 515, 
                "model": "business.payment", 
                "view_type": "list", 
                "menu_id": 97
            }
        }
    }, 
    "id": 521423206
}
'''
payloadJSON = json.loads(payloadSTR)
print('payloadJSON', payloadJSON)
a = payloadJSON['params']['limit']
print('a:', a)
payloadJSON['params']['limit'] = 160
payloadSTR = json.dumps(payloadJSON)
print('payloadJSON ', payloadJSON)