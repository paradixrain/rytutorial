# coding=utf-8
# from sys import byteorder

# @title: 内置类型
# @author: RyanLin
# @date: 2022/01/11

# 主要内置类型有数字、空、布尔、序列、迭代器、映射、类、实例和异常

# n = 100
# print(bin(n))
# print(n.bit_length())
# print(n.bit_count())
# print(n.to_bytes(2, byteorder='big'))
# print((1024).to_bytes(2, byteorder='big'))
# print((1).to_bytes(10, byteorder='little'))
# print((37).to_bytes(10, byteorder='little'))

# 生成器
# def echo(value=None):
#     print("Execution starts when 'next()' is called for the first time.")
#     try:
#         while True:
#             try:
#                 value = (yield value)
#             except Exception as e:
#                 value = e
#     finally:
#         print("Don't forget to clean up when 'close()' is called.")
# 
# generator = echo(1)
# print('1,',next(generator))
# print('2,',next(generator))
# print('3,',generator.send(2))
# generator.throw(TypeError, "spam")
# generator.close()


# 序列：list, tuple, range
# lists = [[]] * 3
# print(lists)
# #[[], [], []]
# lists[0].append(3)
# print(lists)
# #[[3], [3], [3]]
# # 与期待的结果：[[3], [], []]不同，请注意序列 s 中的项并不会被拷贝；它们会被多次引用。 这一点经常会令 Python 编程新手感到困扰。
# # 具体的原因在于 [[]] 是一个包含了一个空列表的单元素列表，所以 [[]] * 3 结果中的三个元素都是对这一个空列表的引用。 修改 lists 中的任何一个元素实际上都是对这一个空列表的修改。
# 
# # 想要每个不同，可以吧 * 3变成 range，并分别append
# lists = [[] for i in range(3)]
# lists[0].append(3)
# lists[1].append(5)
# lists[2].append(7)
# print(lists)
# # [[3], [5], [7]]

# 序列操作通用方法
# x in s
# x not in s
# s + t
# s * n 或 n * s
# s[i]
# s[i:j]
# s[i:j:k]
# len(s)
# min(s)
# max(s)
# s.index(x[, i[, j]])

# 不可变序列： tuple, range
# 不可变序列类型普遍实现而可变序列类型未实现的唯一操作就是对 hash() 内置函数的支持。

# 可变序列： list, str,
# s[i] = x
# s[i:j] = t
# del s[i:j]
# s[i:j:k] = t
# del s[i:j:k]
# s.append(x)
# s.clear()
# s.copy()
# s.extend(t) 或 s += t
# s *= n
# s.insert(i, x)
# s.pop() or s.pop(i)
# s.remove(x)
# s.reverse()

# str.capitalize()
# str.casefold()
# str.center(width[, fillchar])
# str.count(sub[, start[, end]])
# str.encode(encoding='utf-8', errors='strict')
# str.endswith(suffix[, start[, end]])
# str.expandtabs(tabsize=8)
# str.find(sub[, start[, end]])
# str.format(*args, **kwargs)
 
# str.format_map(mapping) mapping是一个字典对象
# people={"name": "John", "age": 33}
# print("My name is {name},i'm {age} old".format_map(people))

# str.index(sub[, start[, end]])
# str.isalnum()
# str.isalpha()
# str.isascii()
# str.isdecimal()
# str.isdigit()
# str.isidentifier()
# str.islower()
# str.isnumeric()
# str.isprintable()
# str.isspace()
# str.istitle()
# str.isupper()
# str.join(iterable)
# str.ljust(width[, fillchar])
# str.lower()
# str.lstrip([chars])

# static str.maketrans(x[, y[, z]]) 此静态方法返回一个可供 str.translate() 使用的转换对照表。
# intab = "aeiou"
# outtab = "12345"
# ot = "t"
# trantab = str.maketrans(intab, outtab)
# trantab2 = str.maketrans(intab, outtab, ot)
# 
# str1 = "this is string example....wow!!!"
# print (str1.translate(trantab))
# print (str1.translate(trantab2))

# str.partition(sep) 在 sep 首次出现的位置拆分字符串，返回一个 3 元组，其中包含分隔符之前的部分、分隔符本身，以及分隔符之后的部分。
# 如果分隔符未找到，则返回的 3 元组中包含字符本身以及两个空字符串
# str.removeprefix(prefix, /)
# str.removesuffix(suffix, /)
# str.replace(old, new[, count])
# str.rfind(sub[, start[, end]])
# str.rindex(sub[, start[, end]])
# str.rjust(width[, fillchar])
# str.rpartition(sep)
# str.rsplit(sep=None, maxsplit=- 1)
# str.rstrip([chars])

# print("wo shi yi ge ".rsplit(maxsplit=2))
# print("wo shi yi ge ".split(maxsplit=2))

# str.split(sep=None, maxsplit=- 1)
# str.splitlines([keepends])
# str.startswith(prefix[, start[, end]])
# str.strip([chars])
# str.swapcase() 请注意 s.swapcase().swapcase() == s 并不一定为真值。如德文的 letter 'ß'， 转化为小写后是ss 
# str.title()
# str.translate(table)
# str.upper()

# 二进制序列类型： bytes, bytearray, memoryview
# bytes 字面值中只允许 ASCII 字符（无论源代码声明的编码为何）。 任何超出 127 的二进制值必须使用相应的转义序列形式加入 bytes 字面值。
# bytes 对象的行为实际上更像是不可变的整数序列

# classmethod fromhex(string) 此 bytes 类方法返回一个解码给定字符串的 bytes 对象
# print(bytes.fromhex('2Ef0 F1f2  '))
# # hex([sep[, bytes_per_sep]])
# print(b'\xf0\xf1\xf2'.hex())

# bytearray 对象是 bytes 对象的可变对应物。
# classmethod fromhex(string)
# hex([sep[, bytes_per_sep]])
# print(bytearray.fromhex('2Ef0 F1f2  '))
# print(bytearray(b'\xf0\xf1\xf2').hex())
# 
# bytes.count(sub[, start[, end]])
# bytearray.count(sub[, start[, end]])
# bytes.removeprefix(prefix, /)
# bytearray.removeprefix(prefix, /)
# bytes.removesuffix(suffix, /)
# bytearray.removesuffix(suffix, /)
# bytes.decode(encoding='utf-8', errors='strict')
# bytearray.decode(encoding='utf-8', errors='strict')
# bytes.endswith(suffix[, start[, end]])
# bytearray.endswith(suffix[, start[, end]])
# bytes.find(sub[, start[, end]])
# bytearray.find(sub[, start[, end]])
# bytes.index(sub[, start[, end]])
# bytearray.index(sub[, start[, end]])
# bytes.join(iterable)
# bytearray.join(iterable)
# bytes.partition(sep)
# bytearray.partition(sep)
# bytes.replace(old, new[, count])
# bytearray.replace(old, new[, count])
# bytes.rfind(sub[, start[, end]])
# bytearray.rfind(sub[, start[, end]])
# bytes.rindex(sub[, start[, end]])
# bytearray.rindex(sub[, start[, end]])
# bytes.rpartition(sep)
# bytearray.rpartition(sep)
# bytes.startswith(prefix[, start[, end]])
# bytearray.startswith(prefix[, start[, end]])
# bytes.translate(table, /, delete=b'')
# bytearray.translate(table, /, delete=b'')
# bytes.center(width[, fillbyte])
# bytearray.center(width[, fillbyte])
# bytes.ljust(width[, fillbyte])
# bytearray.ljust(width[, fillbyte])
# bytes.lstrip([chars])
# bytearray.lstrip([chars])
# bytes.rjust(width[, fillbyte])
# bytearray.rjust(width[, fillbyte])
# bytes.rsplit(sep=None, maxsplit=- 1)
# bytearray.rsplit(sep=None, maxsplit=- 1)
# bytes.rstrip([chars])
# bytearray.rstrip([chars])
# bytes.split(sep=None, maxsplit=- 1)
# bytearray.split(sep=None, maxsplit=- 1)
# bytes.strip([chars])
# bytearray.strip([chars])
# bytes.capitalize()
# bytearray.capitalize()
# bytes.expandtabs(tabsize=8)
# bytearray.expandtabs(tabsize=8)
# bytes.isalnum()
# bytearray.isalnum()
# bytes.isalpha()
# bytearray.isalpha()
# bytes.isascii()
# bytearray.isascii()
# bytes.isdigit()
# bytearray.isdigit()
# bytes.islower()
# bytearray.islower()
# bytes.isspace()
# bytearray.isspace()
# bytes.istitle()
# bytearray.istitle()
# bytes.isupper()
# bytearray.isupper()
# bytes.lower()
# bytearray.lower()
# bytes.splitlines(keepends=False)
# bytearray.splitlines(keepends=False)
# bytes.swapcase()
# bytearray.swapcase()
# bytes.title()
# bytearray.title()
# bytes.upper()
# bytearray.upper()
# bytes.zfill(width)

# 内存视图 memoryview 对象允许 Python 代码访问一个对象的内部数据
# class memoryview(object) 创建一个引用 object 的 memoryview 。 object 必须支持缓冲区协议。支持缓冲区协议的内置对象有 bytes 和 bytearray 。

# z=b'abcefg'
# v = memoryview(z)
# print(v[1])
# print(v[-1])
# print(bytes(v[1:4]))
#
# # 如果下层对象是可写的，则内存视图支持一维切片赋值。 改变大小则不被允许，
# try:
#     v[1]=b'x'
#     print(v[1])
#     print(z)  # 这个会失败，因为string 类型不允许被修改
# except Exception as e:
#     print(e)
#
# # 如果下层对象是可写的，则内存视图支持一维切片赋值。 改变大小则不被允许
# data = bytearray(b'abcefg')
# v = memoryview(data)
# print('readonly?:', v.readonly)
# try:
#     v[0] = ord(b'z')
#     v[1:4] = b'123'
#     print(data)  # 这个会失败，因为string 类型不允许被修改
# except Exception as e:
#     print(e)
    
# memoryview 具有以下一些方法
# __eq__(exporter)
# tobytes(order=None)
# tolist()
# toreadonly()
# release()
# cast(format[, shape])
# # memoryview 具有以下一些属性值
# obj
# nbytes
# readonly
# format
# itemsize
# ndim
# shape
# strides
# suboffsets
# c_contiguous
# f_contiguous
# contiguous

# 集合set 对象是由具有唯一性的 hashable 对象所组成的无序多项集
# 目前有两种内置集合类型，set 和 frozenset
# set 类型是可变的 --- 其内容可以使用 add() 和 remove() 这样的方法来改变
# frozenset 类型是不可变并且为 hashable --- 其内容在被创建后不能再改变；因此它可以被用作字典的键或其他集合的元素

# set 和 frozenset 的实例提供以下操作：
# len(s)
# x in s
# x not in s
# isdisjoint(other)
# issubset(other)
# set <= other
# set < other
# issuperset(other)
# set >= other
# set > other
# union(*others)
# intersection(*others)
# difference(*others)
# symmetric_difference(other)
# copy()


# 下表列出了可用于 set 而不能用于不可变的 frozenset 实例的操作：

# update(*others)
# intersection_update(*others)
# difference_update(*others)
# symmetric_difference_update(other)
# add(elem)
# remove(elem)
# discard(elem)
# pop()
# clear()

# 字典视图对象
# 由 dict.keys(), dict.values() 和 dict.items() 所返回的对象是 视图对象。 该对象提供字典条目的一个动态视图，这意味着当字典改变时，视图也会相应改变。

# len(dictview)
# iter(dictview)
# x in dictview
# reversed(dictview)
# dictview.mapping


# set operations
# dishes = {'eggs': 2, 'sausage': 1, 'bacon': 1, 'spam': 500}
# keys = dishes.keys()
# values = dishes.values()
# print(keys & {'eggs', 'bacon', 'salad'})
# print(keys ^ {'sausage', 'juice'})
# # {'juice', 'sausage', 'bacon', 'spam'}
# 
# # get back a read-only proxy for the original dictionary
# print(values.mapping)
# # mappingproxy({'eggs': 2, 'sausage': 1, 'bacon': 1, 'spam': 500})
# print(values.mapping['spam'])
# # 500


# 上下文管理器类型
# contextmanager.__enter__()
# contextmanager.__exit__(exc_type, exc_val, exc_tb)



# 模块
# 唯一的特殊操作是属性访问: m.name，这里 m 为一个模块而 name 访问定义在 m 的符号表中的一个名称。 模块属性可以被赋值。
# 每个模块都有一个特殊属性 __dict__。 这是包含模块的符号表的字典。 修改此字典将实际改变模块的符号表

# import sys
# for dicts in sys.__dict__.keys():
#     if dicts.startswith('r'):
#         print('before', dicts)
# if 'ryanLin' not in sys.__dict__.keys():
#     sys.__dict__['ryanLin']='ryanTest'
#     for dicts in sys.__dict__.keys():
#         if dicts.startswith('r'):
#             print('after', dicts)

# 代码对象
# 代码对象被具体实现用来表示“伪编译”的可执行 Python 代码，例如一个函数体。 
# 它们不同于函数对象，因为它们不包含对其全局执行环境的引用。 代码对象由内置的 compile() 函数返回，并可通过从函数对象的 __code__ 属性从中提取

# 类型对象
# 类型对象表示各种对象类型。 对象的类型可通过内置函数 type() 来获取。 类型没有特殊的操作。

# 空对象
# 此对象会由不显式地返回值的函数所返回。 它不支持任何特殊的操作。 空对象只有一种值 None (这是个内置名称)。 type(None)() 会生成同一个单例

# 省略符对象
# 此对象常被用于切片 (参见 切片)。 它不支持任何特殊的操作。 省略符对象只有一种值 Ellipsis (这是个内置名称)。 type(Ellipsis)() 会生成 Ellipsis 单例

# 未实现对象
# 此对象会被作为比较和二元运算被应用于它们所不支持的类型时的返回值。 请参阅 比较运算 了解更多信息。 未实现对象只有一种值 NotImplemented。

# 