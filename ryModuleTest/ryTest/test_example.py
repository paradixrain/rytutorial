import pandas


class TestClass:
    def test_char_in_string(self):
        string = "this"
        char = "h"
        assert char in string

    def test_calculation(self):
        result = 20 + 21
        assert 41 == result

# sheets = {'数电': 'SD', '普票': 'PP'}
# for sheet in sheets.keys():
#     print(sheet, sheets[sheet])


filename = r"D:\work\workspace\git\rytutorial\working\线下批量查验清单（含上线前（2024年1-2月）及上线后未调阅）.xlsx"
df = pandas.read_excel(filename, sheet_name="普票", usecols="A:E")

df.index=df['序号'] # 将作为DataFrame的行索引
print(df.loc[16733,])
