# coding=utf-8

# @title: enum --- 对枚举的支持
# @desc: 枚举是与多个唯一常量值绑定的一组符号名（即成员）。枚举中的成员可以进行身份比较，并且枚举自身也可迭代
# @author: RyanLin
# @date: 2022/01/25

# 创建 Enum 枚举是由 class 句法创建的，这种方式易读、易写


# class enum.Enum
# class enum.IntEnum
# class enum.IntFlag
# class enum.Flag
# enum.unique()
# class enum.auto

from enum import Enum
class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3

print(Color.RED)
print(Color['RED'])
mem = Color.RED
print(mem.name, mem.value)
print(repr(Color.RED))
print(type(Color.RED))

for clr in Color:
    print(clr, repr(clr))
    

# 确保唯一值
from enum import Enum, unique
@unique
class Mistake(Enum):
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 3
# 因为THREE和FOUR都是3，所以会报错

