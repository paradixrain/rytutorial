# coding=utf-8
# 使用cProfile来分析python运行速度
# import time


def foo():
    s = 0
    for i in range(1000000):
        s += i
    return s


if __name__ == "__main__":
    import cProfile
    cProfile.run("foo()")