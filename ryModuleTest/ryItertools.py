# coding=utf-8

# @title: itertools --- 为高效循环而创建迭代器的函数
# @desc: 本模块标准化了一个快速、高效利用内存的核心工具集，这些工具本身或组合都很有用。
#        它们一起形成了“迭代器代数”，这使得在纯Python中有可能创建简洁又高效的专用工具。
# @author: RyanLin
# @date: 2022/01/30

# 无穷迭代器：
# count(start, [step])
# cycle(p)
# repeat(elem [,n])
#
# # 根据最短输入序列长度停止的迭代器
# # 下列模块函数均创建并返回迭代器。有些迭代器不限制输出流长度，所以它们只应在能截断输出流的函数或循环中使用
#
# itertools.accumulate(iterable[, func, *, initial=None])
# itertools.chain(*iterables)
# classmethod chain.from_iterable(iterable)
# itertools.combinations(iterable, r)
# itertools.combinations_with_replacement(iterable, r)
# itertools.compress(data, selectors)
# itertools.count(start=0, step=1)
# itertools.cycle(iterable)
# itertools.dropwhile(predicate, iterable)
# itertools.filterfalse(predicate, iterable)
# itertools.groupby(iterable, key=None)
# itertools.islice(iterable, stop)
# itertools.islice(iterable, start, stop[, step])
# itertools.pairwise(iterable)
# itertools.permutations(iterable, r=None)
# itertools.product(*iterables, repeat=1)
# itertools.repeat(object[, times])
# itertools.starmap(function, iterable)
# itertools.takewhile(predicate, iterable)
# itertools.tee(iterable, n=2)
# itertools.zip_longest(*iterables, fillvalue=None)


import itertools
rylists = [1,2,1,1,1,2,3,4,5,3,1]
for i, k in itertools.groupby(sorted(rylists)):
    # for j in i:
    print(i, k, list(k))
    # for kk in k:
    #     print('kk', kk)


def minus(x):
    return x * (-1)

# j = map(minus, rylists)
# for i in j:
#     print(i)