# coding=utf-8
"""
这个模块有一个函数timeit.timeit（stmt = pass，setup = pass，timer = <default timer>，number = 1000000）这个函数有四个参数：

stmt：要测量其执行时间的代码段。
setup：执行stmt之前要运行的代码。通常，它用于导入一些模块或声明一些必要的变量。通过示例将更加清楚。
timer：这是默认的timeit.Timer对象。它有一个合理的默认值，因此我们不必做太多事情。
number：指定将执行stmt的次数。
它会回到执行拍摄（秒）的时间语句，指定的数十倍。
"""

# import math
import timeit


setup_code = 'from math import sqrt'
stmt_code = "sum(sqrt(x) for x in range(10000))"
loop_num = 100
time_cost = timeit.timeit(stmt=stmt_code, setup=setup_code, number=100)
print(time_cost)

