# coding=utf-8

# @title: weakref --- 弱引用
# @desc: weakref 模块允许Python程序员创建对象的 weak references 。
#        弱引用的主要用途是实现保存大对象的高速缓存或映射，但又不希望大对象仅仅因为它出现在高速缓存或映射中而保持存活。 
#        弱引用是一种特殊的对象，能够在不产生引用的前提下，关联目标对象
# @author: RyanLin
# @date: 2022/01/21

# 类
# class weakref.ref(object[, callback])
# weakref.proxy(object[, callback])   # 返回 object 的一个使用弱引用的代理。 此函数支持在大多数上下文中使用代理，而不要求显式地对所使用的弱引用对象解除引用。 
# weakref.getweakrefcount(object)     # 返回指向 object 的弱引用和代理的数量。
# weakref.getweakrefs(object)         # 返回由指向 object 的所有弱引用和代理构成的列表。
# 
# # 方法
# class weakref.WeakKeyDictionary([dict])
# WeakKeyDictionary.keyrefs()
# WeakValueDictionary     #对象具有一个额外方法，此方法存在与 WeakKeyDictionary 对象的 keyrefs() 方法相同的问题。
# WeakValueDictionary.valuerefs()
# weakref.ReferenceType
# weakref.ProxyType
# weakref.CallableProxyType
# weakref.ProxyTypes

import weakref
class Object(list):
    pass

# o = Object()
# r = weakref.ref(o)
# o2 = r()
# s = (o is o2)
# print(s)
# del o, o2
# print(r())

# l1 = [1, 2, 3]
# l2 = l1     # 深度复制
l1 = Object([1,2,3])
l2 = l1
print('address of l1, l2:', id(l1), id(l2))
l2.append(5)
r = weakref.ref(l1)
print('address of r:', id(r), r, ',content is :', r())  # 打印 弱引用的地址，以及内容
print(l1, l2, l1 is l2)
del l1
l2.append(4)
print(l2) # 还存在！！因为是深度复制
print(r)    # 还存在，因为r指向了l2
del l2
print(r)    # 把l2也删除后，弱引用就死了
