# coding=utf-8

# @title: csv --- CSV 文件读写
# @desc: csv 模块实现了 CSV 格式表单数据的读写。其提供了诸如“以兼容 Excel 的方式输出数据文件”或“读取 Excel 程序输出的数据文件”的功能，
#         程序员无需知道 Excel 所采用 CSV 格式的细节。此模块同样可以用于定义其他应用程序可用的 CSV 格式或定义特定需求的 CSV 格式。
# @author: RyanLin
# @date: 2022/02/06


# csv.reader(csvfile, dialect='excel', **fmtparams)

# import csv
# with open('xinyongka.csv', newline='') as csvfile:
#     spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
#     for row in spamreader:
#         print(' \t '.join(row))



# import csv
# with open('eggs.csv', 'w', newline='') as csvfile:
#     spamwriter = csv.writer(csvfile, delimiter=' ',
#                             quotechar='|', quoting=csv.QUOTE_MINIMAL)
#     spamwriter.writerow(['Spam'] * 5 + ['Baked Beans'])
    # spamwriter.writerow(['Spam', 'Lovely Spam', 'Wonderful Spam'])
    
    
import csv
with open('xinyongka.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    i = 0
    for row in reader:
        print(reader.line_num, i, row['XINGMING'], row['SHENFENZHENGHAO'])
        i += 1