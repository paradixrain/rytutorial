# coding=utf-8

# @title: 正则表达式学些
# @author: Ryan Lin
# @date: 2022/1/15


import re
# 这是元字符的完整列表:
# . ^ $ * + ? { } [ ] \ | ( )
# 捕获组合 (...) (?…) (?aiLmsux) (?:…) (?aiLmsux-imsx:…) 
# 命名组合 (?P<name>…) (?P=name) (?#…) (?=…) (?!…) 
# 定长组合(?<=…) (?<!…) (?(id/name)yes-pattern|no-pattern)
# \number \A \b \B \d \D \s \S \w \W \Z

# 版本常量
# re.A
# re.ASCII
# re.DEBUG
# re.I
# re.IGNORECASE
# re.L
# re.LOCALE
# re.M
# re.MULTILINE
# re.S
# re.DOTALL
# re.X
# re.VERBOSE


# 函数
# re.compile(pattern, flags=0)
# re.search(pattern, string, flags=0)
# re.match(pattern, string, flags=0)
# re.fullmatch(pattern, string, flags=0)
# re.split(pattern, string, maxsplit=0, flags=0)
# re.split(r'\b', 'Words, words, words.')
# re.split(r'\W*', '...words...')
# re.split(r'(\W*)', '...words...')
# re.findall(pattern, string, flags=0)
# re.findall(r'\bf[a-z]*', 'which foot or hand fell fastest')
# re.findall(r'(\w+)=(\d+)', 'set width=20 and height=10')
# re.finditer(pattern, string, flags=0)
# re.sub(pattern, repl, string, count=0, flags=0)
# re.subn(pattern, repl, string, count=0, flags=0)
# re.escape(pattern)
# re.purge()

# def displaymatch(match):
#     if match is None:
#         return None
#     return '<Match: %r, groups=%r>' % (match.group(), match.groups())
# 
# pair = re.compile(r".*(.).*\1")
# print(displaymatch(pair.match("7a7ak72278aa8")))

# text = """Ross McFluff: 834.345.1254 155 Elm Street
# Ronald Heathmore: 892.345.3428 436 Finley Avenue
# Frank Burger: 925.541.7625 662 South Dogwood Way
# Heather Albrecht: 548.326.4584 919 Park Place"""
# 
# entries = re.split("\n+", text)
# print(entries)
# s=[re.split(":? ", entry, 3) for entry in entries]
# print(s)

# text = "He was carefully disguised but captured quickly by police."
# for m in re.finditer(r"\w+ly", text):
#     print('%02d-%02d: %s' % (m.start(), m.end(), m.group(0)))
# 
# # 原始字符标记
# re.match(r"\W(.)\1\W", " ff ")
# re.match("\\W(.)\\1\\W", " ff ")


data = """xgei-0/0/0/1    10G-10km-SFP+   1310nm -1.2/0-14.4,0.5]
-1.7/[-8.2,1.5] + Normal
xgei-0/0/0/2 10G-10km-SFP+       1310nm -40.0/[-8.2,1.5]
-40.0/[-14.4,0.5]   Abnormal
"""

pattern = r'xgei.*?ormal'
pattern1 = r'xgei(.*?)ormal'
# 使用pattern1 就无法匹配到xgei和末尾的ormal
result = [s.replace('\n', '\t') for s in re.findall(pattern, data, re.MULTILINE)]
result1 = [s.replace('\n', '\t') for s in re.findall(pattern1, data, re.DOTALL)]
# print('result', result)
# print('result1', result1)

data1 = "1\nstart\ntest1\ntest2\nend\n2"

reg1 = r"start.*end"
# reg2 = r"start(.*)end"
res1 = re.findall(reg1, data1, flags=re.S)
# print(res1)
# res2 = re.findall(pattern, data1, flags=re.DOTALL)
# print(res2)
res3 = re.findall(pattern, data, flags=re.DOTALL)
# print('res3', res3)


# content = '1,2,3,4,5,6'
# content = re.sub(r',(.+)$', '\t\g<1>', content)
# # sub(".*<(.*)>.*", "\g<1>", line)
# print(content)
# content = '1,2,3,4,5,6'
# # 替换最后一个逗号为制表符
# new_content = re.sub(r',(?=[^,]*$)', '\t', content)
# print(new_content)



# 从JSON字符串中提取特定值的函数
def extract_value_from_json(json_str, key):
    pattern = rf'"{key}":\s*"([^"]*)"'
    match = re.search(pattern, json_str)
    if match:
        return match.group(1)
    else:
        return None

# 示例JSON字符串
json_str = ('{"name": "Alice", "age": 30, "city": "New\n'
            'York"}')

# 提取特定键的值
key = "city"
value = extract_value_from_json(json_str, key)

print(f"The value for key '{key}' is: {value}")