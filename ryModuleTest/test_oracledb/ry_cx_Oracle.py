# coding=utf-8
# filename : oracle.py

import cx_Oracle

DEFAULT_MAIN_USER = "jxstanded2020"
DEFAULT_MAIN_USER_PWD = "jxstanded2020"
DEFAULT_CONNECT_STRING = "oracle1.test.baiwang-inner.com:1521/testdb1"


# 连接数据库
def connect_db(user=DEFAULT_MAIN_USER, password=DEFAULT_MAIN_USER_PWD, connect_string=DEFAULT_CONNECT_STRING):
    try:
        connect = cx_Oracle.connect(user, password, connect_string)
        return connect
    except cx_Oracle.DatabaseError as e:
        print(e)


# 查询数据
def query_db(connect, sql):
    try:
        with connect.cursor() as cursor:
            cursor.execute(sql)
            query_result = cursor.fetchall()
            return query_result
    except Exception as e:
        print(e)
        return None


# 插入数据
def insert_db(connect, sql, parameters):
    try:
        with connect.cursor() as cursor:
            cursor.executemany(sql, parameters)
            connect.commit()
        return "insert success"
    except Exception as e:
        print(e)
        return None


# 更新数据
def update_db(connect, sql):
    try:
        with connect.cursor() as cursor:
            cursor.execute(sql)
            connect.commit()
        return "update success"
    except Exception as e:
        print(e)
        return None


# 删除数据
def delete_db(connect, sql):
    try:
        with connect.cursor() as cursor:
            cursor.execute(sql)
            connect.commit()
        return "delete success"
    except Exception as e:
        print(e)
        return None


if __name__ == '__main__':
    with connect_db() as conn:
        # 初始化测试数据
        # 创建测试表
        sql = """create table ryan_table(id number, name varchar2(20))"""
        query_db(conn, sql)

        # 查询数据
        query_sql = """select 1 from dual"""
        result = query_db(conn, query_sql)
        print("query result:", result)

        # 插入数据
        data = [(101, "ryan101"),
                (102, "ryan102")]
        insert_sql = """insert into ryan_table values(:1, :2)"""
        result = insert_db(conn, insert_sql, data)
        print("insert result:", result)

        # 更新数据
        update_sql = """update ryan_table set name = 'update101' where id = 101"""
        result = update_db(conn, update_sql)
        print("update result:", result)

        # 删除数据
        delete_sql = """delete from ryan_table where id = 102"""
        result = delete_db(conn, delete_sql)
        print("delete result:", result)

    print("连接关闭")
