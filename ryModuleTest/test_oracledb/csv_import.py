# coding=utf-8


import oracledb
import csv


# 数据库环境
oracledb.init_oracle_client(lib_dir=r"D:\greenprogram\instantclient-basic-windows.x64-12.2.0.1.0\instantclient_12_2")
un = 'jxstanded2020'
pw = 'jxstanded2020'
cs = 'oracle1.test.baiwang-inner.com:1521/testdb1'


# 假定创建表已经完成
# create table test (id number, name varchar2(25));

# 数据在data.csv中

# Predefine the memory areas to match the table definition.
# This can improve performance by avoiding memory reallocations.
# Here, one parameter is passed for each of the columns.
# "None" is used for the ID column, since the size of NUMBER isn't
# variable.  The "25" matches the maximum expected data size for the
# NAME column

with oracledb.connect(user=un, password=pw, dsn=cs) as con:
    with con.cursor() as cursor:
        cursor.setinputsizes(None, 25)

        # Adjust the number of rows to be inserted in each iteration
        # to meet your memory and performance requirements
        batch_size = 10000

        with open('data.csv', 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            sql = "insert into test (id,name) values (:1, :2)"
            data = []
            for line in csv_reader:
                data.append((line[0], line[1]))
                if len(data) % batch_size == 0:
                    cursor.executemany(sql, data)
                    data = []
            if data:
                cursor.executemany(sql, data)
            con.commit()
