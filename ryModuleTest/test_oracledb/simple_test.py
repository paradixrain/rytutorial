# simple_test.py

import oracledb
import getpass

oracledb.init_oracle_client(lib_dir=r"D:\greenprogram\instantclient-basic-windows.x64-12.2.0.1.0\instantclient_12_2")

un = 'jxstanded2020'
pw = 'jxstanded2020'
cs = 'oracle1.test.baiwang-inner.com:1521/testdb1'

if pw is None:
    pw = getpass.getpass('Enter password: ')

with oracledb.connect(user=un, password=pw, dsn=cs) as connection:
    with connection.cursor() as cursor:
        # 试试查询，success
        # sql = """select * from tabs"""
        # result = cursor.execute(sql)
        # for r in result.description:
        #     print(r)
        # for r in result:
        #     print(r)

        # 试试DML，success
        # 判断ryan_table是否存在，如果表不存在，则创建表
        sql = """select count(*) from user_tables where table_name = 'RYAN_TABLE'"""
        result = cursor.execute(sql)
        for r in result:
            if r[0] == 0:
                sql = """create table RYAN_TABLE(id number, name varchar2(20))"""
                cursor.execute(sql)
                print("ryan_table created")

        # 往 ryan_table 里插入测试数据，success
        # sql = """insert into ryan_table values(1, 'ryan')"""
        # cursor.execute(sql)
        # sql = """insert into ryan_table values(2, 'ryan2')"""
        # cursor.execute(sql)

        # 使用变量绑定来插入数据，方法一，keyword方式，success
        # sql = """insert into ryan_table values(:id, :name)"""
        # cursor.execute(sql, id=3, name='ryan3')
        #
        # # 方法二，列表方式，success
        # sql = """insert into ryan_table values(:1, :2)"""
        # cursor.execute(sql, [4, 'ryan4'])

        # 插入多行的两种方式, success
        sql = """insert into ryan_table values(:1, :2)"""
        cursor.executemany(sql, [(5, 'ryan5'), (6, 'ryan6')])

        data = [(7, 'ryan7'), (8, 'ryan8')]
        cursor.executemany(sql, data)

        # 统计受影响的行数，success
        # 需要先打开开关，插入不统计行数，删除操作才统计行数
        data = [(9, 'ryan9'), (10, 'ryan10'), (11, 'ryan11')]
        cursor.executemany(sql, data, arraydmlrowcounts=True)
        row_counts = cursor.getarraydmlrowcounts()
        print('受影响行数', row_counts)

        # 查询 ryan_table，success
        sql = """select * from ryan_table"""
        result = cursor.execute(sql)
        for r in result:
            print(r)

        # 提交事务，success
        connection.commit()
        print("事务提交成功")

