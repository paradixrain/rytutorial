# coding=utf-8

# @title: numbers --- 数字的抽象基类
# @desc: numbers 模块 (PEP 3141) 定义了数字 抽象基类 的层级结构，其中逐级定义了更多操作。
#        此模块中定义的类型都不可被实例化
# @author: RyanLin
# @date: 2022/01/27

# 基类
# class numbers.Number
# 数字的层次结构的基础。 如果你只想确认参数 x 是不是数字而不关心其类型，则使用 isinstance(x, Number)

# 数字的层次
# class numbers.Complex
# class numbers.Real
# class numbers.Rational
# class numbers.Integral
