# coding=utf-8

# @title: types --- 动态类型创建和内置类型名称
# @desc: 此模块定义了一些工具函数，用于协助动态创建新的类型。
#        它还为某些对象类型定义了名称，这些名称由标准 Python 解释器所使用，但并不像内置的 int 或 str 那样对外公开。
# @author: RyanLin
# @date: 2022/01/22


# 动态类型创建
# types.new_class(name, bases=(), kwds=None, exec_body=None)
# types.prepare_class(name, bases=(), kwds=None)
# types.resolve_bases(bases)
# types.NoneType
# types.FunctionType
# types.LambdaType
# types.GeneratorType
# types.CoroutineType
# types.AsyncGeneratorType
# class types.CodeType(**kwargs)
# types.CellType
# types.MethodType
# types.BuiltinFunctionType
# types.BuiltinMethodType
# types.WrapperDescriptorType
# types.MethodWrapperType
# types.NotImplementedType
# types.MethodDescriptorType
# types.ClassMethodDescriptorType
# class types.ModuleType(name, doc=None)
# types.EllipsisType
# class types.GenericAlias(t_origin, t_args)
# >>> from types import GenericAlias
# types.UnionType
# class types.TracebackType(tb_next, tb_frame, tb_lasti, tb_lineno)
# types.FrameType
# types.GetSetDescriptorType
# types.MemberDescriptorType
# class types.MappingProxyType(mapping)
# class types.SimpleNamespace
# types.DynamicClassAttribute(fget=None, fset=None, fdel=None, doc=None)
# types.coroutine(gen_func)

import types
# 实例

# 正常情况下类的创建及实例化调用
class Test():
    name = "Test"
    def hello(self):
        print("hello worl")
t = Test()
t.hello()
print(t.name)

# 使用type()动态创建类
def hello(self):
    self.name = 10
    print('hello world')
    
t = type("hello", (), {"a":1, "hello":hello})
print(t)
T = t()
print(T.a)
print(T.hello)
T.hello()
print(T.name)


# 使用types.newclass() 创建动态类

# 为什么就是不成功呢？？ new_class和type参数怎么就不一样呢？ types 的 121行写 是meta = type
t1 = types.new_class("hel", (),{} )
# t1 = types.new_class("hello",())
# t1 = types.new_class("hello",(),{"ab":2})               
print(t1)
# T1 = t1()
# print(T1)
# print(T1.name)

a = {"a":1, "b":2}
b = dict(a)
print(b)
