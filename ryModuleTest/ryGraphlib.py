# coding=utf-8

# @title: graphlib --- 操作类似图的结构的功能
# @desc: 提供以拓扑方式对可哈希节点的图进行排序的功能
# @author: RyanLin
# @date: 2022/01/25

# class graphlib.TopologicalSorter(graph=None)

# 方法
# add(node, *predecessors)
# prepare()
# is_active()
# prepare()
# is_active()
# done(*nodes)
# get_ready()
# static_order()

# 拓扑排序是指图中顶点的线性排序，使得对于每条从顶点 u 到顶点 v 的有向边 u -> v，顶点 u 都排在顶点 v 之前。
# 例如，图的顶点可以代表要执行的任务，而边代表某一个任务必须在另一个任务之前执行的约束条件；
# 在这个例子中，拓扑排序只是任务的有效序列。 完全拓扑排序 当且仅当图不包含有向环，也就是说为有向无环图时，完全拓扑排序才是可能的。
# 如果提供了可选的 graph 参数则它必须为一个表示有向无环图的字典，
# 其中的键为节点而值为包含图中该节点的所有上级节点（即具有指向键中的值的边的节点）的可迭代对象。 
# 额外的节点可以使用 add() 方法添加到图中。
#
# 在通常情况下，对给定的图执行排序所需的步骤如下:
# 通过可选的初始图创建一个 TopologicalSorter 的实例。
# 添加额外的节点到图中。
# 在图上调用 prepare()。
# 当 is_active() 为 True 时，迭代 get_ready() 所返回的节点并加以处理。 完成处理后在每个节点上调用 done()。

from graphlib import TopologicalSorter
# 在只需要对图中的节点进行立即排序并且不涉及并行性的情况下，可以直接使用便捷方法 TopologicalSorter.static_order():
graph = {"D": {"B", "C"}, "C": {"A"}, "B": {"A"}}
ts = TopologicalSorter(graph)
print(tuple(ts.static_order()))
# ('A', 'C', 'B', 'D')


# 从 DAG 图中选择一个 没有前驱（即入度为0）的顶点并输出。
# 从图中删除该顶点和所有以它为起点的有向边。
# 重复 1 和 2 直到当前的 DAG 图为空或当前图中不存在无前驱的顶点为止。后一种情况说明有向图中必然存在环。


ts = TopologicalSorter()
ts.add(3, 2, 1)
ts.add(1, 0)
print([*ts.static_order()])
# [2, 0, 1, 3]

ts2 = TopologicalSorter()
ts2.add(1, 0)
ts2.add(3, 2, 1)
print([*ts2.static_order()])
# [0, 2, 1, 3]