# coding=utf-8

# @title: copyreg --- 注册配合 pickle 模块使用的函数
# @desc: copyreg 模块提供了可在封存特定对象时使用的一种定义函数方式。 
# pickle 和 copy 模块会在封存/拷贝特定对象时使用这些函数。 
# 此模块提供了非类对象构造器的相关配置信息。 这样的构造器可以是工厂函数或类实例。
# @author: RyanLin
# @date: 2022/02/02


import copyreg, copy, pickle
class C:
    def __init__(self, a):
        self.a = a

def pickle_c(c):
    print("pickling a C instance...")
    return C, (c.a,)

copyreg.pickle(C, pickle_c)
c = C(1)
d = copy.copy(c)  
# pickling a C instance...
p = pickle.dumps(c)  
# pickling a C instance...