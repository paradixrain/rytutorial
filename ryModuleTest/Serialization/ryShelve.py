# coding=utf-8

# @title: shelve --- Python 对象持久化
# @desc: "Shelf" 是一种持久化的类似字典的对象。 
# 与 "dbm" 数据库的区别在于 Shelf 中的值（不是键！）实际上可以为任意 
# Python 对象 --- 即 pickle 模块能够处理的任何东西。 
# 这包括大部分类实例、递归数据类型，以及包含大量共享子对象的对象。 键则为普通的字符串。
# @author: RyanLin
# @date: 2022/02/02


import shelve

d = shelve.open(filename)  # open -- file may get suffix added by low-level
                           # library

d[key] = data              # store data at key (overwrites old data if
                           # using an existing key)
data = d[key]              # retrieve a COPY of data at key (raise KeyError
                           # if no such key)
del d[key]                 # delete data stored at key (raises KeyError
                           # if no such key)

flag = key in d            # true if the key exists
klist = list(d.keys())     # a list of all existing keys (slow!)

# as d was opened WITHOUT writeback=True, beware:
d['xx'] = [0, 1, 2]        # this works as expected, but...
d['xx'].append(3)          # *this doesn't!* -- d['xx'] is STILL [0, 1, 2]!

# having opened d without writeback=True, you need to code carefully:
temp = d['xx']             # extracts the copy
temp.append(5)             # mutates the copy
d['xx'] = temp             # stores the copy right back, to persist it

# or, d=shelve.open(filename,writeback=True) would let you just code
# d['xx'].append(5) and have it work as expected, BUT it would also
# consume more memory and make the d.close() operation slower.

d.close()                  # close it
# pickling a C instance...
p = pickle.dumps(c)  
# pickling a C instance...