# coding=utf-8

# @title: 测试pandas
# @desc:  
# @author: RyanLin
# @date: 2022/02/15


import pandas as pd
csvIn = "../../book/foundations-for-analytics-with-python-master/csv/supplier_data.csv"
# print(csvIn)
csvOut = "pandasOut.csv"
# myColumnIndex = [0, 3]
# myColumnName = ["Invoice Number", "Purchase Date"]


# 用索引列
# data_frame = pd.read_csv(csvIn)
# data_frame_column_by_index = data_frame.iloc[:, myColumnIndex]
# data_frame_column_by_index.to_csv(csvOut, index=False)

# 用列名
data_frame = pd.read_csv(csvIn)
# data_frame_column_by_name = data_frame.loc[:,myColumnName]
# data_frame_column_by_name.to_csv(csvOut, index = False)
# print(data_frame)

df1 = data_frame["Supplier Name"].unique()
print('df1', df1)

df2 = pd.Series(df1)
print('df2', df2)

# df2.to_csv(csvOut)


# 尝试只要头不要数据
df3 = df1["Supplier Name" == "Supplier X"]
print('df3', df3)


# 现在是这样实现的
# df3 = data_frame.iloc[0:2]
# print(df3)
#
#
# print(list(df3))
# print('head', df3.head())
# print('columns', df3.columns)
# print('keys', df3.keys())



# 只要列标题不要数据。
df = pd.DataFrame(columns = data_frame.columns)
# print('df', df)


# 读取excel中所有sheet，拼成一个df
df_list = pd.read_excel(r'清单.xlsx', sheet_name=None)

# 将所有sheet页的数据拼接为一个dataframe
df = pd.concat(df_list, ignore_index=True)

# 输出拼接后的dataframe
print(df)
