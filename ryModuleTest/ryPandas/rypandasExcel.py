# coding=utf-8

# @title: 测试pandas读取多数据块的excel
# @desc:  顺便把晓阳的excel处理问题变成pandas解决，根据就近就高原则，根据薪级输出对应岗级
# @author: RyanLin
# @date: 2022/03/02

# 测试发现读取多block，以及有合并单元格的excel的时候，data_frame会有问题，会有值的缺失

# undone TODO 模块要学习

import pandas as pd

def main():
    excelFile = '薪点表设计-1.5-扩档，工程序列，营销序列，修订岗位矩阵 - 副本(1).xlsx'
    sheetName1 = '1-薪点表-非业务类'
    sheetName2 = '3-套算'
    
    data_frame1 = pd.read_excel(excelFile, sheet_name=sheetName1,skiprows=32,nrows=12,index_col=0, usecols="B:Q")
    data_frame2 = pd.read_excel(excelFile, sheet_name=sheetName2,skiprows=1,nrows=126,index_col=0, usecols="A:I")
    
    # 输出原始dataFrame数据, 薪点表以及套算表
    # print(data_frame1)
    # print(data_frame2)
    
    levelSeries = '1档 2档 3档 4档 5档 6档 7档 8档 9档 10档 11档 12档 13档 14档 15档'.split()
    level = pd.Series(levelSeries)
    # 薪点表档位
    # print('levelSeries:')
    # print(level)
    
    # 遍历data_frame2中的每一行，去data_frame1中找对应职级的档位薪资，就近就高
    # print('岗级')
    # print(data_frame2.iloc[:, [0,6,7]])
    
    try:
        trxd_series=[]  # 新增一列，套入薪档
        for index, row in data_frame2.iterrows():
            # print('index, row')
            # print(index, row)
            xz = row['标准月薪\n（不含补贴）']
            trxj = str(row['套入薪级']) + '级'
            # print('TRXJ:', trxj)
            df = data_frame1.loc[trxj]
            
            print('trxj:', trxj, 'xz:', xz, 'df:')
            # print(df)
            
            ## TODO. 这里还未完成，定位有问题，最好抽出去做单独函数，返回对应位置，get it
            for i in range(14, -1, -1):
                if df.iloc[i] <= xz:
                    trxd = str(i+1) + '档'
                    trxd_series.append(trxd)
                    print('在当前', index, '位置插入:', trxd)
                    break
                else:
                    print('i:', i, '当前值：', df.iloc[i], 'pass')
                if i == 0:
                    # 如果遍历到第1档还没找到，这按第一档计算
                    trxd = str(i+1) + '档'
                    trxd_series.append(trxd)
                    print('在当前', index, '位置插入:', trxd)
                    
        print('trxd_series:')
        for i, value in enumerate(trxd_series, 1):
            print(i, value)
        
        # 追加列
        data_frame2.insert(len(data_frame2.columns), '套入薪档' ,trxd_series )
        # 查看最后的结果
        print(data_frame2)
        
        # 导出到excel
        data_frame2.to_excel('套入薪档.xls')
    except Exception as e:
        print(e)
    # 把找到的数据插入到 data_frame2的最后，回写对应的“档位”
    # 将data_frame2的结果导出成excel

if __name__ == '__main__':
    main()