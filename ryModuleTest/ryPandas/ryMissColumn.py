# coding=utf-8

# @title: 处理列缺失的场景，假设第一行的字段是完整的
# @desc:  读取第一行title，第二行为参考行，以第二行每列的数据类型为标准，只考虑缺失字段的场景，自动往后延
# @author: RyanLin
# @date: 2022/12/28


import pandas as pd
txtIn = "./misscolumn.txt"
print(txtIn)
failedOut = "./failedOut.txt"


# 用列名
data_frame = pd.read_csv(txtIn, delimiter='\t', nrows=1)
print(data_frame, data_frame.dtypes)

data_frame1 = pd.read_csv(txtIn, delimiter='\t')
print(data_frame1, data_frame1.dtypes)
