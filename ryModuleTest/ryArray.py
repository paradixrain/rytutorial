# coding=utf-8

# @title: array --- 高效的数值数组
# @desc: 此模块定义了一种对象类型，可以紧凑地表示基本类型值的数组：字符、整数、浮点数等。
#        数组属于序列类型，其行为与列表非常相似，不同之处在于其中存储的对象类型是受限的。 
# @author: RyanLin
# @date: 2022/01/20

# 已定义的类型码如下：
# 类型码    'b'    'B'    'u'    'h'    'H'    'i'    'I'    'l'    'L'    'q'    'Q'    'f'    'd'

# # 定义
# class array.array(typecode[, initializer])      # 一个包含由 typecode 限制类型的条目的新数组，并由可选的 initializer 值进行初始化
# 
# # 属性和方法
# array.typecodes
# array.typecode
# array.itemsize
# array.append(x)
# array.buffer_info()
# array.byteswap()
# array.count(x)
# array.extend(iterable)
# array.frombytes(s)
# array.fromfile(f, n)
# array.fromlist(list)
# array.fromunicode(s)
# array.index(x[, start[, stop]])
# array.insert(i, x)
# array.pop([i])
# array.remove(x)
# array.reverse()
# array.tobytes()
# array.tofile(f)
# array.tolist()

import array
# from array import array
print(array.typecodes)

l = [1 ,2, 3, 'L']
arr = array.array('b')
# a = arr.fromlist(l)
a = array.array('u', 'hello \u2641')
# a = array.array('l', [1, 2, 3, 4, 5])
# a = array.array('d', [1.0, 2.0, 3.14])
for i in a:
    print(i)

us = '\u2641\u2631\u3321'
print(us)