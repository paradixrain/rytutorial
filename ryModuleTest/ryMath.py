# coding=utf-8

# @title: math --- 数学函数
# @desc: 该模块提供了对C标准定义的数学函数的访问
# @author: RyanLin
# @date: 2022/01/27
import math
# 数论与表示函数
# math.ceil(x)
# math.comb(n, k)
# math.copysign(x, y)
# math.fabs(x)
# math.factorial(x)
# math.floor(x)
# math.fmod(x, y)
# math.frexp(x)
# math.fsum(iterable)
# math.gcd(*integers)
# math.isclose(a, b, *, rel_tol=1e-09, abs_tol=0.0)
# math.isfinite(x)
# math.isinf(x)
# math.isnan(x)
# math.isqrt(n)
# math.lcm(*integers)
# math.ldexp(x, i)
# math.modf(x)
# math.nextafter(x, y)
# math.nextafter(x, math.inf) 的方向朝上：趋向于正无穷。
# math.nextafter(x, -math.inf) 的方向朝下：趋向于负无穷。
# math.nextafter(x, 0.0) 趋向于零。
# math.nextafter(x, math.copysign(math.inf, x)) 趋向于零的反方向。
# math.perm(n, k=None)
# print(math.perm(4,2))
# print(math.prod([1,2,3,4], start=1))
# math.prod(iterable, *, start=1)
# math.remainder(x, y)
# math.trunc(x)
# math.ulp(x)

# 幂函数与对数函数
# math.exp(x)
# math.expm1(x)
# math.log(x[, base])
# math.log1p(x)
# math.log2(x)
# math.log10(x)
# math.pow(x, y)
# math.sqrt(x)

# 三角函数
# math.acos(x)
# math.asin(x)
# math.atan(x)
# math.atan2(y, x)
# math.cos(x)
# math.dist(p, q)
# math.hypot(*coordinates)
# math.sin(x)
# math.tan(x)

# 角度转换
# math.degrees(x)
# math.radians(x)

# 双曲函数
# math.acosh(x)
# math.asinh(x)
# math.atanh(x)
# math.cosh(x)
# math.sinh(x)
# math.tanh(x)

# 特殊函数
# math.erf(x)
# math.erfc(x)
# math.gamma(x)
# math.lgamma(x)

# 常量
# math.pi
# math.e
# math.tau
# math.inf
# math.nan
