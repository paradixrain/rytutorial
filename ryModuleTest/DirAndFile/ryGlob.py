# coding=utf-8

# @title: glob --- Unix 风格路径名模式扩展
# @desc: glob 模块可根据 Unix 终端所用规则找出所有匹配特定模式的路径名，但会按不确定的顺序返回结果。
#    波浪号扩展不会生效，但 *, ? 以及表示为 [] 的字符范围将被正确地匹配。 
# @author: RyanLin
# @date: 2022/02/02

import glob
glob.glob('./[0-9].*')
['./1.gif', './2.txt']
glob.glob('*.gif')
['1.gif', 'card.gif']
glob.glob('?.gif')
['1.gif']
glob.glob('**/*.txt', recursive=True)
['2.txt', 'sub/3.txt']
glob.glob('./**/', recursive=True)
['./', './sub/']
#如果目录包含以 . 打头的文件，它们默认将不会被匹配。 例如，考虑一个包含 card.gif 和 .card.gif 的目录:


#import glob
glob.glob('*.gif')
['card.gif']
glob.glob('.c*')
['.card.gif']