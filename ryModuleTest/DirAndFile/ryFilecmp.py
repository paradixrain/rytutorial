# coding=utf-8

# @title: filecmp --- 文件及目录的比较
# @desc: filecmp 模块定义了用于比较文件及目录的函数，并且可以选取多种关于时间和准确性的折衷方案。
# @author: RyanLin
# @date: 2022/01/31

# 下面是一个简单的例子，使用 subdirs 属性递归搜索两个目录以显示公共差异文件：

from filecmp import dircmp
def print_diff_files(dcmp):
    for name in dcmp.diff_files:
        print("diff_file %s found in %s and %s" % (name, dcmp.left,
              dcmp.right))
    for sub_dcmp in dcmp.subdirs.values():
        print_diff_files(sub_dcmp)

dcmp = dircmp("E:\\greenprograms\\UltraISO", "E:\\greenprograms\\UltraISOv9.2.0.2536") 
print_diff_files(dcmp) 
print("report:")
print(dcmp.report())
print("report2:")
print(dcmp.report_full_closure())