# coding=utf-8

# @title: pathlib --- 面向对象的文件系统路径
# @desc: 该模块提供表示文件系统路径的类，其语义适用于不同的操作系统。
#        路径类被分为提供纯计算操作而没有 I/O 的 纯路径，以及从纯路径继承而来但提供 I/O 操作的 具体路径。
# @author: RyanLin
# @date: 2022/01/31

from pathlib import Path

p = Path('E:\eclipse')
a = [x for x in p.iterdir() if x.is_dir()]
print(a)

b = list(p.glob('**/*.py'))
print(b)

# 方法
PurePath.drive
PurePath.root
PurePath.anchor
PurePath.parents
PurePath.parent
PurePath.name
PurePath.suffix
PurePath.suffixes
PurePath.stem
PurePath.as_posix()
PurePath.as_uri()
PurePath.is_absolute()
PurePath.is_relative_to(*other)
PurePath.is_reserved()
PurePath.joinpath(*other)
PurePath.match(pattern)
PurePath.relative_to(*other)
PurePath.with_name(name)
PurePath.with_stem(stem)
PurePath.with_suffix(suffix)


# 具体路径
# 具体路径是纯路径的子类。除了后者提供的操作之外，它们还提供了对路径对象进行系统调用的方法

# class pathlib.Path(*pathsegments)
# class pathlib.PosixPath(*pathsegments)
# class pathlib.WindowsPath(*pathsegments)

# 实体路径
# 方法
# classmethod Path.cwd()
# classmethod Path.home()
# Path.stat(*, follow_symlinks=True)
# Path.chmod(mode, *, follow_symlinks=True)
# Path.exists()
# Path.expanduser()
Path.glob(pattern)
Path.group()
Path.is_dir()
Path.is_file()
Path.is_mount()
Path.is_symlink()
Path.is_socket()
Path.is_fifo()
Path.is_block_device()
Path.is_char_device()
Path.iterdir()
Path.lchmod(mode)
Path.lstat()
Path.mkdir(mode=511, parents=False, exist_ok=False)
Path.open(mode='r', buffering=- 1, encoding=None, errors=None, newline=None)
Path.owner()
Path.read_bytes()
Path.read_text(encoding=None, errors=None)
Path.readlink()
Path.rename(target)
Path.replace(target)
Path.resolve(strict=False)
Path.rglob(pattern)
Path.rmdir()
Path.samefile(other_path)
Path.symlink_to(target, target_is_directory=False)
Path.hardlink_to(target)
Path.link_to(target)
Path.touch(mode=438, exist_ok=True)
Path.unlink(missing_ok=False)
Path.write_bytes(data)
