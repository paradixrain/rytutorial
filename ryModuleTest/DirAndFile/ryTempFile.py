# coding=utf-8

# @title: tempfile --- 生成临时文件和目录
# @desc: 该模块用于创建临时文件和目录，它可以跨平台使用。
# TemporaryFile、NamedTemporaryFile、TemporaryDirectory 和 SpooledTemporaryFile 是带有自动清理功能的高级接口，可用作上下文管理器。
# mkstemp() 和 mkdtemp() 是低级函数，使用完毕需手动清理。
# @author: RyanLin
# @date: 2022/02/02

import tempfile

# create a temporary file and write some data to it
fp = tempfile.TemporaryFile()
fp.write(b'Hello world!')
# read data from file
fp.seek(0)
fp.read()
b'Hello world!'
# close the file, it will be removed
fp.close()

# create a temporary file using a context manager
with tempfile.TemporaryFile() as fp:
    fp.write(b'Hello world!')
    fp.seek(0)
    fp.read()
b'Hello world!'

# file is now closed and removed

# create a temporary directory using the context manager
with tempfile.TemporaryDirectory() as tmpdirname:
    print('created temporary directory', tmpdirname)

# directory and contents have been removed