# coding=utf-8

# @title: os.path --- 常用路径操作
# @desc: 该模块在路径名上实现了一些有用的功能：
#        如需读取或写入文件，请参见 open() ；有关访问文件系统的信息，请参见 os 模块。
# @author: RyanLin
# @date: 2022/01/31

import os
pathE = 'E:\eclipse'
pathE = 'E:\eclipse\gs-uploading-files-complete.zip'
# os.path.abspath(path)
# os.path.basename(path)
# # os.path.commonpath(paths)
# os.path.commonprefix(list)
# os.path.dirname(path)
# os.path.exists(path)
# os.path.lexists(path)
# os.path.expanduser(path)
# os.path.expandvars(path)
# os.path.getatime(path)
# os.path.getmtime(path)
# os.path.getctime(path)
# os.path.getsize(path)
a = os.path.getsize(pathE)
print(a)
# os.path.isabs(path)
# os.path.isfile(path)
# os.path.isdir(path)
# os.path.islink(path)
# os.path.ismount(path)
# os.path.join(path, *paths)
# os.path.normcase(path)
# os.path.normpath(path)
# # os.path.realpath(path, *, strict=False)
# os.path.relpath(path, start=os.curdir)
# os.path.samefile(path1, path2)
# os.path.sameopenfile(fp1, fp2)
# os.path.samestat(stat1, stat2)
# os.path.split(path)
# os.path.splitdrive(path)
# os.path.splitext(path)
