# coding=utf-8

# @title: fnmatch --- Unix 文件名模式匹配
# @desc: 此模块提供了 Unix shell 风格的通配符，它们 并不 等同于正则表达式（关于后者的文档参见 re 模块）
# @author: RyanLin
# @date: 2022/02/02

import fnmatch
import os

for file in os.listdir('.'):
    if fnmatch.fnmatch(file, '*.txt'):
        print(file)



import fnmatch, re

regex = fnmatch.translate('*.txt')
regex
'(?s:.*\\.txt)\\Z'
reobj = re.compile(regex)
reobj.match('foobar.txt')
# <re.Match object; span=(0, 10), match='foobar.txt'>