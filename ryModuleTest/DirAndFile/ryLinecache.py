# coding=utf-8

# @title: linecache --- 随机读写文本行
# @desc: linecache 模块允许从一个 Python 源文件中获取任意的行，
# 并会尝试使用缓存进行内部优化，常应用于从单个文件读取多行的场合。
# 此模块被 traceback 模块用来提取源码行以便包含在格式化的回溯中。
# @author: RyanLin
# @date: 2022/02/02

import linecache
linecache.getline(linecache.__file__, 8)
'import sys\n'