# coding=utf-8

# @title: shutil --- 高阶文件操作
# @desc: shutil 模块提供了一系列对文件和文件集合的高阶操作。 
# 特别是提供了一些支持文件拷贝和删除的函数。 对于单个文件的操作，请参阅 os 模块。
# @author: RyanLin
# @date: 2022/02/02

'''
警告 即便是高阶文件拷贝函数 (shutil.copy(), shutil.copy2()) 也无法拷贝所有的文件元数据。
在 POSIX 平台上，这意味着将丢失文件所有者和组以及 ACL 数据。 
在 Mac OS 上，资源钩子和其他元数据不被使用。 这意味着将丢失这些资源并且文件类型和创建者代码将不正确。 
在 Windows 上，将不会拷贝文件所有者、ACL 和替代数据流。
'''

# 这个示例就是上面所描述的 copytree() 函数的实现，其中省略了文档字符串。 它还展示了此模块所提供的许多其他函数。

def copytree(src, dst, symlinks=False):
    names = os.listdir(src)
    os.makedirs(dst)
    errors = []
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if symlinks and os.path.islink(srcname):
                linkto = os.readlink(srcname)
                os.symlink(linkto, dstname)
            elif os.path.isdir(srcname):
                copytree(srcname, dstname, symlinks)
            else:
                copy2(srcname, dstname)
            # XXX What about devices, sockets etc.?
        except OSError as why:
            errors.append((srcname, dstname, str(why)))
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        except Error as err:
            errors.extend(err.args[0])
    try:
        copystat(src, dst)
    except OSError as why:
        # can't copy file access times on Windows
        if why.winerror is None:
            errors.extend((src, dst, str(why)))
    if errors:
        raise Error(errors)
    
    
# 这个例子演示了如何在 Windows 上删除一个目录树，其中部分文件设置了只读属性位。 
# 它会使用 onerror 回调函数来清除只读属性位并再次尝试删除。 任何后续的失败都将被传播。

import os, stat
import shutil

def remove_readonly(func, path, _):
    "Clear the readonly bit and reattempt the removal"
    os.chmod(path, stat.S_IWRITE)
    func(path)

shutil.rmtree(directory, onerror=remove_readonly)


# 在这个示例中，我们创建了一个 gzip 压缩的 tar 归档文件，其中包含用户的 .ssh 目录下的所有文件:

from shutil import make_archive
import os
archive_name = os.path.expanduser(os.path.join('~', 'myarchive'))
root_dir = os.path.expanduser(os.path.join('~', '.ssh'))
make_archive(archive_name, 'gztar', root_dir)
'/Users/tarek/myarchive.tar.gz'