# coding=utf-8

# @title: collections
# @desc: 这个模块实现了特定目标的容器，以提供Python标准内建容器 dict , list , set , 和 tuple 的替代选择
# @author: RyanLin
# @date: 2022/01/16

# namedtuple()    创建命名元组子类的工厂函数
# deque           类似列表(list)的容器，实现了在两端快速添加(append)和弹出(pop)
# ChainMap        类似字典(dict)的容器类，将多个映射集合到一个视图里面
# Counter         字典的子类，提供了可哈希对象的计数功能
# OrderedDict     字典的子类，保存了他们被添加的顺序
# defaultdict     字典的子类，提供了一个工厂函数，为字典查询提供一个默认值
# UserDict        封装了字典对象，简化了字典子类化
# UserList        封装了列表对象，简化了列表子类化
# UserString      封装了字符串对象，简化了字符串子类化

# # ChainMap 对象
# # chainmap 一个 ChainMap 类是为了将多个映射快速的链接到一起，这样它们就可以作为一个单元处理。它通常比创建一个新字典和多次调用 update() 要快很多
# ChainMap 类只更新链中的第一个映射，但lookup会搜索整个链
# class collections.ChainMap(*maps)
# # 属性（attribute）
# maps
# # 方法
# new_child(m=None, **kwargs)
# # 属性（property）
# parents

# from collections import ChainMap
# baseline = {'music': 'bach', 'art': 'rembrandt'}
# adjustments = {'art': 'van gogh', 'opera': 'carmen'}
# cp = ChainMap(adjustments, baseline)
# print(cp)
# adjustments['art']='Mozart'
# print(cp)
# cp['art']='Darwin'
# print(cp)
# print(cp['art'])

# c = ChainMap()
# d = c.new_child()
# e = c.new_child()
# e.maps[0]
# e.maps[-1]
# e.parents

# Counter 对象
# class collections.Counter([iterable-or-mapping])
# 一个 Counter 是一个 dict 的子类，用于计数可哈希对象, 
# 作为 dict 的子类，Counter 继承了记住插入顺序的功能。 Counter 对象进行数学运算时同样会保持顺序。 结果会先按每个元素在运算符左边的出现时间排序，然后再按其在运算符右边的出现时间排序

# 方法
# elements()
# most_common([n])
# subtract([iterable-or-mapping])
# total()

from collections import Counter
# cnt = Counter()
# for word in ['red', 'blue', 'red', 'green', 'blue', 'blue']:
#     cnt[word] += 1
# print(cnt)

# c = Counter()                           # a new, empty counter
# c = Counter('gallahad')                 # a new counter from an iterable
# c = Counter({'red': 4, 'blue': 2})      # a new counter from a mapping
# c = Counter(cats=2, dogs=3)             # a new counter from keyword args
# 
# c['cats'] -= 1
# 
# for i in c.elements():
#     print(i)

# # deque 对象
# class collections.deque([iterable[, maxlen]])
# # 方法
# append(x)       添加 x 到右端。
# appendleft(x)   添加 x 到左端。
# clear()         移除所有元素，使其长度为0.
# copy()          创建一份浅拷贝。
# count(x)        计算 deque 中元素等于 x 的个数。
# extend(iterable)    扩展deque的右侧，通过添加iterable参数中的元素。
# extendleft(iterable)    扩展deque的左侧，通过添加iterable参数中的元素。注意，左添加时，在结果中iterable参数中的顺序将被反过来添加。
# index(x[, start[, stop]])   返回 x 在 deque 中的位置（在索引 start 之后，索引 stop 之前）。 返回第一个匹配项，如果未找到则引发 ValueError。
# insert(i, x)    在位置 i 插入 x 。
# pop()           移去并且返回一个元素，deque 最右侧的那一个。 如果没有元素的话，就引发一个 IndexError。
# popleft()       移去并且返回一个元素，deque 最左侧的那一个。 如果没有元素的话，就引发 IndexError。
# remove(value)   移除找到的第一个 value。 如果没有的话就引发 ValueError。
# reverse()       将deque逆序排列。返回 None 。
# rotate(n=1)     向右循环移动 n 步。 如果 n 是负数，就向左循环。
# # 属性（只读）
# maxlen

# 限长deque提供了类似Unix tail 过滤功能
# def tail(filename, n=10):
#     'Return the last n lines of a file'
#     with open(filename) as f:
#         return deque(f, n)
# # 轮询调度器 可以通过在 deque 中放入迭代器来实现
# def roundrobin(*iterables):
#     "roundrobin('ABC', 'D', 'EF') --> A D E B F C"
#     iterators = deque(map(iter, iterables))
#     while iterators:
#         try:
#             while True:
#                 yield next(iterators[0])
#                 iterators.rotate(-1)
#         except StopIteration:
#             # Remove an exhausted iterator.
#             iterators.popleft()

# defaultdict 对象
# 使用 list 作为 default_factory，很轻松地将（键-值对组成的）序列转换为（键-列表组成的）字典
# class collections.defaultdict(default_factory=None, /[, ...])
# 返回一个新的类似字典的对象。 defaultdict 是内置 dict 类的子类。 它重载了一个方法并添加了一个可写的实例变量。

# from collections import defaultdict
# s = [('yellow', 1), ('blue', 2), ('yellow', 3), ('blue', 4), ('red', 1)]
# d = defaultdict(list)
# for k, v in s:
#     d[k].append(v)
# 
# print(sorted(d.items()))


# namedtuple() 命名元组的工厂函数
# 命名元组赋予每个位置一个含义，提供可读性和自文档性。它们可以用于任何普通元组，并添加了通过名字获取值的能力，通过索引值也是可以的。
# 
# collections.namedtuple(typename, field_names, *, rename=False, defaults=None, module=None)

from collections import namedtuple
Point = namedtuple('Point', ['x', 'y'])
# p = Point(11, y=22)     # instantiate with positional or keyword arguments
# print(p[0] + p[1])             # indexable like the plain tuple (11, 22)
# # 33
# x, y = p                # unpack like a regular tuple
# print(x, y)
# # (11, 22)
# print(p.x + p.y)               # fields also accessible by name
# # 33
# print(p)                       # readable __repr__ with a name=value style
# # Point(x=11, y=22)

# 方法
# classmethod somenamedtuple._make(iterable)
# somenamedtuple._asdict()
# somenamedtuple._replace(**kwargs)
# # 属性
# somenamedtuple._fields
# somenamedtuple._field_defaults
# 
# # 命名元组尤其有用于赋值 csv sqlite3 模块返回的元组
# EmployeeRecord = namedtuple('EmployeeRecord', 'name, age, title, department, paygrade')
# 
# import csv
# for emp in map(EmployeeRecord._make, csv.reader(open("employees.csv", "rb"))):
#     print(emp.name, emp.title)
# 
# import sqlite3
# conn = sqlite3.connect('/companydata')
# cursor = conn.cursor()
# cursor.execute('SELECT name, age, title, department, paygrade FROM employees')
# for emp in map(EmployeeRecord._make, cursor.fetchall()):
#     print(emp.name, emp.title)
#     

# 将字典转化为命名元祖
# d = {'x': 11, 'y': 22}
# print(Point(**d))
# 
# # 子类化对于添加和存储新的名字域是无效的。应当通过 _fields 创建一个新的命名元组来实现它:
# Point3D = namedtuple('Point3D', Point._fields + ('z',))


# OrderedDict 对象，有序字典
# class collections.OrderedDict([items])      返回一个 dict 子类的实例，它具有专门用于重新排列字典顺序的方法。

# 功能
# popitem(last=True)
# move_to_end(key, last=True)


# UserDict 对象
# UserDict 类是用作字典对象的外包装。对这个类的需求已部分由直接创建 dict 的子类的功能所替代；不过，这个类处理起来更容易，因为底层的字典可以作为属性来访问。
# class collections.UserDict([initialdata])

# UserList 对象
# 这个类封装了列表对象。它是一个有用的基础类，对于你想自定义的类似列表的类，可以继承和覆盖现有的方法，也可以添加新的方法。这样我们可以对列表添加新的行为。
# class collections.UserList([list])

# UserString 对象
# UserString 类是用作字符串对象的外包装。对这个类的需求已部分由直接创建 str 的子类的功能所替代；不过，这个类处理起来更容易，因为底层的字符串可以作为属性来访问。
# class collections.UserString(seq)


# 为什么要存在UserDict, UserList UserString？
# 后来在PyPy的文档中发现了原因，也就是这种C实现的结构的内建方法大部分会忽略重载的那个方法。
#链接为：https://doc.pypy.org/en/latest/cpython_differences.html#subclasses-of-built-in-types
# 之前我以为UserDict这样的类是历史遗留问题，现在才知道是有原因的。原来UserDict、UserString、UserList这样的模块是非常必要的
# 例子：
# class NewDict(dict):
#     def __getitem__(self, key):
#         return 42
# 
# d = NewDict(a=1)
# print(d)
# # {'a': 42}
# d2={}
# d2.update(d)
# print(d2)
# #{'a': 1}        输出与期望不符~是因为重载的方法被忽略了，改成class NewDict(UserDict) 就与期望一致了