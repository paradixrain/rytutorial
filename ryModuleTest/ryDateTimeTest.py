# coding='utf-8'
import datetime

# 日期和时间对象可以根据它们是否包含时区信息而分为“感知型”和“简单型”两类。 时区信息根据tzone里设置

# # 常量
# datetime.MINYEAR
# datetime.MAXYEAR
# 
# # datetime 类型
# class datetime.date
# class datetime.time
# class datetime.datetime
# class datetime.timedelta(days=0, seconds=0, microseconds=0, milliseconds=0, minutes=0, hours=0, weeks=0)
# class datetime.tzinfo
# class datetime.timezone

# timedlata 对象
# from datetime import timedelta
# year = timedelta(days=365)
# another_year = timedelta(weeks=40, days=84, hours=23,minutes=50, seconds=600)
# print(year == another_year)
# print(year.total_seconds())
# # 31536000.0

# date 对象
# classmethod date.today()
# classmethod date.fromtimestamp(timestamp)
# classmethod date.fromordinal(ordinal)
# classmethod date.fromisoformat(date_string)
# classmethod date.fromisocalendar(year, week, day)
# print(datetime.date.fromisoformat('2019-12-04'))

# date 实例方法
# date.replace(year=self.year, month=self.month, day=self.day)
# date.timetuple() # 等价于 time.struct_time((d.year, d.month, d.day, 0, 0, 0, d.weekday(), yday, -1))
# date.toordinal()
# date.weekday()
# date.isoweekday()
# date.isocalendar()
# datetime.IsoCalendarDate(year=2004, week=1, weekday=1)
# datetime.IsoCalendarDate(year=2004, week=1, weekday=7)
# date.isoformat()
# date.__str__()
# date.ctime()
# date.strftime(format)
# date.__format__(format)

d = datetime.date.fromordinal(730920) # 730920th day after 1. 1. 0001
t = d.timetuple()
print(d.strftime("%d/%m/%y"))
print(d.strftime("%A %d. %B %Y"))
# for i in t:     
#     print(i)
#     
# # datetime 对象
# # 构造器
# # class datetime.datetime(year, month, day, hour=0, minute=0, second=0, microsecond=0, tzinfo=None, *, fold=0)
# 
# # 其他类方法
# classmethod datetime.today()
# classmethod datetime.now(tz=None)
# classmethod datetime.utcnow()
# classmethod datetime.fromtimestamp(timestamp, tz=None)
# classmethod datetime.utcfromtimestamp(timestamp)
# classmethod datetime.fromordinal(ordinal)
# classmethod datetime.combine(date, time, tzinfo=self.tzinfo)
# classmethod datetime.fromisoformat(date_string)
# classmethod datetime.fromisocalendar(year, week, day)
# classmethod datetime.strptime(date_string, format)
# 
# # 实例方法
# datetime.date()
# datetime.time()
# datetime.timetz()
# datetime.replace(year=self.year, month=self.month, day=self.day, hour=self.hour, minute=self.minute, second=self.second, microsecond=self.microsecond, tzinfo=self.tzinfo, *, fold=0)
# datetime.astimezone(tz=None)
# datetime.utcoffset()
# datetime.dst()
# datetime.tzname()
# datetime.timetuple() # d.timetuple() 等价于:time.struct_time((d.year, d.month, d.day, d.hour, d.minute, d.second, d.weekday(), yday, dst))
# datetime.utctimetuple()
# datetime.toordinal()
# datetime.timestamp()
# datetime.weekday()
# datetime.isoweekday()
# datetime.isocalendar()
# datetime.isoformat(sep='T', timespec='auto')
# datetime.ctime()
# datetime.strftime(format)
# datetime.__format__(format)
# print(datetime.datetime(2019, 5, 18, 15, 17, 8, 132263).isoformat())
# print(datetime.datetime(2002, 12, 4, 20, 30, 40).ctime())
dt = datetime.datetime.strptime("21/11/06 16:30", "%d/%m/%y %H:%M")
print(dt.strftime("%A, %d. %B %Y %I:%M%p"))

# # time 对象
# class datetime.time(hour=0, minute=0, second=0, microsecond=0, tzinfo=None, *, fold=0)
# # 类属性
# time.min
# time.max
# time.resolution
# 
# # 实例属性(只读)
# time.hour
# time.minute
# time.second
# time.microsecond
# time.tzinfo
# time.fold
# 
# # 构造方法
# classmethod time.fromisoformat(time_string)
# print(datetime.time.fromisoformat('04:23:01'))
# print(datetime.time.fromisoformat('04:23:01+04:00'))
# 
# # 实例方法
# time.replace(hour=self.hour, minute=self.minute, second=self.second, microsecond=self.microsecond, tzinfo=self.tzinfo, *, fold=0)
# time.isoformat(timespec='auto')
# time.__str__()
# time.strftime(format)
# time.__format__(format)
# time.utcoffset()
# time.dst()
# time.tzname()


# # tzinfo 对象
# class datetime.tzinfo
# tzinfo.utcoffset(dt)
# tzinfo.dst(dt)
# tzinfo.tzname(dt)
# tzinfo.fromutc(dt)
# 
# timezone 对象
# timezone 类是 tzinfo 的子类，它的每个实例都代表一个以与 UTC 的固定时差来定义的时区。
# 
# class datetime.timezone(offset, name=None)
# timezone.utcoffset(dt)
# timezone.tzname(dt)
# timezone.dst(dt)
# timezone.fromutc(dt)
# timezone.utc

# strftime() 和 strptime() 的行为
