# coding = utf-8

# @title: 测试模拟浏览器，多进程
# @desc: 
# @author: RyanLin
# @date: 2022/06/22

import time

from selenium import webdriver
from multiprocessing.dummy import Pool


def spider(url):
    with webdriver.Chrome('chromedriver') as driver:
        driver.get(url)
        html = driver.page_source
        print(html)
        time.sleep(10)
        driver.close()
        driver.quit()


def run(x):
    url = 'https://www.baidu.com/s?wd=今天有哪些美女&pn={}'
    pages = []
    for i in range(0, x * 10, 10):
        page = url.format(i)
        pages.append(page)
    print(pages)
    pool = Pool(5)
    result = pool.map(spider, pages)
    pool.close()
    pool.join()
    return result


if __name__ == '__main__':
    run(10)
