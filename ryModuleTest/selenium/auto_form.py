# coding = utf-8

# @title: 自动填写表单
# @desc:
# @author: RyanLin
# @date: 2022/06/22
import time

from selenium import webdriver
from selenium.webdriver.support import ui
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from multiprocessing.dummy import Pool

"""
selenium定位方法
通过id定位元素：find_element_by_id(“id_vaule”)
通过name定位元素：find_element_by_name(“name_vaule”)
通过tag_name定位元素：find_element_by_tag_name(“tag_name_vaule”)
通过class_name定位元素：find_element_by_class_name(“class_name”)
通过css定位元素：find_element_by_css_selector();
通过xpath定位元素：find_element_by_xpath(“xpath”)
通过link定位：find_element_by_link_text(“text_vaule”)或者find_element_by_partial_link_text()
"""


# def is_visible(driver, locator, timeout=10):
#     try:
#         ui.WebDriverWait(driver, timeout).until(EC.visibility_of_element_located(locator))
#         return True
#     except TimeoutException:
#         return False


def fill_page(url, params_by_id, commit_button_id):
    with webdriver.Chrome('chromedriver') as driver:
        driver.get(url)
        html = driver.page_source
        # print(html)
        for k, v in params_by_id.items():
            # print('param', k, v)
            time.sleep(3)
            driver.find_element(By.ID, k).click()
            driver.find_element(By.ID, k).send_keys(v)

        # commit
        time.sleep(3)
        driver.find_element(By.ID, commit_button_id).click()

        # 查看结果
        time.sleep(5)
        print(driver.page_source)
        driver.close()
        driver.quit()


def run(x):
    url = 'https://www.baidu.com'
    params = {'kw': '索隆'}
    commit_button_id = 'su'
    fill_page(url, params, commit_button_id)
    # time.sleep(10)


if __name__ == '__main__':
    run(10)
