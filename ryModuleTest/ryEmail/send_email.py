# coding = utf-8

# @filename: C:/Users/Ryan/git/rytutorial/ryModuleTest/ryEmail/send_email.py
# @desc: 发送邮件
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022年4月5日

# _*_ coding: UTF-8 _*_

import smtplib
import email.mime.multipart
import email.mime.text
# from email.mime.text import MIMEText
# from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
import logging


def send_email(smtpHost, sendAddr, password, recipientAddrs, subject='', content='', attach=None):
    msg = email.mime.multipart.MIMEMultipart()
    msg['from'] = sendAddr
    msg['to'] = recipientAddrs
    msg['subject'] = subject
    txt = email.mime.text.MIMEText(content, 'plain', 'utf-8')
    msg.attach(txt)
    print('attach:' + attach)

    # 添加附件，传送D:/软件/yasuo.rar文件
    if attach is None:
        file = 'C:/Users/Long/Desktop/ml_develop/preview.jpg'
    else:
        try:
            file = attach.split('/')[-1]
            print('attach filename:' + attach + ',file=' + file)
            part = MIMEApplication(open(attach, 'rb').read())
            part.add_header('Content-Disposition', 'attachment', filename=file)
            msg.attach(part)
        except Exception as e:
            logging.error(e)

    try:
        smtp = smtplib.SMTP_SSL(host=smtpHost)
        smtp.connect(smtpHost, 465)
        smtp.login(sendAddr, password)
        smtp.sendmail(sendAddr, recipientAddrs, str(msg))
        reply = '附件' + attach + ' 发送至 ' + recipientAddrs + ' 成功！'
        logging.info(reply)
    except Exception as e:
        reply = '发送失败'
        logging.error(e)
    finally:
        smtp.quit()
        logging.info('smtp quit')
    return reply


if __name__ == '__main__':
    smtpHost = "smtp.qq.com"
    sendAddr = '1651168647@qq.com'
    password = 'nauihlkshvvhcgig'
    recipientAddrs = 'paradixrain@qq.com'
    subject = 'test from snowluxry'
    content = '我就是试试能不能发送邮件'
    attach = 'test.png'
    send_email(smtpHost, sendAddr, password, recipientAddrs, subject, content, attach)
