# coding=utf-8

from email.parser import Parser
from email.header import decode_header,Header
from email.utils import parseaddr
import poplib

# 输入邮件地址, 口令和POP3服务器地址:
email = '3424802@qq.com'        # 邮箱地址
password = 'nauihlkshvvhcgig'   # 授权码
pop3_server = 'pop.163.com'

def decode_str(s):
    value, charset = decode_header(s)[0]
    if charset:
        value = value.decode(charset)
    return value

def print_info(msg):
    for header in ['From', 'To', 'Subject']:
        value = msg.get(header, '')
        if header == 'From':
            from_email = value[value.find('<')+1 : -1]
            if from_email != '125567292@qq.com':    # 白名单：只读他的邮件
                return False
        if value:
            if header == 'Subject':
                value = decode_str(value)
            else:
                hdr, addr = parseaddr(value)
                name = decode_str(hdr)
                value = u'%s <%s>' % (name, addr)
    
    attachment_files = []
    for part in msg.walk():
        file_name = part.get_filename()
        contentType = part.get_content_type()
        mycode = part.get_content_charset()
        if file_name:
            h = Header(file_name)
            dh = decode_header(h)
            filename = dh[0][0]
            if dh[0][1]:
                filename = decode_str(str(filename, dh[0][1]))
            attachment_files.append(filename)
            data = part.get_payload(decode=True)
            with open(filename, 'wb') as f:
                f.write(data)
            print '附件已下载完成'
        elif contentType == 'text/plain':
            data = part.get_payload(decode=True)
            content = data.decode(mycode)
    return True

server = poplib.POP3_SSL(pop3_server, 995)
server.set_debuglevel(0)
print server.getwelcome().decode('utf-8')
server.user(email)
server.pass_(password)
resp, mails, octets = server.list()
index = len(mails)
for i in range(1,index+1):
    resp, lines, octets = server.retr(i)
    msg_content = b'\r\n'.join(lines).decode('utf-8')
    msg = Parser().parsestr(msg_content)
    rtn_value = print_info(msg)
    if rtn_value == True:        # 读完邮件就删除
        server.dele(i)
server.quit()
