# _*_ coding: UTF-8 _*_

import smtplib
import email.mime.multipart
import email.mime.text
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

def send_email(smtpHost, sendAddr, password, recipientAddrs, subject='', content='', attach=None):
    msg = email.mime.multipart.MIMEMultipart()
    msg['from'] = sendAddr
    msg['to'] = recipientAddrs
    msg['subject'] = subject
    txt = email.mime.text.MIMEText(content, 'plain', 'utf-8')
    msg.attach(txt)

    # 添加附件
    if attach == None:
        file='/tmp/ryan/1.txt'
    else:
        try:
            file=attach.split('/')[-1]
            part = MIMEApplication(open(attach,'rb').read())
            part.add_header('Content-Disposition', 'attachment', filename=file)
            msg.attach(part)
        except Exception as e:
            print 'attach failed'

    try:
        smtp = smtplib.SMTP_SSL()
        smtp.connect(smtpHost, '465')
        smtp.login(sendAddr, password)
        smtp.sendmail(sendAddr, recipientAddrs, str(msg))
        reply='附件' + attach +' 发送至 ' + recipientAddrs + ' 成功！'
    except Exception as e:
        reply='发送失败'
    finally:
        smtp.quit()
    return reply

SMTPADDR = 'smtp.qq.com'            #发件人邮箱的smtp服务器
SENDADDR = '1651168647@qq.com'        # 发件人
SMTPPSWD = 'nauihlkshvvhcgig'       # 发件人的授权码
emailAddr = 'paradixrain@qq.com'   # 收件人
subject = '发送统计数据'          # 邮件标题
attach = '/tmp/ryan/1.txt'      # 附件完整路径
content = '''为了防止被判断为垃圾邮件，也是醉了，要加首歌词
如果那两个字没有颤抖
我不会发现我难受
怎么说出口
也不过是分手
如果对于明天没有要求
牵牵手就像旅游
成千上万个门口
总有一个人要先走
怀抱既然不能逗留
何不在离开的时候
一边享受 一边泪流
十年之前
我不认识你 你不属于我
我们还是一样
陪在一个陌生人左右
走过渐渐熟悉的街头
十年之后
我们是朋友 还可以问候
只是那种温柔
再也找不到拥抱的理由
情人最后难免沦为朋友
怀抱既然不能逗留
何不在离开的时候
一边享受 一边泪流
十年之前
我不认识你 你不属于我
我们还是一样
陪在一个陌生人左右
走过渐渐熟悉的街头
十年之后
我们是朋友 还可以问候
只是那种温柔
再也找不到拥抱的理由
情人最后难免沦为朋友
直到和你做了多年朋友
才明白我的眼泪
不是为你而流
也为别人而流 
    '''
send_email(SMTPADDR, SENDADDR, SMTPPSWD, emailAddr, subject, content, attach)
