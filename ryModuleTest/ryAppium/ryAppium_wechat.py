# coding=utf-8

"""
使用appnium读取微信读书的内容，需要开启usb调试模式
"""
import re

from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

server = 'http://localhost:4723/wd/hub'
desired_caps = {
    "platformName": "Android",
    "deviceName": "Any",
    "appPackage": "com.tencent.weread",
    "appActivity": ".ReaderFragmentActivity",
    "noReset": True
}


def main_norest():
    # 启动微信读书
    driver = webdriver.Remote(server, desired_caps)

    # 点击书, google
    el1 = driver.find_element(By.XPATH, value=
        "//androidx.recyclerview.widget.RecyclerView[@content-desc=\"book_shelf_defalut_recycler_view\"]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.view.View[2]")
    el1.click()
    print("open book")

    # 读取内容
    for i in range(5):
        page_source = driver.page_source
        pattern = r'content-desc="(.*?)"'
        content = re.findall(pattern, page_source)
        print("page", i, ":", content)

        # 翻页
        TouchAction(driver).tap(x=1006, y=1147).perform()


if __name__ == "__main__":
    try:
        main_norest()
    except Exception as e:
        print(e)
    finally:
        print("finished")
