# coding=utf-8

"""
使用appnium，需要开启usb调试模式
"""

from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

server = 'http://localhost:4723/wd/hub'
desired_caps = {
    "platformName": "Android",
    "deviceName": "Any",
    "appPackage": "com.alibaba.android.rimet",
    "appActivity": ".biz.LaunchHomeActivity",
    "noReset": True
}


def main_withlogin():
    # 启动dd，带初始化
    driver = webdriver.Remote(server, desired_caps)

    loginButton = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID,
                                                                                  "com.alibaba.android.rimet:id/btn_agree")))

    # 单击"登录"按钮
    loginButton.click()

    numberTextbox = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID,
                                                                                    "com.alibaba.android.rimet:id/et_phone")))

    # 输入号码
    numberTextbox.send_keys("18910218998")

    # 单击"下一步"按钮
    driver.find_element(By.ID, "com.alibaba.android.rimet:id/tv").click()

    # 同意服务协议及隐私协议
    driver.find_element(By.ID, "com.alibaba.android.rimet:id/cb_privacy").click()

    # 同意
    driver.find_element(By.ID, "com.alibaba.android.rimet:id/ll_container").click()

    # 密码
    passwdBox = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID,
                                                                                "com.alibaba.android.rimet:id/et_password")))
    passwdBox.send_keys("Admin123")

    # 同意
    driver.find_element(By.ID, "com.alibaba.android.rimet:id/tv").click()

    # 允许获得设备信息
    driver.find_element(By.ID, "com.android.permissioncontroller:id/permission_allow_button").click()

    time.sleep(25)
    # 刷新页面
    driver.refresh()
    time.sleep(25)
    driver.refresh()
    time.sleep(25)

    # KQ
    # driver.find_element(by=By.XPATH,
    #                     value="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout/com.uc.webview.export.WebView/com.uc.webkit.bb/android.webkit.WebView/android.view.View/android.view.View/android.view.View[3]/android.view.View/android.view.View/android.view.View[1]/android.view.View[2]/android.view.View[4]/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View[1]/android.widget.Image").click()
    driver.tap([(127, 1938)], 1000)
    time.sleep(10)

    # 同意获取位置
    # driver.tap([(500, 2000)], 1000)

    # 同意获取位置
    driver.find_element(By.ID, "com.android.permissioncontroller:id/permission_allow_always_button").click()


def main_norest():
    # 启动钉钉
    driver = webdriver.Remote(server, desired_caps)

    # 点击中间的应用
    driver.tap([(542, 2185)], 1000)
    time.sleep(10)
    driver.tap([(542, 2185)], 1000)
    time.sleep(10)

    # KQ
    driver.tap([(127, 1938)], 1000)
    time.sleep(15)

    # KQ点无法触发，后续还是需要todesk


if __name__ == "__main__":
    try:
        main_withlogin()
        # main_norest()
    except Exception as e:
        print(e)
    finally:
        print("finished")
