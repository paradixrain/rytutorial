# coding=utf-8

# @title: random --- 生成伪随机数
# @desc: 该模块实现了各种分布的伪随机数生成器.
#        在实数轴上，有计算均匀、正态（高斯）、对数正态、负指数、伽马和贝塔分布的函数。
#        为了生成角度分布，可以使用 von Mises 分布。
# @author: RyanLin
# @date: 2022/01/27

# 簿记功能
# random.seed(a=None, version=2)
# random.getstate()
# random.setstate(state)
#
# 用于字节数据的函数
# random.randbytes(n)
#
# 整数用函数
# random.randrange(stop)
# random.randrange(start, stop[, step])
# random.randint(a, b)
# random.getrandbits(k)
#
# 序列用函数
# random.choice(seq)
# random.choices(population, weights=None, *, cum_weights=None, k=1)
# random.shuffle(x[, random])
# random.sample(population, k, *, counts=None)
#
# 实值分布
# random.random()
# random.uniform(a, b)
# random.triangular(low, high, mode)
# random.betavariate(alpha, beta)
# random.expovariate(lambd)
# random.gammavariate(alpha, beta)
# random.gauss(mu, sigma)
# random.lognormvariate(mu, sigma)
# random.normalvariate(mu, sigma)
# random.vonmisesvariate(mu, kappa)
# random.paretovariate(alpha)
# random.weibullvariate(alpha, beta)
#
# 替代生成器
# class random.Random([seed])
# class random.SystemRandom([seed])


# 有时能够重现伪随机数生成器给出的序列是很有用处的。 通过重用一个种子值，只要没有运行多线程，相同的序列就应当可在多次运行中重现。



# 多服务器队列的到达时间和服务交付模拟
from heapq import heappush, heappop
from random import expovariate, gauss
from statistics import mean, quantiles

average_arrival_interval = 5.6
average_service_time = 15.0
stdev_service_time = 3.5
num_servers = 3

waits = []
arrival_time = 0.0
servers = [0.0] * num_servers  # time when each server becomes available
for i in range(100_000):
    print('i=', i)
    arrival_time += expovariate(1.0 / average_arrival_interval)
    next_server_available = heappop(servers)
    wait = max(0.0, next_server_available - arrival_time)
    waits.append(wait)
    service_duration = gauss(average_service_time, stdev_service_time)
    service_completed = arrival_time + wait + service_duration
    heappush(servers, service_completed)

print(f'Mean wait: {mean(waits):.1f}   Max wait: {max(waits):.1f}')
print('Quartiles:', [round(q, 1) for q in quantiles(waits)])


