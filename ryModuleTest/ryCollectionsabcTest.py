# coding=utf-8

# @title: collectionns.abc
# @desc: 该模块定义了一些 抽象基类，它们可用于判断一个具体类是否具有某一特定的接口；例如，这个类是否可哈希，或其是否为映射类
#        该模块提供了每一种容器类型所应具备的常用方法，从这些基类中继承了子类后，如果忘记实现某个方法，将抛出 TypeError 异常，提醒我们补充
# @author: RyanLin
# @date: 2022/01/17

# 抽象类（abstract base class,ABC）就是类里定义了纯虚成员函数的类。纯虚函数一般只提供了接口，并不会做具体实现（虽然可以），
# 实现由它的派生类去重写。抽象类不能被实例化(不能创建对象)，通常是作为基类供子类继承，子类中重写虚函数，实现具体的接口。
# 简言之，ABC描述的是至少使用一个纯虚函数的接口，从ABC派生出的类将根据派生类的具体特征，使用常规虚函数来实现这种接口

# 可以被哈希的数据结构：int、float、str、tuple 和 NoneType。
# 不可以被哈希的数据结构：dict、list 和 set


# 参考资料： https://www.jianshu.com/p/1a2be5efb19e


# Collections Abstract Base Classes -- Detailed Descriptions
# class collections.abc.Container
# class collections.abc.Hashable
# class collections.abc.Sized
# class collections.abc.Callable
# class collections.abc.Iterable
# class collections.abc.Collection
# class collections.abc.Iterator
# class collections.abc.Reversible
# class collections.abc.Generator
# class collections.abc.Sequence
# class collections.abc.MutableSequence
# class collections.abc.ByteString
# class collections.abc.Set
# class collections.abc.MutableSet
# class collections.abc.Mapping
# class collections.abc.MutableMapping
# class collections.abc.MappingView
# class collections.abc.ItemsView
# class collections.abc.KeysView
# class collections.abc.ValuesView
# class collections.abc.Awaitable
# class collections.abc.Coroutine
# class collections.abc.AsyncIterable
# class collections.abc.AsyncIterator
# class collections.abc.AsyncGenerator



import collections.abc


class ListBasedSet(collections.abc.Set):
    ''' Alternate set implementation favoring space over speed
        and not requiring the set elements to be hashable. '''
    def __init__(self, iterable):
        self.elements = lst = []
        for value in iterable:
            if value not in lst:
                lst.append(value)

    def __iter__(self):
        return iter(self.elements)

    def __contains__(self, value):
        return value in self.elements

    def __len__(self):
        return len(self.elements)

s1 = ListBasedSet('abcdef')
s2 = ListBasedSet('defghi')
overlap = s1 & s2            # The __and__() method is supported automatically
# for i in overlap:
#     print(i)
    
# 继承方式    ,必需要定义类的基本方法才能 通过isinstance的判断，如果注释掉其中一行则会有TypeError的报错
from _collections_abc import Sequence
class C(Sequence):                      # Direct inheritance
    def __init__(self): pass             # Extra method not required by the ABC
    def __getitem__(self, index):  pass  # Required abstract method
    def __len__(self):  pass             # Required abstract method
    def count(self, value): pass         # Optionally override a mixin method

print(issubclass(C, Sequence))
print(isinstance(C(), Sequence))

# 注册方式
class D:                                 # No inheritance
    def __init__(self): pass              # Extra method not required by the ABC
    def __getitem__(self, index):  pass   # Abstract method
    def __len__(self):  pass              # Abstract method
    def count(self, value): pass          # Mixin method
    def index(self, value): pass          # Mixin method

Sequence.register(D)                     # Register instead of inherit


