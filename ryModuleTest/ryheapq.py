# coding=utf-8

# @title: heapq 堆队列
# @desc: 这个模块提供了堆队列算法的实现，也称为优先队列算法。
#        堆是一个二叉树，它的每个父节点的值都只会小于或等于所有孩子节点（的值）
# @author: RyanLin
# @date: 2022/01/17

# 要创建一个堆，可以使用list来初始化为 []
# 它使用了数组来实现：从零开始计数，对于所有的 k ，都有 heap[k] <= heap[2*k+1] 和 heap[k] <= heap[2*k+2]。

# heapq 的方法
# heapq.heappush(heap, item)
# heapq.heappop(heap)
# heapq.heappushpop(heap, item)
# heapq.heapify(x)
# heapq.heapreplace(heap, item)
# heapq.merge(*iterables, key=None, reverse=False)
# heapq.nlargest(n, iterable, key=None)
# heapq.nsmallest(n, iterable, key=None)
# 后两个函数在 n 值较小时性能最好。 对于更大的值，使用 sorted() 函数会更有效率


# def heapsort(iterable):
#      h = []
#      for value in iterable:
#          heappush(h, value)
#      return [heappop(h) for i in range(len(h))]
#
# print(heapsort([1, 3, 5, 7, 9, 2, 4, 6, 8, 0]))
# # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

# 堆元素可以为元组
# h = []
# heappush(h, (5, 'write code'))
# heappush(h, (7, 'release product'))
# heappush(h, (1, 'write spec'))
# heappush(h, (3, 'create tests'))
# print(heappop(h))
# # (1, 'write spec')

# 优先队列 是堆的常用场合
