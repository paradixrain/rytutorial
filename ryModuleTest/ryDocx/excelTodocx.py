# -*- coding: utf-8 -*-

"""
已有模板“OA模板.docx"，将”data.xlsx"中的信息填写如模板中
"""

from docxtpl import DocxTemplate
from openpyxl import load_workbook


wb = load_workbook(r"data.xlsx")
ws = wb['基础数据']
contexts = []

# 机构名称	修理厂名称	支付月份	支付金额	服务单价	OA落款时间


for row in range(2, ws.max_row + 1):
    jg_name = ws["A" + str(row)].value
    xlc_name = ws["B" + str(row)].value
    zfyf = ws["C" + str(row)].value
    zfje = ws["D" + str(row)].value
    fwdj = ws["E" + str(row)].value
    oatime = ws["F" + str(row)].value

    context = {"机构名称": jg_name,
               "修理厂名称": xlc_name,
               "支付月份": zfyf,
               "支付金额": zfje,
               "服务单价": fwdj,
               "OA落款时间": oatime
               }
    contexts.append(context)

for context in contexts:
    print(context)
    tpl = DocxTemplate(r"OA模板.docx")
    tpl.render(context)
    tpl.save(f'{context["机构名称"]}中支关于申请支付{context["修理厂名称"]}2023年{context["支付月份"]}咨询费的请示.docx')
