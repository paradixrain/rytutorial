import glob
import pandas as pd
from docx import Document


for excelfile in glob.glob('*.xlsx'):
    excel_file_name = excelfile.rstrip('.xlsx')
    print(excel_file_name)
    excelfile = pd.ExcelFile(excelfile)
    for eachsheet in excelfile.sheet_names:
        print(eachsheet)
        cur_sheet_data = pd.read_excel(excelfile, sheet_name=eachsheet)
        cur_sheet_data.fillna('', inplace=True)
        cur_word_file = f'{excel_file_name}_{eachsheet}.docx'
        doc = Document()
        for row in cur_sheet_data.index.values:
            content = cur_sheet_data.iloc[row, 0]
            print(cur_sheet_data.iloc[row, 0])
            doc.add_paragraph(content)
        doc.save(cur_word_file)


