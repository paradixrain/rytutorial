# coding=utf-8
import os, sys

'''
将行程单的时间修改成和电子发票的时间一致
'''


def mod_utime(filename, filename2):
    # filename = r"C:\Users\linyu\OneDrive\tmp\报销\天津20240612\滴滴电子发票.pdf"
    # filename2 = r"C:\Users\linyu\OneDrive\tmp\报销\天津20240612\滴滴出行行程报销单.pdf"

    access_time1 = os.stat(filename).st_atime
    mod_time1 = os.stat(filename).st_mtime
    cre_time1 = os.stat(filename).st_ctime

    # print(access_time1, mod_time1, cre_time1)

    access_time = os.stat(filename2).st_atime
    mod_time = os.stat(filename2).st_mtime
    cre_time = os.stat(filename2).st_ctime

    # print(access_time, mod_time, cre_time)

    os.utime(filename2, (access_time1, mod_time1))

    access_time = os.stat(filename2).st_atime
    mod_time = os.stat(filename2).st_mtime
    cre_time = os.stat(filename2).st_ctime

    print(access_time, mod_time, cre_time)
    print("时间同步完成")


if __name__ == '__main__':
    args = sys.argv[1:]
    mod_utime(args[0], args[1])
