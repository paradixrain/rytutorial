# coding = utf-8
from pickle import FALSE, TRUE


# TC1
# class Solution:
#     # def __init__(self):
#     #     pass
#     def twoSum(self, nums: list[int], target: int) -> list[int]:
#         for i in range(len(nums)):
#             for j in range(i+1, len(nums)):
#                 if nums[i] + nums[j] == target:
#                     return [i, j]
#         return []

# a = Solution()
# b = a.twoSum(nums=[2,3,4], target=6)
# print(b)


# TC19
# class Solution:
#     def isValid(self, s: str) -> bool:
#         while True:
#             if s.find('()')>=0 or s.find('[]')>=0 or s.find('{}')>=0:
#                 s = s.replace('()','').replace('[]','').replace('{}','')
#             elif len(s) > 0:
#                 return False
#             else:
#                 return True

# class Solution:
#     def isValid(self, s: str) -> bool:
#         signs = {'[':']', '{':'}', '(':')'}
#         temp = []
#         print('init s:', s)
#         for value in s:
#             if len(temp) == 0:
#                 temp.append(value)
#                 continue
#             last = temp.pop()
#             if signs.get(last,"") == value:
#                 continue
#             elif value in signs.keys():
#                 temp.append(last)
#                 temp.append(value)
#             else:
#                 return False
#         if len(temp) == 0:
#             return True
#         else: 
#             return False

# TC14 
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, n=None):
#         self.val = val
#         self.next = n
# class Solution:
#     def mergeTwoLists(self, list1: [ListNode], list2: [ListNode]) -> [ListNode]:
#         head = ListNode(-1)
#         curr = head
#         while list1 and list2:
#             if list1.val > list2.val:
#                 curr.next = list2
#                 list2 = list2.next
#             else:
#                 curr.next = list1
#                 list1 = list1.next
#             curr = curr.next
#         if list1:
#             curr.next = list1
#         if list2:
#             curr.next = list2
#         return head.next


# TC25
# class Solution:
#     def removeDuplicates(self, nums: list[int]) -> int:
#         count = 0
#         for value in nums[1:]:
#             if value > nums[count]:
#                 count += 1
#                 nums[count] = value
#         return count+1

# TC26
# class Solution:
#     def removeElement(self, nums: list[int], val: int) -> int:
#         while True:
#             if val not in nums:
#                 break
#             nums.remove(val)
#         return len(nums)


# TC46
# TODO
class Solution:
    def maxSubArray(self, nums: list[int]) -> int:
        max = 0
        for i, v in enumerate(nums):
            if max + v < 0:
                pass


# s = [1,1,2]
s = [0, 0, 1, 1, 1, 2, 2, 3, 3, 4]
a = Solution()
b = a.removeElement(s, 2)
print(b)
