# coding=utf-8
# print('name:', __name__)
# temp = [1, 2, 3, 4, 5]
# temp.insert(-3, 'a')
# print(temp)

# a = 100
# b = 14
# print(divmod(a, b))

# def func(s, i, j):
#     if i < j:
#         func(s, i + 1, j - 1)
#         s[i], s[j] = s[j], s[i]
#
#
# def main():
#     s = [1, 2, 3, 4, 5, 6]
#     func(s, 0, len(s) - 1)
#     print(s)

# a = 'abcd'
# b = 'ca'
# print(a.index(b))

# dicts = {'a': 1, 'b': 2, 'c': 3}
# temp = dicts.copy()
# temp['a'] = 100
# print(dicts)
# print(temp)

# def func(a, *, b):
#     print(b)
# func(1,2,3,4)

# a = 'abcd'
# b = 'c'
# print(a.find(b,4))

# print([1] in [1, 2, 3])

# import math
# def sieve(size):
#     sieve=[True]*size
#     sieve[0]=sieve[1]=False
#     for i in range(2,int(math.sqrt(size))+1):
#         k = i*2
#         while k < size:
#             sieve[k]=False
#             k += i
#     print(sieve)
#     return sum(1 for i in sieve if i)
# print(sieve(16))
#
# a = 0
# _a = 1
# __a = 2

# class vector:
#     __slots__ = ('x', 'y')
#     def __init__(self):
#         pass
# class vector3d(vector):
#     __slots__ = ('z','x')
#     def __init__(self):
#         pass
# v = vector()
# v3 = vector3d()
#
# v3.y = 1
# print(v3.y)

def fn():
    t = []
    i = 0
    while i < 2:
        t.append(lambda x: print(i*x, end=","))
        i += 1
    return t

for f in fn():
    f(3)