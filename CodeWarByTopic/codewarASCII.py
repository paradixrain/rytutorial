# coding = utf-8


class Converter:
    """
    Write a module Converter that can take ASCII text and convert it to hexadecimal.
    The class should also be able to take hexadecimal and convert it to ASCII text.
    To make the conversion well defined, each ASCII character is represented by exactly two hex digits,
    left-padding with a 0 if needed.
    The conversion from ascii to hex should produce lowercase strings (i.e. f6 instead of F6).
    """

    @staticmethod
    def to_ascii(h):
        # take hexadecimal and convert it to ASCII text
        return bytes.fromhex(h).decode('utf-8')

    @staticmethod
    def to_hex(s):
        # take ASCII text and convert it to hexadecimal
        return bytes(s, 'utf-8').hex()


def build_pyramid1(s, n):
    """
    You will get a string s with an even length,
    and an integer n which represents the height of the pyramid and your task is to draw the following pattern.
    build_pyramid("00-00..00-00", 7) should return:
                                    00-00..00-00
                              0000--0000....0000--0000
                        000000---000000......000000---000000
                   00000000----00000000........00000000----00000000
            0000000000-----0000000000..........0000000000-----0000000000
      000000000000------000000000000............000000000000------000000000000
00000000000000-------00000000000000..............00000000000000-------00000000000000
    Each line is seperated with "\n"
    n will always be greater than 3.
    No need to check for invalid parameters
    There are no whitespaces at the end of the lines
    """
    import re
    # s = "00-00..00-00"
    # 按照 数字，符号，数字，符号，数字，符号，数字的格式，把字符串s分割成列表
    result = re.split(r'(\d+|[a-zA-Z]|[-.])', s)
    # 移除result中的空元素
    result = [i for i in result if i != '']
    print(result)
    # 创建一个空列表，用来存放每一行的字符串
    line = []
    # 循环n次，每次循环把result中的元素添加到line中
    for i in range(1, n + 1):
        current_line = ' ' * int(len(s) * n / 2 - len(s) * i / 2) + ''.join([str(j) * i for j in result])
        line.append(current_line)
    return '\n'.join(line)


def build_pyramid(s, n):
    return '\n'.join(''.join(c * i for c in s).center(len(s) * n).rstrip() for i in range(1, n + 1))


def x(n):
    """
    You will get an odd integer n (>= 3) and your task is to draw an X. Each line is separated with \n.
    when n = 3, you should return: "■□■\n□■□\n■□■"
    when n = 7, you should return: "■□□□□□■\n□■□□□■□\n□□■□■□□\n□□□■□□□\n□□■□■□□\n□■□□□■□\n■□□□□□■"
    """
    return '\n'.join(
        ''.join('■' if i == j or i == n - j + 1 else '□' for i in range(1, n + 1)) for j in range(1, n + 1))


def dot(m, n):
    """You will get two integers n (width) and m (height) and your task is to draw the following pattern.
     Each line is seperated with a newline (\n)
     Both integers are equal or greater than 1; no need to check for invalid parameters.
     dot(1,1) return "+---+\n| o |\n+---+"
     dot(3,2) return "+---+---+---+\n| o | o | o |\n+---+---+---+\n| o | o | o |\n+---+---+---+"
     """
    # plan A
    # rtn = ''
    # for i in range(1, 2 * n + 1):
    #     if i % 2 == 1:
    #         line = '+---' * m + '+\n'
    #     else:
    #         line = '| o ' * m + '|\n'
    #     rtn += line
    # rtn += '+---' * m + '+'
    # return rtn

    # plan B
    return '\n'.join('+---' * m + '+' if i % 2 == 1 else '| o ' * m + '|' for i in range(1, 2 * n + 2))


def count(string):
    from collections import Counter
    return Counter(string)


def binary_to_string(binary):
    """
Write a function that takes in a binary string and returns the equivalent decoded text (the text is ASCII encoded).
Each 8 bits on the binary string represent 1 character on the ASCII table.
The input string will always be a valid binary string.
Characters can be in the range from "00000000" to "11111111" (inclusive)
Note: In the case of an empty binary string your function should return an empty string.
    """
    return ''.join(chr(int(binary[i:i + 8], 2)) for i in range(0, len(binary), 8))


# 8级灰度阶梯
GLYPHS = " .,:;xyYX"


def image2ascii(image):
    """
    将 8 位灰度输入图像（2D 列表）转换为 ASCII 表示。
    """
    return '\n'.join(''.join(GLYPHS[int(i / 255 * 8)] for i in line) for line in image)


a = image2ascii([[10, 20, 30, 40, 50],
                 [60, 70, 80, 90, 100],
                 [110, 120, 130, 140, 150],
                 [160, 170, 180, 190, 200],
                 [210, 220, 230, 240, 250]])


def puzzle_tiles(width, height):
    def f():
        yield '  ' + ' _( )__' * width
        for i in range(height):
            if i % 2 == 0:
                yield ' _|' + '     _|' * width
                yield '(_' + '   _ (_' * width
                yield ' |' + '__( )_|' * width
            else:
                yield ' |_' + '     |_' * width
                yield '  _)' + ' _   _)' * width
                yield ' |' + '__( )_|' * width

    return '\n'.join(f())


def ascii_cipher1(message, key):
    def is_prime(n):
        abs_key = abs(n)
        if abs_key < 2:
            return False
        for i in range(2, abs_key):
            if abs_key % i == 0:
                return False
        return True

    def max_prime_factors(n: int) -> int:
        from math import sqrt
        is_negetive = -1 if n < 0 else 1
        abs_key = abs(n)
        factors = []
        while abs_key % 2 == 0:
            factors.append(2)
            abs_key = int(abs_key / 2)
        for i in range(3, int(sqrt(abs_key)) + 1, 2):
            while abs_key % i == 0:
                factors.append(i)
                abs_key = int(abs_key / i)
        if abs_key > 2:
            factors.append(abs_key)
        return max(factors) * is_negetive

    def get_max_prime_factor(n: int) -> int:
        """
        You will get an integer key and your task is to return the largest prime factor of the given key.
        If the key is a prime number, return the key itself.
        """
        # 如果key是素数，返回key
        if is_prime(n):
            return n
        # 如果key不是素数，返回最大素数
        else:
            b = max_prime_factors(n)
            print('max_prime_factor=', b)
            return max_prime_factors(n)

    return ''.join(chr((ord(c) + get_max_prime_factor(key)) % 128) for c in message)


def ascii_cipher(message, key):
    sign, n, rot = key // abs(key), abs(key), -1
    while n > 1:
        rot, n = [(max(i, rot), n // i) for i in range(2, n + 1) if n % i == 0][0]
    return ''.join(chr((ord(c) + rot * sign) % 128) for c in message)


import base64

latin1 = 'latin-1'


def toAscii85(data):
    return base64.a85encode(data.encode('ascii'), adobe=True).decode(latin1)


def fromAscii85(data):
    return base64.a85decode(data, adobe=True).decode('ascii')


def break_pieces(shape):
    """
    You are given a ASCII diagram, comprised of minus signs -, plus signs +, vertical bars | and whitespaces . Your
    task is to write a function which breaks the diagram in the minimal pieces it is made of.
    For example, if the input for your function is this diagram:
+------------+
|            |
|            |
|            |
+------+-----+
|      |     |
|      |     |
+------+-----+
the returned value should be the list of:
+------------+
|            |
|            |
|            |
+------------+
(note how it lost a + sign in the extraction)
as well as
+------+
|      |
|      |
+------+
and
+-----+
|     |
|     |
+-----+
    """
    # 把整个图形存入数组，如果是+则存入方向权值（一定大于等于2），如果是-或者|则直接存入，如果是空格则存入0，遇到\n则y清零
    shape_int = shape.split('\n')
    print(shape_int)
    line_num = len(shape_int)
    column_num = max(len(shape_int[i]) for i in range(line_num))
    print('line_num=', line_num)
    print('column_num=', column_num)

    # 遍历数组，找到一个加号，这个加号作为起点，而且它只有两个方向有线
    # 沿着其中一个方向开始找下一个加号，如果下一个加号也只有两个方向，将他添加进路径，将他从主list中移除
    # 当下一个加号有遇到分叉路，”最小权值“的路线，坐标x+y尽可能小的方向，做深度遍历，直到回到原点，将分叉路上加号的权值-1
    # 当完成一个闭环时候，将起点从主list中移除，返回该路径上经过的所有加号的点的清单
    # 根据清单生成图形
    # 生成图形之前，如果前后两个加号的x后者y一样，则删除该加号
    # 生成图形前，尝试做平移，取所有点的x的最小值，如果该最小值>0，则所有点减去这个minX值，y值同理
    # 图形生成逻辑：遇到列表中的点，则打+号，+和+中间横向补充-，纵向补充|，其余地方补充空格
    # 当主list中
    return 1


shape = '\n+------------+\n' \
        '|            |\n' \
        '|            |\n' \
        '|            |\n' \
        '+------+-----+\n' \
        '|      |     |\n' \
        '|      |     |\n' \
        '+------+-----+'


# 嵌套使用format 花括号
def diamond(n):
    # Make some diamonds!
    if n % 2 == 0 or n < 0:
        return None
    rtn_str = ''
    fmt = '{{:^{}}}'.format(n)
    for i in range(n):
        if i <= n / 2:
            rtn_str += fmt.format("*" * (2 * i + 1)).rstrip() + '\n'
        else:
            rtn_str += fmt.format("*" * (2 * (n - i) - 1)).rstrip() + '\n'
    return rtn_str


# ryan = diamond(7)


def checkered_board(n):
    # Checkered Board
    rtn_arr = []
    for line in range(n):
        line = ['\u25A0' if (line + i + n) % 2 == 1 else '\u25A1' for i in range(n)]
        rtn_arr.append(' '.join(line))
    return '\n'.join(rtn_arr)


ryan = checkered_board(2), "■ □ ■\n□ ■ □\n■ □ ■"


print(ryan)
