# coding=utf-8
import json
# import stringprep
# from itertools import cycle
# from itertools import chain

import time
from time import sleep


class SecureList(list):
    """
    Only-Readable-Once list, which is a list that can only be read once.
    """

    def __getitem__(self, index):
        rtn = super().__getitem__(index)
        super().__delitem__(index)
        return rtn

    def __repr__(self):
        rtn = super().__repr__()
        super().clear()
        return rtn

    def __str__(self):
        rtn = super().__str__()
        super().clear()
        return rtn

    def __len__(self):
        rtn = super().__len__()
        super().clear()
        return rtn


# base = [1, 2, 3, 4]
# a = SecureList(base)
# print(a[0])
# print(len(a))
# print(len(a))
# print(a)
# print(a)
# # print(a[0])
# # print(a[0])
# print(a)

# coding=utf-8

def greet(my_name, your_name):
    return "Hello %s, my name is %s" % (your_name, my_name)


class Person:
    def __init__(self, name):
        self.__name = name

    def greet(self, name):
        return f"Hello {name}, my name is {self.__name}"

    @property
    def name(self):
        return self.__name


# joe = Person('Joe')
# print(joe.greet('Kate'))
# print(joe.name)


class Animal:
    def __init__(self, name, species, age, health, weight, color):
        self.name = name
        self.species = species
        self.age = age
        self.health = health
        self.weight = weight
        self.color = color


def make_class(*attributes):
    """
    利用另一个方法来简单化多参数的构造类，很赞
    """

    class inner:
        def __init__(self, *args):
            for attr, arg in zip(attributes, args):
                setattr(self, attr, arg)

    return inner


Animel = make_class("name", "species", "age", "health", "weight", "color")

dog1 = Animal("Bob", "Dog", 5, "good", "50lb", "brown")
dog2 = Animel("Bob", "Dog", 5, "good", "50lb", "brown")


# print(dog1.name, dog2.name)
# print(dog1.species, dog2.species)
# print(dog1.age, dog2.age)
# print(dog1.health, dog2.health)
# print(dog1.weight, dog2.weight)
# print(dog1.color, dog2.color)


# class Student:
#
#     def __init__(self, first_name, last_name, grade=[]):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.grades = grade[:]
#         # 空列表的地址是相同的，所以此处不能用self.grades = grades，否则会使用引用，导致所有值一样
#
#     def add_grade(self, grade):
#         self.grades.append(grade)
#
#     def get_average(self):
#         return sum(self.grades) / len(self.grades)


# Last semester
# Everything is working fine, no angry emails
# matthewConnorGrades = [44, 53, 27, 60]
# chloeMadisonGrades = [79, 58, 30, 66]
# studentGrades = matthewConnorGrades, chloeMadisonGrades
# matthewConnor = Student('Matthew', 'Connor', matthewConnorGrades)
# chloeMadison = Student('Chloe', 'Madison', chloeMadisonGrades)
# students = matthewConnor, chloeMadison

# for i, student in enumerate(students):
#     print(student.grades, studentGrades[i])

# test.describe("New semester. Something is wrong, can you fix it?")

# Very beginning of the semester
# Initialize students so they can log in to the online report card
# johnDoe = Student('John', 'Doe')
# janeDoe = Student('Jane', 'Doe')
# jamesSmith = Student('James', 'Smith')
# jennaSmith = Student('Jenna', 'Smith')
# students = johnDoe, janeDoe, jamesSmith, jennaSmith

# First graded assessment
# Update students' grades so they can see them on the online report card
# firstAssessmentGrades = [63, 92, 82, 75]
# for i, student in enumerate(students):
#     # print(i, student, student.grades, id(student.grades), firstAssessmentGrades[i])
#     student.add_grade(firstAssessmentGrades[i])


# And then the angry emails started coming in...
# for i, student in enumerate(students):
#     print(student.grades, firstAssessmentGrades[i])


def split_the_bill(x: dict):
    avg = sum(x.values()) / len(x)
    return {k: round(v - avg, 2) for k, v in x.items()}


ryan = split_the_bill({'A': 20, 'B': 15, 'C': 10})


# print(ryan)


def scoreboard(who_ate_what):
    who_score = []
    for person in who_ate_what:
        score = {'name': person.get('name', ''),
                 'score': person.get('chickenwings', 0) * 5 +
                          person.get('hamburgers', 0) * 3 +
                          person.get('hotdogs', 0) * 2}
        who_score.append(score)
    return sorted(sorted(who_score, key=lambda x: x['name']), key=lambda x: (x['score']), reverse=True)


# ryan = scoreboard([])
# print(ryan)


class Sphere:
    def __init__(self, radius, mass):
        self.radius = radius
        self.mass = mass

    def get_radius(self):
        """radius of the Sphere (do not round it)"""
        return self.radius

    def get_mass(self):
        """mass of the Sphere (do not round it)"""
        return self.mass

    def get_volume(self):
        from math import pi
        """volume of the Sphere (rounded to 5 place after the decimal)"""
        return round(4 / 3 * pi * self.radius ** 3, 5)

    def get_surface_area(self):
        from math import pi
        """surface area of the Sphere (rounded to 5 place after the decimal)"""
        return round(4 * pi * self.radius ** 2, 5)

    def get_density(self):
        from math import pi
        """density of the Sphere (rounded to 5 place after the decimal)"""
        return round(self.mass / (4 / 3 * pi * self.radius ** 3), 5)


# ball = Sphere(0.7, 21.28)
# print(ball.get_density())


class Block:
    def __init__(self, args):
        self.width, self.length, self.height = args

    def get_width(self):
        return self.width

    def get_length(self):
        return self.length

    def get_height(self):
        return self.height

    def get_volume(self):
        return self.width * self.length * self.height

    def get_surface_area(self):
        return 2 * (self.width * self.length + self.width * self.height + self.length * self.height)


# block1 = Block([2,2,2])
# print(block1.get_volume())


# def anything(thing):
#     """
#         try to return anything else :)
#         不管什么都返回True，用函数实现不了，需要用类
#     """
#     return thing


class anything(object):
    """
        try to return anything else :)
        不管什么都返回True，用函数实现不了，需要用类
        太牛了，这个思路
    """

    def __init__(self, foo): pass

    def __eq__(self, other): return True

    __ne__ = __lt__ = __le__ = __gt__ = __ge__ = __eq__


EXPERIENCE = {1: 0, 2: 83, 3: 174, 4: 276, 5: 388, 6: 512, 7: 650, 8: 801, 9: 969, 10: 1154, 11: 1358, 12: 1584,
              13: 1833,
              14: 2107, 15: 2411, 16: 2746, 17: 3115, 18: 3523, 19: 3973, 20: 4470, 21: 5018, 22: 5624, 23: 6291,
              24: 7028,
              25: 7842, 26: 8740, 27: 9730, 28: 10824, 29: 12031, 30: 13363, 31: 14833, 32: 16456, 33: 18247, 34: 20224,
              35: 22406, 36: 24815, 37: 27473, 38: 30408, 39: 33648, 40: 37224}
ROCKS = {'Clay': (1, 5), 'Copper': (1, 17.5), 'Tin': (1, 17.5), 'Iron': (15, 35), 'Silver': (20, 40), 'Coal': (30, 50),
         'Gold': (40, 65)}


class Miner:
    def __init__(self, xp=0):
        self.xp = xp
        for level in range(1, 40):
            if EXPERIENCE[level] <= self.xp <= EXPERIENCE[level + 1]:
                self.level = level
        # 使用Next来记录下一级的经验值，也可以
        # self.level = next(i for i in range(40, 0, -1) if exp >= EXPERIENCE[i])
        if xp > EXPERIENCE[40]:
            self.level = 40

    def mine(self, rock):
        level, xp = ROCKS.get(rock)
        if self.level < level:
            return f"You need a mining level of {ROCKS.get(rock)[0]} to mine {rock}."

        self.xp += xp
        if self.level < 40 and self.xp >= EXPERIENCE[self.level + 1]:
            self.level += 1
            return f"Congratulations, you just advanced a Mining level! Your mining level is now {self.level}."
        else:
            return f"You swing your pick at the rock."


# class DefaultList(list):
#     """
#     创建一个允许默认值的列表
#     """
#
#     def __init__(self, list_: list, default_value):
#         self.data = list_
#         self.default_value = default_value
#
#     def __getitem__(self, index):
#         try:
#             return self.data[index]
#         except IndexError:
#             return self.default_value
#
#     def __setitem__(self, index, value):
#         try:
#             self.data[index] = value
#         except IndexError:
#             self.data.extend([self.default_value] * (index - len(self.data) + 1))
#             self.data[index] = value
#
#     def __len__(self):
#         return len(self.data)
#
#     def __repr__(self):
#         return repr(self.data)
#
#     def extend(self, iterable):
#         self.data.extend(iterable)
#
#     def append(self, value):
#         self.data.append(value)
#
#     def remove(self, value):
#         self.data.remove(value)
#
#     def insert(self, index, value):
#         self.data.insert(index, value)
#
#     def pop(self, index=-1):
#         try:
#             return self.data.pop(index)
#         except IndexError:
#             return self.default_value


class Student1:
    def __init__(self, name, fives, tens, twenties):
        self.name = name
        self.fives = fives
        self.tens = tens
        self.twenties = twenties


def most_money(students):
    """
        读取所有学生的钱，并返回最多的钱的人
    """
    money = [(student.name, student.fives * 5 + student.tens * 10 + student.twenties * 20) for student in students]
    money = sorted(money, key=lambda x: x[1])
    if len(money) == 1:
        return money[0][0]
    elif money[0][1] == money[-1][1]:
        return 'all'
    else:
        return money[-1][0]


# def most_money1(students):
#     money = lambda student: 5 * student.fives + 10 * student.tens + 20 * student.twenties
#     if len(students) == 1: return students[0].name
#     D = {money(student): student.name for student in students}
#     return "all" if len(D) == 1 else D[max(D)]


def most_money2(s):
    d = {i.name: i.fives * 5 + i.tens * 10 + i.twenties * 20 for i in s}
    return max(d, key=lambda x: d[x]) if len(set(d.values())) != 1 or len(d) == 1 else 'all'


# phil = Student("Phil", 2, 2, 1)
# cam = Student("Cameron", 2, 2, 0)
# geoff = Student("Geoff", 0, 3, 0)


class Router:
    def __init__(self):
        self.route_table = {}  # 路由表，使用了双主键字典

    def bind(self, path, method, func):
        self.route_table[(path, method)] = func

    def runRequest(self, path, method):
        # return self._routes.get((url, method), lambda: "Error 404: Not Found")()   # 这一行更简单
        try:
            return self.route_table[(path, method)]()
        except KeyError:
            return "Error 404: Not Found"


class keyword_cipher1(object):
    def __init__(self, abc: str, keyword: str):
        self.abc = abc
        self.keyword = ''.join(keyword[k] for k in range(len(keyword)) if keyword[:k + 1].count(keyword[k]) == 1)
        self.cipher = self.keyword + ''.join(i for i in abc if i not in keyword)
        print(self.abc, self.keyword, self.cipher)

    def encode(self, plain):
        return ''.join(self.cipher[self.abc.index(k)] if self.abc.find(k) != -1 else k for k in plain)

    def decode(self, ciphered):
        return ''.join(self.abc[self.cipher.index(k)] if self.cipher.find(k) != -1 else k for k in ciphered)


class keyword_cipher(object):
    """
    利用translate函数真的太厉害了
    """

    def __init__(self, abc, kw):
        key = ''.join(dict.fromkeys(kw + abc).keys())
        self.encode = lambda s: s.translate(str.maketrans(abc, key))
        self.decode = lambda s: s.translate(str.maketrans(key, abc))


# Something goes Here ...

class Fraction:

    def __init__(self, numerator, denominator):
        self.top = numerator
        self.bottom = denominator

    # Equality test

    def __eq__(self, other):
        first_num = self.top * other.bottom
        second_num = other.top * self.bottom
        return first_num == second_num

    # add

    def __add__(self, other):
        from math import gcd
        numerator = self.top * other.bottom + self.bottom * other.top
        denominator = self.bottom * other.bottom
        g = gcd(numerator, denominator)
        numerator, denominator = numerator // g, denominator // g
        return Fraction(numerator, denominator)

    def __repr__(self):
        return f"{self.top}/{self.bottom}"


def gen1(n, iterable):
    """
    操纵迭代器，生成一个新的迭代器，非自己独立完成。以后继续看
    """
    from itertools import cycle
    it = cycle(iterable)
    single_tuple = []
    while True:
        while len(single_tuple) < n:
            single_tuple.append(next(it))
        yield tuple(single_tuple)
        single_tuple = single_tuple[1:]


def gen(n, iterable):
    from itertools import cycle
    iter, r = cycle(iterable), ()

    while True:
        while len(r) < n:
            r += (next(iter),)
        yield r
        r = r[1:]


# ryan mark
class UnexpectedTypeException(Exception):
    pass


def expected_type(return_types):
    def decor(func):
        def wrapper(*args, **kwargs):
            ans = func(*args, **kwargs)
            if not isinstance(ans, return_types):
                raise UnexpectedTypeException(f'Was expecting instance of {return_types}')
            return ans

        return wrapper

    return decor


# class HighScoreTable:
#     def __init__(self, numbers, s=[]):
#         self.numbers = numbers
#         self.score_list = s[:]
#         print(self.numbers, self.score_list)
#
#     def update(self, score):
#         self.score_list.append(score)
#         self.score_list = sorted(self.score_list, reverse=True)
#         while len(self.score_list) > self.numbers:
#             self.score_list.pop()
#
#     def reset(self):
#         self.score_list.clear()
#
#     @property
#     def scores(self):
#         return self.score_list


TEXT2HEX = {'\x0c': '0c', 'l': '6c', 'm': '6d', 'T': '54', 'w': '77', 'y': '79', '>': '3e', '/': '2f', 'Y': '59',
            'h': '68', 'j': '6a', 'c': '63', 'E': '45', 'F': '46', '%': '25', '\n': '0a', 's': '73', 't': '74',
            '?': '3f', 'L': '4c', '<': '3c', 'H': '48', ':': '3a', 'N': '4e', '&': '26', 'r': '72', 'J': '4a',
            'b': '62', '$': '24', 'd': '64', 'I': '49', 'q': '71', '=': '3d', 'Q': '51', '(': '28', 'a': '61',
            '}': '7d', '7': '37', 'R': '52', '#': '23', 'A': '41', 'Z': '5a', 'x': '78', ';': '3b', '\t': '09',
            '*': '2a', 'e': '65', '\r': '0d', 'P': '50', '0': '30', '{': '7b', 'C': '43', 'O': '4f', '\x0b': '0b',
            '2': '32', 'z': '7a', ',': '2c', 'B': '42', '|': '7c', 'W': '57', 'G': '47', 'V': '56', "'": '27',
            '4': '34', '!': '21', 'v': '76', '6': '36', '^': '5e', '3': '33', 'K': '4b', ')': '29', '.': '2e',
            '-': '2d', 'g': '67', '5': '35', '8': '38', 'S': '53', 'D': '44', '~': '7e', 'k': '6b', '@': '40',
            '9': '39', '[': '5b', 'p': '70', 'f': '66', 'M': '4d', 'X': '58', '1': '31', ' ': '20', 'o': '6f',
            '_': '5f', 'i': '69', '\\': '5c', 'U': '55', ']': '5d', 'n': '6e', '`': '60', '"': '22', '+': '2b',
            'u': '75'}


class HexCipher:
    @classmethod
    def encode(cls, s, n):
        for _ in range(n):
            s = s.translate(str.maketrans(TEXT2HEX))
        return s

    @classmethod
    def decode(cls, s, n):
        HEX2TEXT = {v: k for k, v in TEXT2HEX.items()}
        for _ in range(n):
            s = ''.join(HEX2TEXT.get(s[i: i + 2]) for i in range(0, len(s), 2))
        return s


# ryan mark, tod , RPS Knockout Tournament Winner
# 这题需要根据输赢来找到规律，所要猜 对手的规律
# 推测的可能策略一共是： 递增（可等效转化为递减），递增+2，递增+3 +n等
# m开始每个+1，123123 231231 312312
# m开始每个+2，132132 213213 321321
# m开始每个+3，111111 222222 333333
# m开始每个+4=+2
# 发现是错的，题目里的测试用例是，“一定是有循环存在”，所以需要找到循环体。然后根据对手的返回
class RockPaperScissorsPlayer:
    # Your name as displayed in match results.
    def get_name(self):
        pass

    # Used by playground to get your game shape (values: "R", "P" or "S").
    def get_shape(self):
        pass

    # Used by playground to notify you that a new match will start.
    def set_new_match(self, opponentName):
        pass

    # Used by playground to inform you about the shape your opponent played in the game.
    def set_opponent_shape(self, shape):
        pass


class Player(RockPaperScissorsPlayer):
    PRS = {'S': 'R', 'R': 'P', 'P': 'S'}

    def __init__(self):
        self.opp_history = ""
        # 用 opp_start 和 opp_step， 来表示对手的策略，根据start和step来生成自己的手势
        # my_start 和 my_step来生成自己的策略？

    def get_shape(self):
        """
        推测题目策略： 根据self.opp_history 数据，找到循环体——即对手的策略
        # 找到s在opp_history里的位置列表，根据列表猜测下一个手势
        # 如果无法得到循环体则随机生成一个手势
        公式为 m + i ，m 是循环体的长度，i 是偏移量

        发现猜测是错的，题目里还有一种规则是：随机只出一或者两个手势，不符合以上规则
        有点难，暂时不做"""
        nextstr = ""
        for i in range(1, len(self.opp_history) + 1):
            loopstr = self.opp_history[:i]
            print('尝试loopstr:', loopstr)
            j = len(self.opp_history) // len(loopstr) + 1
            wholestr = loopstr * j
            if wholestr.find(self.opp_history) != -1:
                print('在wholestr:', wholestr, '匹配opp_history', self.opp_history, '成功')
                nextstr = wholestr[len(self.opp_history)]
                break
        if nextstr:
            rtn = {'S': 'R', 'R': 'P', 'P': 'S'}[nextstr]
            print(f'出：{rtn}')
            return rtn
        else:
            print('第一局，随机出')
            return self.generate()

    def get_name(self):
        return "MyPlayer"

    @staticmethod
    def generate():
        import random
        return "PRS"[random.randint(0, 2)]

    def set_new_match(self, opponentName):
        self.opp_history = ""
        self.oppentName = opponentName
        print('new game start')

    def set_opponent_shape(self, shape):
        self.opp_history += shape
        print('opp_history', self.opp_history)



PRS = {'S': 'R', 'R': 'P', 'P': 'S'}


# class Player1(RockPaperScissorsPlayer):
#     def __init__(self):
#         self.opponent = ''
#         self.opponent_strategy = {
#             'Vitraj Bachchan': cycle('S'),
#             'Sven Johanson': cycle('SSPRRS'),
#             'Bin Jinhao': cycle('SRSPRP'),
#             'Jonathan Hughes': cycle('RPS'),
#             'Max Janssen': cycle('RP')
#         }
#
#     @staticmethod
#     def get_name(): return "MyPlayer"
#
#     def get_shape(self): return PRS[next(self.opponent_strategy[self.opponent])]
#
#     def set_new_match(self, opponent_name): self.opponent = opponent_name
#
#     def set_opponent_shape(self, shape): pass


# 自定义类型，并比较面积排序
# class Shape:
#     def __init__(self):
#         pass
#
#     def __lt__(self, other):
#         return self.area < other.area


# class Triangle(Shape):
#     def __init__(self, width, height):
#         self.area = width * height / 2
#
#
# class Circle(Shape):
#     def __init__(self, radius):
#         from math import pi
#         self.area = pi * radius ** 2
#
#
# class Rectangle(Shape):
#     def __init__(self, width, height):
#         self.area = width * height
#
#
# class Square(Shape):
#     def __init__(self, side):
#         self.area = side ** 2
#
#
# class CustomShape(Shape):
#     def __init__(self, area):
#         self.area = area


# 构建 Graph 图， Construct Graph Class (simple)


class IllegalArgumentError(Exception): pass


class Graph:
    def __init__(self, v):
        if v < 0:
            raise IllegalArgumentError()
        self.V = v
        self.E = 0
        self.adj = [[] for _ in range(v)]

    def add_edge(self, v, w):
        if v < 0 or w < 0 or self.V <= v or self.V <= w:
            raise IllegalArgumentError()
        self.adj[v].append(w)
        self.adj[w].append(v)
        self.E += 1


# Packet Delivery -- Enforcing Constraints
# 类操作的基本功要求比较高
# class Package1(object):
#     maxs = {"length": 350, "width": 300, "height": 150, "weight": 40}
#
#     def __init__(self, l, w, h, wg):
#         self.length = l
#         self.width = w
#         self.height = h
#         self.weight = wg
#
#     # 不理解，是先init之后，再调用setattr检查，还是先调用setattr，再init？
#     # 经过验证，是init的时候，就会使用setattr进行检查
#     def __setattr__(self, att, v):
#         if v <= 0 or v > Package.maxs[att]: raise DimensionsOutOfBoundError(att, v, Package.maxs[att])
#         self.__dict__[att] = v
#
#     @property
#     def volume(self):
#         return self.length * self.width * self.height


# 抛出自定义异常消息的一种方式
class DimensionsOutOfBoundError1(Exception):
    def __init__(self, dim, val, max):
        self.str = "Package %s==%d out of bounds, should be: 0 < %s <= %d" % (dim, val, dim, max)

    def __str__(self):
        return self.str


# 抛出自定义异常消息的另一种方式
class DimensionsOutOfBoundError(Exception): pass


class Package(object):
    LIMITS = {'length': (0, 350), 'width': (0, 300), 'height': (0, 150), 'weight': (0, 40)}

    def __init__(self, *args):
        self.length, self.width, self.height, self.weight = args

    @property
    def volume(self): return self.length * self.width * self.height

    def __setattr__(self, field, val):
        mi, ma = self.LIMITS[field]
        if not (mi < val <= ma):
            raise DimensionsOutOfBoundError(
                "Package {0}=={1} out of bounds, should be: {2} < {0} <= {3}".format(field, val, mi, ma))
        self.__dict__[field] = val


class Grid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.grid_arr = [['0'] * width for _ in range(height)]

    def plot_point(self, x, y):
        self.grid_arr[y - 1][x - 1] = 'X'

    def __repr__(self):
        return '\n'.join(''.join(self.grid_arr[i]) for i in range(self.height))

    @property
    def grid(self):
        return self.__repr__()


class Cipher(object):
    def __init__(self, map1, map2):
        self.cipher_dict12 = str.maketrans(map1, map2)
        self.cipher_dict21 = str.maketrans(map2, map1)

    def encode(self, s):
        return s.translate(self.cipher_dict12)

    def decode(self, s):
        return s.translate(self.cipher_dict21)


def timer(t1):
    def inner(func):
        def wrapper():
            start_time = time.time()
            func()
            end_time = time.time()
            return t1 > end_time - start_time

        return wrapper

    return inner


@timer(1)
def foo():
    sleep(0.1)


@timer(1)
def bar():
    sleep(1.1)


class Vehicle:
    def __init__(self, seats, wheels, engine):
        self.seats = seats
        self.wheels = wheels
        self.engine = engine


def show_me(instname):
    inst_values = ' and '.join(sorted(instname.__dict__.keys()))
    inst_values = str.replace(inst_values, ' and ', ', ', inst_values.count(' and ') - 1)
    return f"Hi, I'm one of those {instname.__class__.__name__}s! Have a look at my {inst_values}."


class Planet:
    def __init__(self, moon):
        self.moon = moon


# earth = Planet('moon')
#
# porsche = Vehicle(2, 4, 'Gas')
# print(show_me(porsche))
#
# print(porsche.__dict__)


# class Planet:
#     def __init__(self, moon):
#         self.moon = moon


# 使用修饰器来修饰类，这个有点意思
def jsonattr(filepath):
    """Class decorator which reads property/value pairs from a JSON file and
       implements them as part of the decorated class.
       -- filepath - The filepath of the JSON file.
    """
    f = open(filepath, "r")
    data = json.loads(f.read())
    f.close()

    def decorator(cls):
        for p, value in data.items():
            setattr(cls, p, value)
        return cls

    return decorator


@jsonattr("myClass.json")
class MyClass:
    def __init__(self, foo, an_int, this_kata_is_awesome):
        self.foo = foo
        self.an_int = an_int
        self.this_kata_is_awesome = this_kata_is_awesome


# class ryan_class:
#     def __init__(self, ryanin):
#         self.ryanin = ryanin
#
#     def ryanp(self, i: int):
#         return f"integer in, {i}"
#
#     def ryanp(self, s: str):
#         return f"str in, {s}"


# The Fruit Juice
class Jar():
    def __init__(self):
        self.total_amount = 0
        self.fruit_list = {}

    def add(self, amount, kind):
        self.total_amount += amount
        self.fruit_list[kind] = self.fruit_list.get(kind, 0) + amount

    def pour_out(self, amount):
        if amount >= self.total_amount:
            self.total_amount = 0
            self.fruit_list.clear()
        else:
            percentage = amount / self.total_amount
            for k, v in self.fruit_list.items():
                self.fruit_list[k] = v * (1 - percentage)
            self.total_amount -= amount

    def get_total_amount(self):
        return self.total_amount

    def get_concentration(self, kind):
        print(self.total_amount)
        print(self.fruit_list)
        return self.fruit_list.get(kind, 0) / self.total_amount if self.total_amount else 0


# jar = Jar()
# print(jar.get_total_amount())
# print(jar.get_concentration("apple"))
# jar.add(100, "apple")
# print(jar.get_total_amount())
# print(jar.get_concentration("apple"))
# jar.add(100, "apple")
# print(jar.get_total_amount())
# print(jar.get_concentration("apple"))
# jar.add(200, "banana")
# print(jar.get_total_amount())
# print(jar.get_concentration("apple"))
# print(jar.get_concentration("banana"))
# jar.pour_out(200)
# print(jar.get_total_amount())
# print(jar.get_concentration("apple"))
# print(jar.get_concentration("banana"))
# jar.add(200, "apple")
# print(jar.get_total_amount())
# print(jar.get_concentration("apple"))
# print(jar.get_concentration("banana"))


# Binary Search Trees, 二叉树搜索
class Tree(object):
    def __init__(self, root, left=None, right=None):
        assert root and type(root) == Node
        if left: assert type(left) == Tree and left.root < root
        if right: assert type(right) == Tree and root < right.root

        self.left = left
        self.root = root
        self.right = right

    def is_leaf(self):
        return not (self.left or self.right)

    def __str__(self):
        if self.is_leaf():
            return f"[{self.root}]"
        return f"[{self.left or '_'} {self.root} {self.right or '_'}]"

    def __eq__(self, other):
        return str(self) == str(other)

    def __ne__(self, other):
        return not (self == other)


class Node(object):
    def __init__(self, value, weight=1):
        self.value = value
        self.weight = weight

    def __str__(self):
        return str(self.value)

    def __lt__(self, other):
        return self.value < other.value

    def __gt__(self, other):
        return self.value > other.value

    def __eq__(self, other):
        return self.value == other.value

    def __ne__(self, other):
        return self.value != other.value


# end


# PaginationHelper
class PaginationHelper:

    # The constructor takes in an array of items and a integer indicating
    # how many items fit within a single page
    def __init__(self, collection, items_per_page):
        print(collection, items_per_page)
        self.collection = collection
        self.item_per_page = items_per_page

    # returns the number of items within the entire collection
    def item_count(self):
        return len(self.collection)

    # returns the number of pages
    def page_count(self):
        return self.item_count() // self.item_per_page + 1

    def page_item_count(self, page):
        print('page', page)
        if self.page_count() < page + 1 or page < 1:
            return -1
        if page == self.page_count() - 1:
            return self.item_count() % self.item_per_page
        return self.item_per_page

    def page_index(self, item_index):
        if item_index < 0 or item_index >= self.item_count():
            return -1
        return item_index // self.item_per_page


# collection = range(1,25)
# helper = PaginationHelper(collection, 10)
# print(helper.page_count())
# print(helper.page_index(23))
# print(helper.item_count())

# end


# Simple Finite State Machine Compiler

class FSM(object):
    def __init__(self, ins):
        line = [i.strip().split(';') for i in ins.replace(' ', '').split('\n')]
        self.instructions = {}
        for i in line:
            i1, i2, i3 = i[0].strip(), i[1].strip().split(','), int(i[2].strip())
            self.instructions[i1] = (i2, i3)

    def run_fsm(self, start, sequence):
        rtn_path = [start]
        rtn_value = self.instructions[start][1]
        for i in sequence:
            rtn_path.append(self.instructions[rtn_path[-1]][0][i])
            rtn_value = self.instructions[rtn_path[-1]][1]
        return rtn_path[-1], rtn_value, rtn_path


instructions = \
    '''S1; S1, S2; 9
    S2; S1, S3; 10
    S3; S4, S3; 8
    S4; S4, S1; 0'''

# fsm = FSM(instructions)
# ryan = fsm.run_fsm('S1', [0, 1, 1, 0, 1])
# print(ryan)

"""
我的解法，是错误的，没用列表来存储所有history
class VersionManager:
    def __init__(self, version="0.0.1", lastversion=None):
        if not version:
            version = "0.0.1"
        try:
            v = version.split('.')
            ma = int(v[0]) if len(v) > 0 else 0
            mi = int(v[1]) if len(v) > 1 else 0
            pa = int(v[2]) if len(v) > 2 else 0
            self.version = f'{ma}.{mi}.{pa}'
            self.lastversion = lastversion
        except TypeError:
            raise TypeError("Error occured while parsing version")
        except ValueError:
            raise ValueError("Error occured while parsing version!")

    def major(self):
        v = self.version.split('.')
        self.lastversion = self.version
        self.version = f'{int(v[0])+1}.0.0'
        return self

    def minor(self):
        v = self.version.split('.')
        self.lastversion = self.version
        self.version = f'{int(v[0])}.{int(v[1])+1}.0'
        return self

    def patch(self):
        v = self.version.split('.')
        self.lastversion = self.version
        self.version = f'{int(v[0])}.{int(v[1])}.{int(v[2])+1}'
        return self

    def release(self):
        return self.version

    def rollback(self):
        if self.lastversion:
            self.version = self.lastversion
            return VersionManager(self.version)
        else:
            raise ValueError("Cannot rollback!")
"""


class VersionManagerException(Exception): pass


# Versions manager
class VersionManager:
    def __init__(self, version: str = '0.0.1') -> None:
        try:
            self.history = [(0, 0, 1) if version == '' else tuple(map(int, f'{version}.0.0'.split('.')[:3]))]
        except ValueError:
            raise VersionManagerException('Error occured while parsing version!') from None

    def release(self) -> str:
        return '{}.{}.{}'.format(*self.history[-1])

    def major(self) -> 'VersionManager':
        major, _, _ = self.history[-1]
        self.history.append((major + 1, 0, 0))
        return self

    def minor(self) -> 'VersionManager':
        major, minor, _ = self.history[-1]
        self.history.append((major, minor + 1, 0))
        return self

    def patch(self) -> 'VersionManager':
        major, minor, patch = self.history[-1]
        self.history.append((major, minor, patch + 1))
        return self

    def rollback(self) -> 'VersionManager':
        if len(self.history) <= 1: raise VersionManagerException('Cannot rollback!')
        self.history.pop()
        return self


# ryan = VersionManager().major().rollback()
# # ryan.major().minor()
# print(ryan.release())


from copy import copy


# Vector class
class Vector:
    def __init__(self, list_=None):
        self.list_ = copy(list_)
        self.len = len(list_)

    def add(self, bvector):
        if self.len != bvector.len:
            raise Exception("Not the same length")
        else:
            l1 = [i + j for i, j in zip(self.list_, bvector.list_)]
            return Vector(l1)

    def subtract(self, bvector):
        if self.len != bvector.len:
            raise Exception("Not the same length")
        else:
            l1 = [i - j for i, j in zip(self.list_, bvector.list_)]
            return Vector(l1)

    def dot(self, bvector):
        if self.len != bvector.len:
            raise Exception("Not the same length")
        else:
            l1 = [i * j for i, j in zip(self.list_, bvector.list_)]
            return sum(l1)

    def norm(self):
        l1 = sum(i ** 2 for i in self.list_)
        return l1 ** 0.5

    def equals(self, bvector):
        return all(i == j for i, j in zip(self.list_, bvector.list_))

    def __str__(self):
        return f'({",".join(str(i) for i in self.list_)})'


# a = Vector([1, 2])
# b = Vector([3, 4])

# ryan = a.add(b).equals(Vector([4, 6, 8]))
# print(ryan)


# 使用checkLen来检查长度的方法更好
# class Vector(list):
#
#     def checkLen(f):
#         def wrapper(self, other):
#             if len(self) != len(other):
#                 raise ValueError("Vectors must have the same length")
#             return f(self,other)
#         return wrapper
#
#     def __init__(self,lst): super().__init__(lst)
#
#     equals = list.__eq__
#
#     def __str__(self):      return '({})'.format(",".join(map(str, self)))
#
#     def norm(self):         return self.dot(self)**.5
#
#     @checkLen
#     def add(self, b):       return Vector( [n + b[i] for i,n in enumerate(self)] )
#
#     @checkLen
#     def dot(self, b):       return sum( n * b[i] for i,n in enumerate(self) )
#
#     @checkLen
#     def subtract(self, b):  return Vector( [n - b[i] for i,n in enumerate(self)] )


def limiter(limit, unique, lookup):
    def wrapper(class_):
        instances = {}
        lookups = {}

        def getinstance(*args, **kwargs):
            new_obj = class_(*args, **kwargs)
            if "FIRST" not in lookups: lookups["FIRST"] = new_obj

            id = getattr(new_obj, unique)
            if id in instances:
                res = instances[id]
            elif len(instances) < limit:
                instances[id] = new_obj
                res = lookups["LAST"] = new_obj
            else:
                res = lookups[lookup]

            lookups["RECENT"] = res
            return res

        return getinstance

    return wrapper


"""
@test.describe("Sample tests")
def sample_tests():
    @test.it("Test 1")
    def it_1():
        @limiter(2, "ID", "FIRST")
        class FirstInstance:
            def __init__(self, ID, value): self.ID, self.value = ID, value
            def __repr__(self): return "FirstInstance({}, {})".format(self.ID, self.value)
        a = FirstInstance(1, 5)
        b = FirstInstance(2, 8)
        test.assert_equals(id(FirstInstance(1, 20)), id(a))
        test.assert_equals(id(FirstInstance(3, 0)), id(a))
        test.assert_equals(id(FirstInstance(2, 14)), id(b))
        test.assert_equals(id(FirstInstance(8, 7)), id(a))

    @test.it("Test 2")
    def it_2():
        @limiter(2, "ID", "LAST")
        class LastInstance:
            def __init__(self, ID, value): self.ID, self.value = ID, value
            def __repr__(self): return "LastInstance({}, {})".format(self.ID, self.value)
        a = LastInstance(1, 5)
        b = LastInstance(2, 8)
        test.assert_equals(id(LastInstance(1, 20)), id(a))
        test.assert_equals(id(LastInstance(3, 0)), id(b))
        test.assert_equals(id(LastInstance(2, 14)), id(b))
        test.assert_equals(id(LastInstance(8, 7)), id(b))

    @test.it("Test 3")
    def it_3():
        @limiter(2, "value", "RECENT")
        class RecentInstance:
            def __init__(self, ID, value): self.ID, self.value = ID, value
            def __repr__(self): return "RecentInstance({}, {})".format(self.ID, self.value)
        a = RecentInstance(1, 5)
        b = RecentInstance(2, 8)
        test.assert_equals(id(RecentInstance(4, 5)), id(a))
        test.assert_equals(id(RecentInstance(1, 0)), id(a))
        test.assert_equals(id(RecentInstance(9, 8)), id(b))
        test.assert_equals(id(RecentInstance(2, 3)), id(b))
"""


def army_get_secret_from_file():
    return {'secret_func1': lambda self: 'This is secret function, test #01',
            'secret_func2': lambda self, value: 'This is secret function with value = {0}, test #02'.format(value),
            'prop1': 12.345, 'prop2': 'some test values'}


# def create_class(class_name, s_dict):
#     # Python's Dynamic Classes #3
#     # 我的方法不能适应，会报错
#     # TypeError: army_get_secret_from_file.<locals>.<lambda>() missing 1 required positional argument: 'self'
#     class c:
#         def __init__(self):
#             self.name = class_name
#             for k, v in s_dict.items():
#                 if not hasattr(self, k):
#                     setattr(self, k, v)
#     return c



# def create_class(class_name, secrets = None):
#     # 离我答案最近的答案
#     if class_name == None or len(class_name) == 0:
#         return None
#
#     class classname(object):
#         pass
#
#     if secrets is not None:
#         for secret in secrets:
#             setattr(classname, secret, secrets[secret])
#
#     classname.__name__ = class_name
#     return classname


def create_class(class_name, secrets=None):
    # top1的答案
    secrets = secrets or {}
    return type(class_name, (), secrets) if class_name else None


# print(globals())
# armyClass = create_class('new one', army_get_secret_from_file())
# army_object = armyClass()
# print(army_object.__dict__, army_object)
# print(army_object.secret_func1(), "This is secret function, test #01",
#       "Should return from secret function #01")
# print(army_object.secret_func2(44), "This is secret function with value = 44, test #02",
#       "Should return from secret function #02")
# print(army_object.prop1, 12.345, "Should return secret property")
# print(army_object.prop2, "some test values", "Should return secret property")
#
# print(SecureList.__dict__)


class VigenereCipher(object):
    # Vigenère Cipher Helper
    def __init__(self, key: str, alphabet: str):
        self.alphabet = list(alphabet)
        self.key = [alphabet.index(i) for i in key]

    def encode(self, text):
        return "".join([self.alphabet[(self.alphabet.index(text[i]) + self.key[i % len(self.key)]) % len(self.alphabet)]
                        if text[i] in self.alphabet else text[i] for i in range(len(text))])

    def decode(self, text):
        return "".join([self.alphabet[(self.alphabet.index(text[i]) - self.key[i % len(self.key)]) % len(self.alphabet)]
                        if text[i] in self.alphabet else text[i] for i in range(len(text))])


abc = "abcdefghijklmnopqrstuvwxyz"
key = "password"
c = VigenereCipher(key, abc)
r = c.encode('codewars')

print(r)
