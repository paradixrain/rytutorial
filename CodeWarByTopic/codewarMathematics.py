# coding = utf-8

# @title: MATHEMATICS
# @desc:  codewar
# @author: RyanLin
# @date: 2022/02/07

import re
import math
from functools import reduce
from collections import Counter, deque
from itertools import cycle


def differentiate(poly):
    # 求导数 ('-x', '-1'), ('x^4', '4x^3')
    s = re.split('[x^]', poly)
    print(s)

    xloc = poly.find('x')
    if xloc > -1 and xloc + 1 < len(poly):
        if poly[xloc + 1] != '^':
            return 0
    if len(s) == 1:
        return '0'
    elif len(s) == 2:
        if s[0] == '-':
            return '-1'
        elif s[0] == '':
            return '1'
        else:
            return s[0]
    else:
        if s[0] == '-':
            return '{}{}{}'.format(str((-1) * int(s[-1])), 'x^', int(s[-1]) - 1)
        elif s[0] == '':
            return '{}{}{}'.format(str(int(s[-1])), 'x^', int(s[-1]) - 1)
        else:
            if int(s[-1]) - 1 == 1:
                return '{}{}{}'.format(str((int(s[0])) * int(s[-1])), 'x', '')
            else:
                return '{}{}{}'.format(str((int(s[0])) * int(s[-1])), 'x^', int(s[-1]) - 1)


# a = differentiate('-8')
# print(a)
# # a = differentiate('-1')
# # print(a)
# a = differentiate('x')
# print(a)
# # a = differentiate('-3x^4')
# # print(a)
# # a = differentiate('x^4')
# # print(a)
# # a = differentiate('-x^35')
# # print(a)


def create_phone_number(n):
    n = [str(i) for i in n]
    return '({}) {}-{}'.format(''.join(n[0:3]), ''.join(n[3:6]), ''.join(n[6:]))


# "(123) 456-7890"
# a = create_phone_number([1, 2, 3, 4, 5, 6, 7, 8, 9, 0])
# print(a)


from math import ceil


def movie(card, ticket, perc):
    print(card, ticket, perc)
    n = 1
    cardfee = ticket * perc
    print('card', ceil(card + cardfee))
    print('ticket', ceil(ticket * n))
    while ceil(card + cardfee) >= ceil(ticket * n):
        print('card', card + cardfee)
        print('ticket', ticket * n)
        n += 1
        card = card + cardfee
        cardfee = cardfee * perc
    return n


# a = movie(500, 15, 0.9)
# a = movie(100, 10, 0.95)
# a = movie(0, 10, 0.95)
# print(a)


# print( 10 * pow(10,0))
# print( 10 * pow(10,1))
# print( 10 * pow(10,2))


def sumlevel(mid, level):
    return mid


# ryan, undone
def mountains_of_hoiyama(width):
    sum = 0
    level = 0
    for mid in range(width, (width - 1) / 2 + 1, -1):
        sum += mid + level


# Find the First Number in Having a Certain Number of Divisors I
# ryan, undone
def find_min_num1(num_div):
    """
    m ** n有 n+1 个因子，m是2以上任意整数，基于这个，只要找到最小个的素数 m1 ** n1 * m2 ** n2 * m3 ** n3，即可
    if num_div == 6,   6 == 3 * 2 ，所以是2**3 * 3 ** 2，即 4 * 3 = 12
    if num_div == 10,  10 == 5 * 2, 所以是2**5 * 3 ** 2 即 2**4 * 3 == 48
    if num_div == 12,  12 == 3*2*2，所以是 2**3 * 3 **2 * 5 ** 2，即 2**2 * 3 * 5 = 60
    把n 拆分成递减的因子，m 为素数

    上面的算法有问题，无法取到最优，比如num_div = 16的时候应该是120，结果是210
    """
    # 将100的所有因子放入列表
    factors = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]

    # 用辗转相除法得到num_div的所有因子
    factors_num = []
    for i in factors:
        while num_div % i == 0:
            factors_num.append(i)
            num_div = num_div / i
    factors_num.sort(reverse=True)
    print(factors_num)

    rtn_i = 1
    for i in range(len(factors_num)):
        rtn_i *= factors[i] ** (factors_num[i] - 1)
    return rtn_i


def factors(n):
    return set(reduce(list.__add__,
                      ([i, n // i] for i in range(1, int(n ** 0.5) + 1) if n % i == 0)))


def find_min_num(num_div):
    loop_num = 2
    while True:
        if len(factors(loop_num)) == num_div:
            break
        loop_num += 1
    return loop_num


def is_prime(num):
    if num < 2:
        return False
    for i in range(2, int(num ** 0.5) + 1):
        if num % i == 0:
            return False
    return True


def find_nb(m):
    # Build a pile of Cubes
    n = 1
    blocks = 0
    while blocks < m:
        blocks += n * n * n
        if blocks == m:
            return n
        n += 1
    return -1


def bouncing_ball(h, bounce, window):
    # Bouncing Balls
    if not (h > 0 and 0 < bounce < 1 and window < h):
        return -1
    rtn_counts = 0
    while h > window:
        rtn_counts += 1
        h = h * bounce
        if h > window:
            rtn_counts += 1
    return rtn_counts


def calculate_years(principal, interest, tax, desired):
    # Money, Money, Money
    n = 0
    while principal < desired:
        principal = principal + principal * interest * (1 - tax)
        n += 1
    return n


# ryan = calculate_years(1000, 0.05, 0.18, 1100)
def isPP(n):
    """ What's a Perfect Power anyway? """
    for i in range(2, int(n ** 0.5) + 1):
        j = int(n ** (1 / i))
        if j ** i == n:
            return [j, i]
        if j ** (i + 1) == n:
            return [j, i + 1]
        if (j + 1) ** i == n:
            return [j + 1, i]
        if (j + 1) ** (i + 1) == n:
            return [j + 1, i + 1]
    return None


def convert_fracts(lst):
    # Common Denominators
    g2 = math.lcm(*[i[1] for i in lst])
    return [[i[0] * g2 // i[1], i[1] * g2 // i[1]] for i in lst]


def nbMonths(start_price_old, start_price_new, saving_per_month, percent_loss_by_month):
    # Buying a car
    month = 0
    if start_price_old >= start_price_new:
        return [month, start_price_old - start_price_new]
    while True:
        month += 1
        start_price_old *= (100 - percent_loss_by_month - (month // 2) * 0.5) / 100
        start_price_new *= (100 - percent_loss_by_month - (month // 2) * 0.5) / 100
        if start_price_old + saving_per_month * month >= start_price_new:
            break
    return [month, round(start_price_old + saving_per_month * month - start_price_new)]


def gps(s, x):
    # Speed Control
    if len(x) <= 1:
        return 0
    arr = [i[0] - i[1] for i in zip(x[1:], x[:-1])]
    spd = [(3600 * i) / s for i in arr]
    print('arr,', arr, '\n', spd)
    return int(max(spd))


def productFib(prod):
    # Product of consecutive Fib numbers
    fib1, fib2 = 0, 1
    for i in range(prod + 1):
        if fib1 * fib2 == prod:
            return [fib1, fib2, True]
        if fib1 * fib2 > prod:
            return [fib1, fib2, False]
        fib1, fib2 = fib2, fib1 + fib2


def productFib1(prod):
    a, b = 0, 1
    while prod > a * b:
        a, b = b, a + b
    return [a, b, prod == a * b]


def over_the_road(address, n):
    # Over The Road
    return 2 * n + 1 - address


def calc(x):
    # Char Code Calculation
    s = "".join(str(ord(i)) for i in x)
    return 6 * s.count('7')


def is_triangle(a, b, c):
    # Is this a triangle?
    return a + b > c and b + c > a and a + c > b


def step(g, m, n):
    # Steps in Primes
    for i in range(m, n - g + 1):
        if is_prime(i):
            if is_prime(i + g):
                return [i, i + g]


def race(v1, v2, g):
    # Tortoise racing
    if v1 >= v2:
        return None
    t = g / (v2 - v1) * 3600
    return [int(t // 3600), int((t % 3600) // 60), int(t % 60)]


def dig_pow(n):
    return sum(int(x) ** y for y, x in enumerate(str(n), 1))


def sum_dig_pow(a, b):
    # Take a Number And Sum Its Digits Raised To The Consecutive Powers And ....¡Eureka!!
    return [x for x in range(a, b + 1) if x == dig_pow(x)]


def power_of_two(x):
    # Power of two
    from math import log2
    return True if x != 0 and 2 ** int(log2(x)) == x else False


def is_lucky(n):
    # lucky number
    r = sum(int(i) for i in str(n))
    return r % 9 == 0


def sum_triangular_numbers(n):
    # Sum of Triangular Numbers
    if n <= 0:
        return 0
    step = [1]
    for i in range(2, n + 1):
        step.append(step[-1] + i)
    return sum(step)


# ryan = sum_triangular_numbers(6)

def divisors(integer):
    # Find the divisors!
    rtn = []
    for i in range(2, int(integer ** 0.5) + 1):
        if integer % i == 0:
            rtn.append(i)
            rtn.append(integer // i)
    if rtn:
        return sorted(set(rtn))
    return f"{integer} is prime"


# ryan = divisors(13)


def evaporator(content, evap_per_day, threshold):
    # evaporator
    days = 0
    content = 100
    while content > threshold:
        content = content * (1 - evap_per_day / 100)
        days += 1
    return days


# ryan = evaporator(10, 10, 10)


def collatz(n):
    # Collatz Conjecture Length
    # 会遇到一个问题，就是自动转化浮点数的问题，所以使用divmod，也可以使用 int // 2来
    step = 1
    while n != 1:
        print(step, n)
        i, j = divmod(n, 2)
        if j == 0:
            n = i
        else:
            n = n * 3 + 1
        step += 1
    return step


# ryan = collatz(73567465519280238573)


def alphabet(ns):
    # The alphabet product
    ns = sorted(ns)
    # print(ns)
    a, b, ab = ns[0], ns[1], ns[0] * ns[1]
    ns.remove(a)
    ns.remove(b)
    ns.remove(ab)
    # print(ns)
    c, bc = ns[0], b * ns[0]
    # print(a, b, c, ab, bc)
    ns.remove(c)
    ns.remove(bc)
    return ns[0]


# ryan = alphabet([132, 578, 25432, 6, 3, 2, 44, 1156])


def root(x, n):
    # Nth Root of a Number
    return x ** (1.0 / n)


# ryan = root(8, 3)


# def solution(n):
#     # Round by 0.5 steps
#     sn = str(n)
#     ln = sn.split('.')
#     if ln[1] < '25':
#         return float(f'{ln[0]}.0')
#     elif '25' <= ln[1] < '75':
#         return float(f'{ln[0]}.5')
#     else:
#         return float(f'{int(ln[0])+1}.0')
#
#
# def solution(n):
#     return round(n*2)/2


# ryan = solution(4.7599999)


def most_frequent_item_count(collection):
    # Find Count of Most Frequent Item in an Array
    d = Counter(collection)
    return max(d.values()) if d else 0


# ryan = most_frequent_item_count([3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3])


# def dig_pow(n, p):
#     # Playing with digits
#     i_n = list(int(i) for i in str(n))
#     total = sum(i**j for i, j in zip(i_n, range(p, p+len(i_n))))
#     return total//n if total % n == 0 else -1


# ryan = dig_pow(41, 5)


def multiples(m, n):
    # Return the first M multiples of N
    return [n * i for i in range(1, m + 1)]


# ryan = multiples(3, 5)


def number_joy(n):
    # Especially Joyful Numbers
    int_s = sum(int(i) for i in str(n))
    str_s = str(int_s)
    reversed_s = int(''.join(str_s[::-1]))
    print(str_s, reversed_s)
    return n == reversed_s * int_s


# ryan = number_joy(1739)


def buddy_num(n):
    prime_nums = set()
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            prime_nums.add(i)
            prime_nums.add(n // i)
    return sum(prime_nums)


def buddy(start, limit):
    # Buddy Pairs
    for i in range(start, limit + 1):
        j = buddy_num(i)
        if buddy_num(j) == i and j > i:
            return [i, buddy_num(i)]
    return "Nothing"


# ryan = buddy(10, 50)


def thirt(n):
    # A Rule of Divisibility by 13
    last, this = -1, n
    while last != this:
        last, this = this, 0
        l13 = cycle([1, 10, 9, 12, 3, 4])
        for i in str(last)[::-1]:
            this += int(i) * next(l13)
    return this


# ryan = thirt(123457)


def is_nice(arr):
    # Nice Array
    for i in arr:
        if i + 1 not in arr and i - 1 not in arr:
            return False
    return True if arr else False


# ryan = is_nice([2,10,9])


def num_primorial(n):
    # Primorial Of a Number
    primes = 1
    i = 0
    rtn_num = 1
    while True:
        primes += 1
        if is_prime(primes):
            rtn_num *= primes
            i += 1
        if i >= n:
            break
    return rtn_num


# ryan = num_primorial(3)


def f(x, y, z):
    # Cubes in the box
    count = 0
    for size in range(1, min(x, y, z) + 1):
        count += (x - size + 1) * (y - size + 1) * (z - size + 1)
    return count


# ryan = f(1,2,3)


def add_all(lst):
    # Add All
    rtn_num = 0
    if len(lst) < 2:
        return 0
    while True:
        if len(lst) == 2:
            rtn_num += sum(lst)
            break
        lst = sorted(lst)
        m1 = lst.pop(0)
        m2 = lst.pop(0)
        rtn_num += m1 + m2
        lst.append(m1 + m2)
    return rtn_num


# ryan = add_all([1,2,3,4,5])

def remov_nb(n):
    # Is my friend cheating?
    s = n * (n + 1) // 2
    rtn_arr = []
    for x in range(1, n):
        d, p = (s - x).__divmod__(x + 1)
        if d < n and p == 0:
            rtn_arr.append((x, d))
    return rtn_arr


# ryan = remov_nb(26)


# def get_jumps(cycle_list, k):
#     # 可行，但是效率低，需要寻找数学归纳法
#     step, length = 1, len(cycle_list)
#     x = k % length
#     while x:
#         step += 1
#         x = (x + k) % length
#     return step


def get_jumps(cycle_list, k):
    # Jumps in a cycle #1
    return math.lcm(len(cycle_list), k) // k


# ryan = get_jumps([1, 5, 1], 2)


def prod2sum(a, b, c, d):
    # Integers: Recreation Two
    print(a, b, c, d)
    all_list = [abs(a * b + c * d), abs(a * c + b * d), abs(a * d + b * c), abs(a * b - c * d), abs(a * c - b * d),
                abs(a * d - b * c)]
    sums = (a ** 2 + b ** 2) * (c ** 2 + d ** 2)
    all_list = sorted([i for i in set(all_list)])
    rtn_arr = []
    for k, v in enumerate(all_list):
        for m in range(k, len(all_list)):
            if v ** 2 + all_list[m] ** 2 == sums:
                rtn_arr.append([v, all_list[m]])
    return rtn_arr


# ryan = prod2sum(-14, 12, -10, 8)


def the_bee(n):
    # The Bee
    m = 2 * n - 1
    dp = [[0] * m for _ in range(m)]
    for i in range(n):
        dp[i][0] = dp[0][i] = 1
    for i in range(1, m):
        for j in range(1, m):
            if abs(i - j) < n:
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1] + dp[i - 1][j - 1]
    return dp[m - 1][m - 1]


# ryan = the_bee(5), 259123


def hanoi(disks):
    # Hanoi record
    return 2 ** disks - 1


# ryan = hanoi(3)
def count_zeros_n_double_fact(n):
    # Jungerstein's Math Training Room: 1. How many zeros are at the end of n!! ?
    if n % 2 == 1:
        return 0
    count = 0
    divnum = len(str(n))
    list = [i for i in range(1, n + 1) if i % 10 == 0]
    print(list)
    for i in list:
        for j in range(divnum - 1, 0, -1):
            if i % (10 ** j) == 0:
                count += j
                print(i, count, j)
                temp = i // (10 ** j)
                for k in range(divnum - 1, 0, -1):
                    if temp % (5 ** k) == 0:
                        count += k
                        print(i, count, k)
                        break
                break
    return count


def count_zeros_n_double_fact_2(n):
    # another way
    if n % 2 != 0:
        return 0
    k = 0
    while n >= 10:
        k += n // 10
        n //= 5
    return k


# ryan = count_zeros_n_double_fact(326)
# ryan = prod2sum(-14, 12, -10, 8)


def buses(kids, adults, places):
    # Buses!
    if places < 2 or adults < 1:
        return 0
    car_most = adults // 2
    if car_most * places - adults < kids:
        return 0
    else:
        return min(car_most, ((kids + adults) / places).__ceil__())


# ryan = buses(10, 4, 7)


def power_sumDigTerm(n):
    # Numbers that are a power of their sum of digits
    start_loc = -1
    lst = {j ** i for i in range(2, 20) for j in range(1, 200)}
    lst = sorted([i for i in lst])
    print(lst, len(lst))
    for i in lst:
        s = sum(int(j) for j in str(i))
        for j in range(2, len(str(i)) + 2):
            if s ** j == i:
                start_loc += 1
                if start_loc == n:
                    return i


# ryan = power_sumDigTerm(33)


def create_pythagorean_triples(diff, low, high):
    # create_pythagorean_triples
    rtn_arr = []
    for a in range(low, high + 1):
        b = (a ** 2 / diff - diff) / 2
        if b < a:
            continue
        if b.is_integer() and b > 0:
            rtn_arr.append((a, int(b), int(b + diff)))
    return rtn_arr


# ryan = create_pythagorean_triples(564,456,1654)


def all_permuted(n):
    # Shuffle It Up
    a, b = 0, 1
    for i in range(1, n):
        a, b = b, (i + 1) * (a + b)
    return a


# ryan = all_permuted(30)


def position(x, y, n):
    # Sums of consecutive integers
    return (y - (x * (x - 1) // 2)) // x + n


# ryan = position(4, 14, 3)


# def f(n):
#     # Triple your Money!
#     if n == 0: return 0
#     p = 3 ** int(math.log(n, 3))
#     return n + p if n < 2 * p else 3 * (n - p)


# ryan = f(2)


def exp_sum(n):
    # todo
    # initialize the partition function values up to n
    p = [0] * (n + 1)
    p[0] = 1

    # calculate the partition function values using recurrence
    for i in range(1, n + 1):
        j = 1
        while j * (3 * j - 1) // 2 <= i:
            k = j * (3 * j + 1) // 2
            if k <= i:
                p[i] += (-1 if j % 2 == 0 else 1) * (p[i - j * k] + p[i - j * (k - 1)])
            else:
                p[i] += (-1 if j % 2 == 0 else 1) * p[i - j * k]
            j += 1

    return p[n]


# ryan = exp_sum(5)

def solve_runes_valid(n: str):
    if len(n) > 1 and (n.startswith('0') or n.startswith('-0')):
        return False
    return True


# def solve_runes(runes):
#     # Find the unknown digit
#     valid_nums = [i for i in '0123456789' if i not in runes]
#     print(valid_nums)
#     left, right = runes.split("=")
#     # 定位left中的运算符，即出现了数字之后的第一个运算符
#     for k, v in enumerate(left):
#         if v in "+-*" and not all(left[i] in "+-*" for i in range(k)):
#             break
#     x1, x2 = left[:k], left[k+1:]
#     print(x1, x2, right)
#
#     for x in valid_nums:
#         if solve_runes_valid(x1.replace("?", x)) and \
#                 solve_runes_valid(x2.replace("?", x)) and \
#                 solve_runes_valid(right.replace("?", x)):
#             if eval(left.replace("?", x)) == int(right.replace("?", x)):
#                 return int(x)
#     return -1


# r = solve_runes('-?56373--9216=-?47157')


def x_plus_y(s):
    # X plus Y Card problem
    rtn_step = 0
    while s.count('1') != 0:
        loc = s.find('1')
        if loc != len(s) - 1:
            s = s[:loc] + "0" + f'{"0" if s[loc + 1] == "1" else "1"}' + s[loc + 2:]
            rtn_step += 1
        else:
            rtn_step += 1
            break
        print(s)
    return rtn_step


# r = x_plus_y("011101010101")

# def fives(n):
#     for i in range(100):
#         if 5 ** i > n:
#             break
#     return i


def zeros(n):
    # Number of trailing zeros of N!
    num5 = 0
    # for i in range(1, fives(n)):
    for i in range(1, int(math.log(n, 5))+1):
        num5 += n // (5 ** i)
    return num5

# r = zeros(30)
# print(r)


def josephus_survivor(n, k):
    # Josephus Survivor
    value = 0
    for i in range(1, n+1):
        value = (value + k) % i
    return value+1


# r = josephus_survivor(11, 19)


def last_digit(n1, n2):
    # Last digit of a large number
    dict = {0: [0,0,0,0],
            1: [1,1,1,1],
            2: [2,4,8,6],
            3: [3,9,7,1],
            4: [4,6,4,6],
            5: [5,5,5,5],
            6: [6,6,6,6],
            7: [7,9,3,1],
            8: [8,4,2,6],
            9: [9,1,9,1]}
    if n2 == 0:
        return 1
    return dict[n1%10][n2%4-1]

# r = last_digit(4, 2)

# def dbl_linear_slow(n):
#     # Twice linear ， 性能还不够
#     arr = set()
#     arr.add(1)
#     for i in range(n+1):
#         s = min(arr)
#         arr.remove(s)
#         x = s*2 + 1
#         y = s*3 + 1
#         arr.add(x)
#         arr.add(y)
#     # print(arr)
#     return s


def dbl_linear(n):
    # Twice linear
    u, q2, q3 = 1, deque([]), deque([])
    for _ in range(n):
        q2.append(2 * u + 1)
        q3.append(3 * u + 1)
        u = min(q2[0], q3[0])
        if u == q2[0]: q2.popleft()
        if u == q3[0]: q3.popleft()
    return u


# r = dbl_linear(20000)


def perimeter(n):
    # Perimeter of squares in a rectangle
    a, b = 1, 1
    total = 8  # account for first two squares with sides of length 1
    for i in range(2, n+1):
        a, b = b, a + b
        total += 4 * b
    return total


r = perimeter(7)

print(r)
