# coding=utf-8

def archers_ready(archers):
    return True if archers and all(a > 4 for a in archers) else False


def intNum(a):
    for i in range(5):
        yield i + a


# num = intNum(3)
# print(next(num))
# print(next(num))
# print(next(num))


def generator(a):
    # 生成器的用法
    for b in range(1, 100):
        yield f'{a} x {b} = {a * b}'


# r = generator(3)
# ryan = next(r)
# print(next(r))
# print(next(r))
# print(next(r))
