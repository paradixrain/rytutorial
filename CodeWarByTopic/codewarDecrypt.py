# coding=utf-8

def pass_the_door_man1(word):
    for i in range(len(word) - 1):
        if word[i] == word[i + 1]:
            return (ord(word[i]) - 96) * 3


def pass_the_door_man(word):
    import re
    return (ord(re.findall(r'(.)\1', word)[0]) - 96) * 3


ryan = pass_the_door_man("lettuce")
print(ryan)
