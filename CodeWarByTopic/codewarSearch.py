# coding=utf-8
# import copy


# def light_switch(n, lights):
#     result = {0}
#     for s in lights:
#         result |= {x ^ sum(1 << x for x in s) for x in result}
#     return (1 << n) - 1 in result


switches = [
    [0, 1, 2],  # switch 0 controls light 0, 1, 2
    [1, 2],  # switch 1 controls light 1, 2
    [1, 2, 3, 4],  # switch 2 controls light 1, 2, 3, 4
    [1, 4]  # switch 3 controls light 1, 4
]



def light_switch(n, corresponding_lights_list):
    # Light Switch
    original = 0
    expected = int('1' * n, 2)
    switches = [int(''.join(['1' if i in x else '0' for i in range(n)]), 2) for x in corresponding_lights_list]

    def toggle_switch(state, switch):
        return state ^ switch

    # bfs
    from collections import deque
    dq = deque()
    seen = set()
    dq.append(original)
    seen.add(original)
    while len(dq) > 0:
        state = dq.popleft()
        for x in switches:
            s = toggle_switch(state, x)
            if s == expected:
                return True
            if s not in seen:
                dq.append(s)
                seen.add(s)
    return False


# def light_switch(n, lights):
#     # 超牛B的方案
#     result = {0}
#     for s in lights:
#         result |= {x ^ sum(1 << x for x in s) for x in result}
#     return (1 << n) - 1 in result

# def light_switch(n, corresponding_lights_list):
#     # Light Switch，  out of time
#     init_lights_list = [0] * n
#     expected_lights_list = [1] * n
#
#     to_check_list = []
#     checked_list = []
#
#     to_check_list.append(init_lights_list)
#
#     while len(to_check_list) > 0:
#         check_now = to_check_list.pop(0)
#         # print("Check", check_now)
#         if all(check_now[i] == expected_lights_list[i] for i in range(n)):
#             return True
#         if check_now in checked_list:
#             continue
#         for x in corresponding_lights_list:
#             temp = []
#             # print('x', x)
#             for i in range(n):
#                 if i in x:
#                     temp.append(int(not check_now[i]))
#                 else:
#                     temp.append(check_now[i])
#             # print('new added', temp)
#             to_check_list.append(temp)
#         checked_list.append(check_now)
#
#     return False



r = light_switch(5, switches)
print(r)
