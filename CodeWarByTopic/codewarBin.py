# coding=utf-8
# Author: Ryan
# Topic: CodeWar
# Subtopic: Binary
# Description:


def count_bits(n):
    return bin(n).count('1')


def add_binary(a, b):
    # return the sum of two numbers.  and transfer them to binary.
    return bin(a + b)[2:]


def show_bits1(n):
    return list(int(i) for i in (bin(n)[2:].rjust(32, '0') if n >= 0 else bin(int('1'*32, 2) + 1 + n)[2:].rjust(32, '1')))


def showBits(n):
    return [int((1 << i) & n > 0) for i in range(32)][::-1]


def bits_battle(numbers):
    odds = sum(bin(n)[2:].count('1') for n in numbers if n % 2 == 1)
    evens = sum(bin(n)[2:].count('0') for n in numbers if n % 2 == 0 and n > 0)
    return 'odds win' if odds > evens else 'evens win' if evens > odds else 'tie'


def squares_needed(grains):
    return len(bin(grains)) - 2 if grains > 0 else 0


def shared_bits(a, b):
    return True if bin(a & b).count('1') >= 2 else False


def reverse_bits(n):
    return int('0b' + bin(n)[2:][::-1], 2)


def or_arrays1(arr1, arr2, default=0):
    if len(arr1) > len(arr2):
        arr2 = arr2 + [default] * (len(arr1) - len(arr2))
    else:
        arr1 = arr1 + [default] * (len(arr2) - len(arr1))
    return [int(bin(arr1[i] | arr2[i]), 2) for i in range(len(arr1))]


def or_arrays(a1, a2, d=0):
    from itertools import zip_longest
    return [x|y for x,y in zip_longest(a1, a2, fillvalue=d)]


def any_odd1(x):
    """
    Return true when any odd bit of x equals 1; false otherwise.
    """
    for i in range(len(bin(x)) - 2, 1, -2):
        if bin(x)[i] == '1':
            return True
    return False


def any_odd(n):
    return 1 if '1' in bin(n)[2:][-2::-2] else 0


def eliminate_unset_bits(number):
    return int("0b0" + number.replace("0", ""), 2)


def convert_bits(a, b):
    """
    Complete the function to determine the number of bits required to convert integer to integer
    """
    return (bin(a ^ b)).count("1")


def arbitrate(inp, n):
    if inp.find("1") == -1:
        return inp
    return "1".zfill(max(inp.find("1") + 1, 0)).ljust(n, "0")


def sxore(n):
    if n == 0 or n % 4 == 3:
        return 0
    elif n % 4 == 1:
        return 1
    elif n % 4 == 2:
        return n + 1
    else:
        return n


def sxore2(n):
    from itertools import accumulate
    return accumulate(range(n+1), lambda x, y: x ^ y)


def signed_eight_bit_number(number):
    try:
        int_number = int(number)
        if number == str(int_number) and -128 <= int_number <= 127:
            return True
        return False
    except ValueError:
        return False

#
# ryan = signed_eight_bit_number("0")
# print(ryan)


def bits_battle(numbers):
    odds = sum(bin(n)[2:].count('1') for n in numbers if n % 2 == 1)
    evens = sum(bin(n)[2:].count('0') for n in numbers if n % 2 == 0 and n > 0)
    return 'odds win' if odds > evens else 'evens win' if evens > odds else 'tie'


def squares_needed(grains):
    return len(bin(grains)) - 2 if grains > 0 else 0


def shared_bits(a, b):
    return True if bin(a & b).count('1') >= 2 else False


def reverse_bits(n):
    return int('0b' + bin(n)[2:][::-1], 2)


def or_arrays1(arr1, arr2, default=0):
    if len(arr1) > len(arr2):
        arr2 = arr2 + [default] * (len(arr1) - len(arr2))
    else:
        arr1 = arr1 + [default] * (len(arr2) - len(arr1))
    return [int(bin(arr1[i] | arr2[i]), 2) for i in range(len(arr1))]


def or_arrays(a1, a2, d=0):
    from itertools import zip_longest
    return [x | y for x, y in zip_longest(a1, a2, fillvalue=d)]


def any_odd1(x):
    """
    Return true when any odd bit of x equals 1; false otherwise.
    """
    for i in range(len(bin(x)) - 2, 1, -2):
        if bin(x)[i] == '1':
            return True
    return False


def any_odd(n):
    return 1 if '1' in bin(n)[2:][-2::-2] else 0


def eliminate_unset_bits(number):
    return int("0b0" + number.replace("0", ""), 2)


def convert_bits(a, b):
    """
    Complete the function to determine the number of bits required to convert integer to integer
    """
    return (bin(a ^ b)).count("1")


def arbitrate(inp, n):
    if inp.find("1") == -1:
        return inp
    return "1".zfill(max(inp.find("1") + 1, 0)).ljust(n, "0")


def interlockable(a, b):
    return False if a & b else True


ryan = interlockable(3, 6)
print('ryan', ryan)
