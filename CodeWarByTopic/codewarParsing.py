# coding=utf-8

def calc(expr):
    # Reverse polish notation calculator
    expr = expr.split()
    calc_arr = []
    for i in expr:
        if i in "*-/+":
            y = calc_arr.pop()
            x = calc_arr.pop()
            calc_arr.append(eval(f"{x}{i}{y}"))
        else:
            calc_arr.append(i)
    if len(calc_arr) == 1:
        t = calc_arr.pop()
        return eval(f"{t}*1")
    return 0


r = calc("3.5")
# , 2, "Should support division")

print(r)
