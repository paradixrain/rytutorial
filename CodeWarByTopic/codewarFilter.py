# coding=utf-8

def filter_list(lst):
    return list(filter(lambda x: type(x) == int and x >= 0, lst))


def unique_sum(lst):
    print(lst)
    return None if lst is None else sum(set(lst))


candidates = [{
    'desires_equity': True,
    'current_location': 'New York',
    'desired_locations': ['San Francisco', 'Los Angeles']
}, {
    'desires_equity': False,
    'current_location': 'San Francisco',
    'desired_locations': ['Kentucky', 'New Mexico']
}]
job1 = {'equity_max': 0, 'locations': ['Los Angeles', 'New York']}
job2 = {'equity_max': 1.2, 'locations': ['New York', 'Kentucky']}


def match(job, person):
    return list(filter(lambda c: (c['desires_equity'] == (job['equity_max'] > 0) or (not c['desires_equity'])) and
                                 ((c['current_location'] in job['locations']) or
                                  (any(d in job['locations'] for d in c['desired_locations']))), person))


ryan = match(job2, candidates)
print(ryan)
