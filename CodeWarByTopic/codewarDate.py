# coding = utf-8

# @title: Date
# @desc:  codewar
# @author: RyanLin
# @date: 2022/02/09

# from math import sqrt
from datetime import date, timedelta, datetime


def date_nb_days(a0, a, p):
    n = 0
    while a >= a0:
        a0 = a0 * (1 + p / 36000)
        n += 1
    d = date(2016, 1, 1) + timedelta(days=n)
    return date.isoformat(d)


''' 2016-1-1
date_nb_days(100, 101, 0.98) --> "2017-01-01" (366 days)
date_nb_days(100, 150, 2.00) --> "2035-12-26" (7299 days)
'''


# a = date_nb_days(4281, 5087, 2)
# print(a)
# a = date_nb_days(100, 101, 0.98)
# print(a)


def check_coupon(entered_code, correct_code, current_date, expiration_date):
    # The Coupon Code
    dt1 = datetime.strptime(current_date, "%B %d, %Y")
    dt2 = datetime.strptime(expiration_date, "%B %d, %Y")
    return entered_code is correct_code and dt1 <= dt2


# ryan = check_coupon('123', '123', 'September 5, 2014', 'October 1, 2014')


def unlucky_days(year):
    # Unlucky Days
    count = 0
    for i in range(1, 13):
        d1 = date(year, i, 13)
        if d1.weekday() == 4:
            count += 1
    return count


# ryan = unlucky_days(1634)

def make_readable(seconds):
    # Human Readable Time
    h = seconds // 3600
    m = (seconds % 3600) // 60
    s = seconds % 60
    return f"{h:02}:{m:02}:{s:02}"


# r = make_readable(9)


def format_duration(seconds):
    # Human readable duration format
    if seconds == 0:
        return "now"
    y = seconds // (365 * 24 * 3600)
    d = (seconds % (365 * 24 * 3600)) // (24 * 3600)
    h = (seconds % (24 * 3600)) // 3600
    m = (seconds % 3600) // 60
    s = seconds % 60
    rtn_arr = []
    for v, k in zip((y, d, h, m, s), ("year", "day", "hour", "minute", "second")):
        if v > 0:
            cur = f"{v} {k}{'s' if v>1 else ''}"
            rtn_arr.append(cur)
    rtn_str = ', '.join(rtn_arr[:-1]) + f"{' and ' if len(rtn_arr)>1 else ''}{rtn_arr[-1]}"
    return rtn_str


r = format_duration(4)


print(r)
