# coding = utf-8

# @filename: C:/Users/Ryan/git/rytutorial/CodeWarByTopic/codewarNumbers.py
# @desc: codewar 中tags为numbers的题目
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022年3月31日



# Numerical Palindrome #1
def palindrome(num):
    if type(num) == int and num > 0:
        num = str(num)
        if num == num[::-1]:
            return True
        else:
            return False
    return 'Not valid'

a = palindrome(123322)
print(a)
a = palindrome(-450)
print(a)