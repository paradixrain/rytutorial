# coding=utf-8

def search_k_from_end(linked_list, k):
    ll_rev = []
    node = linked_list.head
    while node:
        ll_rev.append(node.data)
        node = node.next
    if len(ll_rev) < k:
        return None
    return ll_rev[len(ll_rev)-k]


def freq_stack1(pops, balloons):
    # Popping Balloons
    # 效率很低的方法
    rtn_arr = []
    for _ in range(pops):
        count_arr = [balloons.count(i) for i in balloons]
        maxi = max(count_arr)
        for i in range(len(count_arr)-1, -1, -1):
            if count_arr[i] == maxi:
                rtn_arr.append(balloons.pop(i))
                print('rtn_arr', maxi, rtn_arr)
                break
    return rtn_arr


def freq_stack(pops, balloons):
    # Popping Balloons
    # 改进了一下性能，只遍历2次
    count_dict = [(balloons[i], 1+balloons[:i].count(balloons[i])) for i in range(len(balloons))]
    count_order_arr = sorted(count_dict, key=lambda x: x[1])
    print(count_dict, count_order_arr)
    return [i[0] for i in count_order_arr[::-1]][:pops]


ryan = freq_stack(4, [5, 7, 5, 7, 4, 5])
print(ryan)
