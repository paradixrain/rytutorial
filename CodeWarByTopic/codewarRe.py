# coding=utf-8
import re

list1 = [
    {'firstName': 'Noah', 'lastName': 'M.', 'country': 'Switzerland', 'continent': 'Europe', 'age': 19,
     'language': 'JavaScript'},
    {'firstName': 'Maia', 'lastName': 'S.', 'country': 'Tahiti', 'continent': 'Oceania', 'age': 28,
     'language': 'JavaScript'},
    {'firstName': 'Shufen', 'lastName': 'L.', 'country': 'Taiwan', 'continent': 'Asia', 'age': 35, 'language': 'HTML'},
    {'firstName': 'Sumayah', 'lastName': 'M.', 'country': 'Tajikistan', 'continent': 'Asia', 'age': 30,
     'language': 'CSS'}
]

list2 = [
    {'firstName': 'Oliver', 'lastName': 'Q.', 'country': 'Australia', 'continent': 'Oceania', 'age': 19,
     'language': 'HTML'},
    {'firstName': 'Lukas', 'lastName': 'R.', 'country': 'Austria', 'continent': 'Europe', 'age': 89, 'language': 'HTML'}
]


def count_developers(lst):
    pattern = '''\'continent\': \'Europe\', \'age\': [0-9]+, \'language\': \'JavaScript\''''
    count = 0
    for person in lst:
        count += len(re.findall(pattern, str(person)))
    return count


def count_developers2(lst):
    return sum(x["language"] == "JavaScript" and x["continent"] == "Europe" for x in lst)


def greet_developers(lst):
    for person in lst:
        person['greeting'] = 'Hi {}, what do you like the most about {}?'.format(person['firstName'],
                                                                                 person['language'])
    return lst


def is_ruby_coming(lst):
    for person in lst:
        if person['language'] == 'Ruby':
            return True
    return False


def get_first_python(users):
    for person in users:
        if person['language'] == 'Ruby':
            return f"{person['first_name']}, {person['country']}"
    return "There will be no Python developers"


def remove_consecutive_duplicates(s):
    s = s.split()
    if len(s) == 0:
        return ''
    l = [s[0]]
    for i in s:
        if i != l[-1]:
            l.append(i)
    return ' '.join(l)


def make_password(phrase):
    p = ''.join(i[0] for i in phrase.split())

    def rp(x):
        tmp = {'i': '1', 'o': '0', 's': '5', 'I': '1', 'O': '0', 'S': '5'}
        return tmp[x.group(0)]

    return re.sub('[ios]', rp, p, flags=re.IGNORECASE)


def valid_number(n):
    pattern = re.compile('[+-]?[0-9]*\.[0-9]{2}')
    if pattern.fullmatch(n):
        return True
    return False


def vowel_2_index(s):
    rtns = ''
    for i in range(len(s)):
        if s[i] in 'aeiouAEIOU':
            rtns += str(i + 1)
        else:
            rtns += s[i]
    return rtns


def domain_name(url):
    # Extract the domain name from a URL
    url = re.sub(r'^.*//', '', url)
    print('url', url)
    url = re.sub(r'www.', '', url)
    pattern = r'[a-z0-9-]+'
    match = re.findall(pattern, url)
    print(match)
    if match:
        return match[0]


def domain_name2(url):
    return url.split("//")[-1].split("www.")[-1].split(".")[0]


def domain_name3(url):
    return re.search(r'(https?://)?(www\d?\.)?(?P<name>[\w-]+)\.', url).group('name')


def count_smileys(arr):
    # Count the smiley faces!
    smiles = 0
    pattern = re.compile(r'[:;][-~]?[)D]')
    for i in arr:
        if pattern.fullmatch(i):
            smiles += 1
    return smiles


# Regex Password Validation
regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$"


def dashatize(n):
    # Dashatize it
    return re.sub(r'(?P<a>[13579])', r'-\g<a>-', str(n)).replace('--', '-').strip('-')


class FileNameExtractor:
    @staticmethod
    def extract_file_name(dirty_file_name):
        r = re.sub(r'(?P<time>\d+)_(?P<file_name>.*)\..*$', r'\g<file_name>', dirty_file_name)
        return r


# b = FileNameExtractor.extract_file_name("1231231223123131_FILE_NAME.EXTENSION.OTHEREXTENSION")
# a = domain_name3("www.xakep.ru")

# Binary multiple of 3
# PATTERN = re.compile(r'^(0|1(01*0)*1)*$')


# def is_valid_IP(ip):
#     # Regular expression pattern to match a valid IPv4 address
#     ip_arr = ip.split('.')
#     if len(ip_arr) == 4:
#         for i in ip_arr:
#             print(i)
#             if not re.fullmatch("25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9]", i):
#                 return False
#     else:
#         return False
#     return True


def is_valid_IP(s):
    return s.count('.') == 3 and all(o.isdigit() and 0 <= int(o) <= 255 and str(int(o)) == o for o in s.split('.'))


r = is_valid_IP('256.9.246.129')

print(r)
