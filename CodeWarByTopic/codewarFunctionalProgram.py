# coding=utf-8
# from functools import partial


# Calculating with Functions
def zero(f=None): return 0 if not f else f(0)


def one(f=None): return 1 if not f else f(1)


def two(f=None): return 2 if not f else f(2)


def three(f=None): return 3 if not f else f(3)


def four(f=None): return 4 if not f else f(4)


def five(f=None): return 5 if not f else f(5)


def six(f=None): return 6 if not f else f(6)


def seven(f=None): return 7 if not f else f(7)


def eight(f=None): return 8 if not f else f(8)


def nine(f=None): return 9 if not f else f(9)


def plus(y): return lambda x: x + y


def minus(y): return lambda x: x - y


def times(y): return lambda x: x * y


def divided_by(y): return lambda x: x / y


# ryan = nine(plus(nine()))


# def add(n):
#     # Functional Addition
#     return lambda x: x + n


# add_one = add(1)
# ryan = add_one(3)
# print("ryan", ryan)


def f1(x): return x * 2


def f2(x): return x + 2


def f3(x): return x ** 2


def f4(x): return x.split()


def f5(xs): return [x[::-1].title() for x in xs]


def f6(xs): return "_".join(xs)


def chained(functions):
    # Unary function chainer
    def f(x):
        for function in functions:
            x = function(x)
        return x

    return f


# ryan = chained([f1, f2, f3])(0)
# print('ryan', ryan)


def factory(x):
    # First-Class Function Factory
    def func(arr):
        return [j * x for j in arr]

    return func


# my_arr = [1, 2, 3]
# threes = factory(4)
# ryan = threes(my_arr)
# print(ryan)


def pair_zeros(arr):
    # Pair Zeros
    flag = False
    rtn_arr = []
    for i in arr:
        if i != 0:
            rtn_arr.append(i)
        elif flag:
            flag = False
        else:
            rtn_arr.append(0)
            flag = True
    return rtn_arr


# ryan = pair_zeros([1,0,1,0,2,0,0])

objs = [{'a': 1, 'b': 2, 'c': 3}, {'a': 4, 'b': 5, 'c': 6}, {'a': 7, 'b': 8, 'c': 9}, {'a': 10, 'b': 11}]


def pluck(objs, name):
    # Pluck
    return [i.get(name, None) for i in objs]


def filter_homogenous(arrays):
    # Homogenous arrays
    rtn_arr = []
    for arr in arrays:
        if len(arr) == 0:
            continue
        else:
            typea = set(type(a) for a in arr)
            if len(typea) == 1:
                rtn_arr.append(arr)
    return rtn_arr


class HTMLGen2:
    # 动态添加类方法
    def __init__(self):
        self.methods = ['a']

    @staticmethod
    def a(s):
        return f'<a>{s}</a>'

    @staticmethod
    def b(s):
        return f'<b>{s}</b>'

    @staticmethod
    def p(s):
        return f'<p>{s}</p>'

    @staticmethod
    def div(s):
        return f'<div>{s}</div>'

    @staticmethod
    def span(s):
        return f'<span>{s}</span>'

    @staticmethod
    def title(s):
        return f'<title>{s}</title>'

    @staticmethod
    def body(s):
        return f'<body>{s}</body>'

    @staticmethod
    def comment(s):
        return f'<!--{s}-->'


class HTMLGen:
    def comment(self, s):
        return '<!--{}-->'.format(s)

    def __getattr__(self, name):
        return lambda s: '<{0}>{1}</{0}>'.format(name, s)


htmlGen = HTMLGen()
a = htmlGen.a('test')
b = htmlGen.comment('test')


def filter_long_words(sentence, n):
    # Filter Long Words
    return [i for i in sentence.split() if len(i) > n]


def boredom(staff):
    # The Office II - Boredom Score
    staff = list(staff.values())
    score = staff.count("accounts") + staff.count("finace") * 2 + staff.count("canteen") * 10 + \
            staff.count("regulation") * 3 + staff.count("trading") * 6 + staff.count("change") * 6 + \
            staff.count("IS") * 8 + staff.count("retail") * 5 + staff.count("cleaning") * 4 + \
            staff.count("pissing about") * 25
    if score <= 80:
        return "kill me now"
    elif score >= 100:
        return "party time!!"
    else:
        return "i can handle this"


# 重写map函数
# Map function issue
def func(n):
    return int(n) % 2 == 0


def map_2(arr, somefunction):
    try:
        return [somefunction(a) for a in arr]
    except ValueError:
        return 'array should contain only numbers'
    except TypeError:
        return 'given argument is not a function'


from typing import Callable


class Node:
    def __init__(self, data, next=None):
        self.data = data
        self.next = next


# Fun with lists: anyMatch + allMatch
def any_match(head: Node, pred: Callable[[any], bool]) -> bool:
    while head:
        if pred(head.data):
            return True
        head = head.next
    return False


def all_match(head: Node, pred: Callable[[all], bool]) -> bool:
    while head:
        if not pred(head.data):
            return False
        head = head.next
    return True


# ryan = map([1, 2, 3, '8'], func)

def gcdi(x, y):
    from math import gcd
    return gcd(x, y)


def lcmu(a, b):
    from math import lcm
    return lcm(a, b)


def som(a, b):
    return a + b


def maxi(a, b):
    return max(a, b)


def mini(a, b):
    return min(a, b)


def oper_array(fct, arr, init):
    # Reducing by steps
    rtn_arr = []
    for i in arr:
        init = fct(i, init)
        rtn_arr.append(init)
    return rtn_arr


# a = [ 18, 69, -90, -78, 65, 40 ]
# r = [ 18, 3, 3, 3, 1, 1 ]
# ryan = oper_array(gcdi, a, a[0])

def is_even(x):
    return not x % 2


def take_while(arr, pred_fun):
    # The takeWhile Function
    rtn_arr = []
    for i in arr:
        if not pred_fun(i):
            break
        rtn_arr.append(i)
    return rtn_arr


class Node:
    def __init__(self, data, next=None):
        self.data = data
        self.next = next


def count_if(head, func):
    # Fun with lists: countIf
    count = 0
    while head:
        if func(head.data):
            count += 1
        head = head.next
    return count


add1 = lambda a: a + 1
this = lambda a: a


def compose(f, g):
    # Function Composition
    return lambda *args: f(g(*args))


list1 = [
    {'firstName': 'Daniel', 'lastName': 'J.', 'country': 'Aruba', 'continent': 'Americas', 'age': 42,
     'language': 'Python'},
    {'firstName': 'Kseniya', 'lastName': 'T.', 'country': 'Belarus', 'continent': 'Europe', 'age': 22,
     'language': 'Ruby'},
    {'firstName': 'Sou', 'lastName': 'B.', 'country': 'Japan', 'continent': 'Asia', 'age': 43, 'language': 'Ruby'},
    {'firstName': 'Hanna', 'lastName': 'L.', 'country': 'Hungary', 'continent': 'Europe', 'age': 95,
     'language': 'JavaScript'},
    {'firstName': 'Jayden', 'lastName': 'P.', 'country': 'Jamaica', 'continent': 'Americas', 'age': 18,
     'language': 'JavaScript'},
    {'firstName': 'Joao', 'lastName': 'D.', 'country': 'Portugal', 'continent': 'Europe', 'age': 25,
     'language': 'JavaScript'}
]


def is_language_diverse(lst):
    # Coding Meetup #13 - Higher-Order Functions Series - Is the meetup language-diverse?
    from collections import Counter
    arr = Counter([i['language'] for i in lst])
    return True if len(arr) <= 1 or arr.most_common()[0][1] <= 2 * arr.most_common()[-1][1] else False


list2 = [
    {'firstName': 'Aba', 'lastName': 'N.', 'country': 'Ghana', 'continent': 'Africa', 'age': 21, 'language': 'Python'},
    {'firstName': 'Abb', 'lastName': 'O.', 'country': 'Israel', 'continent': 'Asia', 'age': 39, 'language': 'Java'}
]


def find_odd_names(lst):
    # Coding Meetup #15 - Higher-Order Functions Series - Find the odd names
    rtn_arr = []
    for i in lst:
        if sum(ord(l) for l in i['firstName']) % 2 == 1:
            rtn_arr.append(i)
    return rtn_arr


def find_odd_names2(lst):
    return [x for x in lst if sum(map(ord, x["firstName"])) % 2]


def zip_with(fn, a1, a2):
    # zipWith
    return [fn(i1, i2) for i1, i2 in zip(a1, a2)]


def zip_with_2(fn, a1, a2):
    return list(map(fn, a1, a2))


class Celsius:
    def __init__(self, t):
        self.t = t

    def CC(self):
        return self.t

    def FC(self):
        return self.t * 9 / 5 + 32

    def CF(self):
        return (self.t - 32) * 5 / 9

    def KC(self):
        return self.t + 273.15

    def CK(self):
        return self.t - 273.15

    def RC(self):
        return (self.t + 273.15) * 9 / 5

    def CR(self):
        return (self.t - 491.67) * 5 / 9

    def DeC(self):
        return (100 - self.t * 2 / 3)

    def CDe(self):
        return (100 - self.t) * 3 / 2

    def NC(self):
        return self.t * 100 / 33

    def CN(self):
        return self.t * 33 / 100

    def Rec(self):
        return self.t * 5 / 4

    def CRe(self):
        return self.t * 4 / 5

    def RoC(self):
        return (self.t - 7.5) * 40 / 21

    def CRo(self):
        return self.t * 21 / 40 + 7.5


def convert_temp(temp, from_scale, to_scale):
    # Temperature converter
    from operator import methodcaller
    s1 = f'C{from_scale}'
    s2 = f'{to_scale}C'
    t1 = methodcaller(s1)(Celsius(temp))
    t2 = methodcaller(s2)(Celsius(t1))
    print(s1, t1, s2, t2)
    return t2


int2 = lambda x: int(round(x, 0))


def other2c(temp, from_scale):
    func_dict = {
        'C': lambda x: x,
        'K': lambda x: int2(x - 273.15),
        'F': lambda x: int2((x - 32) * 5 / 9),
        'R': lambda x: int2((x - 491.67) * 5 / 9),
        'De': lambda x: int2(100 - x * 2 / 3),
        'N': lambda x: int2(x * 100 / 33),
        'Re': lambda x: int2(x * 5 / 4),
        'Ro': lambda x: int2((x - 7.5) * 40 / 21)
    }
    return func_dict[from_scale](temp)


def c2other(temp, to_scale):
    func_dict = {
        'C': lambda x: x,
        'K': lambda x: int2(x + 273.15),
        'F': lambda x: int2(x * 9 / 5 + 32),
        'R': lambda x: int2((x + 273.15) * 9 / 5),
        'De': lambda x: int2((100 - x) * 3 / 2),
        'N': lambda x: int2(x * 33 / 100),
        'Re': lambda x: int2(x * 4 / 5),
        'Ro': lambda x: int2(x * 21 / 40 + 7.5)
    }
    return func_dict[to_scale](temp)


def convert_temp_2(temp, from_scale, to_scale):
    if from_scale == to_scale:
        return temp
    nowc = other2c(temp, from_scale)
    c2res = c2other(nowc, to_scale)
    return c2res


def muln(n):
    from operator import mul
    return lambda x: mul(n, x)


def multiply(n, l):
    # Multiply list by integer (with restrictions)
    return list(map(muln(n), l))


list3 = [
    {'firstName': 'Gabriel', 'lastName': 'X.', 'country': 'Monaco', 'continent': 'Europe', 'age': 49,
     'language': 'PHP'},
    {'firstName': 'Odval', 'lastName': 'F.', 'country': 'Mongolia', 'continent': 'Asia', 'age': 38,
     'language': 'Python'},
    {'firstName': 'Emilija', 'lastName': 'S.', 'country': 'Lithuania', 'continent': 'Europe', 'age': 19,
     'language': 'Python'},
    {'firstName': 'Sou', 'lastName': 'B.', 'country': 'Japan', 'continent': 'Asia', 'age': 49, 'language': 'PHP'},
]


def find_senior(lst):
    # Coding Meetup #7 - Higher-Order Functions Series - Find the most senior developer
    return [j for j in lst if j['age'] == max(i['age'] for i in lst)]


list4 = [
    {'firstName': 'Fatima', 'lastName': 'A.', 'country': 'Algeria', 'continent': 'Africa', 'age': 25,
     'language': 'JavaScript'},
    {'firstName': 'Agustín', 'lastName': 'M.', 'country': 'Chile', 'continent': 'Americas', 'age': 37, 'language': 'C'},
    {'firstName': 'Jing', 'lastName': 'X.', 'country': 'China', 'continent': 'Asia', 'age': 39, 'language': 'Ruby'},
    {'firstName': 'Laia', 'lastName': 'P.', 'country': 'Andorra', 'continent': 'Europe', 'age': 55, 'language': 'Ruby'},
    {'firstName': 'Oliver', 'lastName': 'Q.', 'country': 'Australia', 'continent': 'Oceania', 'age': 65,
     'language': 'PHP'}
]


def age_mod(input_age):
    return input_age // 10 if input_age < 100 else 10


def is_age_diverse(lst):
    # Coding Meetup #9 - Higher-Order Functions Series - Is the meetup age-diverse?
    age_set = set(age_mod(i['age']) for i in lst)
    return True if age_set >= set(range(1, 11)) else False


def all_continents(lst):
    # Coding Meetup #8 - Higher-Order Functions Series - Will all continents be represented?
    continents_set = set(i['continent'] for i in lst)
    all_continents = {'Africa', 'Americas', 'Asia', 'Europe', 'Oceania'}
    return continents_set >= all_continents


# def create_message(s, lst=[]):
#     # Stringing me along
#     print(s, lst)
#     if type(s) == list:
#         return ' '.join(s)
#     else:
#         return lambda *args: create_message(*args, lst + [s])


def create_message_2(s):
    return lambda r="": create_message_2(f"{s} {r}") if r else s


class predicate:
    # Combining predicates
    def __init__(self, func): self.f = func

    def __call__(self, *a, **kw): return self.f(*a, **kw)

    def __invert__(self): return self.__class__(lambda *a, **kw: not self.f(*a, **kw))

    def __or__(self, o): return self.__class__(lambda *a, **kw: self.f(*a, **kw) or o.f(*a, **kw))

    def __and__(self, o): return self.__class__(lambda *a, **kw: self.f(*a, **kw) and o.f(*a, **kw))


@predicate
def is_even(num):
    return num % 2 == 0


@predicate
def is_positive(num):
    return num > 0


# ryan = (is_even & is_positive)(4)


class add(int):
    # A Chain adding function
    def __call__(self, n):
        return add(self + n)


# ryan = add(1)(2)(3)(4)
# addTwo = add(2)
# ryan = addTwo(3)
# print(ryan)

# def add(*args):
#     return sum(args)

# def add(x, y, z):
#     return x + y + z


class CurryPartial:
    # Currying vs. Partial Application
    # 类的方法
    def __init__(self, func, *args):
        self.func = func
        self.args = args

    def __call__(self, *args):
        return CurryPartial(self.func, *(self.args + args))

    def __eq__(self, other):
        try:
            return self.func(*self.args) == other
        except TypeError:
            return CurryPartial(self.func, *self.args[:-1]) == other


# def curry_partial(f, *initial_args):
#     # 函数式编程
#     if not callable(f):
#         return f
#     if (n_args := f.__code__.co_argcount) <= len(initial_args):
#         return f(*initial_args[:n_args or None])
#     return lambda *args: curry_partial(f, *(initial_args + args))


def curry_partial(f, *initial_args):
    "Curries and partially applies the initial arguments to the function"
    return CurryPartial(f, *initial_args)


curriedAdd = CurryPartial(add)

print(curriedAdd(1))  # returns 6
print(CurryPartial(CurryPartial(add, 1, 2), 3))  # returns 6

ryan = CurryPartial(add, 1, 2, 3)
print(ryan)
