# coding = utf-8

def sum_pairs(ints, s):
    # Sum of Pairs
    # 对于场景 l5 = [1]*100000， a = sum_pairs(l5, 13)，太低效了
    first, second = None, None
    for i in range(len(ints)):
        for j in range(i+1, len(ints)):
            # print("i, j: ", i, j)
            if ints[i] + ints[j] == s:
                if second is None or j < second:
                    first, second = i, j
                    print("first, second: ", first, second)
                    break
        else:
            continue
        # break
    if second is None:
        return None
    return [ints[first], ints[second]]


def sum_pairs2(lst, s):
    # Sum of Pairs, 时间复杂度O(1)，空间复杂度O(1)
    cache = set()
    for i in lst:
        if s - i in cache:
            return [s - i, i]
        cache.add(i)


# l5 = [1]*100000
# a = sum_pairs(l5, 13)
# print(a)


def sum_of_intervals(intervals):
    # Sum of Intervals
    ins = sorted(intervals)
    arr = [ins[0]]
    for x, y in ins[1:]:
        lastx, lasty = arr.pop()
        if lastx <= x <= lasty:
            arr.append((lastx, max(lasty, y)))
        else:
            arr.append((lastx, lasty))
            arr.append((x, y))
    return sum((y-x) for x, y in arr)



r = sum_of_intervals([(1, 4), (7, 10), (3, 5)])
# , 7)


print(r)

