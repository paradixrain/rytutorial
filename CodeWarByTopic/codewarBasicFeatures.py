# coding = utf-8
# Author: Ryan 
# Date: 2022/4/16
# Filename: codewarBasicFeatures


import datetime
from itertools import product

def pattern1(n):
    rtn = "1"
    for i in range(2, n + 1):
        rtn += "\n1" + "*" * (i - 1) + str(i)
    return rtn


def pattern(n):
    return "\n".join(["1" + "*" * (i - 1) + str(i) * (i > 1) for i in range(1, n + 1)])


def find_digit(num, nth):
    if nth < 1:
        return -1
    return abs(num) // 10 ** (nth - 1) % 10


def capitals(word):
    return [i for i, c in enumerate(word) if c.isupper()]


def validate_word(word):
    from collections import Counter
    counts = set(Counter(word.lower()).values())
    return len(counts) == 1


def in_asc_order(arr):
    return bool(all(arr[i] <= arr[i + 1] for i in range(len(arr) - 1)))


def binary_pyramid(m, n):
    sums = 0
    for i in range(m, n + 1):
        sums += int(bin(i)[2:], 10)
    return f"{sums:b}"


def hydrate(drink_string):
    drinks = sum(int(i) for i in drink_string.split() if i.isnumeric())
    return f"{drinks} glass{'es' * (drinks > 1)} of water"


def array_leaders1(numbers):
    numbers0 = numbers + [0]
    return [numbers[i] for i in filter(lambda x: numbers0[x] > sum(numbers0[x + 1:]), range(len(numbers)))]


def array_leaders(numbers):
    return [n for (i, n) in enumerate(numbers) if n > sum(numbers[(i + 1):])]


def divisible_by_three(st):
    while True:
        if st == "" or (len(st) == 1 and st[0] in '3690'):
            return True
        elif len(st) == 1:
            return False
        else:
            st = "".join(str(sum(int(i) for i in st)))


def dont_give_me_five(start, end):
    # return amount of numbers that dont contain 5
    return len([i for i in range(start, end + 1) if '5' not in str(i)])


def order_food(lst):
    from collections import Counter
    return Counter(i['meal'] for i in lst)


list1 = [
    {'firstName': 'Noah', 'lastName': 'M.', 'country': 'Switzerland', 'continent': 'Europe', 'age': 19, 'language': 'C',
     'meal': 'vegetarian'},
    {'firstName': 'Anna', 'lastName': 'R.', 'country': 'Liechtenstein', 'continent': 'Europe', 'age': 52,
     'language': 'JavaScript', 'meal': 'standard'},
    {'firstName': 'Ramona', 'lastName': 'R.', 'country': 'Paraguay', 'continent': 'Americas', 'age': 29,
     'language': 'Ruby', 'meal': 'vegan'},
    {'firstName': 'George', 'lastName': 'B.', 'country': 'England', 'continent': 'Europe', 'age': 81, 'language': 'C',
     'meal': 'vegetarian'},
]


def minimum_steps(numbers, value):
    numbers = sorted(numbers)
    for i in range(len(numbers)):
        if sum(numbers[:i + 1]) >= value:
            print(i, numbers[:i + 1])
            return i


def triple_x(s):
    if s.find('xxx') == -1 or s.find('x') != s.rfind('xxx'):
        return False
    return True


def find_admin(lst, lang):
    return list(filter(lambda x: x['language'] == lang and x['githubAdmin'] == 'yes', lst))


def count_languages(lst):
    from collections import Counter
    return Counter(i['language'] for i in lst)


def adjacent_element_product(array):
    return max([array[i] * array[i + 1] for i in range(len(array) - 1)])


def pattern1(n):
    rtn = ""
    for i in range(n):
        j = n - i
        rtn += ' '.join(str(i + 1) * j).center(2 * n - 1) + '\n'
    return rtn


def pattern(n):
    return '\n'.join([' ' + ' '.join(str((i + 1) % 10) * (n - i)).center(2 * n - 1).rstrip() for i in range(n)])


def disarium_number(number):
    return 'Disarium !!' if sum(int(v) ** (i + 1) for i, v in enumerate(str(number))) == number else 'Not !!'


def jumping_number(number):
    return 'Jumping!!' if all(abs(int(str(number)[i]) - int(str(number)[i + 1])) == 1
                              for i in range(len(str(number)) - 1)) else 'Not!!'


def last(s):
    return sorted([i for i in s.split()], key=lambda x: x[-1])


def number(lines):
    return [f'{i + 1}: {line}' for i, line in enumerate(lines)]


def balanced_num(number):
    s_num = str(number)
    if len(s_num) <= 2:
        return "Balanced"
    elif (len(s_num) % 2) == 1:
        mid_pos = (len(s_num)) // 2
    else:
        mid_pos = (len(s_num) - 2) // 2
    # print(mid_pos, s_num[-mid_pos:], sum(int(i) for i in s_num[:mid_pos]), sum(int(i) for i in s_num[mid_pos:]))
    if sum(int(i) for i in s_num[:mid_pos]) == sum(int(i) for i in s_num[-mid_pos:]):
        return "Balanced"
    return "Not Balanced"


def strong_num(number):
    from math import factorial
    if number == sum(factorial(int(str(number)[i])) for i in range(len(str(number)))):
        return "STRONG!!!!"
    return "Not Strong !!"


def show_sequence(n):
    if n < 0:
        return f"{n}<0"
    elif n == 0:
        return f"{n}=0"
    return f"{'+'.join(str(i) for i in range(n + 1))}" + f" = {int(n * (n + 1) / 2)}"


def sum_of_a_beach(beach):
    words = ['sand', 'water', 'fish', 'sun']
    return sum(beach.lower().count(i) for i in words)


def add(a):  # 这个很有意思
    """Write a function that adds from two invocations.
        add(3)(4) // 7
    add(12)(20) // 32
    """
    return lambda b: b + a


def multiply_all(y):
    """multiply_all([1, 2, 3])(1), [1, 2, 3]"""
    return lambda x: list(i * x for i in y)


def check_exam(arr1, arr2):
    return max(0, sum(4 if arr1[i] == arr2[i] else -1 for i in range(len(arr1)) if arr2[i] != ""))


def last_survivor(letters, coords):
    for i in coords:
        letters = letters[:i] + letters[i + 1:]
    return letters


def alphabet_war(fight):
    left = {'w': 4, 'p': 3, 'b': 2, 's': 1}
    right = {'m': 4, 'q': 3, 'd': 2, 'z': 1}
    return 'Left side wins!' if sum(left[i] for i in fight if i in left) > sum(
        right[i] for i in fight if i in right) else \
        'Right side wins!' if sum(left[i] for i in fight if i in left) < sum(right[i] for i in fight if i in right) else \
            'Let\'s fight again!'


def middle_me(N, X, Y):
    return X.center(N + 1, Y) if N < 0 or N % 2 == 1 else X


def kaprekar_split1(n):
    if n == 1:
        return 0
    n_square = str(n ** 2)
    for i in range(len(n_square) - 1):
        if int(n_square[:i + 1]) + int(n_square[i + 1:]) == n:
            return i + 1
    return -1


def kaprekar_split(n):
    square = str(n * n)
    for i in range(0, len(square)):
        if int(square[i:]) + int(square[:i] or 0) == n:
            return i
    return -1


def plant(seed, water, fert, temp):
    return f"{'-' * water + seed * fert}" * water if 20 <= temp <= 30 else f"{'-' * water}" * water + f"{seed}"


def expression_out(exp):
    """
    "+"   -->  "Plus"
    "-"   -->  "Minus"
    "*"   -->  "Times"
    "/"   -->  "Divided By"
    "**"  -->  "To The Power Of"
    "="   -->  "Equals"
    "!="  -->  "Does Not Equal"
    """
    operator_dict = {'+': 'Plus', '-': 'Minus', '*': 'Times', '/': 'Divided By', '**': 'To The Power Of',
                     '=': 'Equals', '!=': 'Does Not Equal'}
    number_dict = {'0': 'Zero', '1': 'One', '2': 'Two', '3': 'Three', '4': 'Four', '5': 'Five', '6': 'Six',
                   '7': 'Seven', '8': 'Eight', '9': 'Nine', '10': 'Ten'}
    n1, op, n2 = exp.split()
    if op not in operator_dict:
        return "That's not an operator!"
    return f"{number_dict[n1]} {operator_dict[op]} {number_dict[n2]}"


def doubleton(num):
    while True:
        if len(set(str(num + 1))) == 2:
            return num + 1
        num += 1


def shifter(st):
    return sum(1 for word in set(st.split()) if all(w in "HIMNOSWXZ" for w in word))


def one_punch(item):
    return 'Broken!' if item == '' or type(item) != str else ' '.join(sorted(item.split())).translate(
        str.maketrans('aeAE', 'aaaa')).replace('a', '')


def sing():
    rtn = []
    for i in range(0, 98):
        rtn.append(
            f'{99 - i} bottle{"s" if 99 - i > 1 else ""} of beer on the wall, {99 - i} bottle{"s" if 99 - i > 1 else ""} of beer.')
        rtn.append(
            f'Take one down and pass it around, {99 - i - 1} bottle{"s" if 99 - i - 1 > 1 else ""} of beer on the wall.')
    rtn.append("1 bottle of beer on the wall, 1 bottle of beer.")
    rtn.append("Take one down and pass it around, no more bottles of beer on the wall.")
    rtn.append('No more bottles of beer on the wall, no more bottles of beer.')
    rtn.append("Go to the store and buy some more, 99 bottles of beer on the wall.")
    return rtn


def time_correct(t):
    import re
    if t == "" or t is None:
        return t
    elif (match_time := re.match(r'[0-9][0-9]:[0-9][0-9]:[0-9][0-9]$', t)) is None:
        return None
    else:
        h, m, s = match_time.group().split(':')
        h, m, s = int(h), int(m), int(s)
        return f'{((h * 3600 + m * 60 + s) // 3600) % 24:02d}:{((m * 60 + s) // 60) % 60:02d}:{s % 60:02d}'


def average(arr):
    """
    Write a method, that gets an array of integer-numbers and return an array of the averages of each integer-number
    and his follower, if there is one.
    """
    return [sum(arr[i:i + 2]) / 2 for i in range(len(arr) - 1)] if arr is not None else []


def score_matrix(matrix):
    # calculate the total sum for the matrix
    rtn_sum = 0
    for i, row in enumerate(matrix):
        for j, col in enumerate(row):
            rtn_sum += (-1) ** (i + j + 2) * col
    return rtn_sum


def burner(C, H, O):
    # 根据c,h,o三个原子数量，决算水，二氧化碳，甲烷的数量
    # 这题的算法很神奇！要再研究一下，题目中有强制要求，先生成H2，所以才能有以下的算法
    H2O = min(H // 2, O)
    CO2 = min(C, (O - H2O) // 2)
    CH4 = min(C - CO2, (H - 2 * H2O) // 4)
    return H2O, CO2, CH4


def burner_perfect(C, H, O):
    # 如果题目要求，所有的原子要尽可能的组合生成这三种，那么应当使用这种方法
    # 排序，定位最大和最小元素
    from collections import OrderedDict
    element_dict = OrderedDict(sorted([('H', H), ('O', O), ('C', C)], key=lambda x: x[1], reverse=True))
    min_i = element_dict.popitem()
    mid_i = element_dict.popitem()
    max_i = element_dict.popitem()
    H20, O2, CH4 = 0, 0, 0
    if min_i[0] == 'H':
        for i in range(0, min_i[1] // 2 + 1):
            H20 = i
            CH4 = (min_i[1] - 2 * i) // 4
            CO2 = C - CH4
            if (i * 2 + CH4 * 4 == H) and (H20 + 2 * CO2 == O):
                return H20, CO2, CH4
    if min_i[0] == 'O':
        for i in range(0, min_i[1] + 1):
            H20 = i
            CH4 = (min_i[1] - 2 * i) // 4
            CO2 = C - CH4
            if (i * 2 + CH4 * 4 == H) and (H20 + 2 * CO2 == O):
                return H20, CO2, CH4
    if min_i[0] == 'C':
        for i in range(0, min_i[1] + 1):
            CO2 = i
            CH4 = min_i[1] - i
            H2O = (H - 4 * CH4) // 2
            if H20 + 2 * CO2 == O:
                return H20, CO2, CH4
    return "Not possible"


def apparently(s: str):
    s = s.split()
    for i in range(len(s)):
        if s[i] in ['and', 'but'] and ((i != len(s) - 1 and s[i + 1] != 'apparently') or (i == len(s) - 1)):
            s[i] += ' apparently'
    return ' '.join(s)


def apparently(s):
    import re
    return re.sub(r'(?<=\b(and|but)\b(?! apparently\b))', ' apparently', s)  # 这个正则我一直没写出来，主要是前置推导符不会


def get_villain_name(birthdate):
    first = ["The Evil", "The Vile", "The Cruel", "The Trashy", "The Despicable", "The Embarrassing",
             "The Disreputable", "The Atrocious", "The Twirling", "The Orange", "The Terrifying", "The Awkward"]
    last = ["Mustache", "Pickle", "Hood Ornament", "Raisin", "Recycling Bin", "Potato", "Tomato", "House Cat",
            "Teaspoon", "Laundry Basket"]
    m, d = birthdate.month, birthdate.day
    return first[m - 1] + ' ' + last[d % 10]


def multiples1(s1, s2, s3):
    from math import lcm
    return [lcm(s1, s2) * i for i in range(1, s3 // lcm(s1, s2) + 1)]


def multiples2(s1, s2, s3):
    from math import gcd, ceil
    return [s1 * s2 // gcd(s1, s2) * i for i in range(1, ceil(s3 * gcd(s1, s2) / (s1 * s2)))]


def multiples(s1, s2, s3):
    return [x for x in range(s1, s3) if x % s1 == 0 and x % s2 == 0]


def amaro_plan(pirate_num):
    rtn = []
    rtn += [0, 1] * ((pirate_num - 1) // 2)
    rtn += [0] * ((pirate_num - 1) % 2)
    rtn.insert(0, pirate_num * 20 - sum(rtn))
    return rtn


def get_strings(city):
    from collections import Counter
    city = Counter(city.lower())
    return ','.join(f'{k}:{"*" * v}' for k, v in city.items() if k != " ")


def max_tri_sum(numbers):
    numbers = sorted(list(set(numbers)), reverse=True)
    return sum(numbers[0:3])


def pendulum1(values):
    values = sorted(values)
    rtn_lst = []
    flag = False
    for i, v in enumerate(values):
        if flag:
            rtn_lst.append(v)
            flag = False
        else:
            flag = True
            rtn_lst.insert(0, v)
    return rtn_lst


def pendulum(a):
    a = sorted(a)
    return a[::2][::-1] + a[1::2]


def tidyNumber(n):
    return str(n) == ''.join(sorted(str(n)))


def joust1(list_field: tuple, v_knight_left: int, v_knight_right: int) -> tuple:
    from math import ceil
    loc_left = list_field[0].index('>')
    loc_right = list_field[1].index('<')
    length = len(list_field[0])
    if (v_knight_left == v_knight_right == 0) or (loc_left > loc_right - 1):
        return list_field
    gap = loc_right - loc_left
    time = ceil(gap / (v_knight_left + v_knight_right))
    left = f"{' ' * (loc_left + v_knight_left * time - 2) + '$->'}".ljust(length, ' ')
    right = f"{' ' * (loc_right - v_knight_right * time) + '<-P'}".ljust(length, ' ')
    return left, right


def joust(lf: tuple, vkl: int, vkr: int) -> tuple:
    pos1 = lf[0].find(">")
    pos2 = lf[1].find("<")

    if pos1 >= pos2 or vkl == 0 and vkr == 0:
        return lf

    while pos2 > pos1:
        pos1 += vkl
        pos2 -= vkr

    res1 = list(" " * len(lf[0]))
    res2 = list(" " * len(lf[0]))

    res1[pos1] = "$->"
    res2[pos2] = "<-P"

    return "".join(res1[2:]), "".join(res2[:-2])


def friday_the_thirteenths(start, end=None):
    from datetime import datetime
    rtn_list = []
    if end is None:
        end = start + 1
    else:
        end += 1
    for year in range(start, end):
        for month in range(1, 13):
            if datetime(year, month, 13).weekday() == 4:
                rtn_list.append(datetime(year, month, 13))
    return ' '.join(datetime.strftime(i, "%#m/%#d/%Y") for i in rtn_list)  # 月份不自动补零, python 3.10用的 #
    # return ' '.join(f"{int(str(i.strftime('%m')))}/{int(str(i.strftime('%d')))}/{int(str(i.strftime('%Y')))}" for i in rtn_list)


def friday_the_thirteenths1(start, end=None):
    from datetime import date
    return ' '.join(
        f'{d:%-m/%-d/%Y}' for year in range(start, (end or start) + 1)  # 月份不自动补零， python 3.8 用的是 -
        for month in range(1, 13) if (d := date(year, month, 13)).weekday() == 4
    )


def men_from_boys(arr):
    return sorted(filter(lambda x: x % 2 == 0, set(arr))) + sorted(filter(lambda x: x % 2 == 1, set(arr)), reverse=True)


def next_happy_year(year):
    for i in range(year + 1, 9999):
        if len(set(str(i))) == 4:
            return i


def min_sum(arr):
    rtn_i = 0
    arr = sorted(arr)
    for i in range(len(arr) // 2):
        rtn_i += arr[i] * arr[-i - 1]
    return rtn_i


def max_gap(numbers):
    numbers = sorted(numbers)
    n_arr = [abs(numbers[i + 1] - numbers[i]) for i in range(len(numbers) - 1)]
    return max(n_arr)


def min_value1(digits):
    return sum(v * 10 ** i for i, v in enumerate(sorted(list((set(digits))), reverse=True)))


def min_value(digits):
    return int("".join(map(str, sorted(set(digits)))))


def make_acronym(phrase):
    if phrase == "":
        return ""
    if not isinstance(phrase, str):
        return "Not a string"
    if not phrase.replace(' ', '').isalpha():
        return "Not letters"
    return "".join(i[0].upper() for i in phrase.split())


def reverse_it(data):
    """ 把data反转，如果data是 int, str, float的话"""
    if type(data) in [int, str, float]:
        return type(data)(str(data)[::-1])
    return data


def faro_cycles(deck_size):
    """ use faro cycle to shuffle a deck of cards
    and return the number of cycles required to shuffle the deck"""
    rtn_i = 0
    arr_i = list(range(deck_size))
    while True:
        rtn_i += 1
        arr_i = arr_i[::2] + arr_i[1::2]
        if arr_i == list(range(deck_size)):
            return rtn_i


def ideal_trader(prices):
    """
    Write a function that accepts a list of price points on the chart
    The default money is 1
    returns the maximum possible profit from trading.
    """
    money = 1
    for i in range(len(prices) - 1):
        if prices[i + 1] > prices[i]:
            money *= 1 + (prices[i + 1] - prices[i]) / prices[i]
    return money


def silent_thief1(module_name):      # 这个很有意思， 不允许import 但是又要能使用
    return globals()['_''_builtins_''_']['_''_imp''ort_''_'](module_name)


def silent_thief(module_name):       # 这个很有意思， 不允许import 但是又要能使用 ，用utf编码替代了上面的方法
    return globals()['\x5f\x5f\x62\x75\x69\x6c\x74\x69\x6e\x73\x5f\x5f']['\x5f\x5f\x69\x6d\x70\x6f\x72\x74\x5f\x5f'](module_name)


# func = list(list(globals().items())[7][1].items())[6][1]
# def silent_thief2(name):
#     return func(name)


# def hackermann(m, n, f):
#     if m <= 0:
#         return n + f
#     if m > 0 and n <= 0:
#         return hackermann(m - f, 1, f)
#     if m > 0 and n > 0:
#         return hackermann(m - f, hackermann(m, n - f, f), f)


def hackermann(m, n, f):
    """
    这题我没有做出来，下面是copy来的
    递归转循环，以后再做
    """
    stack = [m]

    while stack:
        m = stack.pop()
        if m <= 0:
            n = n + f
        elif n <= 0:
            n = 1
            stack.append(m - f)
        else:
            n = n - f
            stack.append(m - f)
            stack.append(m)
    return n


def multiiter(*params):
    args = []
    for param in params:
        args.append(range(param))
    return product(*args)


def remove_smallest(n, arr: list):
    arr_copy = arr.copy()
    while n > 0 and len(arr):
        arr.remove(min(arr))
        n -= 1
    return arr


def degradation(item: 'Item') -> int:
    name = item.name.lower()
    if 'aged brie'        in name: return 1
    if 'backstage passes' in name:
        if item.sell_in <=  0: return -item.quality
        if item.sell_in <=  5: return 3
        if item.sell_in <= 10: return 2
        return 1
    return -(2 if item.sell_in < 0 else 1) * (2 if 'conjured' in name else 1)


def update_quality(items: list) -> None:
    for item in items:
        if 'sulfuras' in item.name.lower(): continue
        item.sell_in -= 1
        item.quality = max(0, min(item.quality + degradation(item), max(item.quality, 50)))


