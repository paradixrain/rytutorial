# coding = utf-8
# import random
import hashlib
import itertools
import re
import string
import statistics
# import math
import operator
import bisect

from copy import copy
from collections import Counter
from typing import Optional
from itertools import accumulate
from operator import mul
from itertools import zip_longest
from fractions import Fraction
from math import gcd



# from codewarMathematics import is_prime


def swap(string_):
    return ''.join([i.lower() if i.isupper() else i.upper() for i in string_])


def pass_hash(s):
    s = hashlib.md5(s.encode(encoding='utf-8'))
    return s.hexdigest()


def get_weight(name):
    return sum(ord(i) if i.isalpha() else 0 for i in name.swapcase())


# ASCII Shift Encryption/Decryption
def ascii_encrypt(plaintext):
    loc = 0
    s = []
    for i in plaintext:
        s.append(chr(ord(i) + loc))
        loc += 1
    return ''.join(s)


def ascii_decrypt(plaintext):
    loc = 0
    s = []
    for i in plaintext:
        s.append(chr(ord(i) - loc))
        loc += 1
    return ''.join(s)


def ascii_encrypt3(plaintext):
    return ''.join(chr(ord(c) + i) for i, c in enumerate(plaintext))


def ascii_decrypt2(plaintext):
    return ''.join(chr(ord(c) - i) for i, c in enumerate(plaintext))


# char_to_ascii
def char_to_ascii(s):
    if s == "":
        return None
    d = {}
    for i in set(s):
        if i.isalpha():
            d[i] = ord(i)
    return d


def char_to_ascii2(s):
    if s: return {c: ord(c) for c in set(s) if c.isalpha()}


# Sum of all arguments
def sum_args(*args):
    return sum(args)


# For the sake of argument
def numbers(*args):
    for i in args:
        # print(type(i))
        if (not type(i) == type(1)) and (not type(i) == type(1.1)):
            return False
    return True


def numbers2(*args):
    return all(type(a) in (int, float) for a in args)


def add(*args):
    return sum((i + 1) * v for i, v in enumerate(args))


# Some Circles
import math


def sum_circles(*args):
    return f"We have this much circle: {round(sum(0.25 * math.pi * r ** 2 for r in args))}"


# Unpacking Arguments
# 这道题很有意思，提供了一个args的新思路，就是【】里的参数和 直接传多个参数，是一个意思
def spread(func, args):
    return func(*args)


# Closure Counter
# 闭包函数
def counter():
    num = 0

    def d():
        nonlocal num
        num += 1
        return num

    return d


# Alphabetical Grid
def grid(N):
    if N < 0:
        return None
    elif N == 0:
        return ''
    else:
        line = '\n'.join(' '.join(chr(((j + i) % 26) + 97) for j in range(N))
                         for i in range(N))
        return line


# How Green Is My Valley?
def make_valley(arr):
    arr = sorted(arr, reverse=True)
    left = arr[::2]
    right = arr[1::2][::-1]
    return left + right


# a = make_valley([17, 17, 15, 14, 8, 7, 7, 5, 4, 4, 1])
# print(a)


class Node(object):
    def __init__(self, data, next=None):
        self.data = data
        self.next = next


def get_nth(node, index):
    # 接受一个链表和一个整数索引并返回存储在nth索引位置的节点
    if node is None:
        # 如果链表为空，抛出异常
        raise ValueError('Linked list is empty')
    if index == 0:
        return node
    # index不合法，则返回异常
    if index < 0:
        raise IndexError('Index is out of range')
    return get_nth(node.next, index - 1)


"""define the function to create backronyms. Transform the given string ,without spaces, to a backronym,
using the preloaded dictionary and return a string of words, separated with a single space
(but no trailing spaces)."""
dictionary = {'A': 'awesome', 'B': 'beautiful', 'C': 'confident', 'D': 'disturbing', 'E': 'eager',
              'F': 'fantastic', 'G': 'gregarious', 'H': 'hippy', 'I': 'ingestable', 'J': 'joke',
              'K': 'klingon', 'L': 'literal', 'M': 'mustache', 'N': 'newtonian', 'O': 'oscillating',
              'P': 'perfect', 'Q': 'queen', 'R': 'rant', 'S': 'stylish', 'T': 'turn', 'U': 'underlying',
              'V': 'volcano', 'W': 'weird', 'X': 'xylophone', 'Y': 'yogic', 'Z': 'zero'}


def make_backronym(s):
    """define the function to create backronyms. Transform the given string ,without spaces, to a backronym,
using the preloaded dictionary and return a string of words, separated with a single space
(but no trailing spaces)."""
    if s is None:
        return None
    if s == "":
        return ""
    s = s.upper()
    s = s.replace(" ", "")
    s = s.replace("-", "")
    return " ".join([dictionary[i] for i in s])


def make_backronym2(s):
    # create a list of words from the string
    words = [i for i in s]
    # print(words)
    # create a list of backronyms
    backronyms = []
    # loop through the list of words
    for word in words:
        # if the word.upper is in the dictionary, add the backronym to the list
        if word.upper() in dictionary:
            backronyms.append(dictionary[word.upper()])
        # if the word is not in the dictionary, add the word to the list
        else:
            backronyms.append(word)
    # return the list of backronyms, separated by a single space
    return ' '.join(backronyms)


# given a number and a binary tree,return True if the given number is in the tree
class Node2:
    def __init__(self, value: int, left: Optional[Node] = None, right: Optional[Node] = None):
        self.value = value
        self.left = left
        self.right = right


# def search(n: int, root: Optional[Node]) -> bool:
#     """ Determines if a value is in a binary tree (NOT bst) """
#     if root is None:
#         return False
#     if root.value == n:
#         return True
#     return search(n, root.left) or search(n, root.right)


def sum_from_string(strng):
    """给定一个由数字、字母、符号组成的随机字符串，您需要对字符串中的数字求和。连续整数应被视为单个数字。
    所有数字都应视为正整数。如果字符串中没有给出数字，它应该返回0"""
    import re
    # 使用re模块匹配strng中所有整数
    ints = re.findall("[0-9]{1,}", strng)
    return sum([int(i) for i in ints])


"""Complete the function that takes one argument, a list of words,
and returns the length of the longest word in the list."""


def longest(words):
    return max([len(i) for i in words])


"""DropCaps means that the first letter of the starting word of the paragraph 
should be in caps and the remaining lowercase, just like you see in the newspaper.

But for a change, let"s do that for each and every word of the given String. 
Your task is to capitalize every word that has length greater than 2, leaving smaller words as they are.

*should work also on Leading and Trailing Spaces and caps."""


def drop_cap(s):
    import re
    # 使用re模块替换s中所有长度大于2的单词的首字母为大写，其余为小写
    return re.sub(r'\b\w{3,}\b', lambda m: m.group(0).capitalize(), s)


"""Given two words and a letter, return a single word that's a combination of both words, 
merged at the point where the given letter first appears in each word. 
The returned word should have the beginning of the first word and the ending of the second, 
with the dividing letter in the middle. You can assume both words will contain the dividing letter.
"""


# def string_merge(string1, string2, letter):
#     # 将string1和string2中出现的letter替换为letter+string1[index]
#     return string1[:string1.index(letter)] + string2[string2.index(letter):]


# 定义一个方法，将字符串转为字符数组
def string_to_char_array(s):
    return [c for c in s]


def is_same_language(lst):
    return len(set(i['language'] for i in lst)) == 1


def get_average(lst):
    return int(sum(i['age'] for i in lst) / len(lst))


def handler(key, is_caps=False, is_shift=False):
    """
    The keyboard handler is a function which receives three parameters as input:
Key - the entered character on the keyboard.
isCaps (or is_caps) - boolean variable responsible for the enabled 'Caps Lock'. (by default false)
isShift (or is_shift) - boolean variable which is responsible for whether 'Shift' is pressed. (by default false)
Your task to write a function that returns the entered character.
    """
    keyboard = 'qwertyuiopasdfghjklzxcvbnm1234567890[]\;\',./`-='
    keyboard_upper = 'QWERTYUIOPASDFGHJKLZXCVBNM!@#$%^&*(){}|:"<>?~_+'
    if type(key) == str and len(key) == 1:
        if 0 <= keyboard.find(key) <= 25:
            if is_shift == is_caps:
                return key
            elif is_shift ^ is_caps:
                return keyboard_upper[keyboard.find(key)]
        if 0 <= keyboard.find(key) > 25:
            if is_shift:
                return keyboard_upper[keyboard.find(key)]
            else:
                return key
    return "KeyError"


def flatten_and_sort(array):
    from itertools import chain
    return chain.from_iterable(array)


def scrolling_text(text):
    return [text[i:].upper() + text[:i].upper() for i in range(len(text))]


def f(x, a, b, c):
    lst = [a, b, c, a]
    return lst[lst.index(x) + 1]


def fold_to(distance):
    from math import log, ceil
    paper_thickness = 0.0001
    if distance < 0:
        return None
    elif 0 <= distance < paper_thickness:
        return 0
    else:
        return ceil(log(distance / paper_thickness, 2))


def row_weights(array):
    odds_sum = sum(array[i] for i in range(0, len(array), 2))
    evens_sum = sum(array[i] for i in range(1, len(array), 2))
    return odds_sum, evens_sum


def extra_perfect(n):
    return [i for i in range(1, n + 1) if i % 2 != 0]


def product_array(numbers):
    from functools import reduce
    return [reduce(lambda x, y: x * y, numbers[:i] + numbers[i + 1:]) for i in range(len(numbers))]


def even_numbers1(arr, n):
    return [i for i in arr if i % 2 == 0][::-1][:n][::-1]


def even_numbers(arr, n):
    return [i for i in arr if i % 2 == 0][-n:]


def nth_smallest(arr, pos):
    return sorted(arr)[pos - 1]


def odd_or_even1(n):
    if n % 2 == 1:
        return "either"
    else:
        if n % 4 == 0:
            return "even"
        else:
            return "odd"


def odd_or_even(n):
    return ("Even", "Either", "Odd", "Either")[n % 4]


def compute_depth(n):
    s_n = str(n)
    step = 1
    while len(set(s_n)) != 10:
        step += 1
        s_n += str(n * step)
    return step


def transpose_two_strings(arr):
    from itertools import zip_longest
    return '\n'.join(f"{i} {j}" for i, j in zip_longest(*arr, fillvalue=' '))


# def generateName():
#     from random import choice
#     import string
#     while True:
#         rtn_str = ''.join(choice(string.ascii_letters) for _ in range(6))
#         if photoManager.nameExists(rtn_str):
#             continue
#         else:
#             return rtn_str


def reverse1(seq):
    rtn_seq = list()
    for i in seq:
        rtn_seq.insert(0, i)
    return rtn_seq


def reverse2(seq):
    rtn_seq = list()
    while seq:
        rtn_seq.append(seq.pop())
    return rtn_seq


def reverse(seq):
    for i in range(len(seq) // 2):
        seq[i], seq[-i - 1] = seq[-i - 1], seq[i]
    return seq


def custom_fib(signature, indexes, n):
    def fib(nn):
        for ii in range(nn + 1):
            if ii < len(signature):
                yield signature[ii]
            else:
                new_val = sum(signature[j] for j in indexes)
                signature.append(new_val)
                signature.pop(0)
                yield new_val

    return list(fib(n))[n]


def custom_fib2(signature, indexes, n):
    from collections import deque
    fib = deque(signature)
    for _ in range(n):
        fib.append(sum(map(fib.__getitem__, indexes)))
        fib.popleft()
    return fib[0]


def custom_fib3(s, ee, n):
    while n > 0:
        s, n = s[1:] + [sum(s[i] for i in ee)], n - 1
    return s[0]


def compare(s1, s2):
    from itertools import zip_longest
    for s1_i, s2_i in zip_longest(s1.split('.'), s2.split('.'), fillvalue='0'):
        if int(s1_i) < int(s2_i):
            return -1
        elif int(s1_i) > int(s2_i):
            return 1
    return 0


def hackermann(m, n, f):
    stack = [m]
    while stack:
        m = stack.pop()
        if m <= 0:
            n += f
        elif n <= 0:
            n = 1
            stack.append(m - f)
        else:
            n -= f
            stack.append(m - f)
            stack.append(m)
    return n


def deck_of_cards(): return [f"{num} of {hspd}" for hspd in ['hearts', 'spades', 'diamonds', 'clubs'] for num in
                             ['ace', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'jack',
                              'queen', 'king']]


def xp_to_target_lvl(*args):
    from itertools import accumulate
    print('args', args)
    try:
        current_xp, target_lvl = args[0], args[1]
        if target_lvl < 1 or target_lvl > 170 or current_xp < 0:
            return "Input is invalid."
    except:
        return "Input is invalid."

    exp_list = [314]
    rate = 0.25
    for i in range(2, 171):
        rate_i = round(rate - 0.01 * (i // 10), 2)
        exp_list.append(int(exp_list[-1] * (1 + rate_i)))

    exp_list = list(accumulate(exp_list))
    for i, v in enumerate(exp_list):
        # print(i + 1, v)
        pass

    if current_xp >= exp_list[target_lvl - 2] or target_lvl - 2 < 0:
        return f"You have already reached level {target_lvl}."
    return exp_list[target_lvl - 2] - current_xp


def is_valid_walk(walk):
    return len(walk) == 10 and walk.count('n') == walk.count('s') and walk.count('e') == walk.count('w')


def is_narcissistic(i):
    return sum(int(d) ** len(str(i)) for d in str(i)) == i


def interest(p, r, n):
    return [round(p + p * r * n), round(p * (1 + r) ** n)]


# def consecutive(arr, a, b):
#     return any((arr[i] == a and arr[i + 1] == b) or (arr[i] == b and arr[i + 1] == a) for i in range(len(arr) - 1))


def remove(s):
    return ' '.join(i for i in s.split() if i.count('!') != 1)


def get_winner(ballots):
    ball_dict = {}
    max_ball = 0
    max_person = ""
    ticket_number = len(ballots)
    for i in ballots:
        ball_dict[i] = ball_dict.get(i, 0) + 1
        if ball_dict.get(i, 0) > max_ball:
            max_ball = ball_dict.get(i, 0)
            max_person = i

    if max_ball > ticket_number / 2:
        return max_person
    else:
        return None


def remainder(a, b):
    # Find the Remainder
    ma, mi = max(a, b), min(a, b)
    return None if mi == 0 else ma % mi


def same_case(a, b):
    # Check same case
    if not a.isalpha() or not b.isalpha():
        return -1
    if (a.isupper() and b.isupper()) or (a.islower() and b.islower()):
        return 1
    return 0


def type_validation(variable, _type):
    # For Twins: 1. Types
    return variable.__class__.__name__ == _type


def series_sum(n):
    # Sum of the first nth term of Series
    rtn_number = 0
    for i in range(n):
        rtn_number += 1 / (1 + 3 * i)
    return '{:.2f}'.format(round(rtn_number, 2))


def tower_builder(n_floors):
    # Build Tower
    bottom_width = n_floors * 2 - 1
    fmt = '{:^' + str(bottom_width) + '}'
    return [fmt.format('*' * (2 * i - 1)) for i in range(1, n_floors + 1)]


def tower_builder2(n):
    return [("*" * (i * 2 - 1)).center(n * 2 - 1) for i in range(1, n + 1)]


def stock_list(list_of_art, list_of_cat):
    # Help the bookseller !
    dict_books = {}
    for i in list_of_art:
        j = i.split(' ')
        dict_books[i[0]] = int(j[1]) + dict_books.get(i[0], 0)
    if sum(dict_books.values()) == 0:
        return ''
    return ' - '.join("({0} : {1})".format(i, dict_books.get(i, 0)) for i in list_of_cat)


def high(x):
    # Highest Scoring Word
    w = x.split()
    m = 0
    max_world = ""
    for i in w:
        if sum(ord(k) - 96 for k in i) > m:
            m = sum(ord(k) - 96 for k in i)
            max_world = i
    return max_world


def data_reverse(data):
    groups = len(data) // 8
    for i in range(groups // 2):
        # 分组
        temp = copy(data[8 * i:i * 8 + 8])
        data[8 * i:i * 8 + 8] = copy(data[(groups - 1 - i) * 8:(groups - i) * 8])
        data[(groups - 1 - i) * 8:(groups - i) * 8] = copy(temp)
    return data


def choose_best_sum(t, k, ls):
    # Best travel
    try:
        return max(sum(i) for i in itertools.combinations(ls, k) if sum(i) <= t)
    except:
        return None


def comp(array1, array2):
    # Are they the "same"?
    return sorted([i ** 2 for i in array1]) == sorted(array2) if array1 is not None and array2 is not None else False


def nb_year(p0, percent, aug, p):
    # Growth of a Population
    year = 0
    pn = p0

    while pn < p:
        pn = int(p0 * (100 + percent) / 100) + aug
        p0 = pn
        year += 1
    return year


def vert_mirror(strng):
    s = strng.split('\n')
    s = [i[::-1] for i in s]
    return '\n'.join(s)


def hor_mirror(strng):
    s = strng.split('\n')
    return '\n'.join(s[::-1])


def oper(fct, s):
    return fct(s)


def reverse_letter(s):
    # Simple Fun #176: Reverse Letter
    return ''.join(i for i in s[::-1] if i in string.ascii_letters)


def words_to_marks(s):
    return sum([ord(i) - 96 for i in s])


def meeting(s):
    # Meeting
    s = sorted(s.upper().split(';'))
    s = [(i.split(':')[0], i.split(':')[1]) for i in s]
    s = sorted(s, key=lambda x: (x[1], x[0]))
    rtn = ""
    for i in s:
        rtn += "({0}, {1})".format(i[1], i[0])
    return rtn


def solution(s):
    # Break camelCase
    return re.sub(r'([A-Z])', r' \1', s)


def loc(letter, keyboard):
    return max(i.find(letter) for i in keyboard) + 1


def i_loc(letter, keyboard):
    for i in range(len(keyboard)):
        if letter in keyboard[i]:
            return i


def presses(phrase):
    # Multi-tap Keypad Text Entry on an Old Mobile Phone
    keyboard = ['1', 'abc2', 'def3', 'ghi4', 'jkl5', 'mno6', 'pqrs7', 'tuv8', 'wxyz9', '*', ' 0', '#']
    count = sum(loc(i, keyboard) for i in phrase.lower())
    # dif = [i_loc(i, keyboard) for i in phrase.lower()]
    # dif = [1 if dif[i] == dif[i + 1] else 0 for i in range(len(dif) - 1)]
    # count += sum(dif)
    return count


# def longest_consec(strarr, k):
#     # Consecutive strings
#     if len(strarr) == 0 or k > len(strarr) or k <= 0:
#         return ""
#     else:
#         intarr = [len(i) for i in strarr]
#         intarr = [sum(intarr[i:i + k]) for i in range(len(intarr) + 1 - k)]
#         maxloc = intarr.index(max(intarr))
#     return "".join(strarr[maxloc:maxloc + k])


def alternate_case(s):
    # Alternate case
    return ''.join(i.lower() if i.isupper() else i.upper() for i in s)


def ordered_count(inp):
    # Ordered Count of Characters
    return sorted(Counter(inp).most_common(), key=lambda x: inp.index(x[0]))


def solve(arr):
    # Simple remove duplicates
    rtn_arr = []
    for i in range(len(arr)):
        if arr[::-1][i] not in rtn_arr:
            rtn_arr.append(arr[::-1][i])
    return rtn_arr[::-1]


def reves(m):
    return m.groups(0)[0][::-1]


def reverse_words(text):
    return re.sub(r'([^ ]+)', reves, text)


def reverse_words1(str):
    # reverse words
    return re.sub(r'\S+', lambda w: w.group(0)[::-1], str)


def palindrome_length(s):
    return len(s) if s == s[::-1] else 1


def longest_palindrome(s):
    # longest_palindrome
    # 取s的子集，进行判断是否回文
    rtn_maxlen = 0
    for i in range(len(s) + 1):
        for j in range(i, len(s) + 1):
            rtn_maxlen = max(rtn_maxlen, palindrome_length(s[i:j]))
    return rtn_maxlen


data = """Rome:Jan 81.2,Feb 63.2,Mar 70.3,Apr 55.7,May 53.0,Jun 36.4,Jul 17.5,Aug 27.5,Sep 60.9,Oct 117.7,Nov 111.0,Dec 97.9
London:Jan 48.0,Feb 38.9,Mar 39.9,Apr 42.2,May 47.3,Jun 52.1,Jul 59.5,Aug 57.2,Sep 55.4,Oct 62.0,Nov 59.0,Dec 52.9
Paris:Jan 182.3,Feb 120.6,Mar 158.1,Apr 204.9,May 323.1,Jun 300.5,Jul 236.8,Aug 192.9,Sep 66.3,Oct 63.3,Nov 83.2,Dec 154.7
NY:Jan 108.7,Feb 101.8,Mar 131.9,Apr 93.5,May 98.8,Jun 93.6,Jul 102.2,Aug 131.8,Sep 92.0,Oct 82.3,Nov 107.8,Dec 94.2
Vancouver:Jan 145.7,Feb 121.4,Mar 102.3,Apr 69.2,May 55.8,Jun 47.1,Jul 31.3,Aug 37.0,Sep 59.6,Oct 116.3,Nov 154.6,Dec 171.5
Sydney:Jan 103.4,Feb 111.0,Mar 131.3,Apr 129.7,May 123.0,Jun 129.2,Jul 102.8,Aug 80.3,Sep 69.3,Oct 82.6,Nov 81.4,Dec 78.2
Bangkok:Jan 10.6,Feb 28.2,Mar 30.7,Apr 71.8,May 189.4,Jun 151.7,Jul 158.2,Aug 187.0,Sep 319.9,Oct 230.8,Nov 57.3,Dec 9.4
Tokyo:Jan 49.9,Feb 71.5,Mar 106.4,Apr 129.2,May 144.0,Jun 176.0,Jul 135.6,Aug 148.5,Sep 216.4,Oct 194.1,Nov 95.6,Dec 54.4
Beijing:Jan 3.9,Feb 4.7,Mar 8.2,Apr 18.4,May 33.0,Jun 78.1,Jul 224.3,Aug 170.0,Sep 58.4,Oct 18.0,Nov 9.3,Dec 2.7
Lima:Jan 1.2,Feb 0.9,Mar 0.7,Apr 0.4,May 0.6,Jun 1.8,Jul 4.4,Aug 3.1,Sep 3.3,Oct 1.7,Nov 0.5,Dec 0.7"""

data1 = """Rome:Jan 90.2,Feb 73.2,Mar 80.3,Apr 55.7,May 53.0,Jun 36.4,Jul 17.5,Aug 27.5,Sep 60.9,Oct 147.7,Nov 121.0,Dec 97.9
London:Jan 58.0,Feb 38.9,Mar 49.9,Apr 42.2,May 67.3,Jun 52.1,Jul 59.5,Aug 77.2,Sep 55.4,Oct 62.0,Nov 69.0,Dec 52.9
Paris:Jan 182.3,Feb 120.6,Mar 188.1,Apr 204.9,May 323.1,Jun 350.5,Jul 336.8,Aug 192.9,Sep 66.3,Oct 63.3,Nov 83.2,Dec 154.7
NY:Jan 128.7,Feb 121.8,Mar 151.9,Apr 93.5,May 98.8,Jun 93.6,Jul 142.2,Aug 131.8,Sep 92.0,Oct 82.3,Nov 107.8,Dec 94.2
Vancouver:Jan 155.7,Feb 121.4,Mar 132.3,Apr 69.2,May 85.8,Jun 47.1,Jul 31.3,Aug 37.0,Sep 69.6,Oct 116.3,Nov 154.6,Dec 171.5
Sydney:Jan 123.4,Feb 111.0,Mar 151.3,Apr 129.7,May 123.0,Jun 159.2,Jul 102.8,Aug 90.3,Sep 69.3,Oct 82.6,Nov 81.4,Dec 78.2
Bangkok:Jan 20.6,Feb 28.2,Mar 40.7,Apr 81.8,May 189.4,Jun 151.7,Jul 198.2,Aug 197.0,Sep 319.9,Oct 230.8,Nov 57.3,Dec 9.4
Tokyo:Jan 59.9,Feb 81.5,Mar 106.4,Apr 139.2,May 144.0,Jun 186.0,Jul 155.6,Aug 148.5,Sep 216.4,Oct 194.1,Nov 95.6,Dec 54.4
Beijing:Jan 13.9,Feb 14.7,Mar 18.2,Apr 18.4,May 43.0,Jun 88.1,Jul 224.3,Aug 170.0,Sep 58.4,Oct 38.0,Nov 19.3,Dec 2.7
Lima:Jan 11.2,Feb 10.9,Mar 10.7,Apr 10.4,May 10.6,Jun 11.8,Jul 14.4,Aug 13.1,Sep 23.3,Oct 1.7,Nov 0.5,Dec 10.7"""

towns = ["Rome", "London", "Paris", "NY", "Vancouver", "Sydney", "Bangkok", "Tokyo",
         "Beijing", "Lima", "Montevideo", "Caracas", "Madrid", "Berlin", "Lon"]


def mean(town, s):
    # Rainfall
    city = s.split('\n')
    for i in city:
        j = i.split(':')
        if j[0] == town:
            k = [float(t.split(' ')[1]) for t in j[1].split(',')]
            return statistics.mean(k)
    return -1


def variance(town, s):
    city = s.split('\n')
    for i in city:
        j = i.split(':')
        if j[0] == town:
            k = [float(t.split(' ')[1]) for t in j[1].split(',')]
            return statistics.pvariance(k)
    return -1


def calculate(num1, operation, num2):
    # Basic Calculator
    try:
        return eval(f'({num1}) {operation} ({num2})')
    except:
        return None


def switcher(arr):
    # Numbers to Letters
    d = {str(97 + 26 - ord(i)): i for i in 'abcdefghijklmnopqrstuvwxyz'}
    d['28'] = '?'
    d['27'] = '!'
    d['29'] = ' '
    return ''.join(d[i] for i in arr)


def max_product(lst, n):
    # Product Of Maximums Of Array (Array Series #2)
    lst = sorted(lst, reverse=True)
    return list(accumulate(lst, mul))[n - 1]


# def meeting(rooms):
#     # The Office IV - Find a Meeting Room
#     return 'None available!' if 'O' not in rooms else rooms.index('O')


def sum_of_integers_in_string(s):
    # Sum of integers in string
    return sum(int(i) for i in re.findall(r'\d+', s))


def decomp(n):
    # Factorial decomposition
    arr_orgin = list(range(1, n + 1))
    dict_num = dict()
    # print(arr_orgin)
    for i in range(len(arr_orgin)):
        div_num = arr_orgin[i]
        if arr_orgin[i] == 1:
            continue
        dict_num[div_num] = dict_num.get(div_num, 0)
        # print('before', i, div_num, arr_orgin, dict_num)
        for j in range(i, n, div_num):
            if arr_orgin[j] != 1 and arr_orgin[j] % div_num == 0:
                arr_orgin[j] = arr_orgin[j] // div_num
                dict_num[div_num] = dict_num.get(div_num, 0) + 1
        # print('after', i, div_num, arr_orgin, dict_num)
    rtn_arr = []
    for k, v in dict_num.items():
        if v > 1:
            rtn_arr.append(f"{k}^{v}")
        else:
            rtn_arr.append(f"{k}")
    return " * ".join(rtn_arr)


def binary_array_to_number(arr):
    # Ones and Zeros
    return int(''.join(str(i) for i in arr), 2)


def string_transformer(s):
    # String transformer
    s = s.swapcase()
    r1 = list(re.finditer(r'([a-zA-Z]+)|([ ]+)', s))
    comb_r = []
    for i in r1[::-1]:
        a, b = i.start(), i.group(0)
        comb_r.append([a, b])
    return ''.join(i[1] for i in comb_r)


def string_transformer1(s):
    return ' '.join(s.swapcase().split(' ')[::-1])


def is_anagram(test, original):
    # Anagram Detection
    return sorted(test.lower()) == sorted(original.lower())


def ip_to_int32(ip):
    # IPv4 to int32
    rtn_str = ''.join(bin(int(i))[2:].zfill(8) for i in ip.split("."))
    return int(rtn_str, 2)


def vaporcode(s):
    # V A P O R C O D E
    return '  '.join(i.upper() for i in s if i != ' ')


def prefill(n, v=None):
    # Prefill an Array
    try:
        return int(n) * [v]
    except TypeError:
        return f"{n} is invalid"


def balance(left, right):
    # Exclamation marks series #17
    l = left.count('!') * 2 + left.count('?') * 3
    r = right.count('!') * 2 + right.count('?') * 3
    if l == r:
        return 'Balance'
    elif l < r:
        return 'Right'
    else:
        return 'Left'


def name_value(my_list):
    # Word values
    return [sum(ord(j) - 96 for j in v if j.isalpha()) * (k + 1) for k, v in enumerate(my_list)]


def longest_word(s):
    # Give me back the longest word!
    return sorted(s.split(' '), key=len)[-1]


def max_ball(v0):
    # Ball Upwards
    return round(10 * v0 / 9.81 / 3.6)


def capitalize(s, ind):
    # Indexed capitalization
    return ''.join(s[i].upper() if i in ind else s[i] for i in range(len(s)))


def no_odds(values):
    return list(filter(lambda x: x % 2 == 0, values))


def devidebyn(s, n):
    return True if s % n == 0 else False


def howmuch(m, n):
    # How Much?
    rtn_arr = []
    for i in range(min(m, n), max(n, m) + 1):
        if devidebyn(i - 1, 9) and devidebyn(i - 2, 7):
            rtn_arr.append([f"M: {i}", f"B: {(i - 2) // 7}", f"C: {(i - 1) // 9}"])
    return rtn_arr


def calculate_2(s):
    # Basic Math (Add or Subtract)
    return str(eval(s.replace("minus", "-").replace("plus", "+")))


def sum_no_duplicates(l):
    # Sum a list but ignore any duplicates
    return sum(i for i in l if l.count(i) == 1)


def difference_of_squares(n):
    # Difference Of Squares
    return sum(range(1, n + 1)) ** 2 - sum(i ** 2 for i in range(1, n + 1))


# ryan = difference_of_squares(5)

def open_or_senior(data):
    # Categorize New Member
    return ['Senior' if i[0] > 54 and i[1] > 7 else 'Open' for i in data]


# ryan = open_or_senior([(45, 12),(55,21),(19, -2),(104, 20)])

def arithmetic(a: int, b, op: str):
    # Make a function that does arithmetic!
    d = {'add': operator.add, 'subtract': operator.sub, "multiply": operator.mul, "divide": operator.truediv}
    return d[op](a, b)


def even_or_odd(s):
    # Even or Odd - Which is Greater?
    o = sum(int(i) for i in s if int(i) % 2 == 1)
    e = sum(int(i) for i in s) - o
    if e > o:
        return 'Even is greater than Odd'
    elif e < o:
        return 'Odd is greater than Even'
    else:
        return 'Even and Odd are the same'


# ryan = even_or_odd('12')


def last(*args):
    # Last , 判断是否能迭代
    print(len(args), hasattr(*args, '__iter__'))
    return args[0][-1] if len(args) == 1 and hasattr(*args, '__iter__') else args[-1]


# ryan = last([1,2])


# def solution(st, limit):
#     # Limit string length - 1
#     return st[:limit]+'...'*max(0, len(st)>limit)


# ryan = solution('Testing String',8)


def filter_lucky(lst):
    # Find the lucky numbers
    return [i for i in lst if '7' in str(i)]


# ryan = filter_lucky([71,9907,69])


equals_9 = lambda x: x == 9


def one(sq, fun):
    # Enumerable Magic #5- True for Just One?
    return sum(1 for i in sq if fun(i)) == 1


# arr = (1, 2, 3, 4, 9)
# ryan = one(arr, equals_9)


def generate_integers(m, n):
    # Series of integers from m to n
    return list(range(m, n + 1))


# ryan = generate_integers(2, 5)


def create_dict(keys, values):
    # Dictionary from two lists
    values = values[:len(keys)]
    return dict(zip_longest(keys, values))


# keys = ['a', 'b', 'c', 'd']
# values = [1, 2, 3]
# ryan = create_dict(keys, values)


def consonant_count(s):
    # Count consonants
    return sum(1 for i in s.upper() if i not in 'AEIOU' and i in string.ascii_letters)


# ryan = consonant_count('aaaaa')


def two_decimal_places(number):
    # Formatting decimal places #1
    return round((number * 100).__ceil__() / 100, 2) if number < 0 else round((number * 100).__floor__() / 100, 2)


# ryan = two_decimal_places(-7488.83685834983)
# print('ryan:', ryan)
# ryan = generate_integers(2, 5)


# Linked Lists - Push & BuildOneTwoThree
# class Node(object):
#     def __init__(self, data):
#         self.data = data
#         self.next = None


def push(head, data):
    if head is None:
        head = Node(data)
        return head
    else:
        new_node = Node(data)
        new_node.next = head
        return new_node


def build_one_two_three():
    head = Node(3)
    head = push(head, 2)
    head = push(head, 1)
    return head


# ryan = push(None, 1)
# ryan = push(ryan, 2)
# ryan = build_one_two_three()
# print('ryan:', ryan.data, ryan.next.data, ryan.next.next.data)


def rake_garden(garden):
    # Help Suzuki rake his garden!
    return ' '.join(i if i in ['gravel', 'rock'] else 'gravel' for i in garden.split())


# garden1 = 'slug spider rock gravel gravel gravel gravel gravel gravel gravel'
# ryan = rake_garden(garden1)
# print(ryan)
# 'gravel gravel rock gravel gravel gravel gravel gravel gravel gravel'


def save(sizes, hd):
    # Computer problem series #1: Fill the Hard Disk Drive
    count = 1
    for i in range(len(sizes)):
        if sum(sizes[:count]) >= hd:
            break
        count += 1
    return count - 1


# ryan = save([4, 4, 4, 3, 3], 11)


def trim(phrase, size):
    # Trimming a string
    if len(phrase) <= size:
        return phrase
    elif size <= 3:
        return phrase[:size] + '...'
    else:
        return phrase[:size - 3] + '...'


# ryan = trim("iMFn zkiC,", 3)


def evens_and_odds(n):
    # Evens and Odds
    if n % 2 == 0:
        return f'{str(bin(n))[2:]}'
    else:
        return f'{hex(n)[2:]}'
    # return f"{n:x}" if n % 2 else f"{n:b}"


# ryan = evens_and_odds(13)


def to_time(seconds):
    # All Star Code Challenge #22
    h = seconds // 3600
    m = (seconds % 3600) // 60
    return f'{h} hour(s) and {m} minute(s)'


# ryan = to_time(323500)


# def is_negative_zero(n):
#     # 判断 -0
#     i, j = -0, -0.0
#     # print(id(i), id(j), id(n))
#     if n is i or i is j:
#         # 这种方式在本机执行的时候可以识别-0，但是提交到codewar以后就是不同的值
#         return True
#     return False


def is_negative_zero(n):
    # Is It Negative Zero (-0)?
    return str(n) == '-0.0'


# print(divmod(-0.0, 100))
# ryan = is_negative_zero(-1.0)


def get_missing_element(seq):
    # Return the Missing Element
    return (set(range(0, 10)) - set(seq)).pop()
    # return 45 - sum(seq)  这个更好


# ryan = get_missing_element([0,5,1,3,2,9,7,6,4])


def sum_of_n(n):
    # Basic Sequence Practice
    num = lambda x: range(0, x + 1) if x > 0 else range(0, x - 1, -1)
    return [sum(num(i)) for i in num(n)]


# ryan = sum_of_n(3)


# def solve(s,g):
#     # GCD sum
#     if s % g == 0:
#         return g, s - g
#     else:
#         return -1


# ryan = solve(6, 3)


def arr_check(arr):
    return all(type(i) == list for i in arr)


# ryan = arr_check([[1], [2], [3]])


def nickname_generator(name):
    # Nickname Generator
    return "Error: Name too short" if len(name) < 4 else name[:4] if name[2] in 'aeiou' else name[:3]


def product(s):
    # Exclamation
    return s.count("!") * s.count("?")


# ryan = product('!!??!!')


def initialize_names(name):
    # Initialize my name
    n_list = [v if k == 0 or k == len(name.split()) - 1 else f'{v[0]}.' for k, v in enumerate(name.split())]
    return ' '.join(n_list)


# ryan = initialize_names('Alice Betty Catherine Davis')


def owned_cat_and_dog(cat_years, dog_years):
    # Cat Years, Dog Years (2)
    return [(cat_years >= 15) * 1 + (cat_years >= 24) * 1 + (cat_years > 24) * (cat_years - 24) // 4,
            (dog_years >= 15) * 1 + (dog_years >= 24) * 1 + (dog_years > 24) * (dog_years - 24) // 5]


"""
Cat Years
15 cat years for first year
+9 cat years for second year
+4 cat years for each year after that
Dog Years
15 dog years for first year
+9 dog years for second year
+5 dog years for each year after that
"""


# ryan = owned_cat_and_dog(15, 15)


def who_is_online(friends):
    # Who's Online?
    d = dict()
    for f in friends:
        if f["status"] == "online":
            if f["last_activity"] <= 10:
                if "online" in d.keys():
                    d["online"].append(f["username"])
                else:
                    d["online"] = [f["username"]]
            else:
                if "away" in d.keys():
                    d['away'].append(f["username"])
                else:
                    d['away'] = [f["username"]]
        else:
            if "offline" in d.keys():
                d["offline"].append(f["username"])
            else:
                d["offline"] = [f["username"]]
    return d


friends = [{"username": "David", "status": "online", "last_activity": 10},
           {"username": "Lucy", "status": "offline", "last_activity": 22},
           {"username": "Bob", "status": "online", "last_activity": 104}]


# test.assert_equals(who_is_online(friends), {"online": ["David"], "offline": ["Lucy"], "away": ["Bob"]})
# ryan = who_is_online(friends)


def args_count(arg=None, *args, **kwargs):
    # How many arguments
    count = 0
    if arg is not None:
        count = 1
    return count + len(args) + len(kwargs)


# ryan = args_count(32, a1=12)


def sum_fracts(lst):
    # Irreducible Sum of Rationals
    if not lst:
        return None
    s = Fraction()
    for l in lst:
        s = s + Fraction(l[0], l[1])

    return s if '/' not in str(s) else list(s.as_integer_ratio())


# ryan = sum_fracts([[1, 3], [5, 3]])


# Linked Lists - Length & Count
def length(node):
    node_length = 0
    while node:
        node_length += 1
        node = node.next
    return node_length


def count(node, data):
    node_count = 0
    while node:
        if node.data == data:
            node_count += 1
        node = node.next
    return node_count


# ryan = length(Node(99))


# def remove(s):
#     # Exclamation marks series #3: Remove all exclamation marks from sentence except at the end
#     for i in range(len(s) - 1, -1, -1):
#         if s[i] != '!':
#             return s[:i].replace('!', '') + s[i:]


# ryan = remove('Hi!!!')


def filter_even_length_words(words):
    # filterEvenLengthWords
    return [i for i in words if len(i) % 2 == 0]


# ryan = filter_even_length_words(["One", "Two", "Three", "Four"])


def caffeine_buzz(n):
    # Caffeine Script
    x = n % 3 == 0
    y = n % 4 == 0
    z = n % 2 == 0
    if x and y and z:
        return "CoffeeScript"
    elif x and z:
        return "JavaScript"
    elif x:
        return "Java"
    else:
        return "mocha_missing!"


# ryan = caffeine_buzz(1)


def sabb(stg, value, happiness):
    # The Office VI - Sabbatical
    sabbatical = (value + happiness + sum(1 for c in stg if c in "sabbatical")) > 22
    return "Sabbatical! Boom!" if sabbatical else "Back to your desk, boy."


def tail_swap(s):
    # Tail Swap
    x = s[0].split(':')
    y = s[1].split(':')
    return [f'{x[0]}:{y[1]}', f'{y[0]}:{x[1]}']


# ryan = tail_swap(['abc:123', 'cde:456'])


def what_is_the_time(time_in_mirror):
    # Clock in Mirror
    h, m = time_in_mirror.split(':')
    m = 60 - int(m)
    m = 0 if m == 60 else m
    if m == 0:
        h = 12 - int(h) % 12
    else:
        h = 11 - int(h) % 12
    h = 12 if h == 0 else h
    return f"{h:02}:{m:02}"


# ryan = what_is_the_time("04:01")


def get_sum_of_digits(num):
    # Debug Sum of Digits of a Number
    sum = 0
    for x in str(num):
        sum += int(x)
    return sum


# ryan = get_sum_of_digits(123)


def is_letter(s):
    # Regexp Basics - is it a letter?
    return True if s.isalpha() and len(s) == 1 else False


# def mean(lst):
#     # Calculate mean and concatenate string
#     m = sum(int(i) for i in lst if 47<ord(i)<58)/sum(1 for i in lst if 47<ord(i)<58)
#     n = ''.join(i for i in lst if i.isalpha())
#     return [m, n]


# ryan = mean(["1", "1", "1", "1", "1", "1", "1", "1", "1", "0", "a", "a", "d", "d", "g", "q", "u", "v", "y", "y"])


def what_time_is_it(angle):
    # Clocky Mc Clock-Face
    h, m = angle.__divmod__(30)
    return f'{int(h) if h > 0 else 12:02}:{int(m * 2):02}'


# ryan = what_time_is_it(280.012)


def vampire_test(x, y):
    # Vampire Numbers
    return sorted(str(x * y)) == sorted(f'{x}{y}')


def unused_digits(*numbers):
    # Filter unused digits
    l = set("0123456789")
    s = set(''.join(str(n) for n in numbers))
    return ''.join(list(l - s))


# ryan = unused_digits(12, 34, 56, 78)


def cartesian_neighbor(x, y):
    # Cartesian neighbors
    return [(x - 1, y), (x + 1, y), (x - 1, y - 1), (x - 1, y + 1), (x, y + 1), (x, y - 1), (x + 1, y - 1),
            (x + 1, y + 1)]


# ryan = cartesian_neighbor(2,2)


def get_ages(sum_, difference):
    # Calculate Two People's Individual Ages
    if sum_ < 0 or difference < 0 or (sum_ - difference) < 0:
        return None
    else:
        return (sum_ - difference) / 2 + difference, (sum_ - difference) / 2


# ryan = get_ages(24, 4)

def mixed_fraction(s):
    # Simple fraction to mixed number converter
    nu, de = s.split('/')
    nu, de = int(nu), int(de)
    if de == 0:
        raise ZeroDivisionError
    else:
        flag = -1 * nu * de > 0
        nu, de = abs(int(nu)), abs(int(de))
        x = nu // de
        y = nu - de * x
        nd_gc = gcd(de, y)
        print(x, y, de, nd_gc, flag)
        if y == 0:
            rtn = f'{x}'
        elif x == 0:
            rtn = f'{y // nd_gc}/{de // nd_gc}'
        else:
            rtn = f'{x} {y // nd_gc}/{de // nd_gc}'
        return f'{"-" if flag else ""}{rtn}'


# ryan = mixed_fraction('-10/-7')


def land_perimeter(arr):
    # Land perimeter
    a1 = 0
    for k, v in enumerate(arr):
        for i, j in enumerate(v):
            if j == 'X':
                a1 += 4
                if i <= len(v) - 2 and v[i + 1] == 'X':
                    a1 -= 2
                if k <= len(arr) - 2 and arr[k + 1][i] == 'X':
                    a1 -= 2
    return f'Total land perimeter: {a1}'


# ryan = land_perimeter(['XOOO',
#                        'XOXO',
#                        'XOXO',
#                        'OOXX',
#                        'OOOO'])


# def solve(a):
#     # Even odd disparity
#     return sum(0 if not type(i) == int else ([1, -1][i % 2]) for i in a)


# ryan = solve([0, 15, 'z', 16, 'm', 13, 14, 'c', 9, 10, 13, 'u', 4, 3])


def count_vegetables(s):
    # Help Suzuki count his vegetables....
    s = Counter(s.split())
    veg = ["cabbage", "carrot", "celery", "cucumber", "mushroom", "onion", "pepper", "potato", "tofu", "turnip"]
    arr = [(s[i], i) for i in s if i in veg]
    return sorted(arr, key=lambda x: (x[0], x[1]), reverse=True)


# s = 'potato tofu cucumber cabbage turnip pepper onion carrot celery mushroom potato tofu cucumber cabbage'
# lst1 = [(2, 'tofu'),
#         (2, 'potato'),
#         (2, 'cucumber'),
#         (2, 'cabbage'),
#         (1, 'turnip'),
#         (1, 'pepper'),
#         (1, 'onion'),
#         (1, 'mushroom'),
#         (1, 'celery'),
#         (1, 'carrot')]
#
# ryan = count_vegetables(s)


def index_of(head, value):
    # Fun with lists: indexOf
    index = 0
    while head:
        if head.data == value:
            return index
        head = head.next
        index += 1
    return -1


# n3 = Node(3)
# n2 = Node(2, n3)
# n1 = Node(1, n2)
# ryan = index_of(n1, 2)


# def compare(a, b):
#     # Compare 2 digit numbers
#     sa, sb = set(str(a)), set(str(b))
#     c= sa & sb
#     return "100%" if sa == sb else f"{len(c)*50}%"


# ryan = compare(34, 43)


def crashing_weights(weights):
    # Crashing Boxes
    for i in range(len(weights) - 1):
        l1, l2 = weights[i], weights[i + 1]
        weights[i + 1] = [i2 if i1 <= i2 else i1 + i2 for i1, i2 in zip(l1, l2)]
    return weights[-1]


example2 = [[1, 3, 3, 2, 2],
            [2, 2, 2, 2, 1],
            [4, 2, 6, 2, 1]]


# ryan = crashing_weights(example2)


# class iam:
#     def __init__(self, desc=None, c=0):
#         self.count = c
#         if desc:
#             self.desc = desc
#         else:
#             self.count += 1
#             self.desc = ""
#
#     def __call__(self, *args):
#         if len(args) == 0:
#             return iam(args[1:], c=self.count)
#         else:
#             self.desc = args[0]
#             return self.__repr__()
#
#     def __repr__(self):
#         return 'I am ' + "very "*self.count + self.desc


# ryan = iam("happy")
# print(ryan, type(ryan))
# # I am happy should equal 'I am happy'
# # 类型不匹配
#
# ryan1 = iam()()("tall")
# print(ryan1)
# print(ryan, type(ryan1))

def iam(desc=None, c=0):
    # I am very very very....
    if desc:
        return 'I am ' + "very " * c + desc
    else:
        return lambda a=desc, t=c + 1: iam(a, t)


# ryan = iam("happy")
# print(ryan, type(ryan))

# ryan1 = iam()()("tall")
# print(ryan, type(ryan1))


def narcissistic(value):
    # Does my number look big in this?
    return value == sum(int(i) ** len(str(value)) for i in str(value))


# ryan = narcissistic(371)


def ka_co_ka_de_ka_me(word):
    rtn_str = "ka"
    vowel = "aeiouAEIOU"
    letter_array = []
    for k, v in enumerate(word):
        letter_array.append(v)
        if len(letter_array) > 1:
            x, y = letter_array[0], letter_array[1]
            if y in vowel:
                rtn_str = f'{rtn_str}{x}'
            else:
                if x in vowel:
                    rtn_str = f'{rtn_str}{x}ka'
                else:
                    rtn_str = f'{rtn_str}{x}'
            letter_array.pop(0)
    else:
        rtn_str = f'{rtn_str}{"".join(letter_array)}'
    return rtn_str


# ryan = ka_co_ka_de_ka_me("maintenance")


def camel_case(s):
    # CamelCase Method
    return ''.join(i.capitalize() for i in s.split())


# ryan = camel_case("camel case method")


def as_sum_of_powers_of_3(n):
    # Expressing Integers as Sum of Powers of Three
    # 效率很低，当 n = -5679822 的时候超时
    init_num = 0
    if n == 0:
        return "0"
    method = "+-0"
    checked_num = set()
    to_check_list = {"0": init_num}

    while True:
        k, v = to_check_list.popitem()
        if v == n:
            return k[1:]
        if 3 ** (len(k)-2) > abs(n):
            continue
        if k in checked_num:
            continue
        checked_num.add(k)
        for m in method:
            if m == "+":
                to_check_list[f'{k}{m}'] = v + 3 ** (len(k)-1)
                # print("add:", f'{k}{m}', v + 3 ** (len(k)-1))
            elif m == "-":
                to_check_list[f'{k}{m}'] = v - 3 ** (len(k)-1)
                # print("add:", f'{k}{m}', v - 3 ** (len(k)-1))
            elif m == "0":
                to_check_list[f'{k}{m}'] = v
                # print("add:", f'{k}{m}', v)
    return False


# ryan = as_sum_of_powers_of_3(194959106)
# print(ryan)



# from itertools import product
# def as_sum_of_powers_of_3(n):
#     # 效率还是不够快
#     powerof3 = [3**i for i in range(20)]
#     print(powerof3)
#     iloc = int(abs(n) ** (1/3))+2
#     possible = product("+-0", repeat=iloc)
#     for i in possible:
#         # print(i)
#         temp = 0
#         for k, v in enumerate(i):
#             if v == '+':
#                 temp += 3 ** k
#             elif v == '-':
#                 temp -= 3 ** k
#         print(i, temp)
#         if temp == n:
#             return ''.join(i)


def as_sum_of_powers_of_3_2(n):
    # 最简练的方式，神奇，原来就是三进制
    if not n: return '0'
    r = ''
    while n != 0:
        k = n % 3
        r += '0+-'[k]
        if k == 2: k = -1
        n = (n - k) // 3
    return r


# ryan = as_sum_of_powers_of_3(-8)


def shorterest_time(n, m, speeds):
    # Simple Fun #326a: The Shorterest Time
    purewalk = n * speeds[3]
    pureelev = (abs(n-m) + n) * speeds[0] + 2 * speeds[1] + speeds[2]
    combined = speeds[3]*abs(n-m) + speeds[0] * m + speeds[1] * 2 + speeds[2]
    return min(pureelev, purewalk, combined)


# ryan = shorterest_time(4, 4, [1, 2, 3, 10])


def keep_order(ary, val):
    # Keep the Order
    lst = bisect.bisect_left(ary, val)
    return lst


# ryan = keep_order([1, 2, 3, 4, 7], 5)


def give_change(amount):
    # You Got Change?
    lst = [100, 50, 20, 10, 5, 1]
    rtn = []
    for k, v in enumerate(lst):
        d, m = amount.__divmod__(v)
        rtn.append(d)
        amount -= lst[k] * d
    return tuple(rtn[::-1])


# ryan = give_change(365)


def consecutive(arr):
    # How many consecutive numbers are needed?
    return abs(max(arr) - min(arr)) + 1 - len(arr) if arr else 0


# ryan = consecutive([-10, 10])

def initials(name):
    # C.Wars
    return '.'.join(i[0].upper() for i in name.split()[:-1]) + '.' + name.split()[-1].capitalize()

# ryan = initials('Barack hussein obama')


def ipv4_address(address):
    # Regexp Basics - is it IPv4 address?
    pattern = r'^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$'
    if re.match(pattern, address):
        octets = address.split('.')
        if all(0 <= int(octet) <= 255 and len(octet) == len(str(int(octet))) for octet in octets):
            return True
    return False


# ryan = ipv4_address("\n127.0.0.1")

def number2words(num):
    # Write out numbers
    ones = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    tens = ['', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
    teens = ['eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen','ten']
    thousands = ['', 'thousand', 'million']

    if num == 0:
        return 'zero'

    words = ''
    num_str = str(num).zfill(6)
    num_chunks = [num_str[i:i+3] for i in range(0, 6, 3)][::-1]

    for i, chunk in enumerate(num_chunks):
        chunk_words = ''
        if chunk[0] != '0':
            chunk_words += ones[int(chunk[0])] + ' hundred '
        if chunk[1:] != '00':
            if chunk[1] == '0':
                chunk_words += ones[int(chunk[2])]
            elif chunk[1] == '1':
                chunk_words += teens[int(chunk[2])-1]
            else:
                chunk_words += tens[int(chunk[1])]
                chunk2 = ones[int(chunk[2])]
                if chunk2 != '':
                    chunk_words += '-' +chunk2
        if chunk_words != '':
            chunk_words += ' ' + thousands[i]
        words = chunk_words + ' ' + words

    return words.strip()


# r = number2words(11)


def find_children(dancing_brigade):
    # Where is my parent!?(cry)
    rtn_str = ""
    for letter in string.ascii_uppercase:
        if letter in dancing_brigade:
            rtn_str += letter
        rtn_str += letter.lower() * dancing_brigade.count(letter.lower())
    return rtn_str


# r = find_children("AaaaaZazzz")


def find_subarray_with_same_element(a, k):
    # Simple Fun #208: Find Sub Array With Same Element
    # 用正则可能更快
    str_a = ''.join(str(i) for i in a)
    k = str(k)
    m = re.finditer(r'{}+'.format(k), str_a)
    max_len, start, end = 0, 0, 0
    for i in m:
        s, e = i.span()
        if e - s >= max_len:
            end = e
            start = s + 1
            max_len = end - start + 1
    return start - 1, end - 1


# r = find_subarray_with_same_element([2, 1, 1, 1, 1, 3, 3, 4, 5, 1, 1, 1, 1], 1)


# def length_of_railway(sounds):
#     # Happy Coding : a Spy On the Train
#     wu = re.finditer("呜呜呜", sounds)
#     kuang_period = []
#     rtn_meter = 0
#     for i in wu:
#         _, e = i.span()
#         kuang_period.append(e)
#     if sounds.count("呜呜呜") == 0:
#         rtn_meter += 10 * sounds.count("哐当")
#     else:
#         loc = sounds.find("呜呜呜")
#         rtn_meter += 10 * sounds[:loc].count("哐当")
#     for k, v in enumerate(kuang_period):
#         dang = sounds[v:].count("哐当")
#         if k == 0:
#             rtn_meter += 20 * dang
#         elif k % 2 == 1:
#             rtn_meter -= 10 * dang
#         elif k % 2 == 0:
#             rtn_meter += 10 * dang
#         print(rtn_meter)
#     return rtn_meter

def length_of_railway(sounds):
    if "呜呜呜" not in sounds:
        return sounds.count("哐当")*10
    lst = sounds.split("呜呜呜")
    meter = 0
    for i, v in enumerate(lst):
        if i % 2 == 1:
            meter += v.count("哐当")*20
        else:
            meter += v.count("哐当")*10
    return meter


# r = length_of_railway('呜呜呜哐当哐当哐当哐当哐当呜呜呜哐当哐当哐当哐当哐当')


def rotate(str_):
    # All Star Code Challenge #15
    rtn_arr = []
    for i in range(len(str_)):
        rtn_arr.append(f"{str_[i+1:]}{str_[:i+1]}")
    return rtn_arr


r = rotate(" ")

print(r)



