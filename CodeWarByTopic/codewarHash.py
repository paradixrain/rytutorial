# coding=utf-8


state_capitals = [{'state': 'Maine', 'capital': 'Augusta'}]


def capital(capitals):
    return [f"The capital of {i.get('state') or i.get('country')} is {i['capital']}" for i in capitals]


def convert_hash_to_array(h):
    return [[k, v] for k, v in sorted(h.items(), key=lambda x: x[0])]


def player_manager(players):
    if players is None:
        return []
    p = players.split(',')
    return [{"player": p[i].strip(), "contact": int(p[i + 1])} for i in range(0, len(p) - 1, 2)]


def populate_dict(keys, default):
    return {k: default for k in keys}


def my_hash_map1(list_of_strings):
    rtn_hashmap = {}
    for s in list_of_strings:
        if rtn_hashmap.get(sum(ord(c) for c in s)) is None:
            rtn_hashmap[sum(ord(c) for c in s)] = [s]
        else:
            rtn_hashmap[sum(ord(c) for c in s)].append(s)
    return rtn_hashmap


def my_hash_map(lst):
    # 使用defaultdict来实现，会更快
    from collections import defaultdict
    dct = defaultdict(list)
    for w in lst: dct[sum(map(ord, w))].append(w)
    return dct


def unscramble1(scramble):
    from itertools import permutations
    global word_list  # It's unnecessary but to remember you that you were provided with a
    word = [''.join(p) for p in permutations(scramble)]
    return [w for w in word if w in word_list]  # 这个性能有点低


def unscramble(scramble):
    # 如何提高速度是这题的关键
    return [i for i in word_list if sorted(i) == sorted(scramble)]


# def code_for_same_protein(seq1,seq2):
#     return all(codons[seq1[c:c + 3]] == codons[seq2[c:c + 3]] for c in range(0, len(seq1), 3))


def closest_pair_tonum(upper_lim):
    z_max = int((2 * upper_lim) ** 0.5)
    square_list = [i ** 2 for i in range(1, z_max + 1)]
    for m in range(upper_lim - 1, 1, -1):
        for n in range(m - 1, 0, -1):
            if m + n in square_list and m - n in square_list:
                return m, n


def colour_association(arr):
    return [{i[0]: i[1]} for i in arr]


objA = { 'a': 10, 'b': 20, 'c': 30 }
objB = { 'a': 3, 'c': 6, 'd': 3 }
objC = { 'a': 5, 'd': 11, 'e': 8 }
objD = { 'c': 3 }


def combine1(*obj_input):
    # 可以用counter来进行合并，合并后的相同key值的value值相加，这个超牛B
    from collections import Counter
    return sum((Counter(a) for a in obj_input), Counter())


def combine(*obj_input):
    obj_rtn = {}
    for i in obj_input:
        for k, v in i.items():
            obj_rtn[k] = v + obj_rtn.get(k, 0)
    return obj_rtn


def naughty_or_nice(data):
    Nice, Naughty = 0, 0
    for m, mv in data.items():
        Nice += sum(1 for d, v in mv.items() if v == 'Nice')
        Naughty += sum(1 for d, v in mv.items() if v == 'Naughty')
    return "Nice!" if Nice >= Naughty else "Naughty!"


ryan = naughty_or_nice({'January': {'1': 'Nice', '2': 'Nice', '3': 'Nice', '4': 'Naughty', '5': 'Nice', '6': 'Naughty', '7': 'Nice', '8': 'Naughty', '9': 'Nice', '10': 'Nice', '11': 'Nice', '12': 'Naughty', '13': 'Nice', '14': 'Nice', '15': 'Naughty', '16': 'Naughty', '17': 'Naughty', '18': 'Naughty', '19': 'Nice', '20': 'Nice', '21': 'Nice', '22': 'Nice', '23': 'Nice', '24': 'Nice', '25': 'Nice', '26': 'Nice', '27': 'Naughty', '28': 'Naughty', '29': 'Nice', '30': 'Nice', '31': 'Naughty'}, 'February': {'1': 'Nice', '2': 'Naughty', '3': 'Nice', '4': 'Naughty', '5': 'Nice', '6': 'Nice', '7': 'Nice', '8': 'Naughty', '9': 'Naughty', '10': 'Naughty', '11': 'Nice', '12': 'Nice', '13': 'Nice', '14': 'Nice', '15': 'Nice', '16': 'Naughty', '17': 'Nice', '18': 'Naughty', '19': 'Nice', '20': 'Nice', '21': 'Nice', '22': 'Naughty', '23': 'Naughty', '24': 'Nice', '25': 'Naughty', '26': 'Nice', '27': 'Nice', '28': 'Naughty'}, 'March': {'1': 'Nice', '2': 'Nice', '3': 'Nice', '4': 'Naughty', '5': 'Nice', '6': 'Naughty', '7': 'Naughty', '8': 'Nice', '9': 'Nice', '10': 'Naughty', '11': 'Nice', '12': 'Nice', '13': 'Nice', '14': 'Naughty', '15': 'Naughty', '16': 'Naughty', '17': 'Naughty', '18': 'Nice', '19': 'Naughty', '20': 'Naughty', '21': 'Nice', '22': 'Nice', '23': 'Nice', '24': 'Nice', '25': 'Nice', '26': 'Nice', '27': 'Naughty', '28': 'Nice', '29': 'Nice', '30': 'Naughty', '31': 'Naughty'}, 'April': {'1': 'Nice', '2': 'Nice', '3': 'Nice', '4': 'Naughty', '5': 'Naughty', '6': 'Nice', '7': 'Nice', '8': 'Naughty', '9': 'Naughty', '10': 'Naughty', '11': 'Nice', '12': 'Nice', '13': 'Nice', '14': 'Nice', '15': 'Nice', '16': 'Naughty', '17': 'Naughty', '18': 'Naughty', '19': 'Nice', '20': 'Nice', '21': 'Nice', '22': 'Naughty', '23': 'Nice', '24': 'Naughty', '25': 'Naughty', '26': 'Nice', '27': 'Nice', '28': 'Naughty', '29': 'Naughty', '30': 'Nice'}, 'May': {'1': 'Nice', '2': 'Naughty', '3': 'Nice', '4': 'Nice', '5': 'Nice', '6': 'Nice', '7': 'Nice', '8': 'Nice', '9': 'Nice', '10': 'Nice', '11': 'Naughty', '12': 'Nice', '13': 'Naughty', '14': 'Naughty', '15': 'Naughty', '16': 'Naughty', '17': 'Naughty', '18': 'Nice', '19': 'Nice', '20': 'Nice', '21': 'Nice', '22': 'Naughty', '23': 'Naughty', '24': 'Naughty', '25': 'Naughty', '26': 'Nice', '27': 'Naughty', '28': 'Naughty', '29': 'Nice', '30': 'Nice', '31': 'Naughty'}, 'June': {'1': 'Nice', '2': 'Nice', '3': 'Nice', '4': 'Naughty', '5': 'Nice', '6': 'Nice', '7': 'Naughty', '8': 'Nice', '9': 'Nice', '10': 'Nice', '11': 'Nice', '12': 'Naughty', '13': 'Naughty', '14': 'Nice', '15': 'Naughty', '16': 'Nice', '17': 'Nice', '18': 'Nice', '19': 'Nice', '20': 'Naughty', '21': 'Nice', '22': 'Naughty', '23': 'Nice', '24': 'Naughty', '25': 'Naughty', '26': 'Naughty', '27': 'Nice', '28': 'Nice', '29': 'Naughty', '30': 'Naughty'}, 'July': {'1': 'Nice', '2': 'Nice', '3': 'Nice', '4': 'Naughty', '5': 'Naughty', '6': 'Naughty', '7': 'Naughty', '8': 'Naughty', '9': 'Naughty', '10': 'Naughty', '11': 'Naughty', '12': 'Naughty', '13': 'Naughty', '14': 'Naughty', '15': 'Nice', '16': 'Nice', '17': 'Naughty', '18': 'Nice', '19': 'Nice', '20': 'Nice', '21': 'Nice', '22': 'Naughty', '23': 'Nice', '24': 'Naughty', '25': 'Nice', '26': 'Naughty', '27': 'Nice', '28': 'Nice', '29': 'Nice', '30': 'Nice', '31': 'Naughty'}, 'August': {'1': 'Naughty', '2': 'Naughty', '3': 'Naughty', '4': 'Naughty', '5': 'Naughty', '6': 'Nice', '7': 'Nice', '8': 'Naughty', '9': 'Nice', '10': 'Naughty', '11': 'Naughty', '12': 'Naughty', '13': 'Nice', '14': 'Naughty', '15': 'Naughty', '16': 'Nice', '17': 'Naughty', '18': 'Nice', '19': 'Nice', '20': 'Nice', '21': 'Nice', '22': 'Nice', '23': 'Naughty', '24': 'Nice', '25': 'Nice', '26': 'Naughty', '27': 'Nice', '28': 'Naughty', '29': 'Nice', '30': 'Nice', '31': 'Nice'}, 'September': {'1': 'Naughty', '2': 'Naughty', '3': 'Nice', '4': 'Naughty', '5': 'Naughty', '6': 'Naughty', '7': 'Naughty', '8': 'Naughty', '9': 'Naughty', '10': 'Naughty', '11': 'Nice', '12': 'Naughty', '13': 'Naughty', '14': 'Nice', '15': 'Naughty', '16': 'Naughty', '17': 'Nice', '18': 'Nice', '19': 'Naughty', '20': 'Naughty', '21': 'Nice', '22': 'Naughty', '23': 'Naughty', '24': 'Naughty', '25': 'Naughty', '26': 'Naughty', '27': 'Naughty', '28': 'Nice', '29': 'Nice', '30': 'Nice'}, 'October': {'1': 'Naughty', '2': 'Naughty', '3': 'Nice', '4': 'Naughty', '5': 'Nice', '6': 'Nice', '7': 'Nice', '8': 'Nice', '9': 'Nice', '10': 'Naughty', '11': 'Nice', '12': 'Nice', '13': 'Nice', '14': 'Naughty', '15': 'Naughty', '16': 'Nice', '17': 'Nice', '18': 'Nice', '19': 'Naughty', '20': 'Naughty', '21': 'Nice', '22': 'Nice', '23': 'Naughty', '24': 'Nice', '25': 'Nice', '26': 'Naughty', '27': 'Naughty', '28': 'Nice', '29': 'Nice', '30': 'Naughty', '31': 'Naughty'}, 'November': {'1': 'Nice', '2': 'Nice', '3': 'Nice', '4': 'Nice', '5': 'Nice', '6': 'Nice', '7': 'Nice', '8': 'Nice', '9': 'Nice', '10': 'Naughty', '11': 'Nice', '12': 'Nice', '13': 'Naughty', '14': 'Nice', '15': 'Nice', '16': 'Naughty', '17': 'Naughty', '18': 'Nice', '19': 'Nice', '20': 'Naughty', '21': 'Nice', '22': 'Naughty', '23': 'Nice', '24': 'Naughty', '25': 'Naughty', '26': 'Naughty', '27': 'Naughty', '28': 'Nice', '29': 'Naughty', '30': 'Naughty'}, 'December': {'1': 'Nice', '2': 'Nice', '3': 'Nice', '4': 'Naughty', '5': 'Naughty', '6': 'Naughty', '7': 'Nice', '8': 'Naughty', '9': 'Naughty', '10': 'Naughty', '11': 'Naughty', '12': 'Nice', '13': 'Naughty', '14': 'Naughty', '15': 'Naughty', '16': 'Nice', '17': 'Nice', '18': 'Nice', '19': 'Naughty', '20': 'Nice', '21': 'Nice', '22': 'Nice', '23': 'Naughty', '24': 'Nice', '25': 'Naughty', '26': 'Naughty', '27': 'Naughty', '28': 'Naughty', '29': 'Nice', '30': 'Naughty', '31': 'Naughty'}})
print(ryan)
