# coding = utf-8

# @title: fundermental
# @desc:  codewar
# @author: RyanLin
# @date: 2022/01/20

def find_it(seq):
    for i in seq:
        if seq.count(i) % 2 == 1:
            return i

def spin_words(sentence):
    l = []
    for i in sentence.split():
        if len(i)>4:
            l.append(i[::-1])
        else:
            l.append(i)
    return ' '.join(l)

def spin_words2(sentence):
    return " ".join([x[::-1] if len(x) >= 5 else x for x in sentence.split(" ")])


def digital_root(n):
    while n>9:
        n = sum(int(i) for i in str(n))
    return n


def array_diff(a, b):
    for bn in b:
        ac = a.count(bn)
        for i in range(ac):
            a.remove(bn)
    return a


a = array_diff([1,2,1,2,3,1,4], [1])
print(a)