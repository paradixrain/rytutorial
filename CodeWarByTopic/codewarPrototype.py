# coding=utf-8

class Warrior:
    def __init__(self, name):
        self.name = name
        self.health = 100

    def strike(self, enemy, swings):
        # health cannot go below zero
        enemy.health = max(0, enemy.health - (swings * 10))


def lstzip(a, b, fn):
    return [fn(x, y) for x, y in zip(a, b)]


def move(self, direction):
    x, y = int(self.position[0]), int(self.position[1])
    if direction == 'up':
        x -= 1
    elif direction == 'down':
        x += 1
    elif direction == 'left':
        y -= 1
    elif direction == 'right':
        y += 1

    if x < 0 or x > 4 or y < 0 or y > 4:
        raise ValueError('Invalid move')
    self.position = str(x) + str(y)
    return self.position

# Hero.move = move
