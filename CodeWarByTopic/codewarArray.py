# coding = utf-8
import re
from itertools import zip_longest


# from codewarMathematics import is_prime


def multiplication_table(size):
    # Multiplication table
    return [[i * j for j in range(1, size + 1)] for i in range(1, size + 1)]


def up_array(arr):
    # +1 Array
    if any(i < 0 or i > 9 for i in arr) or len(arr) == 0:
        return None
    arr[-1] += 1
    for i in range(len(arr) - 1, 0, -1):
        if arr[i] == 10:
            arr[i] = 0
            arr[i - 1] += 1

    if arr[0] == 10:
        arr = [1, 0] + arr[1:]
    return arr


def queue_time(customers, n):
    # The Supermarket Queue
    arr = [[0] for _ in range(n)]
    for i in customers:
        min_num = min(arr[k][0] for k in range(n))
        for j in range(n):
            if arr[j][0] == min_num:
                arr[j][0] += i
                arr[j].append(i)
                break
    return max(arr[i][0] for i in range(n))


def queue_time1(customers, n):
    l = [0] * n
    for i in customers:
        l[l.index(min(l))] += i
    return max(l)


fighters = [
    ["Ryu", "E.Honda", "Blanka", "Guile", "Balrog", "Vega"],
    ["Ken", "Chun Li", "Zangief", "Dhalsim", "Sagat", "M.Bison"]
]
moves = ["up", "left", "down", "right"] * 2


def street_fighter_selection(f, initial_position, m):
    # Street Fighter 2 - Character Selection
    rtn_arr = []
    (x, y) = initial_position
    for i in m:
        if i == "up":
            x = max(0, x - 1)
        if i == "down":
            x = min(1, x + 1)
        if i == "left":
            y = 5 if y == 0 else y - 1
        if i == "right":
            y = 0 if y == 5 else y + 1
        rtn_arr.append(f[x][y])
    return rtn_arr


def reverse_alternate(s):
    # Reverse every other word in the string
    arr = [i for i in s.split()]
    for i in range(len(arr)):
        if i % 2 == 1:
            arr[i] = arr[i][::-1]
    return ' '.join(arr)


def Xbonacci(signature, n):
    # Fibonacci, Tribonacci and friends
    arr = [i for i in signature][:n]
    l = len(signature)
    for i in range(l, n):
        arr.append(sum(arr[i - l:]))
    return arr


def partlist(arr):
    # Parts of a list
    return [(' '.join(arr[:i + 1]), ' '.join(arr[i + 1:])) for i in range(len(arr) - 1)]


def two_sum(numbers, target):
    # Two Sum
    for i in range(len(numbers) - 1):
        tn = target - numbers[i]
        if tn in numbers[i + 1:]:
            tl = numbers[i + 1:].index(tn)
            return [i, i + tl + 1]


def tribonacci(signature, n):
    # Tribonacci Sequence
    rtn = []
    for i in range(n):
        if i > len(signature) - 1:
            signature.append(sum(signature[-3:]))
        rtn.append(signature[i])
    return rtn


def stanton_measure(arr):
    # Stanton measure
    return arr.count(arr.count(1))


def gimme(input_array):
    # Find the middle element
    return input_array.index(sorted(input_array)[1])


def number_of_occurrences(element, sample):
    # Number Of Occurrences
    return sample.count(element)


def highest_rank(arr):
    # Highest Rank Number in an Array
    rtn_max = arr[0]
    for i in set(arr):
        if arr.count(i) > arr.count(rtn_max) or (arr.count(i) == arr.count(rtn_max) and i > rtn_max):
            rtn_max = i
    return rtn_max


def flatten(lst):
    # Flatten
    rtn_arr = []
    for i in lst[:]:
        if type(i) == list:
            for j in i:
                rtn_arr.append(j)
        else:
            rtn_arr.append(i)
    return rtn_arr


def check_three_and_two(array):
    # Check three and two
    a = set(array)
    if len(a) == 2:
        aa = a.pop()
        if array.count(aa) == 2 or array.count(aa) == 3:
            return True
    return False


def even_last(numbers):
    # Evens times last
    return sum(v for k, v in enumerate(numbers) if k % 2 == 0) * (0 if len(numbers) < 1 else numbers[-1])


def bingo(array):
    # Bingo ( Or Not )
    return 'WIN' if {2, 7, 9, 14, 15} <= set(array) else 'LOSE'


dictionary = ['abnormal',
              'arm-wrestling',
              'absolute',
              'airplane',
              'airport',
              'amazing',
              'apple',
              'ball']


def autocomplete(input_, dictionary):
    # Autocomplete! Yay!
    input_ = ''.join(re.findall(r'[a-zA-Z]', input_))
    print(input_, dictionary)
    return [i for i in dictionary if ''.join(re.findall(r'[a-zA-Z]', i)).lower().startswith(input_)][:5]


def arithmetic_sequence_elements(a, d, n):
    # Arithmetic progression
    return ', '.join([str(a + d * i) for i in range(n)])


def deep_count(a, count=0):
    # Array Deep Count
    return len(a) + sum(deep_count(i) for i in a if isinstance(i, list))


# a = deep_count([1, 2, [3, 4, [5]]])
# print(a)


def solve(arr):
    # Max-min arrays
    arr = sorted(arr)
    rtn_arr = []
    flag = True
    while arr:
        if flag:
            rtn_arr.append(arr.pop())
            flag = not flag
        else:
            rtn_arr.append(arr.pop(0))
            flag = not flag
    return rtn_arr


# a = solve([52, 77, 72, 44, 74, 76, 40])
# [77,40,76,44,74,52,72])


def small_enough(array, limit):
    # Small enough? - Beginner
    return all(i <= limit for i in array)


# a = small_enough([66, 101], 200)


def two_oldest_ages(ages):
    # Two Oldest Ages
    return sorted(ages)[-2:]


# a = two_oldest_ages([1, 5, 87, 45, 8, 8])


def fizzbuzz(n):
    # Fizz Buzz
    return ['FizzBuzz' if i % 15 == 0 else 'Buzz' if i % 5 == 0 else 'Fizz' if i % 3 == 0 else i for i in
            range(1, n + 1)]


# a = fizzbuzz(10)


def find_longest(arr):
    # Most digits
    return sorted(arr, key=lambda x: len(str(x)), reverse=True)[0]
    # return max(xs, key=lambda x: len(str(x)))  # 用max的key 也可以


# def minimum_number(numbers):
#     # Transform To Prime
#     n = 0
#     while not is_prime(sum(numbers) + n):
#         n += 1
#     return n


# ryan = minimum_number([50, 39, 49, 6, 17, 28])


def largest(n, xs):
    # Largest Elements
    return sorted(xs)[len(xs) - n:]


# ryan = largest(2, [7,6,5,4,3,2,1])


def killer(suspect_info, dead):
    # Who is the killer?
    for k, v in suspect_info.items():
        if all(d in v for d in dead):
            return k


# ryan = killer({'James': ['Jacob', 'Bill', 'Lucas'], 'Johnny': ['David', 'Kyle', 'Lucas'], 'Peter': ['Lucy', 'Kyle']}, ['Lucas', 'Bill'])


def duplicates(array):
    # Find Duplicates
    return [v for k, v in enumerate(array) if array[:k].count(v) == 1]


# ryan = duplicates([1, 2, 4, 4, 3, 3, 1, 5, 3, '5'])


def sortme(words):
    # Sort Arrays (Ignoring Case)
    return sorted(words, key=str.lower)


# ryan = sortme(["Hello", "there", "I'm", "fine"])


def yahtzee_upper(dice):
    # Yahtzee upper section scoring
    d = dict()
    for k in set(dice):
        d[k] = dice.count(k) * k
    return max(d.values())


# ryan = yahtzee_upper([1654, 1654, 50995, 30864, 1654, 50995, 22747,
#                       1654, 1654, 1654, 1654, 1654, 30864, 4868, 1654, 4868, 1654,
#                       30864, 4868, 30864])


# def solve(arr):
#     # Dominant array elements
#     return [v for k, v in enumerate(arr[:-1]) if v > max(arr[k+1:])] + [arr[-1]]


# ryan = solve([16,17,14,3,14,5,2])


def delete_nth(order, max_e):
    # Delete occurrences of an element if it occurs more than n times
    return [v for k, v in enumerate(order) if order[:k].count(v) < max_e]


# ryan = delete_nth([1,1,3,3,7,2,2,2,2], 3)


def find_end(lst, index):
    count = 1
    for i in range(index + 1, len(lst)):
        if lst[i] == lst[index]:
            count += 1
        else:
            break
    return count


# ryan = find_end([4, 4, 1, 1, 1], 2)

def set_reducer(inp):
    # Set Reducer
    while len(inp) > 1:
        loc = 0
        temp = []
        while loc < len(inp):
            temp_loc = find_end(inp, loc)
            temp.append(temp_loc)
            loc = loc + temp_loc
        inp = temp
    return inp[0]


# from itertools import groupby
#
# def set_reducer(inp):
#     while len(inp) > 1:
#         inp = [len(list(b)) for a, b in groupby(inp)]
#     return inp[0]


# ryan = set_reducer([8, 1, 6, 1, 2, 7, 7, 7, 7, 6, 5, 3, 2, 1, 8])


def is_int_array(arr):
    # Is Integer Array?
    return type(arr) == list and ((len(arr) == 0) or (all(type(i) in (int, float) and i == int(i) for i in arr)))


# ryan = is_int_array([1.0, 2.0, 3.0001])


# def bingo(ticket,win):
#     # Lottery Ticket
#     mini_win = 0
#     for i in ticket:
#         if chr(i[1]) in i[0]:
#             mini_win += 1
#     return 'Winner!' if mini_win >= win else 'Loser!'


# ryan = bingo([['ABC', 65], ['HGR', 74], ['BYHT', 74]], 1)


def is_monotone(heights):
    # Monotone travel
    if len(heights) < 2:
        return True
    for i, j in zip(heights[:-1], heights[1:]):
        if i > j:
            return False
    return True


# ryan = is_monotone([5,5,5,5,5,5,5])


def diagonal_sum(array):
    # Find sum of top-left to bottom-right diagonals
    return sum(v[k] for k, v in enumerate(array))


# ryan = diagonal_sum([
#     [1, 2, 3],
#     [4, 5, 6],
#     [7, 8, 9]])


def meeting(rooms, need):
    # The Office V - Find a Chair
    if need == 0:
        return "Game On"
    rtn_arr = []
    for i in rooms:
        j = max(i[1] - i[0].count('X'), 0)
        if sum(rtn_arr) + j <= need:
            rtn_arr.append(j)
        elif need < sum(rtn_arr) + j:
            rtn_arr.append(need - sum(rtn_arr))
        if sum(rtn_arr) == need:
            break
    print(rtn_arr)
    if sum(rtn_arr) < need:
        return "Not enough!"
    else:
        return rtn_arr


# ryan = meeting([["XXX", 1], ["XXXXXX", 6], ["X", 2], ["XXXXXX", 8], ["X", 3], ["XXX", 1]], 5)


def create_box(c, r):
    ## The 'spiraling' box
    rtn_arr = []
    for i in range(1, r + 1):
        line = []
        for j in range(1, c + 1):
            line.append(min(i, j, c - j + 1, r - i + 1))
        rtn_arr.append(line)
    return rtn_arr


# ryan = create_box(7, 8)


def complete_series(seq):
    # Complete Series
    if len(seq) == len(set(seq)):
        return list(range(max(seq) + 1))
    else:
        return [0]


def maze_runner(maze, directions):
    # Maze Runner
    len_m = len(maze)
    start_x, start_y = -1, -1
    end_x, end_y = -1, -1
    for i, line in enumerate(maze):
        if line.count(2) > 0:
            start_x = i
            start_y = line.index(2)
        if line.count(3) > 0:
            end_x = i
            end_y = line.index(3)

    for d in directions:
        if d == 'N':
            start_x -= 1
        elif d == 'S':
            start_x += 1
        elif d == 'W':
            start_y -= 1
        else:
            start_y += 1

        if start_x == end_x and start_y == end_y:
            return "Finish"
        if not 0 <= start_x < len_m or not 0 <= start_y < len_m:
            return "Dead"
        if maze[start_x][start_y] == 1:
            return "Dead"

    return "Lost"


# maze = [[1, 1, 1, 1, 1, 1, 1],
#         [1, 0, 0, 0, 0, 0, 3],
#         [1, 0, 1, 0, 1, 0, 1],
#         [0, 0, 1, 0, 0, 0, 1],
#         [1, 0, 1, 0, 1, 0, 1],
#         [1, 0, 0, 0, 0, 0, 1],
#         [1, 2, 1, 0, 1, 0, 1]]
#
#
# ryan = maze_runner(maze,["N","N","N","N","N","E","E","E","E","E"])


def in_array(array1, array2):
    # Which are in?
    rtn_arr = []
    for i in sorted(set(array1)):
        for j in array2:
            if i in j:
                rtn_arr.append(i)
                break
    return rtn_arr


# a1 = ["arp", "mice", "bull"]
# a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
# r = ['arp']
# ryan = in_array(a1, a2)


def sel_reverse(arr, l):
    # Selective Array Reversing
    if not l:
        return arr
    rtn_arr = []
    for i in range(len(arr) // l + 1):
        rtn_arr.extend(arr[l * i:i * l + l][::-1])
    return rtn_arr


# ryan = sel_reverse([2,4,6,8,10,12,14,16], 3)


def cleaned_counts(data):
    # Noisy Cell Counts
    rtn_arr = []
    for k, v in enumerate(data):
        if k == 0 or v >= rtn_arr[k - 1]:
            rtn_arr.append(v)
        else:
            rtn_arr.append(rtn_arr[k - 1])
    return rtn_arr


# ryan = cleaned_counts([5, 5, 6, 5, 5, 5, 5, 6])

def value_check(tree, i):
    if i * 2 + 2 > len(tree) - 1:
        return 2
    if tree[i] == tree[i * 2 + 1] + tree[i * 2 + 2]:
        return 1
    return -1


def find_incorrect_value(tree):
    # Finding the incorrect value in a Binary Tree
    for i in range(len(tree) // 2):
        if value_check(tree, i) != 1:
            if value_check(tree, i * 2 + 2) == 2:
                return i * 2 + 2, tree[i] - tree[i * 2 + 1]
            elif value_check(tree, i * 2 + 1) == 2:
                return i * 2 + 1, tree[i] - tree[i * 2 + 2]
            elif value_check(tree, i * 2 + 2) == -1:
                return i * 2 + 2, tree[i] - tree[i * 2 + 1]
            elif value_check(tree, i * 2 + 1) == -1:
                return i * 2 + 1, tree[i] - tree[i * 2 + 2]
            else:
                return i, tree[i * 2 + 1] + tree[i * 2 + 2]
    return 0, 0


# ryan = find_incorrect_value([27, 13, 15, 6, 7, 5, 9])


def move_zeros(lst):
    zeros = lst.count(0)
    lst = [i for i in lst if i]
    lst.extend([0] * zeros)
    return lst


# ryan = move_zeros([1, 2, 0, 1, 0, 1, 0, 3, 0, 1])


def snakes_and_ladders(board, dice):
    # Snakes & Ladders
    loc = 0
    for d in dice:
        backup_loc = loc
        loc += d
        if not 0 <= loc < len(board):
            loc = backup_loc
            continue
        if board[loc] == 0:
            if loc == len(board) - 1:
                return loc
            continue
        else:
            loc += board[loc]
    return loc


dice = [2, 1, 5, 1, 5, 4]
board = [0, 0, 3, 0, 0, 0, 0, -2, 0, 0, 0]


# ryan = snakes_and_ladders(board, dice)


def all_non_consecutive(arr):
    # Find all non-consecutive numbers
    rtn_arr = []
    for k, v in enumerate(arr[:-1]):
        if arr[k + 1] - v != 1:
            rtn_arr.append({'i': k + 1, 'n': arr[k + 1]})
    return rtn_arr


# ryan = all_non_consecutive([1,2,3,4,6,7,8,10])

def add(*args):
    # Decreasing Inputs
    rtn_num = 0
    for k, v in enumerate(args):
        rtn_num += v / (k + 1)
    return round(rtn_num)


# ryan = add(4, -3, -2)


def generate(n):
    # Array with distance of N
    return [0] if n <= 0 else [
        *reversed(range(2, n, 2)),
        n, 0,
        *range(2, n, 2),
        *reversed(range(1, n, 2)),
        n,
        *range(1, n, 2),
    ]


# r = generate(3)

def find_hack(arr):
    # Find Cracker.
    rtn_arr = []
    score_dict = {"A": 30,
                  "B": 20,
                  "C": 10,
                  "D": 5}
    for i in arr:
        s = sum(i[2].count(k) * v for k, v in score_dict.items()) + \
            20 * (((i[2].count("A") + i[2].count("B")) > 4) * (len(i[2]) == (i[2].count("A") + i[2].count("B"))))
        s = min(s, 200)
        if s != i[1]:
            print(i[0], i[1], s)
            rtn_arr.append(i[0])
    return rtn_arr


array = [['Doe Watts', 230, ['B', 'A', 'A', 'A', 'B', 'B']], ['John Bradley', 300, ['B', 'B', 'A', 'B', 'A', 'A', 'A']],
         ['Jack Watts', 180, ['B', 'B', 'A', 'B', 'A']], ['Bill Webb', 90, ['A', 'A', 'A']],
         ['John Webb', 150, ['B', 'B', 'B', 'B']], ['Kabin Bradley', 200, ['B', 'B', 'A', 'A', 'B', 'A', 'A', 'B']],
         ['Jack Lawrence', 100, ['A', 'A', 'B', 'B']], ['John Bradley', 280, ['A', 'A', 'A', 'B', 'B', 'A', 'A', 'B']]]


# r = find_hack(array)
# ["name2", "name4"])

def same_structure_as(original, other):
    # Nesting Structure Comparison
    if type(original) != type(other):
        return False
    for i, j in zip_longest(original, other):
        if type(i) != list and type(j) != list:
            continue
        if type(i) != type(j):
            return False
        if type(i) == list:
            if len(i) != len(j) or not same_structure_as(i, j):
                return False
    return True


# r = same_structure_as([1,[1,1]],[[2,2],2])

def solution(a, b):
    # Mean Square Error
    rtn_num = 0
    for i, j in zip(a, b):
        rtn_num += abs(i - j) ** 2
    return rtn_num / len(a)


# r = solution([10, 20, 10, 2], [10, 25, 5, -2])

def snail(array):
    # Initialize variables
    result = []
    if len(array) == 1 and len(array[0]) == 0:
        return result
    n = len(array)
    top = left = 0
    bottom = right = n - 1
    # Loop through the array
    while top <= bottom and left <= right:
        # Traverse right
        for i in range(left, right + 1):
            result.append(array[top][i])
        top += 1
        # Traverse down
        for i in range(top, bottom + 1):
            result.append(array[i][right])
        right -= 1
        # Traverse left
        if top <= bottom:
            for i in range(right, left - 1, -1):
                result.append(array[bottom][i])
            bottom -= 1
        # Traverse up
        if left <= right:
            for i in range(bottom, top - 1, -1):
                result.append(array[i][left])
            left += 1
    return result


def snail_clever(array):
    # 使用rotate的方式逐个剔除外圈，也很聪明
    out = []
    while len(array):
        out += array.pop(0)
        array = list(zip(*array))[::-1]  # Rotate
    return out


arraya = [[1, 2, 3],
          [4, 5, 6],
          [7, 8, 9]]


# r = snail(arraya)

def pick_peaks(arr):
    # Pick peaks
    pos = []
    peaks = []
    for i in range(1, len(arr) - 1):
        if arr[i] > arr[i - 1]:
            if arr[i] > arr[i + 1]:
                pos.append(i)
                peaks.append(arr[i])
            elif arr[i] == arr[i + 1]:
                for j in range(i, len(arr) - 1):
                    if arr[j + 1] > arr[j]:
                        break
                    elif arr[j + 1] < arr[j]:
                        pos.append(i)
                        peaks.append(arr[i])
                        break

    return {'pos': pos, 'peaks': peaks}


def pick_peaks_better(arr):
    pos = []
    prob_peak = False
    for i in range(1, len(arr)):
        if arr[i] > arr[i - 1]:
            prob_peak = i
        elif arr[i] < arr[i - 1] and prob_peak:
            pos.append(prob_peak)
            prob_peak = False
    return {'pos': pos, 'peaks': [arr[i] for i in pos]}


r = pick_peaks_better([1, 2, 5, 4, 3, 2, 3, 6, 4, 1, 2, 3, 3, 4, 5, 3, 2, 1, 2, 3, 5, 5, 4, 3])

print('ryan ', r)
