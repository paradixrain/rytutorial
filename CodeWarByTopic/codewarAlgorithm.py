# coding=utf-8
import re
# import copy
import string
import math
from collections import Counter
# from collections import OrderedDict
# import itertools
# import time
from copy import deepcopy
from itertools import permutations, product
from itertools import permutations as pmts
from itertools import count
from itertools import zip_longest
from itertools import combinations

# from itertools import product
# from itertools import combinations_with_replacement as cwr

# from codewarMathematics import is_prime
# from math import log

# from functools import lru_cache

s = "QUES"
ranks = sorted(set(["".join(i) for i in permutations(s)]))


def seven(m: int):
    """
    :param m: int
    :return: tuple
    Return on the stack number-of-steps, last-number-m-with-at-most-2-digits
    此方法可以判断一个数字能否被7整除
    """
    step = 0
    while m > 99:
        m = m // 10 - 2 * (m % 10)
        step += 1
    print(m, step)
    if m % 7 == 0:
        return m, step
    return "failed"


def prime_factors(n):
    """
    Given a positive number n > 1 find the prime factor decomposition of n.
    The result will be a string with the following form :
    "(p1**n1)(p2**n2)...(pk**nk)"
    """
    factors = []  # 求素因数
    d = 2
    while d * d <= n:
        while n % d == 0:
            factors.append(d)
            n //= d
        d += 1
    if n > 1:
        factors.append(n)
    return ''.join(f'({i}**{factors.count(i)})' if factors.count(i) > 1 else f'({i})' for i in sorted(set(factors)))


def find_outlier(integers):
    if integers[0] % 2 == integers[1] % 2:
        return next(filter(lambda x: x % 2 != integers[0] % 2, integers))
    else:
        return next(filter(lambda x: x % 2 != integers[2] % 2, integers))


def drop_while(arr, pred):
    i = 0
    while i < len(arr):
        if not pred(arr[i]):
            break
        i += 1
    return arr[i:]


def find_all1(sum_dig, digs):
    # 判断是是否有解
    if digs * 9 < sum_dig or digs > sum_dig:
        return []

    # 找最小数
    gap = sum_dig - digs
    min_arr = [1] * digs
    index = -1
    while gap > 0:
        if gap > 8:
            min_arr[index] = 9
            gap -= 8
        else:
            min_arr[index] += gap
            gap = 0
        index -= 1
    print("min_arr", min_arr)

    # 找最大数
    avg = sum_dig // digs
    max_arr = [avg] * digs
    gap = sum_dig - avg * digs
    index = 1
    while gap > 0:
        if gap == digs - index:  # 刚好是均等分
            for i in range(index, digs):
                max_arr[i] += 1
            gap = 0
        else:  # 剩余的数字不够均等分
            pass
        index += 1
    print("max_arr", max_arr)

    # 找符合条件的个数，嗯，目前没有找到合适的数论方法，ryantodo


def find_all(sum_dig, digs):
    # 暴力搜索
    from itertools import combinations_with_replacement
    combs = combinations_with_replacement(list(range(1, 10)), digs)
    target = [''.join(str(x) for x in list(comb)) for comb in combs if sum(comb) == sum_dig]
    if not target:
        return []
    return [len(target), int(target[0]), int(target[-1])]


def f1(n, k):
    return next(n * m for m in count(1) if all(int(d) < k for d in str(n * m)))


def f2(n, k):
    s = set(map(str, range(0, k)))
    return next(n * m for m in count(1) if set(str(n * m)) == s)


def find_f1_eq_f2(n, k):
    return next(m for m in count(n + 1) if f1(m, k) == f2(m, k))


def poly_add(p1, p2):
    return [x + y for x, y in zip_longest(p1, p2, fillvalue=0)]


def pascal_pyr_layer(n):
    # Pascal's Triangle
    pascal_triangle = []
    for i in range(1, n + 1):
        # every triangle step
        pascal_triangle_now = []
        for j in range(1, i + 1):
            # every line
            cur_line = []
            for k in range(1, j + 1):
                # every column
                if j == 1:
                    # first line
                    cur_line.append(1)
                elif j == i and (k == 1 or k == i):
                    # last line, head and tail
                    cur_line.append(1)
                elif k == 1:
                    # line head
                    cur_value = pascal_triangle[j - 2][k - 1] + pascal_triangle[j - 1][k - 1]
                    cur_line.append(cur_value)
                elif k == j:
                    # line tail
                    cur_value = pascal_triangle[j - 2][k - 2] + pascal_triangle[j - 1][k - 1]
                    cur_line.append(cur_value)
                elif j == i:
                    # last line, not head nor tail
                    cur_value = pascal_triangle[j - 2][k - 2] + pascal_triangle[j - 2][k - 1]
                    cur_line.append(cur_value)
                else:
                    # middle
                    cur_value = pascal_triangle[j - 2][k - 2] + pascal_triangle[j - 2][k - 1] + pascal_triangle[j - 1][
                        k - 1]
                    cur_line.append(cur_value)
            pascal_triangle_now.append(cur_line)
            # print('after loop ', i, pascal_triangle_now)
        pascal_triangle = deepcopy(pascal_triangle_now)
        # print('after loop', i, pascal_triangle)
    return pascal_triangle


def pascal_pyr_layer2(n):
    res = []

    for i in range(n):
        line = []
        for k in range(i + 1):
            if k != 0 and k != i:
                line.append(res[i - 1][k - 1] + res[i - 1][k])
            else:
                line.append(1)
        res.append(line)

    for i in range(n):
        for k in range(len(res[i])):
            res[i][k] = res[i][k] * res[n - 1][i]

    return res


def pyramid(n):
    rtn_arr = []
    for i in range(1, n + 1):
        rtn_arr.append([1] * i)
    return rtn_arr


def rev_rot(strng, sz):
    # Reverse or rotate?
    if sz <= 0 or len(strng) < sz:
        return ""
    rtn_str = ""
    for i in range(0, len(strng) // sz):
        j = strng[i * sz: i * sz + sz]
        if sum(int(k) ** 3 for k in j) % 2 == 0:
            rtn_str += j[::-1]
        else:
            rtn_str += j[1:]
            rtn_str += j[0]
    return rtn_str


# def decompose(n):
#     # Square into Squares. Protect trees!
#     from itertools import combinations
#     square_list = [i ** 2 for i in range(1, n)]
#     match_list = []
#     for i in range(2, n - 2):
#         for j in combinations(square_list, i):
#             if sum(j) == n ** 2:
#                 match_list.append(j)
#             if (n - 1) ** 2 in match_list:
#                 break
#         else:
#             continue
#         break
#
#     if match_list:
#         math_list = sorted(match_list, key=lambda x: x[0], reverse=True)[0]
#         return [i ** 0.5 for i in math_list]
#     return None


def decompose1(n):
    # Square into Squares. Protect trees!
    # 构造完整数组，完整数组的位置可以反推出对应构成 [lay(n)] = [lay(n-1)]+[n**2+lay(n-1)]
    square_list = [0]
    for i in range(1, n):
        for j in range(len(square_list)):
            tmp = i ** 2 + square_list[j]
            square_list.append(tmp)
    print(square_list)
    if n ** 2 in square_list:
        loc = square_list.index(n ** 2)
        # 把loc转成二进制，可以得到其位置
        loc = bin(loc)[::-1]
        print(loc)
        return [i + 1 for i in range(len(loc)) if loc[i] == '1']


def decompose2(n, a=None):
    if a is None: a = n * n
    if a == 0: return []
    for m in range(min(n - 1, int(a ** .5)), 0, -1):
        sub = decompose2(m, a - m * m)
        if sub is not None: return sub + [m]


def shorten_number(suffixes, base):
    # Number Shortening Filter
    def convert(x):
        try:
            i, j = int(x), 0
            while j < len(suffixes):
                if i / base > 1 and j != len(suffixes) - 1:
                    i = i // base
                    j += 1
                else:
                    break
            return f'{i}{suffixes[j]}'
        except:
            return f'{x}'

    return convert


def decrypt(encrypted_text, n):
    # Simple Encryption #1 - Alternating Split
    if not encrypted_text or n < 1 or len(encrypted_text) < 2:
        return encrypted_text
    l = len(encrypted_text)
    for i in range(n):
        rtn_str = []
        for i in range(l):
            if i % 2 == 0:
                rtn_str.append(encrypted_text[l // 2:][i // 2])
            else:
                rtn_str.append(encrypted_text[i // 2])
        encrypted_text = "".join(rtn_str)
    return encrypted_text


def encrypt(text, n):
    if not text or n < 0 or len(text) < 2:
        return text
    for i in range(n):
        text = text[1::2] + text[0::2]
    return text


def find_uniq(arr):
    # Find the unique number
    x = set(arr)
    for i in x:
        if arr.count(i) == 1:
            return i


def unique_in_order(iterable):
    # Unique In Order
    rtn_arr = []
    for i in iterable:
        if not rtn_arr or i != rtn_arr[-1]:
            rtn_arr.append(i)
    return rtn_arr


# ryan = unique_in_order('AAAABBBCCDAABBB')


from math import factorial as f


def factorial(n):
    # factorial
    if n > 12 or n < 0:
        raise ValueError(n)
    return f(n)


# ryan = factorial(-1)


def solution_largest5(digits):
    # Largest 5 digit number in a series
    # 正则表达式的贪心匹配，有意思，以后有空再研究
    # dig_arr = re.findall(r'[0-9]{5}', digits)  # 最初的版本，匹配1234567898765的时候有问题哦
    # 因为，只能匹配出，12345， 和67898，而不能匹配出98765
    dig_arr = [int(digits[i:i + 5]) for i in range(0, len(digits) - 4)]
    return max(dig_arr)


# number = "12345678987651"
# ryan = solution(number)


def isLeapYear(year):
    # Leap Years
    return False if year % 4 != 0 or (year % 100 == 0 and year % 400 != 0) else True


# ryan = isLeapYear(1104)


def find_deleted_number(arr, mixed_arr):
    # Lost number in number sequence
    dif = set(arr) - set(mixed_arr)
    return dif.pop() if len(dif) > 0 else 0


# ryan = find_deleted_number([1,2,3,4,5], [3,4,2,1,5])


def add_letters(*letters):
    # Alphabetical Addition
    ascii_letter = 'z' + string.ascii_letters[:25]
    loc = sum(ascii_letter.index(i) for i in letters) % 26
    return ascii_letter[loc]


# ryan = add_letters('')


def reverse_number(n):
    # Reverse a Number
    if not n:
        return n
    arr_n = [i for i in str(abs(n))][::-1]
    return int(n / abs(n)) * int(''.join(arr_n))


# ryan = reverse_number(-123)


def palindrome_chain_length(n):
    # Palindrome chain length
    step = 0
    while not n == int(str(n)[::-1]):
        step += 1
        n = n + int(str(n)[::-1])
    return step


# ryan = palindrome_chain_length(89)


def make_move(sticks):
    # 21 Sticks
    return sticks % 4


# ryan = make_move(3)


def fibonacci(n: int) -> int:
    # Fibonacci
    x, y = 0, 1
    for i in range(n):
        x, y = y, y + x
    return x


# ryan = fibonacci(34)


def nth_fib(n):
    # N-th Fibonacci
    x, y, c = 0, 1, 1
    while c < n:
        x, y = y, y + x
        c += 1
    return x


# ryan = nth_fib(5)


def max_rot(n):
    # Rotate for a Max
    n = str(n)
    l = [n]
    for i in range(0, len(n) - 1):
        n = n[:i] + n[i + 1:] + n[i]
        l.append(n)
    return int(max(l))


# ryan = max_rot(38458215)


def hamming(a, b):
    # Hamming Distance
    return sum(1 for i, j in zip(a, b) if i != j)


# ryan = hamming("hello world","hello tokyo")


def permute_a_palindrome(input):
    # Permute a Palindrome
    flag = 0
    for i in set(input):
        if input.count(i) % 2 == 1:
            flag += 1
    return flag < 2


# ryan = permute_a_palindrome("abcb")


def validate_sudoku(board):
    # Sudoku board validator
    num_set = {1, 2, 3, 4, 5, 6, 7, 8, 9}
    for k, v in enumerate(board):
        if set(v) != num_set:
            return False

    transposed = list(map(list, zip(*board)))
    for k, v in enumerate(transposed):
        if set(v) != num_set:
            return False

    for line in range(0, 9, 3):
        for col in range(0, 9, 3):
            temp_set = set(board[line][col:col + 3]) | \
                       set(board[line + 1][col:col + 3]) | \
                       set(board[line + 2][col:col + 3])
            if temp_set != num_set:
                return False
    return True


# ryan = validate_sudoku([[8, 4, 7, 2, 6, 5, 1, 0, 3],
#                         [1, 3, 6, 7, 0, 8, 2, 4, 5],
#                         [0, 5, 2, 1, 4, 3, 8, 6, 7],
#                         [4, 2, 0, 6, 7, 1, 5, 3, 8],
#                         [6, 7, 8, 5, 3, 2, 0, 1, 4],
#                         [3, 1, 5, 4, 8, 0, 7, 2, 6],
#                         [5, 6, 4, 0, 1, 7, 3, 8, 2],
#                         [7, 8, 1, 3, 2, 4, 6, 5, 0],
#                         [2, 0, 3, 8, 5, 6, 4, 7, 1]])
#
# print(ryan)

def is_solved(board):
    # Tic-Tac-Toe Checker
    board = [j for i in board for j in i]
    for i in range(3):
        s = set(board[v] for v in range(i, 9, 3))
        if len(s) == 1:
            s_item = s.pop()
            if s_item > 0:
                return s_item
        s = set(board[3 * i:3 * i + 3])
        if len(s) == 1:
            s_item = s.pop()
            if s_item > 0:
                return s_item
    s = {board[0], board[4], board[8]}
    if len(s) == 1:
        s_item = s.pop()
        if s_item > 0:
            return s_item
    s = {board[2], board[4], board[6]}
    if len(s) == 1:
        s_item = s.pop()
        if s_item > 0:
            return s_item
    if 0 in board:
        return -1
    return 0


# board = [[1, 1, 1],
#          [0, 2, 2],
#          [0, 0, 0]]


# ryan = is_solved(board)


# def solution(start, finish):
#     # Cats and shelves
#     return (finish - start) // 3 +  (finish - start) % 3


# ryan = solution(1, 5)


# def decompose(num):
#     # Decompose a number
#     rtn_arr = []
#     i = 2
#     while num > 1:
#         temp = int(log(num, i))
#         if temp < 2:
#             break
#         num -= i ** temp
#         i += 1
#         rtn_arr.append(temp)
#     return [rtn_arr, num]


# ryan = decompose(8331299)


PRESET_COLORS = {'green': '#FFFFFF', }


def parse_html_color(color):
    # Parse HTML/CSS Colors
    color = PRESET_COLORS.get(color.lower(), color)

    if len(color) == 7:
        r, g, b = (int(color[i:i + 2], 16) for i in range(1, 7, 2))
    else:
        r, g, b = (int(color[i + 1] * 2, 16) for i in range(3))

    return dict(zip("rgb", (r, g, b)))


# def simple_assembler(program):
#     # Simple assembler interpreter
#     # return a dictionary with the registers
#     # 程序没有问题，但是效率不高，容易超时。
#     len_program, cur_loc = len(program), 0
#     dict_p = {}
#     while cur_loc < len_program:
#         cmd = program[cur_loc].split()
#         print(cmd, dict_p)
#         match cmd[0]:
#             case 'mov':
#                 if cmd[2] in dict_p:
#                     dict_p[cmd[1]] = dict_p[cmd[2]]
#                 else:
#                     dict_p[cmd[1]] = int(cmd[2])
#             case 'inc':
#                 dict_p[cmd[1]] = dict_p.get(cmd[1], 0) + 1
#             case 'dec':
#                 dict_p[cmd[1]] = dict_p.get(cmd[1], 0) - 1
#             case 'jnz':
#                 if cmd[1] == '0':
#                     cur_loc += int(cmd[2])-1
#                 elif dict_p.get(cmd[1], 0) != 0:
#                     cur_loc += int(cmd[2])
#                     continue
#         cur_loc += 1
#     return dict_p


def simple_assembler(program):
    # Simple assembler interpreter
    d, i = {}, 0
    while i < len(program):
        cmd, r, v = (program[i] + ' 0').split()[:3]
        if cmd == 'inc': d[r] += 1
        if cmd == 'dec': d[r] -= 1
        if cmd == 'mov': d[r] = d[v] if v in d else int(v)
        if cmd == 'jnz' and (d[r] if r in d else int(r)): i += int(v) - 1
        i += 1
    return d


code = '''\
mov c 12
mov b 0
mov a 200
dec a
inc b
jnz a -2
dec c
mov a b
jnz c -5
jnz 0 1
mov c a'''

# code = '''\
# mov a 5
# inc a
# dec a
# dec a
# jnz a -1
# inc a'''


# ryan = simple_assembler(code.splitlines())

# , {'a': 409600, 'c': 409600, 'b': 409600})


s = "-[------->+<]>-.-[->+++++<]>++.+++++++..+++.[--->+<]>-----.---[->+++<]>.-[--->+<]>---.+++.------.--------.-[--->+<]>."


def brainfuck_interpreter(program):
    program_counter = 0
    tape_index = 0
    tape = [0] * 30000
    return_stack = []
    output = ""

    while program_counter < len(program):
        match program[program_counter]:
            case ">":
                tape_index += 1
                program_counter += 1
            case "<":
                tape_index -= 1
                program_counter += 1
            case "+":
                tape[tape_index] = (tape[tape_index] + 1) & 0xFF
                program_counter += 1
            case "-":
                tape[tape_index] = (tape[tape_index] - 1) & 0xFF
                program_counter += 1
            case ".":
                output = output + chr(tape[tape_index])
                program_counter += 1
            case ",":
                raise NotImplementedError("Input isn't implemented, I'm not making it that easy :^)")
            case "[":
                if tape[tape_index] != 0:
                    return_stack.append(program_counter)
                    program_counter += 1
                else:
                    bracket_count = 1
                    while bracket_count:
                        program_counter += 1
                        if program[program_counter] == "[":
                            bracket_count += 1
                        elif program[program_counter] == "]":
                            bracket_count -= 1
                    program_counter += 1
            case "]":
                program_counter = return_stack.pop()
    return output


# ryan = brainfuck_interpreter(s)

# def get_reward(corrupted_banks: list):
#     # Clustering corrupted banks , 超时了
#     cur_bank_loop = set()
#     rtn_num = 0
#     while corrupted_banks:
#         start, end = corrupted_banks.pop()
#         print(start, end)
#         cur_bank_loop.add(start)
#         while end != start:
#             cur_bank_loop.add(end)
#             for k, v in enumerate(corrupted_banks):
#                 if v[0] == end:
#                     end = v[1]
#                     cur_bank_loop.add(end)
#                     break
#             corrupted_banks.remove(v)
#         rtn_num += 2 ** len(cur_bank_loop) * len(cur_bank_loop)
#         print(cur_bank_loop, rtn_num)
#         cur_bank_loop.clear()
#     return rtn_num


def get_reward(banks: list[tuple[int, int]]):
    # Clustering corrupted banks
    fees = 0
    G = dict(banks)
    while G:
        n, (i, j) = 1, G.popitem()
        while j != i:
            n, j = n + 1, G.pop(j)
        fees += n * 2 ** n
    return fees


# ryan = get_reward([(1, 2), (2, 1)])

# ryan = get_reward([(337, 140), (791, 1435), (1121, 1619), (578, 2032), (155, 1575), (492, 327), (48, 966),
#                    (1987, 1207), (323, 1938), (908, 304), (814, 798), (1590, 896), (1127, 211), (1969, 1493),
#                    (996, 456), (1203, 60), (329, 353), (1577, 863), (39, 1871), (1517, 1166), (209, 1038),
#                    (1757, 1675), (197, 1373), (1230, 806), (531, 1163), (518, 982), (1182, 59), (1999, 1005),
#                    (1895, 537), (737, 120), (551, 1190), (833, 1404), (1835, 784), (267, 1030), (265, 599),
#                    (2006, 2060), (1065, 1616), (739, 1398), (741, 97), (836, 628), (1238, 634), (159, 2056), (297, 152),
#                    (1256, 1387), (666, 950), (1501, 1061), (1209, 48), (1104, 318), (79, 2027), (251, 204), (559, 370),
#                    (80, 477), (1216, 1126), (1436, 7), (1236, 461), (489, 245), (1124, 1696), (1184, 1063),
#                    (1378, 1868), (1811, 172), (907, 1693), (397, 813), (1998, 709), (564, 1337), (1112, 1366),
#                    (1771, 898), (1710, 412), (1295, 1248), (229, 717), (700, 996), (1780, 1449), (1042, 507),
#                    (1304, 538), (454, 1259), (561, 28), (1784, 1262), (835, 1611), (1512, 962), (1403, 1079),
#                    (311, 1285), (294, 298), (1582, 1077), (404, 113), (1875, 1130), (438, 2020), (1248, 32),
#                    (1885, 1032), (1806, 388), (1211, 1815), (2044, 2002), (1036, 52), (1653, 464), (352, 804),
#                    (1231, 1059), (331, 210), (1632, 751), (1354, 1101), (1773, 668), (1883, 98), (1298, 1012),
#                    (1447, 1239), (1004, 486), (1145, 545), (851, 1258), (189, 132), (224, 1721), (827, 1931),
#                    (807, 386), (1191, 438), (284, 375), (394, 1512), (1907, 1585), (1221, 1451), (1107, 1007),
#                    (12, 332), (1340, 1419), (701, 2065), (640, 1361), (1913, 1092), (1888, 1680), (725, 656), (479, 30),
#                    (2012, 1212), (1369, 1763), (963, 1919), (961, 1319), (1500, 1579), (1495, 1522), (787, 34),
#                    (997, 807), (226, 1803), (320, 1747), (1753, 130), (1689, 836), (1479, 1133), (1957, 1644),
#                    (1743, 556), (245, 1231), (484, 233), (1470, 995), (1764, 2061), (2002, 1234), (1049, 1171),
#                    (402, 340), (1410, 610), (1824, 1306), (622, 1318), (227, 1254), (1336, 850), (1384, 1851),
#                    (95, 1384), (1572, 10), (1535, 679), (338, 910), (1619, 707), (916, 1564), (1328, 1959), (50, 376),
#                    (299, 530), (1080, 881), (1006, 1921), (276, 1500), (986, 621), (1786, 1908), (1838, 1095),
#                    (1425, 1703), (1625, 1949), (1680, 1427), (173, 907), (979, 600), (540, 1681), (1521, 1483),
#                    (977, 173), (368, 964), (137, 1053), (1831, 1484), (1480, 51), (1847, 1238), (77, 300), (682, 534),
#                    (703, 1800), (1742, 1170), (689, 1817), (1349, 2013), (1565, 1648), (1654, 1218), (44, 1842),
#                    (1226, 1401), (570, 660), (976, 272), (940, 68), (713, 561), (690, 1058), (568, 1994), (1457, 2025),
#                    (555, 1728), (705, 1206), (119, 1111), (785, 1557), (1460, 1125), (1161, 2052), (826, 202),
#                    (1728, 1431), (465, 133), (1260, 1454), (764, 1682), (437, 1291), (1526, 117), (68, 1878),
#                    (203, 1021), (1148, 1603), (1473, 1056), (225, 1113), (1239, 1735), (1758, 674), (1637, 1312),
#                    (1935, 1808), (2048, 764), (285, 1102), (476, 820), (1177, 1657), (1125, 1016), (92, 421),
#                    (2023, 934), (760, 887), (1942, 1439), (845, 1872), (1249, 967), (423, 1781), (104, 1581),
#                    (321, 624), (1277, 1761), (383, 1561), (1030, 323), (125, 708), (847, 1227), (841, 88), (409, 156),
#                    (9, 116), (2009, 1566), (2060, 83), (1257, 1730), (1798, 572), (1424, 949), (1828, 1180), (384, 391),
#                    (1097, 1827), (259, 1822), (1606, 849), (1270, 1407), (1418, 1409), (473, 1356), (2040, 383),
#                    (1106, 331), (1449, 736), (1276, 1172), (1018, 1315), (1303, 871), (4, 1880), (1246, 1723),
#                    (370, 2068), (1034, 474), (1062, 352), (1639, 488), (1920, 783), (178, 1195), (236, 492),
#                    (1661, 1971), (683, 746), (757, 1458), (973, 189), (538, 1784), (1054, 718), (1404, 1309),
#                    (529, 1553), (136, 1080), (704, 876), (1621, 1108), (1931, 1811), (1808, 1532), (1301, 1428),
#                    (1396, 311), (2015, 1314), (738, 1752), (52, 856), (1964, 287), (135, 1399), (1822, 94), (792, 1390),
#                    (651, 251), (1012, 168), (1693, 254), (1206, 613), (148, 451), (1961, 1351), (758, 499),
#                    (1375, 1922), (934, 1942), (1095, 1233), (1499, 893), (1391, 1850), (813, 2031), (7, 201), (234, 22),
#                    (1923, 1269), (1714, 1164), (1817, 346), (1417, 595), (466, 1639), (341, 1168), (1981, 1830),
#                    (1428, 1736), (872, 1367), (15, 855), (273, 1823), (1441, 851), (170, 1037), (1266, 1678),
#                    (545, 249), (1951, 692), (358, 926), (256, 1393), (1904, 946), (552, 979), (1086, 1299), (629, 276),
#                    (904, 1132), (420, 584), (1469, 997), (1948, 102), (1429, 757), (711, 1999), (2011, 1263),
#                    (1195, 1799), (201, 923), (171, 1727), (89, 1182), (1863, 1034), (1944, 231), (865, 1211),
#                    (1218, 1629), (237, 951), (576, 428), (1025, 1121), (1600, 1562), (1716, 858), (1928, 1502),
#                    (1701, 1076), (716, 632), (96, 1622), (172, 756), (359, 1948), (585, 1665), (885, 1273),
#                    (1262, 1268), (1685, 258), (110, 895), (1039, 1048), (1614, 696), (972, 1204), (71, 1415),
#                    (510, 1978), (1801, 633), (520, 1856), (720, 1739), (727, 503), (1478, 1029), (1273, 138),
#                    (1110, 355), (1939, 974), (2018, 947), (874, 1738), (956, 650), (999, 2055), (427, 713),
#                    (1214, 1088), (1084, 961), (982, 1857), (1846, 115), (1421, 1924), (1672, 1786), (200, 1573),
#                    (657, 1514), (941, 716), (1091, 1724), (272, 605), (1130, 1083), (140, 1746), (744, 369),
#                    (258, 1766), (453, 520), (1787, 1001), (2008, 1726), (1078, 792), (1532, 665), (2004, 1162),
#                    (1849, 224), (1950, 357), (1264, 171), (1832, 182), (1288, 1917), (1514, 1473), (469, 1658),
#                    (399, 1602), (187, 433), (240, 1486), (1082, 4), (1720, 394), (1169, 1962), (1031, 1928),
#                    (1860, 955), (123, 828), (35, 281), (81, 1175), (309, 1406), (78, 161), (742, 760), (1652, 1271),
#                    (1687, 1413), (1461, 1348), (1140, 1018), (1972, 1215), (702, 2024), (156, 238), (888, 285),
#                    (1484, 1912), (919, 829), (1766, 458), (535, 570), (960, 295), (169, 1191), (718, 1762),
#                    (1451, 1773), (621, 702), (1930, 1146), (750, 814), (86, 1590), (1868, 443), (67, 449), (1584, 1358),
#                    (1028, 1430), (848, 1605), (721, 1592), (1087, 1199), (121, 159), (243, 454), (2068, 1914),
#                    (313, 351), (1337, 583), (1472, 554), (1143, 944), (2059, 948), (728, 879), (2029, 1242), (242, 866),
#                    (1737, 1022), (499, 1645), (42, 774), (1966, 1758), (1093, 988), (1746, 194), (1558, 1729),
#                    (428, 1903), (1953, 1085), (0, 90), (1988, 2022), (1688, 197), (2057, 2049), (395, 1593),
#                    (691, 1819), (1763, 57), (11, 1666), (1905, 432), (153, 325), (1792, 136), (1765, 1297),
#                    (1275, 1179), (1387, 738), (328, 422), (1550, 1750), (22, 1718), (1254, 1706), (1848, 786),
#                    (2069, 670), (1890, 1090), (1938, 1859), (212, 1638), (1427, 1354), (335, 234), (1013, 1277),
#                    (991, 326), (1399, 888), (188, 686), (1109, 282), (1358, 1757), (120, 1874), (809, 1846),
#                    (1497, 579), (1980, 1054), (1035, 1481), (778, 1600), (1922, 215), (2039, 414), (232, 40),
#                    (289, 880), (1779, 1385), (2032, 1542), (1364, 920), (467, 555), (1712, 937), (196, 350),
#                    (414, 1722), (970, 1221), (343, 698), (1309, 312), (571, 1891), (938, 1471), (474, 306), (903, 142),
#                    (955, 1688), (195, 620), (129, 1389), (1955, 1464), (2052, 724), (734, 874), (628, 1897),
#                    (166, 1782), (1915, 1405), (314, 1954), (498, 1229), (625, 1372), (82, 1760), (277, 1751), (73, 279),
#                    (541, 1066), (64, 839), (2067, 1612), (636, 274), (608, 400), (747, 33), (25, 1203), (1240, 1554),
#                    (1781, 1667), (837, 335), (1272, 134), (859, 1261), (1994, 1690), (779, 1960), (709, 740),
#                    (1925, 1177), (1235, 107), (475, 1376), (1839, 1584), (784, 1698), (905, 511), (730, 1664),
#                    (1307, 1508), (1735, 37), (332, 1209), (1394, 280), (1914, 644), (1199, 475), (1153, 1057),
#                    (707, 524), (418, 2059), (1617, 1976), (525, 761), (694, 1161), (2043, 1250), (802, 1418), (38, 284),
#                    (923, 1517), (1995, 683), (1840, 551), (1520, 975), (349, 1296), (1342, 564), (995, 1429), (65, 365),
#                    (857, 1753), (139, 1064), (1294, 504), (1051, 271), (771, 196), (133, 1230), (1677, 1106),
#                    (1993, 1019), (376, 1013), (408, 835), (252, 265), (386, 2040), (91, 883), (1631, 1313), (614, 1507),
#                    (1715, 1477), (910, 1414), (1466, 1925), (304, 1945), (875, 1006), (1452, 1570), (1426, 711),
#                    (55, 1400), (88, 1276), (2072, 316), (1155, 200), (537, 24), (752, 689), (1841, 1661), (1570, 1388),
#                    (1934, 1251), (1897, 473), (878, 1474), (1518, 817), (388, 1067), (1659, 1417), (804, 71),
#                    (18, 1216), (1694, 741), (572, 1604), (1156, 2043), (152, 1420), (766, 1395), (441, 1305),
#                    (731, 395), (828, 1442), (27, 129), (1005, 1587), (890, 1836), (300, 3), (1224, 1831), (509, 1887),
#                    (748, 1275), (122, 1501), (1588, 1448), (2010, 1068), (26, 729), (463, 439), (1611, 139),
#                    (1865, 297), (1280, 1240), (1548, 1208), (1730, 1941), (762, 343), (1692, 324), (615, 1089),
#                    (1864, 183), (672, 827), (1422, 1687), (393, 2064), (527, 223), (1250, 1854), (1752, 1131),
#                    (708, 243), (1696, 104), (915, 347), (669, 969), (642, 199), (1650, 212), (1566, 66), (1101, 1583),
#                    (1821, 980), (1122, 160), (516, 180), (1623, 612), (16, 54), (2035, 1918), (1662, 303), (658, 922),
#                    (746, 1150), (1174, 1185), (990, 1255), (54, 450), (94, 1341), (90, 777), (1721, 53), (524, 1821),
#                    (1747, 916), (2000, 1349), (754, 1368), (1834, 1890), (360, 381), (1507, 1607), (268, 750),
#                    (1669, 424), (1947, 506), (1641, 1158), (274, 82), (1857, 1550), (470, 1812), (142, 86),
#                    (2020, 1302), (390, 1952), (1392, 337), (37, 1402), (1782, 270), (1580, 1424), (861, 1328),
#                    (1796, 733), (1401, 1304), (900, 1257), (1234, 917), (1651, 578), (114, 74), (1419, 1609),
#                    (1800, 897), (109, 559), (2049, 1701), (1037, 1397), (550, 541), (1261, 385), (430, 1155),
#                    (1539, 1375), (1592, 924), (24, 12), (17, 1278), (1102, 652), (897, 1197), (2014, 296), (1047, 690),
#                    (1599, 64), (1491, 1228), (113, 2009), (1529, 1141), (221, 368), (2001, 330), (217, 651),
#                    (246, 1491), (1306, 831), (1859, 11), (1823, 1330), (1259, 1332), (1475, 1457), (1243, 2067),
#                    (1989, 2070), (74, 2018), (983, 169), (1290, 767), (1152, 35), (1656, 1601), (1553, 1159),
#                    (610, 1840), (118, 939), (1543, 1618), (723, 580), (1547, 1000), (971, 952), (1458, 971), (353, 749),
#                    (1975, 1892), (1878, 392), (1933, 1308), (554, 1189), (935, 39), (1029, 1103), (1178, 216),
#                    (886, 1798), (1263, 1026), (1096, 478), (1360, 963), (623, 359), (1185, 408), (1533, 1875),
#                    (468, 220), (179, 1955), (145, 1453), (1133, 748), (1486, 589), (1061, 320), (1118, 802), (422, 990),
#                    (1674, 1214), (793, 344), (1492, 1360), (260, 1123), (41, 1982), (1613, 1932), (1139, 669),
#                    (1343, 1996), (141, 1801), (1854, 1555), (2064, 510), (1776, 797), (1670, 592), (1146, 1173),
#                    (870, 1695), (789, 1673), (1176, 1357), (684, 1489), (2051, 118), (183, 50), (974, 596), (1675, 540),
#                    (797, 2042), (239, 2028), (1733, 773), (2055, 1160), (606, 2012), (1665, 1844), (695, 821),
#                    (1785, 1498), (749, 1463), (1560, 892), (230, 1599), (1690, 1151), (1356, 1777), (519, 1720),
#                    (1299, 111), (1291, 96), (194, 1094), (1598, 653), (32, 410), (1627, 1594), (1326, 905),
#                    (1602, 1091), (1225, 1183), (1564, 47), (2056, 763), (1363, 403), (60, 1863), (1880, 976),
#                    (339, 704), (182, 805), (69, 1480), (1820, 1515), (547, 1862), (855, 1888), (521, 283), (873, 1178),
#                    (361, 1536), (461, 1284), (326, 731), (1113, 1965), (1179, 277), (1616, 1320), (1610, 515),
#                    (1092, 1003), (763, 1610), (1585, 206), (662, 1370), (248, 1346), (770, 214), (1144, 795),
#                    (829, 703), (2036, 1697), (2026, 1213), (1488, 1853), (1609, 842), (1876, 532), (646, 1980),
#                    (244, 1569), (1804, 720), (374, 533), (1193, 1725), (471, 878), (593, 141), (1755, 485),
#                    (1321, 1198), (1175, 1411), (1032, 1245), (871, 838), (582, 128), (1557, 462), (378, 374),
#                    (893, 1244), (1108, 489), (1538, 1893), (1777, 1835), (1750, 1578), (47, 1272), (902, 1224),
#                    (1060, 1192), (293, 1363), (645, 1915), (208, 884), (3, 900), (1353, 236), (51, 1772), (176, 42),
#                    (719, 1107), (1490, 1321), (1348, 1958), (1332, 1997), (883, 803), (366, 1074), (456, 1303),
#                    (2005, 384), (281, 315), (777, 1282), (536, 1899), (616, 1540), (1636, 1504), (1009, 398),
#                    (1952, 455), (70, 1333), (1603, 402), (504, 248), (371, 472), (988, 361), (263, 1052), (619, 643),
#                    (1732, 1078), (1448, 468), (1279, 1911), (444, 785), (946, 544), (864, 401), (927, 387),
#                    (1481, 1984), (1924, 930), (112, 1478), (1635, 1329), (968, 617), (1440, 490), (307, 397),
#                    (1067, 1805), (310, 1598), (2031, 1073), (1223, 93), (1601, 1307), (2071, 1597), (1167, 778),
#                    (101, 915), (20, 801), (952, 659), (1040, 1744), (1297, 8), (579, 1148), (573, 1082), (1740, 2016),
#                    (162, 1779), (308, 244), (1268, 826), (517, 1956), (815, 815), (1731, 38), (1648, 1934), (1189, 176),
#                    (980, 822), (978, 834), (1912, 420), (1513, 647), (1797, 174), (2016, 732), (1412, 585), (1552, 418),
#                    (818, 373), (1843, 1907), (501, 1205), (1377, 918), (1569, 1326), (365, 2), (595, 109), (2042, 494),
#                    (459, 1142), (1809, 105), (292, 371), (767, 1392), (497, 390), (1120, 940), (805, 1513),
#                    (1791, 1495), (302, 1993), (816, 727), (1454, 890), (1289, 247), (1420, 2057), (1405, 1820),
#                    (210, 1630), (1649, 719), (185, 63), (1056, 1969), (1622, 928), (1896, 1256), (1769, 1497),
#                    (1705, 1814), (264, 864), (1166, 415), (486, 268), (1414, 1008), (889, 739), (115, 99), (193, 222),
#                    (1555, 446), (1958, 2030), (1312, 953), (774, 255), (1245, 1705), (1861, 2026), (1971, 808),
#                    (1704, 635), (1559, 18), (1711, 185), (1350, 1455), (1003, 755), (1918, 1386), (380, 1027),
#                    (856, 267), (706, 1894), (1946, 655), (1604, 427), (1141, 1438), (214, 301), (36, 444), (1508, 1311),
#                    (19, 1909), (644, 1310), (1057, 825), (693, 870), (1228, 1112), (128, 1883), (1094, 1331),
#                    (834, 1377), (1618, 457), (679, 623), (1439, 1882), (1633, 1232), (846, 419), (357, 723),
#                    (1196, 1280), (1310, 1485), (407, 1676), (932, 811), (817, 598), (753, 1637), (446, 81), (523, 1527),
#                    (1468, 1834), (928, 775), (1815, 754), (271, 259), (1836, 680), (786, 782), (1305, 1129), (282, 483),
#                    (513, 977), (627, 1450), (1954, 382), (926, 673), (442, 1571), (1150, 1165), (1509, 1876),
#                    (1407, 973), (99, 2010), (1882, 2008), (866, 1293), (21, 1184), (917, 1625), (495, 2036), (775, 587),
#                    (1991, 646), (858, 122), (712, 1568), (147, 175), (318, 1353), (66, 2029), (1741, 1869), (462, 1628),
#                    (2038, 1889), (1996, 61), (1976, 364), (1794, 198), (726, 85), (421, 1790), (410, 1591), (1658, 535),
#                    (2041, 927), (1411, 1755), (1678, 1702), (199, 1886), (1204, 609), (1300, 345), (567, 1281),
#                    (1368, 794), (1916, 1423), (1494, 1317), (1383, 314), (1546, 431), (882, 1156), (1467, 221),
#                    (1536, 1606), (1810, 1655), (1647, 1017), (2027, 1440), (298, 1783), (653, 568), (1323, 1995),
#                    (1274, 193), (1173, 712), (325, 1295), (1788, 1621), (1900, 17), (116, 1930), (1379, 1737),
#                    (452, 1187), (1157, 744), (174, 1985), (205, 1615), (740, 308), (1724, 269), (1352, 1916), (87, 968),
#                    (1676, 339), (1474, 1714), (801, 1135), (1982, 1237), (1901, 230), (1385, 1864), (389, 1927),
#                    (1284, 1794), (1361, 84), (975, 919), (1850, 467), (1505, 1093), (588, 49), (670, 336), (1381, 20),
#                    (1698, 1345), (485, 356), (1455, 844), (2034, 1672), (1069, 2063), (175, 960), (1691, 891),
#                    (877, 1577), (218, 123), (958, 1636), (1586, 1075), (1149, 722), (432, 1391), (2033, 1796),
#                    (1870, 1640), (605, 913), (1729, 2048), (876, 1421), (1265, 413), (1683, 240), (1578, 832),
#                    (581, 648), (688, 2039), (1986, 1795), (604, 1355), (1893, 1049), (937, 1816), (1058, 987),
#                    (367, 597), (812, 470), (261, 625), (1540, 745), (1726, 396), (494, 1904), (31, 393), (85, 799),
#                    (909, 181), (315, 1516), (2046, 1110), (655, 903), (126, 1809), (957, 730), (563, 146), (62, 434),
#                    (401, 342), (34, 615), (496, 1662), (1511, 852), (831, 547), (887, 124), (612, 1715), (939, 2058),
#                    (1754, 854), (993, 55), (1059, 389), (1542, 608), (124, 278), (1088, 0), (880, 406), (1805, 1139),
#                    (1666, 558), (1021, 765), (1825, 154), (1048, 2014), (765, 2034), (1527, 1225), (13, 1011),
#                    (860, 1936), (768, 2004), (1530, 207), (1162, 1935), (1997, 2066), (819, 576), (439, 2011),
#                    (965, 1210), (305, 1961), (544, 1623), (1736, 16), (1760, 1482), (1827, 178), (1697, 1832),
#                    (160, 1145), (1367, 575), (375, 1028), (431, 1580), (948, 1929), (1050, 480), (542, 1646),
#                    (458, 1933), (1446, 1035), (931, 667), (1046, 565), (944, 349), (482, 1545), (211, 2051),
#                    (1055, 1867), (2030, 845), (667, 965), (1537, 1065), (580, 1748), (842, 44), (296, 1506),
#                    (1194, 319), (1435, 144), (1686, 1741), (1575, 1689), (1968, 1740), (1292, 114), (1406, 1122),
#                    (825, 1977), (1739, 1749), (1187, 192), (659, 36), (1751, 809), (416, 601), (964, 70), (1921, 452),
#                    (257, 1829), (413, 191), (1862, 843), (921, 1194), (920, 860), (1853, 29), (1709, 743), (1707, 859),
#                    (1098, 823), (1477, 1754), (1908, 1902), (324, 1378), (411, 527), (1163, 1983), (1043, 1452),
#                    (2066, 1461), (1255, 1953), (2065, 177), (1892, 1339), (206, 1322), (1493, 1634), (1318, 465),
#                    (488, 526), (884, 1620), (1927, 1825), (1879, 853), (5, 76), (1814, 497), (250, 1943), (283, 41),
#                    (1965, 1923), (1334, 1833), (1949, 208), (1644, 705), (1867, 931), (1278, 405), (2061, 762),
#                    (854, 1462), (1450, 1979), (592, 1847), (106, 901), (1748, 985), (648, 1526), (1576, 1898),
#                    (1253, 1109), (800, 110), (1874, 150), (911, 1617), (546, 1381), (1147, 571), (849, 1383),
#                    (1115, 1789), (1351, 645), (607, 1699), (515, 294), (1959, 1780), (2062, 426), (472, 184),
#                    (1131, 787), (478, 273), (1388, 1374), (1906, 1539), (161, 1334), (668, 567), (1317, 560),
#                    (1744, 1476), (954, 1670), (912, 867), (1432, 1674), (1081, 1422), (146, 1631), (843, 2050),
#                    (1172, 1711), (1583, 1236), (599, 1219), (1858, 726), (1762, 362), (613, 1099), (1293, 902),
#                    (1135, 1289), (1703, 239), (1937, 770), (1017, 1081), (1812, 1380), (1168, 1290), (1974, 1220),
#                    (460, 1264), (1322, 1663), (235, 1939), (783, 1759), (1807, 1352), (1629, 232), (396, 875),
#                    (61, 1490), (1837, 602), (1487, 2005), (108, 1343), (207, 1937), (1929, 1533), (1215, 1972),
#                    (1767, 837), (280, 675), (918, 242), (1504, 1051), (1551, 721), (1595, 1804), (1197, 2071),
#                    (382, 1968), (1271, 72), (1044, 302), (186, 663), (1541, 1097), (117, 833), (966, 75), (83, 1010),
#                    (618, 1002), (440, 153), (1186, 753), (1073, 322), (1816, 594), (1023, 363), (100, 1432), (839, 228),
#                    (933, 429), (591, 155), (279, 437), (1370, 627), (2024, 1020), (832, 691), (412, 31), (881, 256),
#                    (1596, 289), (1019, 1652), (906, 1505), (1607, 710), (254, 793), (557, 1709), (781, 348),
#                    (643, 1416), (1476, 2035), (584, 766), (168, 906), (1941, 521), (1393, 1716), (634, 1154),
#                    (1770, 56), (532, 699), (675, 2045), (1324, 886), (1241, 769), (1170, 257), (756, 684), (1010, 305),
#                    (733, 1771), (1700, 861), (1719, 528), (1681, 706), (1286, 991), (699, 999), (1962, 666),
#                    (447, 1117), (1302, 1547), (1333, 1468), (1573, 1596), (158, 261), (1347, 482), (1660, 1776),
#                    (1684, 1947), (1909, 1447), (415, 188), (1233, 380), (1052, 569), (1229, 1595), (1608, 108),
#                    (1594, 195), (594, 1818), (1768, 1873), (1372, 1841), (1444, 1365), (1041, 333), (1201, 334),
#                    (1587, 1045), (445, 781), (1210, 148), (1437, 552), (852, 135), (2019, 1503), (1329, 43),
#                    (1242, 1323), (1325, 1246), (1400, 1765), (1315, 2003), (600, 682), (1002, 1096), (558, 209),
#                    (457, 170), (1077, 970), (811, 218), (1977, 213), (1756, 1694), (808, 476), (1516, 299), (790, 1434),
#                    (677, 1327), (1519, 5), (2045, 151), (419, 1445), (130, 471), (780, 1098), (696, 614), (269, 1896),
#                    (1903, 1710), (290, 1523), (1, 1379), (959, 1181), (769, 1679), (1339, 447), (417, 186), (687, 1990),
#                    (1545, 487), (1871, 1541), (647, 100), (1345, 235), (222, 1009), (1395, 163), (433, 508),
#                    (1567, 1858), (1655, 998), (549, 1813), (6, 317), (782, 1839), (1682, 252), (566, 1992), (151, 158),
#                    (1373, 73), (1523, 1731), (381, 1167), (426, 509), (562, 1641), (795, 166), (1190, 262), (511, 588),
#                    (56, 1394), (1287, 1845), (583, 338), (131, 1529), (2047, 1848), (2054, 1116), (53, 404), (577, 606),
#                    (295, 1906), (796, 2069), (1899, 1520), (1390, 529), (597, 1316), (435, 80), (216, 638),
#                    (1945, 1810), (601, 313), (943, 1913), (1365, 582), (1269, 1041), (2, 1347), (1341, 889), (330, 685),
#                    (1359, 58), (1591, 899), (869, 1188), (1232, 972), (661, 1025), (962, 637), (638, 641), (287, 2046),
#                    (1718, 9), (219, 1525), (1402, 701), (810, 1479), (1932, 162), (664, 658), (1382, 121), (1202, 165),
#                    (1483, 1249), (373, 1325), (344, 1300), (455, 1105), (1045, 790), (1014, 938), (2003, 1186),
#                    (33, 932), (334, 19), (1894, 904), (350, 1426), (1346, 562), (336, 1494), (1702, 563), (526, 1712),
#                    (637, 354), (1544, 26), (1889, 103), (1100, 1472), (1528, 978), (1282, 1552), (598, 1778),
#                    (649, 1957), (631, 1677), (587, 956), (1984, 616), (164, 954), (286, 1713), (1075, 1768), (539, 629),
#                    (1188, 1742), (1330, 164), (729, 1243), (1881, 1572), (1802, 1910), (930, 1466), (936, 1870),
#                    (1884, 1767), (503, 759), (1374, 1437), (181, 517), (255, 179), (355, 1733), (49, 1981),
#                    (1070, 1974), (724, 1176), (1462, 636), (840, 1265), (233, 1792), (138, 734), (1105, 1200),
#                    (1311, 935), (759, 14), (596, 1650), (1116, 1692), (722, 430), (1085, 697), (1371, 1149), (650, 846),
#                    (1165, 263), (1852, 286), (1917, 1446), (480, 237), (1053, 989), (799, 1802), (1137, 1973),
#                    (548, 943), (1562, 1364), (342, 288), (1362, 1050), (57, 1535), (1397, 1071), (385, 577),
#                    (620, 1524), (425, 788), (1778, 539), (822, 1559), (1887, 865), (1456, 681), (586, 1147), (76, 1877),
#                    (543, 205), (1734, 1989), (1123, 1247), (493, 894), (710, 2021), (1465, 1459), (1749, 830),
#                    (1471, 1574), (892, 1707), (1919, 246), (2063, 543), (953, 869), (967, 1143), (676, 1558),
#                    (1593, 1685), (924, 436), (448, 958), (2070, 1152), (685, 678), (1181, 693), (773, 2033), (1638, 13),
#                    (1285, 1252), (1132, 548), (1963, 448), (275, 264), (377, 677), (105, 1988), (1679, 366), (392, 590),
#                    (10, 379), (732, 912), (351, 341), (761, 1998), (1445, 1946), (788, 219), (1605, 929), (1628, 1070),
#                    (1646, 622), (202, 1967), (989, 649), (1251, 253), (1208, 735), (306, 1043), (1207, 187), (743, 618),
#                    (1745, 661), (481, 1369), (1083, 1654), (1960, 1950), (102, 2041), (1376, 1642), (556, 1039),
#                    (1453, 695), (969, 1487), (1129, 1286), (391, 810), (1111, 1531), (2007, 1169), (429, 542),
#                    (1320, 1926), (1725, 1492), (1789, 1153), (528, 1201), (1866, 1031), (247, 227), (317, 460),
#                    (8, 2047), (590, 463), (1563, 1719), (1180, 1683), (867, 1530), (30, 566), (862, 1806), (1640, 23),
#                    (945, 1589), (806, 147), (1936, 992), (356, 768), (400, 688), (1761, 818), (1956, 604), (59, 840),
#                    (981, 45), (1117, 796), (949, 593), (362, 459), (1626, 908), (1772, 1544), (477, 518), (1219, 266),
#                    (1502, 1861), (333, 1940), (665, 1288), (1237, 1174), (1738, 79), (1066, 1260), (1695, 372),
#                    (2028, 619), (2013, 885), (97, 1732), (84, 662), (1970, 1033), (451, 1086), (735, 1986),
#                    (1898, 1425), (1416, 1563), (436, 2054), (1442, 1560), (167, 1465), (1657, 2044), (1134, 360),
#                    (1833, 1144), (63, 671), (569, 1298), (1973, 1567), (671, 484), (611, 909), (1128, 1120),
#                    (1000, 672), (1151, 500), (632, 1223), (1331, 493), (1119, 957), (626, 260), (992, 776), (1366, 758),
#                    (635, 772), (1498, 226), (681, 1944), (1079, 1828), (1090, 112), (602, 1659), (46, 1643), (899, 942),
#                    (406, 1649), (1534, 1062), (1722, 1283), (794, 933), (1515, 1656), (2021, 1826), (213, 1072),
#                    (717, 630), (838, 125), (1985, 1202), (925, 981), (434, 250), (896, 1084), (1708, 307), (1525, 217),
#                    (1423, 1901), (1851, 1704), (1319, 1499), (947, 553), (1076, 1), (820, 607), (1531, 95), (1803, 423),
#                    (1434, 1647), (898, 1797), (984, 546), (508, 1412), (288, 1551), (1252, 941), (641, 65), (500, 1613),
#                    (1503, 1267), (132, 442), (1826, 586), (514, 725), (1727, 779), (1160, 654), (450, 1359),
#                    (950, 1734), (1071, 1292), (1136, 1023), (951, 1791), (1064, 1217), (1205, 6), (894, 742),
#                    (215, 1653), (850, 1015), (692, 986), (1496, 1193), (1142, 1991), (1845, 642), (449, 1900),
#                    (348, 1770), (1335, 1879), (1433, 1745), (204, 1537), (1316, 1138), (23, 1362), (107, 1588),
#                    (266, 2017), (639, 819), (914, 495), (72, 549), (1943, 657), (863, 1128), (270, 1528), (1482, 1565),
#                    (1464, 925), (1774, 1708), (1089, 1226), (1213, 1338), (895, 1469), (1308, 1100), (1072, 1920),
#                    (1873, 603), (316, 1222), (913, 1785), (1819, 293), (1790, 2062), (1872, 496), (1597, 505),
#                    (1612, 310), (301, 399), (1283, 1764), (29, 1137), (1699, 1475), (372, 1510), (674, 1047), (58, 516),
#                    (1855, 290), (879, 921), (1663, 2019), (363, 1140), (98, 77), (177, 877), (319, 1865), (1556, 21),
#                    (680, 525), (1630, 1119), (1967, 857), (1016, 664), (75, 1069), (483, 498), (1667, 1127),
#                    (1992, 780), (575, 1669), (1198, 1793), (1008, 771), (1589, 367), (1844, 1335), (2037, 89),
#                    (929, 1408), (1830, 737), (736, 203), (553, 1124), (43, 409), (745, 358), (403, 131), (522, 1546),
#                    (617, 1586), (327, 847), (1723, 700), (1001, 816), (143, 469), (1415, 1905), (490, 984), (844, 275),
#                    (1114, 2037), (1522, 911), (1430, 1403), (220, 1371), (1126, 812), (154, 1104), (506, 1342),
#                    (574, 1467), (312, 145), (1979, 1040), (998, 1769), (1877, 1756), (1398, 1382), (487, 872),
#                    (379, 1964), (776, 425), (533, 1055), (1926, 46), (1910, 1136), (1510, 241), (755, 225),
#                    (1074, 1627), (1314, 1014), (1247, 824), (127, 441), (1027, 1852), (1664, 149), (823, 1518),
#                    (1438, 69), (1183, 611), (678, 882), (191, 522), (803, 292), (853, 1843), (1431, 714), (505, 1436),
#                    (1579, 167), (405, 1788), (354, 752), (1642, 479), (278, 1266), (1856, 1004), (1940, 747),
#                    (1338, 157), (1267, 626), (1386, 1895), (223, 1866), (1813, 1042), (180, 715), (1011, 1885),
#                    (443, 573), (1886, 1635), (1842, 25), (624, 1881), (1020, 581), (901, 1294), (633, 557), (1380, 377),
#                    (1281, 848), (987, 1787), (40, 445), (2053, 1433), (502, 1837), (1706, 1324), (1554, 868),
#                    (1220, 800), (1459, 1196), (1409, 1576), (163, 873), (942, 143), (1463, 1511), (1571, 502),
#                    (1227, 513), (654, 1396), (1668, 2007), (560, 1336), (630, 1538), (1983, 501), (1671, 1253),
#                    (1713, 694), (609, 328), (1355, 1509), (821, 407), (534, 481), (157, 1633), (1673, 435),
#                    (1159, 1118), (1799, 1496), (14, 1700), (714, 1350), (345, 321), (1063, 2000), (698, 1691),
#                    (1506, 1686), (303, 789), (198, 1024), (1645, 2015), (922, 137), (660, 1443), (1296, 416),
#                    (1068, 1344), (1489, 1651), (1413, 78), (144, 1963), (134, 291), (1313, 1549), (565, 1444),
#                    (603, 1860), (1033, 983), (190, 1775), (1099, 1774), (387, 1951), (530, 1235), (1327, 519),
#                    (1154, 631), (1990, 190), (165, 1115), (464, 2001), (686, 91), (1015, 1488), (985, 92), (830, 1884),
#                    (751, 512), (1344, 466), (1717, 2072), (1634, 1301), (1485, 994), (1389, 126), (1911, 1279),
#                    (1759, 1582), (1024, 1460), (1222, 1340), (715, 523), (1408, 1521), (798, 1134), (1568, 1519),
#                    (369, 531), (868, 676), (228, 1534), (262, 1556), (1793, 1060), (1829, 1470), (45, 640), (346, 1632),
#                    (1138, 1970), (2022, 15), (697, 1157), (322, 106), (772, 491), (589, 1456), (1158, 1660), (192, 417),
#                    (512, 1743), (1038, 862), (1443, 1410), (93, 1274), (1022, 841), (1561, 1671), (1026, 993),
#                    (238, 687), (1891, 1241), (1581, 1036), (1007, 119), (253, 378), (824, 101), (184, 2038),
#                    (1244, 309), (364, 1287), (891, 329), (1775, 1626), (241, 87), (1624, 1855), (1795, 514), (652, 27),
#                    (994, 1087), (424, 453), (1818, 62), (291, 1987), (149, 1717), (1103, 1543), (249, 1046),
#                    (1978, 2023), (28, 959), (673, 550), (663, 1838), (656, 728), (1200, 639), (491, 1668), (1171, 1975),
#                    (1217, 440), (1643, 945), (1574, 1114), (1524, 1824), (1164, 1966), (2025, 1270), (1615, 1044),
#                    (1869, 2006), (111, 1441), (340, 791), (2050, 1548), (507, 1849), (398, 914), (1192, 1624),
#                    (2017, 67), (2058, 1614), (347, 591), (1258, 1684), (1549, 411), (1620, 1807), (231, 936),
#                    (1783, 1608), (150, 229), (103, 127), (1212, 574), (1902, 536), (1357, 2053)]
#                   )


def longest_repetition(chars):
    # Character with longest consecutive repetition
    max_length = 0
    length = 0
    temp_char = ""
    max_char = ""
    for k, v in enumerate(chars):
        if v == temp_char:
            length += 1
        else:
            temp_char = v
            length = 1
        if length > max_length:
            max_length = length
            max_char = v
        # print(v, max_char, length, max_length)
    return max_char, max_length


# r = longest_repetition("bbbaaabaaaa")


def all_fibonacci_numbers():
    # Fibonacci Streaming
    x, y = 0, 1
    while True:
        yield y
        x, y = y, x + y


# r = list(itertools.islice(all_fibonacci_numbers(), 30))


def n_closestPairs_tonum(upper_limit, k):
    # The Sum and The Rest of Certain Pairs of Numbers have to be Perfect Squares (more Challenging)
    pairs = []
    m = upper_limit
    while m > 0:
        m = m - 1
        x = 1
        n = m - x ** 2
        while 0 < n < m:
            if ((m + n) ** 0.5).is_integer():
                pairs.append([m, n])
                if len(pairs) == k:
                    return pairs
            x += 1
            n = m - x ** 2
    return pairs


# r = n_closestPairs_tonum(21040, 18)


def odds_iter():
    # 生成奇数迭代器，从3开始
    n = 1
    while True:
        n += 2
        yield n


def not_devisible(n):
    # 判断是否可被整除
    return lambda x: x % n != 0


def primes_iter(m):
    # 生成小于m的素数迭代
    it = odds_iter()
    n = next(it)
    while n < m:
        yield n
        n = next(it)
        it = filter(not_devisible(n), it)


# def find_emirp(m):
#     # Emirps
#     # 效率不高
#     l, c, s = 0, 0, 0
#     mm = int(f'{"9"*(len(str(m)))}')
#     lm = list(primes_iter(mm))
#     for i in lm:
#         if i > m:
#             break
#         if int(str(i)[::-1]) in lm and str(i) != str(i)[::-1]:
#             l += 1
#             c = i
#             s += i
#     return [l, c, s]


# start = time.time()
# # r = find_emirp(20000)
# end = time.time()

# r = list(primes_iter(200))

# print(r, f'{end - start}')

# 试试用集合


def find_emirp(m):
    # Emirps
    # 事实表明，用集合的速度比用数组要快很多！
    # 但是还是没有通过codewar的时间校验,直到把最后的add替换为列表生成语句
    max_num = int('9' * len(str(m)))
    rtn_set = {i for i in range(10, max_num + 1) if i % 2 != 0}
    for i in range(3, int(max_num ** 0.5) + 2):
        for j in range(2, int(max_num // i) + 2):
            rtn_set.discard(i * j)
    final_rtn_set = set(i for i in rtn_set if int(str(i)[::-1]) in rtn_set and i < m and str(i) != str(i)[::-1])
    # for i in rtn_set:
    #     if int(str(i)[::-1]) in rtn_set and i < m and str(i) != str(i)[::-1]:
    #         final_rtn_set.add(i)
    return [len(final_rtn_set), max(final_rtn_set), sum(final_rtn_set)] if len(final_rtn_set) > 0 else [0, 0, 0]


# start = time.time()
# r = find_emirp(781493)
# end = time.time()


def largest_sum(arr):
    # Compute the Largest Sum of all Contiguous Subsequences
    biggest, cur = 0, 0
    for v in arr:
        if cur + v > 0:
            cur += v
            biggest = max(biggest, cur)
        else:
            cur = 0
    return biggest


# r = largest_sum([195, -189, -179, 179, 162])


def count_inversions(array):
    # Calculate number of inversions in array
    length = len(array)
    inversions = 0
    for i in range(length):
        m, n = 0, 1
        while m < n < length - i:
            if array[m] > array[n]:
                inversions += 1
                array[m], array[n] = array[n], array[m]
            print(inversions, 'after', array)
            m += 1
            n += 1
    return inversions


# r = count_inversions([6,5,4,3,3,3,3,2,1])


def light_bulbs(lights, n):
    # Simple Fun #219: Light Bulbs
    arr = [1, 0]
    for i in range(n):
        lights_next = []
        for k, v in enumerate(lights):
            if lights[k - 1] == 1:
                lights_next.append(arr[v])
            else:
                lights_next.append(v)
        lights = lights_next.copy()
    return lights


# r = light_bulbs([0,0,1,1,1], 5),  [1,1,1,0,1]


def point_vs_vector(point, vector):
    # [Geometry A-1] Locate point - to the right, to the left or on the vector?
    v1 = [point[0] - vector[0][0], point[1] - vector[0][1]]
    v2 = [vector[1][0] - vector[0][0], vector[1][1] - vector[0][1]]
    cross_product = v1[0] * v2[1] - v1[1] * v2[0]
    if cross_product > 0:
        return 1
    elif cross_product < 0:
        return -1
    else:
        return 0


# r = point_vs_vector([9509, 1474], [[-3259, -7172], [-8383, 293]])


def cantor(n: int) -> str:
    # Cantor's pairing function
    x = int((2 * n + 0.25) ** 0.5 - 0.5)
    dif = n - x * (x + 1) // 2
    if dif == 0:
        if x % 2 == 0:
            return f'{x}/1'
        else:
            return f'1/{x}'
    else:
        if x % 2 == 0:
            return f'{x - dif + 2}/{dif}'
        else:
            return f'{dif}/{x - dif + 2}'


# r = cantor(7)


def cantor2(nested_list):
    # Cantor's Diagonals
    arr = [1, 0]
    return [arr[v[k]] for k, v in enumerate(nested_list)]


example5 = [[1, 0, 0],
            [0, 0, 0],
            [0, 0, 1]]


# r = cantor2(example5)


def valid_parentheses(s):
    # Valid Parentheses
    stack = []
    for i in s:
        if i == ')':
            if len(stack) == 0 or stack[-1] != '(':
                return False
            else:
                stack.pop(-1)
        elif i == '(':
            stack.append(i)
        else:
            continue
    else:
        if len(stack) != 0:
            return False
    return True


# r = valid_parentheses("hi(hi))")


def count_islands(matrix):
    # Count the Islands
    def dfs(row, col):
        if row < 0 or row >= len(matrix) or col < 0 or col >= len(matrix[0]) or matrix[row][col] != 1:
            return 0
        matrix[row][col] = -1  # mark visited
        dfs(row + 1, col)
        dfs(row - 1, col)
        dfs(row, col + 1)
        dfs(row, col - 1)
        dfs(row - 1, col - 1)
        dfs(row + 1, col + 1)
        dfs(row - 1, col + 1)
        dfs(row + 1, col - 1)

    count = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if matrix[i][j] == 1:
                dfs(i, j)
                count += 1
    return count


image = [
    [0, 0, 0, 0],
    [0, 1, 1, 0],
    [0, 1, 1, 0],
    [0, 0, 0, 0]
]


# r = count_islands(image)


def count_checkerboard(width, height, resolution):
    # Checkerboard Resolution
    # 横向只有两种纹路，白开始和黑开始，计算两种纹路的黑格数
    x, y = width.__divmod__(resolution * 2)
    white_line = x * resolution + max(0, y - resolution)
    black_line = x * resolution + min(resolution, y)
    # 计算一共有几个百行和黑行
    x, y = height.__divmod__(resolution * 2)
    white = x * resolution + min(y, resolution)
    black = height - white

    return white * white_line + black * black_line


# r = count_checkerboard(11, 6, 1)


def to_bcd(n):
    if n < 0:
        sign = "-"
        n = abs(n)
    else:
        sign = ""
    bcd = ""
    while n > 0:
        bcd = f"{n % 10:04b} {bcd}"
        n //= 10
    if not bcd:
        bcd = "0000"
    return sign + bcd.rstrip()


# r = to_bcd(-10)

def candies_to_buy(amount_of_kids_invited):
    # Kids and candies
    list_of_kids = list(range(1, amount_of_kids_invited + 1))
    return math.lcm(*list_of_kids)


# r = candies_to_buy(10)


def get_card(n):
    if (n % 2 == 0 and (n // 2) % 2 == 1) or n == 4:
        return n // 2
    else:
        return 1


def card_game(n):
    # Card Game
    alice = 0
    bob = 0
    flag = True
    while n > 0:
        this_turn = get_card(n)
        if flag:
            alice += this_turn
        else:
            bob += this_turn
        flag = not flag
        n -= this_turn
    return alice


# r = card_game(100000000000)


def find_arr(s):
    s_arr = combinations(str(s), 2)
    return [int(i[0]) + int(i[1]) for i in s_arr]


# r = find_arr(156)

def find_number(s):
    # Going backwards: Number from every possible sum of two digits
    if len(s) == 1:
        if s[0] < 10:
            return s[0]
        else:
            a = s[0] // 2
            return int(f'{a}{s[0] - a}')
    lens = int((2 * len(s)) ** 0.5) + 1
    c_b = s[1] - s[0]
    cpb = s[lens - 1]
    b = (cpb - c_b) // 2
    a = s[0] - b
    rtn_arr = [a, b]
    for i in range(2, lens):
        rtn_arr.append(s[i - 1] - a)
    return int(''.join(str(i) for i in rtn_arr))


# r = find_number([16])


def fusc(n):
    assert type(n) == int and n >= 0
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n % 2 == 0:
        return fusc(int(n / 2))
    else:
        return fusc(int(n / 2)) + fusc(int(n / 2) + 1)


# r = fusc(85)


def path_finder_failed(area):
    # Path Finder #3: the Alpinist
    # 代码有问题，只向右和向下，没有考虑先向下向右再向上，走M型或者Z型的场景
    arr = [[int(i) for i in line] for line in area.split('\n')]
    lens = len(arr)
    # create an arr filled with 0
    step_arr = [[0 for _ in range(lens)] for _ in range(lens)]
    for line_num, line in enumerate(arr):
        for col_num, value in enumerate(line):
            if line_num == 0:
                if col_num == 0:
                    step_arr[line_num][col_num] = 0
                else:
                    step_arr[line_num][col_num] = step_arr[line_num][col_num - 1] + abs(
                        value - arr[line_num][col_num - 1])
            elif col_num == 0:
                step_arr[line_num][col_num] = step_arr[line_num - 1][col_num] + abs(value - arr[line_num - 1][col_num])
            else:
                line_up = step_arr[line_num - 1][col_num]
                col_left = step_arr[line_num][col_num - 1]
                step_arr[line_num][col_num] = min(col_left + abs(value - arr[line_num][col_num - 1]),
                                                  line_up + abs(value - arr[line_num - 1][col_num]))
    # print(line_num, col_num, lens, step_arr)
    for k, v in enumerate(step_arr):
        print(k, v)
    return step_arr[lens - 1][lens - 1]


h = """892867393010233323838
598847235652636429537
134569276858538093301
935100322012691891651
602305800670537330054
989501499446930273233
880554921933963068249
173932092521063999031
245643258628875895437
634525080387357275760
183482068820534846922
247994083285739164353
180094528683265162423
444728530940369462052
095867656476376201035
298341525202325841719
112435313507562874040
541530376720252787623
780033956791675167359
467697854986620436111
226110895318511953914"""

# should return 52 not 54
e = "\n".join([
    "700000",
    "077770",
    "077770",
    "077770",
    "077770",
    "000007"
])
g = """217323
564587
318067
880251
107522
145928"""


def path_finder_3(area):
    # Path Finder #3: the Alpinist
    # 牛逼，自己搞定了
    area = area.split('\n')
    lens = len(area)
    # path_arr = []
    # 一定要使用变相的广度遍历，这里用字典来存，要找到每个点相邻的最小差，然后入字典，当一个点的周围都被遍历完毕，则该点的最小值就有了
    # for i in range(lens):
    #     line = []
    #     for j in range(lens):
    #         line.append('X')
    #     path_arr.append(line)

    to_search_list = {"0_0": 0}  # 存储不确定的清单
    searched_list = set()

    while True:
        # 取 to_search_list 里最小值的项目，这个一定就是最小的路径，遍历之
        # print(to_search_list)
        min_key = min(to_search_list, key=to_search_list.get)
        steps = to_search_list.pop(min_key)
        x, y = min_key.split("_")
        x, y = int(x), int(y)
        # path_arr[x][y] = steps
        if x == y == lens - 1:
            # for i in range(lens):
            #     print(path_arr[i])
            return steps

        # 从4个方向找，把最小节点的4个方向，加入到不确定清单中
        # print("checking", x, y, steps)
        searched_list.add(min_key)

        if y - 1 >= 0 and f"{x}_{y - 1}" not in searched_list:
            move_left = abs(int(area[x][y - 1]) - int(area[x][y])) + steps
            to_search_list[f"{x}_{y - 1}"] = min(to_search_list.get(f"{x}_{y - 1}", move_left), move_left)
        if y + 1 < lens and f"{x}_{y + 1}" not in searched_list:
            move_right = abs(int(area[x][y + 1]) - int(area[x][y])) + steps
            to_search_list[f"{x}_{y + 1}"] = min(to_search_list.get(f"{x}_{y + 1}", move_right), move_right)
        if x - 1 >= 0 and f"{x - 1}_{y}" not in searched_list:
            move_up = abs(int(area[x - 1][y]) - int(area[x][y])) + steps
            to_search_list[f"{x - 1}_{y}"] = min(to_search_list.get(f"{x - 1}_{y}", move_up), move_up)
        if x + 1 < lens and f"{x + 1}_{y}" not in searched_list:
            move_down = abs(int(area[x + 1][y]) - int(area[x][y])) + steps
            to_search_list[f"{x + 1}_{y}"] = min(to_search_list.get(f"{x + 1}_{y}", move_down), move_down)


# r = path_finder(g)
#
# print(r)


def fib_digits(n):
    # Calculate Fibonacci return count of digit occurrences
    a, b = 0, 1
    while n > 0:
        a, b = b, a + b
        n -= 1
    x = Counter(str(a))
    return sorted([(i[1], int(i[0])) for i in x.items()], key=lambda x: (x[0], x[1]), reverse=True)


# r = fib_digits(10)


def sum_of_regular_numbers(arr):
    # Simple Fun #191: Sum Of Regular Numbers
    result = 0
    i = -1
    while i < len(arr) - 2:
        i += 1
        step = arr[i + 1] - arr[i]
        j = i + 1
        while j < len(arr) - 1 and arr[j + 1] - arr[j] == step:
            j += 1
        if j - i >= 2:
            result += sum(arr[i:j + 1])
            i = j - 1
        print(i, j, step, result)
    return result


def sum_of_regular_numbers_2(arr):
    res, value, save = 0, arr[1] - arr[0], arr[:2]
    for x, y in zip(arr[1:], arr[2:]):
        if y - x == value:
            save.append(y)
        else:
            if len(save) >= 3: res += sum(save)
            value, save = y - x, [x, y]
    if len(save) >= 3: res += sum(save)
    return res


# r = sum_of_regular_numbers([59, 58, 57, 55, 53, 51])


def rocks(n):
    # Simple Fun #151: Rocks
    rtn_num = 0
    for i in range(1, len(str(n)) + 1):
        if n < 10 ** i:
            rtn_num += (n - 10 ** (i - 1) + 1) * i
        else:
            rtn_num += 9 * 10 ** (i - 1) * i
    return rtn_num


# r = rocks(36123011)

# def solution(arr):
#     # Millipede of words
#     for i in iter(permutations(arr)):
#         for k, v in enumerate(i[:-1]):
#             if v[-1] != i[k+1][0]:
#                 break
#         else:
#             return True
#     return False


# r = solution(['e', 't', 'east'])


def title_to_number(title):
    # Excel sheet column numbers
    rtn_num = 0
    for k, v in enumerate(title[::-1]):
        rtn_num += (ord(v) - 64) * 26 ** k
    return rtn_num


# r = title_to_number('AB')

def find_duplicate_phone_numbers(phoneNumbers):
    # Simple Fun #186: Duplicate Phone Numbers
    alp = "ABCDEFGHIJKLMNOPRSTUVWXY1234567890"
    number_dict = str.maketrans('ABCDEFGHIJKLMNOPRSTUVWXY1234567890', '2223334445556667778889991234567890')
    rtn_dict = []
    for p in phoneNumbers:
        v = "".join(i for i in p if i.upper() in alp).upper().translate(number_dict)
        v = f"{v[:3]}-{v[3:]}"
        rtn_dict.append(v)
    print('p', rtn_dict)
    rtn_arr = []
    for p in set(rtn_dict):
        if rtn_dict.count(p) > 1:
            rtn_arr.append(f"{p}:{rtn_dict.count(p)}")
    return sorted(rtn_arr)


testarr = [
    "4873279",
    "ITS-EASY",
    "888-4567",
    "3-10-10-10",
    "888-GLOP",
    "TUT-GLOP",
    "967-11-11",
    "310-GINO",
    "F101010",
    "888-1200",
    "-4-8-7-3-2-7-9-",
    "487-3279"]
result = [
    "310-1010:2",
    "487-3279:4",
    "888-4567:3"]


# r = find_duplicate_phone_numbers(testarr)


def pairwise(arr, n):
    # Simple Fun #162: Pair Wise
    found_index = set()
    for k, v in enumerate(arr):
        if k in found_index:
            continue
        kk = k + 1
        while kk < len(arr):
            if kk not in found_index:
                if arr[k] + arr[kk] == n:
                    found_index.add(k)
                    found_index.add(kk)
                    break
            kk += 1
    return sum(found_index)


# r = pairwise([1, 4, 2, 3, 0, 5],7)


def replace_dashes_as_one(s):
    # Simple Fun #161: Replace Dashes As One
    # n = 1
    # while n > 0:
    #     s, n = re.subn(r'-\s*-', '-', s)
    # return s
    return re.sub("-[\s-]*-", '-', s)


# r = replace_dashes_as_one("we-are- - - code----warriors.-")
# "we-are- code-warriors.-"


def greatest_distance(arr):
    # Greatest Position Distance Between Matching Array Values
    rtn_max = 0
    for k, v in enumerate(arr):
        if arr[::-1].index(v) + k == len(arr) - 1:
            continue
        print(k, v, arr[::-1].index(v), len(arr))
        rtn_max = max(len(arr) - arr[::-1].index(v) - k - 1, rtn_max)
    return rtn_max


def greatest_distance_2(arr):
    return max(i - arr.index(x) for i, x in enumerate(arr))


# r = greatest_distance([3, 3, 1, 1, -5, -1, 4, -4, -4, 1, -5, -2])


def prefix_sums_to_suffix_sums(prefix_sums):
    # Simple Fun #250: Prefix Sums To Suffix Sums
    last_one = prefix_sums[-1]
    rtn_arr = [last_one]
    for k, v in enumerate(prefix_sums[:-1]):
        rtn_arr.append(last_one - prefix_sums[k])
    return rtn_arr


# r = prefix_sums_to_suffix_sums([1, -4, 2, 90, 100, -1])


def spaghetti_code(plate):
    # Spaghetti Code - #2 Hard
    # 找到所有字母的位置
    count_dict = {}
    spaghetti_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    searched_list = set()
    to_search_list = []
    width = len(plate[0])
    height = len(plate)
    for i in range(height):
        for j in range(width):
            if (i, j) not in searched_list and plate[i][j] in spaghetti_list:
                to_search_list.append((i, j))
                count = 0
                name = 'S'
                while len(to_search_list) > 0:
                    x, y = to_search_list.pop()
                    count += 1
                    searched_list.add((x, y))
                    if plate[x][y] != 'S':  # 找名字
                        name = plate[x][y]
                    if x - 1 >= 0 and (x - 1, y) not in searched_list and plate[x - 1][y] in spaghetti_list:
                        to_search_list.append((x - 1, y))
                    if x + 1 < height and (x + 1, y) not in searched_list and plate[x + 1][y] in spaghetti_list:
                        to_search_list.append((x + 1, y))
                    if y - 1 >= 0 and (x, y - 1) not in searched_list and plate[x][y - 1] in spaghetti_list:
                        to_search_list.append((x, y - 1))
                    if y + 1 < width and (x, y + 1) not in searched_list and plate[x][y + 1] in spaghetti_list:
                        to_search_list.append((x, y + 1))
                count_dict[name] = count
    return max(count_dict, key=count_dict.get) if len(count_dict) > 0 else ""


sample_test_cases = [
    [*'SSSSSSSSS      '],
    [*'________S__SSS_'],
    [*' S   S  A    S '],
    [*'_S___S__S____S_'],
    [*' B   S       S '],
    [*'_S___SSSSSCSSS_'],
]


# r = spaghetti_code(sample_test_cases)


def order_weight(strng):
    # Weight for weight
    arr = strng.split(" ")
    return ' '.join(sorted(arr, key=lambda x: (sum(int(i) for i in x), x)))


# r = order_weight("2000 10003 1234000 44444444 9999 11 11 22 123")


def abbr_long(s):
    d = {
        "Alaska": "AK",
        "Arizona": "AZ",
        "Connecticut": "CT",
        "Georgia": "GA",
        "Hawaii": "HI",
        "Iowa": "IA",
        "Kansas": "KS",
        "Kentucky": "KY",
        "Louisiana": "LA",
        "Maine": "ME",
        "Maryland": "MD",
        "Minnesota": "MN",
        "Mississippi": "MS",
        "Missouri": "MO",
        "Montana": "MT",
        "Nevada": "NV",
        "Pennsylvania": "PA",
        "Tennessee": "TN",
        "Texas": "TX",
        "Vermont": "VT",
        "Virginia": "VA",
        "Northern Mariana Islands": "MP",
        "U.S. Virgin Islands": "VI"
    }
    if s in d.keys(): return d[s]
    if s.find(' ') > 0: return s.split(' ')[0][0] + s.split(' ')[-1][0]
    return s[:2].upper()


N = "Alab,Alas,Am,Ari,Ark,Ca,Col,Con,De,Di,F,Ge,Gu,H,Id,Il,In,Io,Ka,Ke,L,Mai,Mar,Mas,Mic,Min,Missi,Misso,Mo,Neb,Nev," \
    "New H,New J,New M,New Y,North C,North D,Northe,Oh,Ok,Or,Pe,Pu,R,South C,South D,Ten,Tex,U.,Ut,Ve,Vi,Wa,We,Wi," \
    "Wy".split(",")
C = "AL AK AS AZ AR CA CO CT DE DC FL GA GU HI ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND MP " \
    "OH OK OR PA PR RI SC SD TN TX VI UT VT VA WA WV WI WY".split()


def abbr(s):
    # US Postal Codes
    return next(c for n, c in zip(N, C) if s.startswith(n))


# r = abbr("Alabama")


# from itertools import permutations
#
# history = []
# possible_solutions = []
#
# def guess(matches):
#     # digits
#     # using these variables as globals allows us to store data
#     # between the multiple times this function is called without
#     # having to create a class!
#     global possible_solutions
#     global history
#
#     # First time the "guesser" is run
#     if matches == -1:
#         # generate all possible 4-digit permutations
#         #     [1,2,3,4] is not the same as [4,3,2,1] with permutations,
#         #     but they are the same with combinations!
#         possible_solutions = list(permutations([0,1,2,3,4,5,6,7,8,9], 4))
#
#         # empty the history (for when this function is used with multiple "sessions")
#         # where the list changes and we're given another matches = -1 input
#         history = []
#
#         # start with the most basic guess, because why not
#         # no matter what our code down below will give us
#         # valuable information based on how many of the
#         # numbers in this guess are "matches"
#         first = [0,1,2,3]
#
#         # add the first guess to the history
#         history.append(first)
#
#         # return the first guess
#         return first
#     else:
#         # go through all possible solutions and
#         # remove the ones that dont share exatly
#         # 'match' number of values
#         possible_solutions = [
#             sol for sol in possible_solutions
#             if sum(x == y for x, y in zip(history[-1], sol)) == matches
#         ]
#         # To explain the comment above better, let's assume our first guess gets 2 matches:
#         #
#         # We need to go through all possible solutions and remove the ones
#         # that don't share *exactly* 2 values with our previous guess.
#         #
#         # If any of the possible solutions share exactly 2 values with the previous guess,
#         # we keep it since we know somewhere in those solutions is the correct guess given
#         # they all share at least 2 numbers with the previous guess, and we guessed 2 numbers
#         # correctly
#         #
#         # If any of the possible solutions share all 4 values, we know it's not worth keeping
#         # since we only matched on 2 and this "solution" is invalid!
#         #
#         # If any possible solution shares 3 numbers with our previous guess,
#         # one of the shared digits is guaranteed to be incorrect since we only matched 2
#         #
#         # If any possible solution shares only 1 number with our previous guess,
#         # we're missing one of the two correct numbers, so we can throw these away
#         #
#         # If our previous solution shares 0 numbers with any possible solution, and we matched on two,
#         # we can safely throw these possible solutions away since they doesn't have the two matching values
#         #
#         # By filtering this way, we can cut the number of possibilities down significantly,
#         # no matter how many "correct" values are in our previous solution!
#         #
#         # Even if we get 0 matches, it still tells us useful information for filtering!
#
#         # store the next guess
#         history.append(possible_solutions[0])
#
#         # for the sake of simplicity, we'll return the first possible solution
#         return possible_solutions[0]
#
#     return previous
#     # 这个return previous有点意思，返回上一个？

def find_missing_letter(chars):
    # Find the missing letter
    for i, j in zip(chars[:-1], chars[1:]):
        if ord(i) + 1 != ord(j):
            return chr(ord(i) + 1)


# r = find_missing_letter(['a','b','c','d','f'])


def cakes(recipe, available):
    # Pete, the baker
    return min(available.get(k, 0) // v for k, v in recipe.items())


# recipe = {"flour": 500, "sugar": 200, "eggs": 1}
# available = {"flour": 1200, "sugar": 1200, "eggs": 5, "milk": 200}
# r = cakes(recipe, available)


def valid_braces(s):
    # Valid Braces
    str_stack = []
    left_str = "{(["
    right_str = "})]"

    for i in s:
        if i in left_str:
            str_stack.append(i)
        elif i in right_str:
            if len(str_stack) == 0:
                return False
            cur = str_stack.pop()
            if right_str.index(i) != left_str.index(cur):
                return False
    else:
        if len(str_stack) > 0:
            return False
    return True


# r = valid_braces( "())({}}{()][][" )


def pig_it(text):
    # Simple Pig Latin
    return ' '.join(f"{i[1:]}{i[0]}ay" if i.isalpha() else i for i in text.split())


# r = pig_it('Hello world !')


def solution_Encoder(n):
    # Roman Numerals Encoder
    rtn_str = ""
    dict_day = {"M": 1000, "CM": 900, "D": 500, "CD": 4100, "C": 100, "XC": 90, "L": 50, "XL": 40,
                "X": 10, "IX": 9, "V": 5, "IV": 4, "I": 1}
    for k, v in dict_day.items():
        rtn_str += k * (n // v)
        n = n % v
    return rtn_str


# r = solution(91)
# ,'MCMLXXXIX


def solution_Decoder(roman):
    # Roman Numerals Decoder
    """complete the solution by transforming the roman numeral into an integer"""
    dict_day = {"M": 1000, "CM": 900, "D": 500, "CD": 400, "C": 100, "XC": 90, "L": 50, "XL": 40,
                "X": 10, "IX": 9, "V": 5, "IV": 4, "I": 1}
    rtn_num = 0
    loc = 0
    while loc < len(roman):
        for day, dayvalue in sorted(dict_day.items(), key=lambda item: item[1], reverse=True):
            if roman[loc:].find(day) == 0:
                rtn_num += dayvalue
                loc += len(day)
                break
    return rtn_num


# r = solution('IV')

# @lru_cache(maxsize=None)
# def solution(s, t):
#     # Chaser's schedule
#     # RecursionError: maximum recursion depth exceeded in comparison
#     # 后续应该考虑一下递归转循环，否则数字再大一些就会达到recursion深度，当前是okay的
#     if t == 0 or s == 0:
#         return 0
#     elif t == 1:
#         return 2*s
#     elif t == 2:
#         return 3*s
#     elif t > 2:
#         return max(2 * s + solution(s, t-2), s + solution(s, t-1), 3 * s - 1 + solution(s - 1, t - 2))


def solution_Chaser(s, t):
    # Chaser's schedule
    # 这个方法真绝了， 是是纯粹用数学的方式来解决了
    n = min((t - 1) // 2, s // 3)
    return t * s + (n + 1) * s - 3 * (n + 1) * n // 2 if t else 0


# r = solution_Chaser(35, 266)
#
# print(r)


MORSE_CODE = {'.-': 'A', '-...': 'B', '-.-.': 'C', '-..': 'D', '.': 'E', '..-.': 'F', '--.': 'G', '....': 'H',
              '..': 'I', '.---': 'J', '-.-': 'K', '.-..': 'L', '--': 'M', '-.': 'N', '---': 'O', '.--.': 'P',
              '--.-': 'Q', '.-.': 'R', '...': 'S', '-': 'T', '..-': 'U', '...-': 'V', '.--': 'W', '-..-': 'X',
              '-.--': 'Y', '--..': 'Z', '-----': '0', '.----': '1', '..---': '2', '...--': '3', '....-': '4',
              '.....': '5', '-....': '6', '--...': '7', '---..': '8', '----.': '9', '.-.-.-': '.', '--..--': ',',
              '..--..': '?', '.----.': "'", '-.-.--': '!', '-..-.': '/', '-.--.': '(', '-.--.-': ')', '.-...': '&',
              '---...': ':', '-.-.-.': ';', '-...-': '=', '.-.-.': '+', '-....-': '-', '..--.-': '_', '.-..-.': '"',
              '...-..-': '$', '.--.-.': '@', '...---...': 'SOS'}


def decode_morse(morse_code):
    # Decode the Morse code
    morse_code = morse_code.strip().replace("   ", " ! ")
    rtn_str = ""
    for i in morse_code.split():
        rtn_str += MORSE_CODE.get(i, " ")
    return rtn_str


# r = decode_morse('.... . -.--   .--- ..- -.. .')


def decode_bits(bits):
    # Decode the Morse code, advanced
    # 找到最长的连续1，进行切分，看是否切分后的每个单元都全是0或者全是1
    bits = bits.strip("0")
    unit_len = 0
    for i in range(len(bits), 0, -1):
        arr = [bits[i * j:i * j + i] for j in range(len(bits) // i + 1)]
        print(arr)
        if all(len(set(v)) == 1 for v in arr if v):
            unit_len = i
            break
    s = bits.replace('1' * unit_len, '.').replace('0' * unit_len, ' ')
    return s.replace("       ", "A").replace("   ", "!").replace("...", "-").replace(" ", "").replace("!", " ").replace(
        "A", "   ")


# r = decode_bits("101")

def first_non_repeating_letter(s):
    # First non-repeating character
    for i in s:
        if s.lower().count(i.lower()) == 1:
            return i
    else:
        return ""


# r = first_non_repeating_letter('abba')


def solution(args):
    # Range Extraction
    rtn_arr = []
    temp_queue = []
    for i in args:
        if len(temp_queue) == 0 or temp_queue[-1] + 1 == i:
            temp_queue.append(i)
        else:
            if len(temp_queue) < 2:
                rtn_arr.append(f"{temp_queue[0]}")
            elif len(temp_queue) < 3:
                rtn_arr.append(f"{temp_queue[0]}")
                rtn_arr.append(f"{temp_queue[1]}")
            else:
                rtn_arr.append(f"{temp_queue[0]}-{temp_queue[-1]}")
            temp_queue.clear()
            temp_queue.append(i)
        print(rtn_arr, "hi", temp_queue)
    else:
        if len(temp_queue) < 2:
            rtn_arr.append(f"{temp_queue[0]}")
        elif len(temp_queue) < 3:
            rtn_arr.append(f"{temp_queue[0]}")
            rtn_arr.append(f"{temp_queue[1]}")
        else:
            rtn_arr.append(f"{temp_queue[0]}-{temp_queue[-1]}")
    return ','.join(rtn_arr)


# r = solution([-6,-3,-2,-1,0,1,3,4,5,7,8,9,10,11,14,15,17,18,19,20])
# , '-6,-3-1,3-5,7-11,14,15,17-20')

def permutations(s):
    # So Many Permutations!
    return set(''.join(i) for i in pmts(s, len(s)))


# r = sorted(permutations('aabb'))
# , ['aabb', 'abab', 'abba', 'baab', 'baba', 'bbaa'])


def parse(d):
    # Make the Deadfish Swim
    rtn_arr = []
    start = 0
    for i in d:
        if i == "i":
            start += 1
        elif i == "d":
            start -= 1
        elif i == "s":
            start *= start
        elif i == "o":
            rtn_arr.append(start)
    return rtn_arr


# r = parse("isoisoiso")


def list_squared(m, n):
    # Integers: Recreation One
    rtn_arr = []
    for z in range(m, n + 1):
        rtn_set = set()
        for i in range(1, int(z ** 0.5) + 1):
            x, y = divmod(z, i)
            if y == 0:
                rtn_set.add(i)
                rtn_set.add(x)
        sumn = sum(i ** 2 for i in rtn_set)
        if (sumn ** 0.5).is_integer():
            rtn_arr.append([z, sumn])
    return rtn_arr


# r = list_squared(250, 500)


a = """.......WW.WW...W..WW...WWWW...
WWWW..W...W......W....W.....W.
.W...W.W.........W..WWW....W.W
....WW.W..W..WW.W....W..WW.WW.
.WW.W..WWW....W...W....WW...W.
...W.....WW.....W....W...WWW.W
....W..WWW.WWW.W....WW.....W.W
......W.W.W.W...W.......WW.W.W
WWW.W..W.....W....WW..W..W....
.W...WWW..W..W.W.WW..W..W....W
.W...W.WWW.....W.W......W...W.
...WW..W.W...W.W.W.W...W.....W
.WW......W.W......WW..W..W..W.
...W.W...WW..W....W......WW...
.W..W..W.WW....WW.W.W.WWWW....
W.W.....WW.W..WW...W..W.WW.WW.
W..W.W.W..W.......W.....WW....
.....W...W...W....W.....WWW..W
.WW.W.W..WW..W.W..W.W...W.W..W
..WW..WW.......W....W.....W..W
W...W.W.W.W.........W..W...WW.
W..WW...W...W...W.W.....WW....
.W..W...W........W.W....W.....
......W....WW....WWW...W.W.WWW
W...WW.W....W..W.WW...W.......
.WW.W..W....W.WW....WWW...W.WW
.W............W.W..........W..
........W.W.WW...WWW..........
WWW.W..WW...W..WW...W....W.W..
W....WW....W.W.WWW.WW...WWWW.."""


def path_finder_1(maze):
    # Path Finder #1: can you reach the exit?
    maze = maze.split('\n')
    N = len(maze)
    searched_point = set()
    to_search_list = [(0, 0)]
    while len(to_search_list):
        x, y = to_search_list.pop()
        if x == y == N - 1:
            return True
        searched_point.add((x, y))
        if x - 1 >= 0 and (x - 1, y) not in searched_point and maze[x - 1][y] != "W":
            to_search_list.append((x - 1, y))
        if x + 1 < N and (x + 1, y) not in searched_point and maze[x + 1][y] != "W":
            to_search_list.append((x + 1, y))
        if y - 1 >= 0 and (x, y - 1) not in searched_point and maze[x][y - 1] != "W":
            to_search_list.append((x, y - 1))
        if y + 1 < N and (x, y + 1) not in searched_point and maze[x][y + 1] != "W":
            to_search_list.append((x, y + 1))
    return False


# r = path_finder(a)


def path_finder(maze):
    # Path Finder #2: shortest path
    maze = maze.splitlines()
    N = len(maze)
    searched_point = set()
    to_search_list = {"0_0": 0}
    step = 0

    while len(to_search_list):
        min_key = min(to_search_list, key=to_search_list.get)
        step = to_search_list.pop(min_key)
        x, y = min_key.split("_")
        x, y = int(x), int(y)
        if x == y == N - 1:
            return step
        searched_point.add(min_key)

        for d_x, d_y in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            i, j = x + d_x, y + d_y
            if 0 <= i < N and 0 <= j < N:
                if maze[i][j] != 'W' and f"{i}_{j}" not in searched_point:
                    to_search_list[f"{i}_{j}"] = min(to_search_list.get(f"{i}_{j}", 99999), step + 1)
    return False


c = "\n".join([
    "......",
    "......",
    "......",
    "......",
    "......",
    "......"
])

d = "\n".join([
    "......",
    "......",
    "......",
    "......",
    ".....W",
    "....W."
])


# r = path_finder(c)


def validate(n):
    # Validate Credit Card Number
    n = [int(i) for i in str(n)]
    if len(n) % 2 == 0:
        n = [v * 2 if k % 2 == 0 else v for k, v in enumerate(n)]
    else:
        n = [v * 2 if k % 2 == 1 else v for k, v in enumerate(n)]
    n = [v if v < 10 else v - 9 for v in n]
    return sum(n) % 10 == 0


# r = validate(2121)


def int32_to_ip(int32):
    # int32 to IPv4
    arr = []
    for i in range(3, -1, -1):
        x, int32 = divmod(int32, 256 ** i)
        arr.append(x)
    return '.'.join(str(i) for i in arr)


# r = int32_to_ip(2154959208)


class Sudoku(object):
    # Validate Sudoku with size `NxN`
    def __init__(self, data):
        self.board = data
        self.num_set = set(range(1, len(data)+1))
        self.N = int(len(data) ** 0.5)

    def is_valid(self):
        for line in self.board:
            for col in line:
                if type(col) != int:
                    return False
        for k, v in enumerate(self.board):
            if set(v) != self.num_set:
                return False

        transposed = list(map(list, zip(*self.board)))
        for k, v in enumerate(transposed):
            if set(v) != self.num_set:
                return False

        for line in range(0, self.N ** 2, self.N):
            for col in range(0, self.N ** 2, self.N):
                temp_set = set(self.board[line][col:col + self.N])
                for k in range(1, self.N):
                    temp_set = temp_set | set(self.board[line + k][col:col + self.N])
                if temp_set != self.num_set:
                    return False
        return True

goodSudoku2 = Sudoku([[True]])

# r = goodSudoku2.is_valid()
# r = int32_to_ip(2154959208)


class RomanNumerals:
    @staticmethod
    def to_roman(n):
        rtn_str = ""
        dict_day = {"M": 1000, "CM": 900, "D": 500, "CD": 400, "C": 100, "XC": 90, "L": 50, "XL": 40,
                    "X": 10, "IX": 9, "V": 5, "IV": 4, "I": 1}
        for k, v in dict_day.items():
            rtn_str += k * (n // v)
            n = n % v
        return rtn_str

    @staticmethod
    def from_roman(roman):
        dict_day = {"M": 1000, "CM": 900, "D": 500, "CD": 400, "C": 100, "XC": 90, "L": 50, "XL": 40,
                    "X": 10, "IX": 9, "V": 5, "IV": 4, "I": 1}
        rtn_num = 0
        loc = 0
        while loc < len(roman):
            for day, dayvalue in sorted(dict_day.items(), key=lambda item: item[1], reverse=True):
                if roman[loc:].find(day) == 0:
                    rtn_num += dayvalue
                    loc += len(day)
                    break
        return rtn_num

# r = RomanNumerals.from_roman('MMVIII')
#
# print(r)

# r = RomanNumerals.to_roman(2415)


def ip2int(addr):
    octets = list(map(int, addr.split('.')))
    return (octets[0] << 24) + (octets[1] << 16) + (octets[2] << 8) + octets[3]


def ips_between(start, end):
    # Count IP Addresses
    start_int = ip2int(start)
    end_int = ip2int(end)
    return end_int - start_int


# r = ips_between("20.1.1.0", "20.1.0.50")


def triple_double(num1, num2):
    # Triple trouble
    for i in range(10):
        if f"{i}" * 3 in str(num1) and f"{i}" * 2 in str(num2):
            return 1
    return 0


# r = triple_double(451999277, 41177722899)


def parse_molecule(formula):
    # Molecule to atoms， todo
    # blank 2
    return 1


# r = parse_molecule("K4[ON(SO3)2]2")

# , {'K': 4,  'O': 14,  'N': 2,  'S': 4},

# print(r)


def halving_sum(n):
    # Halving Sum
    rtn_n = n
    step = 1
    while n != 1:
        n = n // (step * 2)
        rtn_n += n
    return rtn_n

# r = halving_sum(25)


def get_pins(observed):
    # The observed PIN
    arr = []
    rtn_arr = []
    phone_dict = {"1": "124",
                  "2": "1235",
                  "3": "236",
                  "4": "1457",
                  "5": "24568",
                  "6": "3569",
                  "7": "478",
                  "8": "57890",
                  "9": "689",
                  "0": "80"}
    for k, v in enumerate(observed):
        arr.append(phone_dict[v])
    for v in list(product(*arr, repeat=1)):
        rtn_arr.append(''.join(v))
    return rtn_arr


r = get_pins('11')


print(r)
