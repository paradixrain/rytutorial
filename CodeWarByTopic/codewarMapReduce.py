# coding=utf-8

def elements_sum(arr, d=0):
    return sum(r[i] if i < len(r) else d for i, r in enumerate(reversed(arr)))


def calculate_total(team1, team2):
    return sum(team1) > sum(team2)


ryan = elements_sum([[3, 2, 1, 0], [4, 6, 5, 3, 2], [9, 8, 7, 4]])
b = elements_sum([[3, 2, 1, 0], [4, 6, 5, 3, 2], [9, 8, 7, 4]])
c = elements_sum([[3, 2, 1, 0], [4, 6, 5, 3, 2], []], 5)
print(ryan)
