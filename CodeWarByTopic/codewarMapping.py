# coding=utf-8
def map_vector(vector, circle1, circle2):
    x1, y1 = vector[0], vector[1]
    c1x, c1y, r1 = circle1[0], circle1[1], circle1[2]
    c2x, c2y, r2 = circle2[0], circle2[1], circle2[2]
    x2 = c2x + (x1 - c1x) * r2 / r1
    y2 = c2y + (y1 - c1y) * r2 / r1
    return x2, y2


def to_1D(x, y, size):
    line, column = size[0], size[1]
    return line * y + x

def to_2D(n, size):
    line, column = size[0], size[1]
    return n % line, n // line


ryan = map_vector((50, 88), (-25, 128, 100), (50, 50, 100))
print(ryan)