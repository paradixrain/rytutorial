# coding = utf-8

import re
import string
from collections import Counter
from re import sub


def high_and_low(numbers):
    n = sorted([int(i) for i in numbers.split()])
    return '{} {}'.format(n[-1], n[0])


def descending_order(num):
    n = ''.join(sorted(str(num), reverse=True))
    print(n)
    return int(n)


def accum(s):
    n = [(s[i].lower() * (i + 1)).title() for i in range(len(s))]
    return '-'.join(n)


def decode(strng):
    return strng.translate(str.maketrans('1234567890', '9876043215'))


# a = decode("4103432323")
# # print(a)
#
# s = "sort the inner content in descending order"
# a = ' '.join(word[0] + ''.join(sorted(word[-2:0:-1], reverse=True)) + word[-1] for word in s.split())
# b = ' '.join(''.join(sorted(word[-2:0:-1], reverse=True)) for word in s.split())


def likes(names):
    people_count = len(names)
    if people_count == 0:
        rtn_str = 'no one likes this'
    elif people_count == 1:
        rtn_str = f'{names[0]} likes this'
    elif people_count == 2:
        rtn_str = f'{names[0]} and {names[1]} like this'
    elif people_count == 3:
        rtn_str = f'{names[0]}, {names[1]} and {names[2]} like this'
    else:
        rtn_str = f'{names[0]}, {names[1]} and {people_count - 2} others like this'
    return rtn_str


def duplicate_count(text):
    t = set(text.lower())
    rtn_count = 0
    for item in t:
        if text.lower().count(item) > 1:
            rtn_count += 1
    return rtn_count


def duplicate_encode(word):
    return ''.join("(" if word.count(c) == 1 else ")" for c in word)


# Dot Calculator
def calculator(txt):
    txt = txt.split(' ')
    e = f'{len(txt[0])} {txt[1]} {len(txt[2])}'
    e = eval(e)
    return '.' * e


def flag():
    FLAG = """dflaskjf; klcqwjer9qieiqucwepirom9028905839045809ds8gf[90g8sd[fh8f-[gh8-09wer0=t]mxcvnmz.xvchku9erpiot789-348tipahjrgfkhzxvxmb,mncxmh6t3q90485ut9rsqklgfjkj#Q$YTEWOPJGPOEDSJC)$%#@^*@$%690uiodrj;lstj3;wpiot8ujdfpgioja90-8-0cx8v#KL%J,,.345.3.l0-0----0000-00---0-----0..-.-.--.....--.-.-.----5345..9.........4.....3.........456...4.........3.....2.........7.....3424.......................564...544......4...5.8...9.....6...3.7.........79345.9.565.....1...1.3...7.....1...1.33452.......................013..89345......4......9......732......8........6......8........9......9........846..78654......................698............8...3............6.....098345.....73............6...6............186...........................79843..9879878....6.......7.......8.......5.......8.......6.......9.......9.....12387.....7............................786982..........8........45623..77833...........9...............109841....................345.....723....5...1...6...2...6...3....5......78234.....45....1...8...7...8...2...3....873....4...5...........................4.....7..........1...5............9.4...93459......1...............1...............1............................4.....3..345634..3...7...3........7.5....3455......1.....3.........2.....867843....1............................5321452............1...............1...............1....743745.....1...............1............................2345754..5.....44........6.....354335....3.....13........4.....42........7542345................4....2...533....52...3.....6....2.3..9.....98...1..7.6.....6....7...88.....5....6....9...964....................------0--0-=0-=-0-00000000-00-0-0-000000,0,,0000jasdkl;jferiotupvn23c014985901c79840p72x812m598723189057m2190857mx98012479812798m1407395813479857m2149857198x0579814320758937981982347384957983g"""

    flags = []

    print(flags)
    block = 0
    # FLAG = FLAG.replace('\n','')
    for i in range(len(FLAG) // 16 + 1):
        line = FLAG[i * 16:(i + 1) * 16]
        print(line)

    print(flags)
    print('\n'.join(flags))

    # Put the flag in this variable
    flag = "FLAG{THIS-IS-NOT-THE-FLAG}"


def alphabet_position(text):
    return " ".join(str(ord(c) - 96) if (96 < ord(c) < 123) else str(ord(c) - 64) for c in text if
                    (96 < ord(c) < 123) or (64 < ord(c) < 91))


def alphabet_position2(text):
    return ' '.join(str(ord(c) - 96) for c in text.lower() if c.isalpha())


def persistence(n):
    from functools import reduce
    count = 0
    i_n = n
    s_n = str(n)
    while i_n > 9:
        i_n = reduce(lambda x, y: x * y, [int(c) for c in s_n])
        s_n = str(i_n)
        count += 1
    return count


def title_case(t, minor_words=''):
    # Title Case
    t = t.title().split()
    print('t', t)
    mw = minor_words.split()
    for i in mw:
        for j in range(1, len(t)):
            if i.lower() == t[j].lower():
                t[j] = i.lower()
    return ' '.join(t)


def mix(s1, s2):
    # Strings Mix
    rtn_dict = dict()
    for i in s1 + s2:
        if i.islower():
            if s1.count(i) == s2.count(i) > 1:
                rtn_dict[i * s1.count(i)] = '=:'
            elif s1.count(i) < s2.count(i) and 1 < s2.count(i):
                rtn_dict[i * s2.count(i)] = '2:'
            elif s2.count(i) < s1.count(i) and 1 < s1.count(i):
                rtn_dict[i * s1.count(i)] = '1:'
            else:
                pass

    rtn_arr = sorted(rtn_dict.items(), key=lambda x: x[0])
    rtn_arr = sorted(rtn_arr, key=lambda x: x[1])
    rtn_arr = sorted(rtn_arr, key=lambda x: len(x[0]), reverse=True)

    return '/'.join(i[1] + i[0] for i in rtn_arr)


def mix2(s1, s2):
    from collections import Counter
    res = []
    c1 = Counter([c for c in s1 if c.islower()])
    c2 = Counter([c for c in s2 if c.islower()])
    for c in c1 | c2:
        if c1[c] > 1 and c1[c] > c2[c]: res += ['1:' + c * c1[c]]
        if c2[c] > 1 and c2[c] > c1[c]: res += ['2:' + c * c2[c]]
        if c1[c] > 1 and c1[c] == c2[c]: res += ['=:' + c * c1[c]]
    return '/'.join(sorted(res, key=lambda a: [-len(a), a]))


# def solve(s):
#     # Consonant value
#     transtab = str.maketrans('aeiou', '     ')
#     s = s.translate(transtab)
#     return max(sum(ord(i)-96 for i in j) for j in s.split(' '))


def rpel(matchobj):
    return str(len(matchobj.group(0)))


def abbreviate(s):
    # abbreviate
    return re.sub(r'(?<=[a-zA-Z])(?P<second>[a-zA-Z]{2,})(?=[a-zA-Z])', rpel, s)


def is_pangram(s):
    # Detect Pangram
    return all(i in s.lower() for i in string.ascii_lowercase)


def camel(matchobj):
    return '-' + matchobj.group(0).lower()


def kebabize(s):
    # Kebabize
    s = re.sub(r'\d', '', s)
    if s:
        s = s[0].lower() + s[1:]
        return re.sub(r'([A-Z])', camel, s)
    return ''


def cat_mouse(x):
    # Cat and Mouse - Easy Version
    return 'Escaped!' if abs(x.index('m') - x.index('C')) > 4 else 'Caught!'


def spacify(s):
    # Spacify
    rtn_str = ""
    for i in range(len(s)):
        rtn_str += s[i]
        if i != len(s) - 1:
            rtn_str += ' '
    return rtn_str


def solve(s):
    # Simple string characters
    a = sum(1 for i in s if i.isupper())
    b = sum(1 for i in s if i.islower())
    c = sum(1 for i in s if i.isdigit())
    return [a, b, c, len(s) - a - b - c]


def contain_all_rots(strng, arr):
    # All Inclusive?
    arr = [''.join(i for i in x if i in strng) for x in arr]
    s = [strng[i:] + strng[:i] for i in range(len(strng))]
    return all(ss in arr for ss in s)


def seven_ate9(str_):
    # SevenAte9
    while str_.replace('797', '77') != str_:
        str_ = str_.replace('797', '77')
    return str_


def solution(pairs):
    # Building Strings From a Hash
    return ','.join(f'{k} = {v}' for (k, v) in sorted(pairs.items(), key=lambda x: str(x[0])))


def split_in_parts(s, part_length):
    # Split In Parts
    return ' '.join(s[i: i + part_length] for i in range(0, len(s), part_length))


ad = (
    "123 Main Street St. Louisville OH 43071,432 Main Long Road St. Louisville OH 43071,786 High Street Pollocksville NY 56432,"
    "54 Holy Grail Street Niagara Town ZP 32908,3200 Main Rd. Bern AE 56210,1 Gordon St. Atlanta RE 13000,"
    "10 Pussy Cat Rd. Chicago EX 34342,10 Gordon St. Atlanta RE 13000,58 Gordon Road Atlanta RE 13000,"
    "22 Tokyo Av. Tedmondville SW 43098,674 Paris bd. Abbeville AA 45521,10 Surta Alley Goodtown GG 30654,"
    "45 Holy Grail Al. Niagara Town ZP 32908,320 Main Al. Bern AE 56210,14 Gordon Park Atlanta RE 13000,"
    "100 Pussy Cat Rd. Chicago EX 34342,2 Gordon St. Atlanta RE 13000,5 Gordon Road Atlanta RE 13000,"
    "2200 Tokyo Av. Tedmondville SW 43098,67 Paris St. Abbeville AA 45521,11 Surta Avenue Goodtown GG 30654,"
    "45 Holy Grail Al. Niagara Town ZP 32918,320 Main Al. Bern AE 56215,14 Gordon Park Atlanta RE 13200,"
    "100 Pussy Cat Rd. Chicago EX 34345,2 Gordon St. Atlanta RE 13222,5 Gordon Road Atlanta RE 13001,"
    "2200 Tokyo Av. Tedmondville SW 43198,67 Paris St. Abbeville AA 45522,11 Surta Avenue Goodville GG 30655,"
    "2222 Tokyo Av. Tedmondville SW 43198,670 Paris St. Abbeville AA 45522,114 Surta Avenue Goodville GG 30655,"
    "2 Holy Grail Street Niagara Town ZP 32908,3 Main Rd. Bern AE 56210,77 Gordon St. Atlanta RE 13000")

code = (
    "OH 43071,NY 56432,ZP 32908,AE 56210,RE 13000,EX 34342,SW 43098,AA 45521,GG 30654,ZP 32908,AE 56215,RE 13200,EX 34345,"
    "RE 13222,RE 13001,SW 43198,AA 45522,GG 30655,XX 32321,YY 45098")


def travel(r, zipcode):
    # Salesman's Travel
    r = r.split(',')
    row = []
    for rr in r:
        ri = rr.split(' ')
        if ' '.join(ri[-2:]) == zipcode:
            row.append((ri[0], ' '.join(ri[1:-2]), ' '.join(ri[-2:])))
    print(row)
    return f'{zipcode}:' + ','.join(f'{i[1]}' for i in row) + '/' + ','.join(f'{i[0]}' for i in row)


def sort_my_string(s):
    # Odd-Even String Sort
    return s[::2] + ' ' + s[1::2]


def solve_(s):
    # Longest vowel chain
    return max(len(i) for i in re.findall(r'[aeiou]+', s))


def fly_by(lamps, drone):
    # Drone Fly-By
    lamps_on = lamps.replace('x', 'o')
    return lamps_on[:drone.index('T') + 1] + lamps[drone.index('T') + 1:]


def capitalize(s):
    # Alternate capitalization
    return [''.join(s[i].upper() if i % 2 == 0 else s[i] for i in range(len(s))),
            ''.join(s[i].upper() if i % 2 == 1 else s[i] for i in range(len(s)))]


def purify(s: str) -> str:
    # The Culling of Stratholme
    # 这个正则表达式真的不会写，太烦人了
    return ' '.join(re.sub(r"[\S]?[iI][^\siI]?", "", s).split())


# a = purify("5I5 ii39i9i1 iiiiiiiiiiiiii1139iii39i i5i9i 1")
# # "13 1")
# print(a)

def find_missing_number(sequence):
    # Broken sequence
    try:
        s = sorted(int(i) for i in sequence.split())
        if not s:
            return 0
        if list(range(1, max(s) + 1)) == s:
            return 0
        else:
            return min(set(range(1, max(s))) - set(s))
    except ValueError:
        return 1


# a = find_missing_number("2 3 4 5 6")
# #,4,"It must work for missing middle terms")
# print(a)


def scale(strng, k, v):
    # Scaling Squared Strings
    rtn_str = ''
    for s in strng.split('\n'):
        sk = ''.join(j * k for j in s)
        sk = '\n'.join([sk] * v)
        rtn_str = rtn_str + sk + '\n'
    return rtn_str.strip('\n')


# Moves in squared strings (II)
def rot(strng):
    arr = [i for i in strng]
    return ''.join(arr[::-1])


def selfie_and_rot(strng):
    arr = strng.split('\n')
    rtn_arr = []
    for i in arr:
        rtn_arr.append(f"{i}{'.' * len(i)}")
    print(rtn_arr)
    rtn_str = '\n'.join(rtn_arr)
    return f'{rtn_str}\n{rot(rtn_str)}'


def oper(fct, s):
    return fct(s)


# a = oper(selfie_and_rot, "xZBV\njsbS\nJcpN\nfVnP")
#            "xZBV....\njsbS....\nJcpN....\nfVnP....\n....PnVf\n....NpcJ\n....Sbsj\n....VBZx")


real_lines = [
    "On the 12th day of Christmas my true love gave to me",
    "12 drummers drumming,",
    "11 pipers piping,",
    "10 lords a leaping,",
    "9 ladies dancing,",
    "8 maids a milking,",
    "5 golden rings,",
    "7 swans a swimming,",
    "6 geese a laying,",
    "4 calling birds,",
    "3 French hens,",
    "2 turtle doves and",
    "a partridge in a pear tree."]


def get_index(x):
    if x == 'a':
        return 1
    elif x == 'On':
        return 99
    else:
        return int(x)


def song_sorter(lines):
    return sorted(lines, key=lambda x: get_index(x.split()[0]), reverse=True)


# a = song_sorter(real_lines)


def vowel_one(s):
    # Vowel one
    return re.sub(r'.', lambda x: '1' if x.group(0) in 'aeiou' else '0', string=s, flags=re.IGNORECASE)


# a = vowel_one("Codewars")


def incrementer(nums):
    # Incrementer
    return [int(i + k + 1) % 10 for k, i in enumerate(nums)]


# a = incrementer([1, 2, 3])

# return masked string
def maskify(cc):
    # Credit Card Mask
    if len(cc) > 4:
        cc = '#' * (len(cc) - 4) + cc[-4:]
    return cc


# a = maskify('-Wp(0')


def switcheroo(s):
    # Switcheroo
    return s.translate(str.maketrans('ab', 'ba'))


# ryan = switcheroo("abc")


def gordon(a):
    # Hells Kitchen
    a = a.upper().translate(str.maketrans('AEIOU', '@****'))
    return '!!!! '.join(a.split()) + '!!!!'


# ryan = gordon('What feck damn cake')

def count_consonants(text):
    t_dict = Counter(text.lower())
    print(t_dict.items())
    return sum(1 for x in t_dict.items() if x[0] in string.ascii_letters and x[0] not in 'aeiouAEIOU')


# ryan = count_consonants('Count my unique consonants!!')


def alphabet_war(fight):
    # Alphabet war - airstrike - letters massacre
    copy_fight = [i for i in fight]
    lcos = [k for k, v in enumerate(fight) if v == '*']
    for i in lcos:
        copy_fight[i] = '_'
        if i >= 1:
            copy_fight[i - 1] = '_'
        if i < len(fight) - 1:
            copy_fight[i + 1] = '_'
    left = copy_fight.count('w') * 4 + copy_fight.count('p') * 3 + copy_fight.count('b') * 2 + copy_fight.count('s')
    right = copy_fight.count('m') * 4 + copy_fight.count('q') * 3 + copy_fight.count('d') * 2 + copy_fight.count('z')
    return "Right side wins!" if right > left else "Let's fight again!" if right == left else "Left side wins!"


# ryan = alphabet_war('z*dq*mw*pb*s*')


def find_screen_height(width, ratio):
    # Find Screen Size
    ratio = [int(x) for x in ratio.split(':')]
    x, y = ratio[0], ratio[1]
    height = width * y // x
    return f'{width}x{height}'


# ryan = find_screen_height(372, "29:4")


def shorter_reverse_longer(a: str, b: str):
    # shorter concat [reverse longer]
    if len(a) >= len(b):
        return f'{b}{a[::-1]}{b}'
    else:
        return f'{a}{b[::-1]}{a}'


# ryan = shorter_reverse_longer("first", "abcde")


def simplify(poly):
    # Simplifying multilinear polynomials
    poly = poly.replace('+', ' ').replace('-', ' -')
    poly = poly.split()
    poly = [''.join(sorted(i)) for i in poly]

    d = dict()
    for x in poly:
        loc = 0
        while True:
            if x[loc] in string.ascii_letters:
                break
            loc += 1
        # print(x[:loc], x[loc:])
        if x[:loc] == '':
            d[x[loc:]] = 1 + d.get(x[loc:], 0)
        elif x[:loc] == '-':
            d[x[loc:]] = -1 + d.get(x[loc:], 0)
        else:
            d[x[loc:]] = int(x[:loc]) + d.get(x[loc:], 0)
    for k in list(d.keys()):
        if d[k] == 0:
            del d[k]
    print(d)

    r = sorted(sorted(d), key=len)
    print('r:', r)
    rtn_str = ""
    for k, v in enumerate(r):
        rtn_str += f'{"+" if d[v] == 1 else "" if d[v] == 1 else "-" if d[v] == -1 else "+" + str(d[v]) if d[v] > 0 else d[v]}{v}'
    return rtn_str.strip('+')


# ryan = simplify("-8fk+5kv-4yk+7kf-qk+yqv-3vqy+4ky+4kf+yvqkf")


def sponge_meme(s):
    # sPoNgEbOb MeMe
    return ''.join(v.upper() if k % 2 == 0 else v.lower() for k, v in enumerate(s))


# ryan = sponge_meme("stop Making spongebob Memes!")


def modify_multiply(st, loc, num):
    # Multiply Word in String
    return '-'.join([st.split()[loc]] * num)


# ryan = modify_multiply("This is a string",3 ,5)


# def solution(value):
#     # Substituting Variables Into Strings: Padded Numbers
#     return f'Value is {str(value).rjust(5, "0")}'
#     # f'Value is {value:05d}' 也可以


# ryan = solution(5)


# def solution(full_text, search_text):
#     # Return substring instance count
#     return full_text.count(search_text)


# ryan = solution('abcdeb','b')


# def solve(a,b):
#     # Unique string characters
#     return ''.join([i for i in a if i not in b] + [i for i in b if i not in a])


# ryan = solve("xyab","xzca")


def dominator(arr):
    # What dominates your array?
    for i in set(arr):
        if arr.count(i) > len(arr) // 2:
            return i
    return -1


# ryan = dominator([3,4,3,2,3,1,3,3])


def even_chars(st):
    # Return a string's even characters.
    if len(st) < 2 or len(st) > 100:
        return "invalid string"
    return [v for k, v in enumerate(st) if k % 2 == 1]


# ryan = even_chars("abcdefghijklm")


def wave(people):
    # Mexican Wave
    rtn_lst = []
    for k, v in enumerate(people):
        if v.isalpha():
            s = people[:k] + people[k].upper() + people[k + 1:]
            rtn_lst.append(s)
    return rtn_lst


# ryan = wave('code wars')


def dative(word):
    # Hungarian Vowel Harmony (easy)
    front_vowel = 'eéiíöőüű'
    back_vowel = 'aáoóuú'
    for k, v in enumerate(word[::-1]):
        if v in front_vowel:
            return word + 'nek'
        elif v in back_vowel:
            return word + 'nak'
    return word


def covfefe(s):
    # Covfefe
    return s.replace("coverage", "covfefe") if "coverage" in s else f"{s} covfefe"


# ryan = dative("ablak")


def alphabetic(s):
    # Alphabetically ordered
    for i, j in zip(s[:-1], s[1:]):
        if i > j:
            return False
    return True


# ryan = alphabetic('door')


def string_expansion(s):
    # Simple Simple Simple String Expansion
    rtn_s = ""
    for i in range(len(s) - 1, -1, -1):
        if s[i] in string.ascii_letters:
            loc = i
            while loc >= 0:
                if loc == 0:
                    rtn_s = s[i] + rtn_s
                    break
                else:
                    if s[loc - 1] in '0123456789':
                        rtn_s = int(s[loc - 1]) * s[i] + rtn_s
                        break
                    loc -= 1
    return rtn_s


# def string_expansion(s):
#     # 这个更好
#     m,n = '',1
#     for j in s:
#         if j.isdigit():
#             n = int(j)
#         else:
#             m += j*n
#     return m


# ryan = string_expansion('3n6s7f3n')


def borrow(s):
    # Borrower Speak
    return ''.join(i for i in s.lower() if i.isalpha())


# def phone(dr, num):
#     phone_pattern = re.compile(r'\+\d{1,2}-\d{3}-\d{3}-\d{4}')
#     name_pattern = re.compile(r'<.*?>')
#
#     lines = dr.split("\n")
#     num_matches = []
#     for line in lines:
#         phone_match = phone_pattern.search(line)
#         if phone_match and phone_match.group()==f'+{num}':
#             name_match = name_pattern.search(line)
#             name = name_match.group()[1:-1]
#             address = line[:].replace(name_match.group(), "").replace(phone_match.group(), "")
#             address = ''.join(i if i.isalnum() or i in '.-' else ' ' for i in address)
#             address = address.replace("  ", " ").replace("  ", " ").replace("  ", " ").strip()
#             num_matches.append({"name": name, "address": address})
#
#     if not num_matches:
#         return "Error => Not found: {}".format(num)
#     elif len(num_matches) > 1:
#         return "Error => Too many people: {}".format(num)
#     else:
#         return "Phone => {}, Name => {}, Address => {}".format(num, num_matches[0]["name"], num_matches[0]["address"])


def phone(dir, num):
    # Phone Directory
    if dir.count("+" + num) == 0:
        return "Error => Not found: " + num

    if dir.count("+" + num) > 1:
        return "Error => Too many people: " + num

    for line in dir.splitlines():
        if "+" + num in line:
            name = sub(".*<(.*)>.*", "\g<1>", line)
            line = sub("<" + name + ">|\+" + num, "", line)
            address = " ".join(sub("[^a-zA-Z0-9.-]", " ", line).split())
            return "Phone => %s, Name => %s, Address => %s" % (num, name, address)


dr = ("/+1-541-754-3010 156 Alphand_St. <J Steeve>\n 133, Green, Rd. <E Kustur> NY-56423 ;+1-541-914-3010;\n"
      "+1-541-984-3012 <P Reed> /PO Box 530; Pollocksville, NC-28573\n :+1-321-512-2222 <Paul Dive> Sequoia Alley PQ-67209\n"
      "+1-741-984-3090 <Peter Reedgrave> _Chicago\n :+1-921-333-2222 <Anna Stevens> Haramburu_Street AA-67209\n"
      "+1-111-544-8973 <Peter Pan> LA\n +1-921-512-2222 <Wilfrid Stevens> Wild Street AA-67209\n"
      "<Peter Gone> LA ?+1-121-544-8974 \n <R Steell> Quora Street AB-47209 +1-481-512-2222!\n"
      "<Arthur Clarke> San Antonio $+1-121-504-8974 TT-45120\n <Ray Chandler> Teliman Pk. !+1-681-512-2222! AB-47209,\n"
      "<Sophia Loren> +1-421-674-8974 Bern TP-46017\n <Peter O'Brien> High Street +1-908-512-2222; CC-47209\n"
      "<Anastasia> +48-421-674-8974 Via Quirinal Roma\n <P Salinger> Main Street, +1-098-512-2222, Denver\n"
      "<C Powel> *+19-421-674-8974 Chateau des Fosses Strasbourg F-68000\n <Bernard Deltheil> +1-498-512-2222; Mount Av.  Eldorado\n"
      "+1-099-500-8000 <Peter Crush> Labrador Bd.\n +1-931-512-4855 <William Saurin> Bison Street CQ-23071\n"
      "<P Salinge> Main Street, +1-098-512-2222, Denve\n")


# ryan = phone(dr, "48-421-674-8974")


# Moves in squared strings (III)
def diag_1_sym(s):
    n = len(s.split("\n"))
    lines = s.split("\n")
    result = []
    for i in range(n):
        result.append("")
        for j in range(n):
            result[i] += lines[j][i]
    return "\n".join(result)


def rot_90_clock(s):
    n = len(s.split("\n"))
    lines = s.split("\n")
    result = []
    for i in range(n):
        result.append("")
        for j in range(n - 1, -1, -1):
            result[i] += lines[j][i]
    return "\n".join(result)


def selfie_and_diag1(s):
    n = len(s.split("\n"))
    lines = s.split("\n")
    diag = diag_1_sym(s).split("\n")
    result = []
    for i in range(n):
        result.append(lines[i] + "|" + diag[i])
    return "\n".join(result)


def oper_2(fct, s):
    if fct == diag_1_sym:
        return diag_1_sym(s)
    elif fct == rot_90_clock:
        return rot_90_clock(s)
    elif fct == selfie_and_diag1:
        return selfie_and_diag1(s)


# [Code Golf] Reversed Upper Index
# f = lambda x: min(k for k,v in enumerate(x[::-1]) if v.isupper())
# f=lambda s:[x<"a"for x in s][::-1].index(1)

# ryan = f("Codewars")


def format_words(words):
    # Format words into a sentence
    if not words:
        return ""
    words = [w for w in words if w]
    rtn_w = ', '.join(words[:-1]) if len(words) > 1 else words[0] if words else ""
    return rtn_w + f'{" and " if len(words) > 1 else ""}' + f'{words[-1] if len(words) > 1 else ""}'


# ryan = format_words(['one'])
# one, two, three and four


def encrypt(text, rule):
    # Basic Encryption
    encrypted_text = ''
    for letter in text:
        encrypted_text += chr((ord(letter) + rule) % 256)
    return encrypted_text


# ryan = encrypt('m\t0\x0bA^V,`s|.2X', rule = 327)

from itertools import permutations, zip_longest


def next_bigger_2(n):
    # 效率受限
    strn = str(n)
    print(next(permutations(strn, len(strn))))
    setn = {int(''.join(i)) for i in permutations(strn, len(strn)) if int(''.join(i)) > n}
    if len(setn) == 0:
        return -1
    print(setn)
    return min(setn)


def next_bigger_3(n):
    # Next bigger number with the same digits
    strn = str(n)
    lstn = [int(i) for i in strn]
    rtn_arr = set()
    for i in range(len(lstn) - 1, 0, -1):
        for j in range(i - 1, -1, -1):
            if lstn[i] > lstn[j]:
                # print(i, j, lstn[i], lstn[j])
                num = strn[:j] + strn[i] + ''.join(sorted(strn[j:i] + strn[i + 1:]))
                rtn_arr.add(int(num))
    if rtn_arr:
        return min(rtn_arr)
    return -1


def next_bigger(n):
    s = list(str(n))
    for i in range(len(s) - 2, -1, -1):
        if s[i] < s[i + 1]:
            t = s[i:]
            m = min(filter(lambda x: x > t[0], t))
            t.remove(m)
            t.sort()
            s[i:] = [m] + t
            return int("".join(s))
    return -1


# ryan = next_bigger(59884848459853)


def insert_dash(num):
    # Insert dashes
    rtn_str = ""
    odds = "13579"
    for k, v in enumerate(str(num)[:-1]):
        rtn_str = rtn_str + v
        if v in odds and str(num)[k + 1] in odds:
            rtn_str += "-"
    rtn_str += str(num)[-1]
    return rtn_str


# ryan = insert_dash(454793)


def christmas_tree(height):
    # Pattern 01: Merry Christmas (sometimes little bit out of time;-))
    rtn_arr = []
    max_length = (height // 3 - 1) * 2 + 5
    for i in range(3 * (height // 3)):
        block, line = divmod(i, 3)
        first_line = block * 2 + 1
        cur_line = first_line + 2 * line
        rtn_arr.append(('*' * cur_line).center(max_length).rstrip())
    if height >= 3:
        rtn_arr.append('###'.center(max_length).rstrip())
    return '\r\n'.join(rtn_arr)


# r = christmas_tree(3)

def second_symbol(s, symbol):
    # Find the index of the second occurrence of a letter in a string
    loc = s.find(symbol)
    if loc > -1:
        loc1 = s[loc + 1:].find(symbol)
        if loc1 > -1:
            return loc + loc1 + 1
    return -1


def second_symbol_best(s, c):
    return s.find(c, s.find(c) + 1)


# r = second_symbol('Hello world!!!', 'l')
# , 7, 'Find the index of the second symbol "o" in the string')


def Split_Strings(s):
    # Split Strings
    d = zip_longest(s[::2], s[1::2], fillvalue="_")
    return [''.join(i) for i in d]


# r = Split_Strings('abcdefg')


def generate_hashtag(s):
    # The Hashtag Generator
    rtn_str = ''.join(i.capitalize() for i in s.split()).replace(' ', '')
    if len(rtn_str) > 140 or len(rtn_str) == 0:
        return False
    return "#" + rtn_str


# r = generate_hashtag('')


def scramble(s1, s2):
    # Scramblies
    for i in set(s2):
        if s1.count(i) < s2.count(i):
            return False
    return True


# r = scramble('cedewaraaossoqqyt', 'codewars')


def strip_comments(strng, markers):
    # Strip Comments
    rtn_arr = []
    for s in strng.split('\n'):
        loc = min(s.find(m) if s.find(m) > -1 else len(s) for m in markers) if markers else len(s)
        rtn_arr.append(s[:loc].rstrip())
    return '\n'.join(rtn_arr)


# r = strip_comments('§', ['#', '!'])
# , 'apples, pears\ngrapes\nbananas')

def to_weird_case(words):
    # WeIrD StRiNg CaSe
    return ' '.join("".join(v.upper() if k % 2 == 0 else v.lower() for k, v in enumerate(w)) for w in words.split())


# r = to_weird_case('THIs iS a TEST')


# def sum_strings(x, y):
#     x = int(x) if x else 0
#     y = int(y) if y else 0
#     return str(x+y)

def sum_strings(x, y):
    l, res, carry = max(len(x), len(y)), "", 0
    x, y = x.zfill(l), y.zfill(l)
    for i in range(l - 1, -1, -1):
        carry, d = divmod(int(x[i]) + int(y[i]) + carry, 10)
        res += str(d)
    return ("1" * carry + res[::-1]).lstrip("0") or "0"


# r = sum_strings("123", "")


def clean_string(s):
    # Backspaces in string
    while "#" in s:
        loc = s.find("#")
        if loc == 0:
            s = s[1:]
        else:
            s = s[:loc - 1] + s[loc + 1:]
    return s


# r = clean_string('##c')
# , "ac")

def alphanumeric(password: str):
    # Not very secure
    return password.isalnum()


# r = alphanumeric("PassW0rd")


def is_merge(s: str, part1, part2):
    # Merged String Checker
    if sorted(s) == sorted(part1+part2):
        loc = -1
        for k, v in enumerate(part1):
            loc = s[loc+1:].find(v)
            if loc == -1:
                return False
        loc = -1
        for k, v in enumerate(part2):
            loc = s[loc+1:].find(v)
            if loc == -1:
                return False
    else:
        return False
    return True


# r = is_merge("codewars", "code", "wasr")


def to_underscore(s):
    # Convert PascalCase string into snake_case
    rtn_str = ""
    s = str(s)
    for k, v in enumerate(s):
        if k == 0 or k == len(s) - 1:
            rtn_str += v.lower()
        elif v.isupper():
            rtn_str += "_" + v.lower()
        else:
            rtn_str += v
    return rtn_str


r = to_underscore("TestController")


print(r)
