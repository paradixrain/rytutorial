# coding=utf-8
import re
import string


def decipher_str(matchobj):
    w = matchobj.group(2)[-1] + matchobj.group(2)[1:-1] + matchobj.group(2)[0] if len(
        matchobj.group(2)) > 1 else matchobj.group(2)
    return chr(int(matchobj.group(1))) + w


def decipher_this(s):
    # Decipher this!
    rtn_arr = []
    for i in s.split():
        j = re.sub(r'([0-9]+)([a-z]?.*)', decipher_str, i)
        rtn_arr.append(j)
    return ' '.join(rtn_arr)


def rot13(message):
    # ROT 13
    d = str.maketrans('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                      'nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM')
    return message.translate(d)


# ryan = rot13('test')
# print("ryan:", ryan)
#  "A wise old owl lived in an oak"


def keyword_cipher(msg, keyword):
    # Keyword Cipher
    letters = 'abcdefghijklmnopqrstuvwxyz'
    keyword_book = [v for k, v in enumerate(keyword) if keyword.count(v) < 2 and v in letters]
    keyword_book = "".join(keyword_book) + "".join(i for i in letters if i not in keyword_book)
    return msg.lower().translate(str.maketrans(letters, keyword_book))


# ryan = keyword_cipher("bEhrgBwuThZQcprNJQIFmFC", "godjczkvvjtqeeynmx")


def decode(encoded, b, c):
    # Decode a cipher - simple
    # Calculate the value of d for the first character
    d = 0
    # Initialize an empty list to store the decoded characters
    decoded = []
    # Loop through the encoded list and decode each character
    for e in encoded:
        # Calculate the value of the original character
        index = (e - d - b - c) % (b * c)
        # Convert the index to the corresponding ASCII letter
        letter = string.ascii_letters[index]
        # Add the decoded character to the list
        decoded.append(letter)
        # Update the value of d for the next character
        d += (index + b + c) % (b * c)
    # Convert the list of characters to a string and return it
    return ''.join(decoded)


# ryan = decode([20, 41, 63], 10, 10)

# def rot13(message):
#     # ROT13
#     Input = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#     Output = "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm"
#     return str.translate(message, str.maketrans(Input, Output))

ryan = rot13("EBG13 rknzcyr.")


print(ryan)

# wticljt dljt
# wajcmka fmka
