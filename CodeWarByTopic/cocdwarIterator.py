# coding=utf-8

def sum(*args):
    # 利用迭代器，遍历参数列表
    it = iter(args)
    sum = 0
    while True:
        try:
            c = next(it)
            if type(c) == int:
                sum += c
        except StopIteration:
            return sum


def bin_rota(arr):
    rtn_arr = []
    flag = 1
    for i in range(len(arr)):
        rtn_arr += arr[i][::flag]
        flag = -flag
    return rtn_arr


ryan = bin_rota([
    ["Bob", "Nora"],
    ["Ruby", "Carl"]])
print('\033[0;32m' + repr(ryan) + '\033[0m')
print('\033[1;32m' + repr(ryan) + '\033[0m')
print('\033[2;32m' + repr(ryan) + '\033[0m')
print('\033[3;32m' + repr(ryan) + '\033[0m')
print('\033[4;32m' + repr(ryan) + '\033[0m')
print('\033[5;32m' + repr(ryan) + '\033[0m')
print('\033[6;32m' + repr(ryan) + '\033[0m')
print('\033[7;32m' + repr(ryan) + '\033[0m')
print('\033[8;32m' + repr(ryan) + '\033[0m')
print(ryan)
