# coding = utf-8
import sys
import time

from functools import reduce
from collections import deque
# from collections import OrderedDict
# from itertools import accumulate

# from inspect import getsource
# from random import randint
# from fractions import Fraction

starttime = time.time()

def total_bytes(object):
    # return the total byte size of the object.
    return sys.getsizeof(object)


# a = total_bytes("hello")
# b = total_bytes(123)


def operator(a, n, b):
    # Guess Operator
    if n == 0: return 1 + b
    if n == 1: return a + b
    if n == 2: return a * b
    if n == 3: return a ** b
    return reduce(lambda x, _: a ** x, [1] * (b + 1))


act = operator
a = act(3, 4, 3)


# def game(n):
#     # Playing on a chessboard
#     board = [[(j, i) for i in range(j + 1, j + n + 1)] for j in range(1, n + 1)]
#     total = Fraction()
#     for line in board:
#         for column in line:
#             print(column)
#             total = total + Fraction(column[0], column[1])
#
#     return [total.numerator] if total.denominator == 1 else [total.numerator, total.denominator]


# def game(n):
#     # Playing on a chessboard
#     total = Fraction()
#     for j in range(1, n+1):
#         for i in range(j+1, j+n+1):
#             total += Fraction(j, i)
#
#     return [total.numerator] if total.denominator == 1 else [total.numerator, total.denominator]


def game(n):
    if n % 2 == 0:
        return [n ** 2 // 2]
    return [(n ** 2 // 2) * 2 + 1, 2]


# ryan = game(3000001)


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Point({}:{})'.format(self.x, self.y)

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):
        return not (self == other)


# class WordSearch(object):
#     # Word Search Grid
#
#     DIRS = {(x, y) for x in range(-1, 2) for y in range(-1, 2) if (x, y) != (0, 0)}
#
#     def __init__(self, puzzle):
#         self.grid = [list(line) for line in puzzle.split('\n')]
#
#     def search(self, word):
#         for x, r in enumerate(self.grid):
#             for y, c in enumerate(r):
#                 if c == word[0]:
#                     for dx, dy in self.DIRS:
#                         for n in range(1, len(word)):
#                             if not (0 <= x + dx * n < len(self.grid) and 0 <= y + dy * n < len(r)) or \
#                                     self.grid[x + dx * n][y + dy * n] != word[n]:
#                                 break
#                         else:
#                             return Point(y, x), Point(y + dy * n, x + dx * n)
#
#
# puzzle = ('jefblpepre\n'
#           'camdcimgtc\n'
#           'oivokprjsm\n'
#           'pbwasqroua\n'
#           'rixilelhrs\n'
#           'wolcqlirpc\n'
#           'screeaumgr\n'
#           'alxhpburyi\n'
#           'jalaycalmp\n'
#           'clojurermt')
#
# example = WordSearch(puzzle)

# ryan = example.search('aj')


def find_max(n):
    # Prime Cycles
    primes = [1, 2, 3, 5, 7, 11, 13, 17, 19, 23]
    all_posibilities = set()
    num = n
    while num not in all_posibilities:
        all_posibilities.add(num)
        cur = 1
        for i in str(num):
            cur = cur * primes[int(i)]
        print(cur)
        num = cur
    return max(all_posibilities)


# ryan = find_max(8)


from random import randint,seed
# Don't rely on luck.
# 保证生成的数字是一样的
luck1 = randint(1, 100)
seed(1)
guess = randint(1,100)
seed(1)
luck2 = randint(1, 100)


# print(guess, luck1, luck2)


def direction_in_grid(n, m):
    # Simple Fun #183: Direction In Grid
    if n % 2 == 1:
        if m >= n:
            return "R"
        else:
            if m % 2 == 1:
                return "D"
            else:
                return "U"
    if n % 2 == 0:
        if m >= n:
            return "L"
        else:
            if m % 2 == 1:
                return "D"
            else:
                return "U"


# r = direction_in_grid(4,2)


def press_button(n):
    # Simple Fun #169: Press Button
    rtn_num = 0
    step = 1
    for i in range(n-1, 0, -1):
        rtn_num += i * step
        print(i, step, rtn_num)
        step += 1
    rtn_num += n
    return rtn_num


# r = press_button(4)

def rotate_paper(number):
    # Simple Fun #156: Rotate Paper By 180 Degrees
    mirror_letters = '025689'
    if all(i in mirror_letters for i in str(number)):
        if number == number.translate(str.maketrans("025689", "025986"))[::-1]:
            return True
    return False


# r = rotate_paper("96")


def not_so_random(b, w):
    # Simple Fun #158: Not So Random
    if b % 2 == 1:
        return "Black"
    return "White"


# r = not_so_random(11111,22222)


def six_column_encryption(msg):
    # Simple Fun #133: Six Column Encryption
    line_num = len(msg)//6 if len(msg) % 6 == 0 else len(msg)//6+1
    msg = msg.ljust(line_num*6, ".").replace(" ", ".")
    return ' '.join(msg[i::6] for i in range(6))


# r = six_column_encryption('vaolq tfyjk zqscjb pw kxey ld rkvxy fyrppr sqnho')
# 'vtz.erf. afqpykys oysw.vrq ljc.lxpn qkjkdyph ..bx..ro'


def decrypt(s):
    # Simple Fun #126: Decrypt Number
    for i in range(1, 11):
        temp_num = i * (10 ** len(s)) + int(s)
        n, d = divmod(temp_num, 11)
        print(temp_num, n, d)
        if d == 0:
            return n
    return "impossible"


# r = decrypt("262080024354921895")

def reverse_mod(m, n):
    # m = 8, n = 26
    y = 0
    x = m + y*26 -n
    while not 0<=x<=25:
        y += 1
        x = m + y*26 - n
    return x


def cipher26(message):
    # Simple Fun #46: Cipher26
    rtn_str = ""
    for k, v in enumerate(message):
        if k == 0:
            rtn_str += v
        else:
            rtn_str += chr(97 + reverse_mod(ord(v)-97, sum(ord(i)-97 for i in rtn_str)))
    return rtn_str


# r = cipher26('taiaiaertkixquxjnfxxdh')
# thisisencryptedmessage


def odd(s):
    # Simple Fun #121: Mr.Odd
    count = 0
    o_loc = 0
    d_loc = 0
    while o_loc < len(s):
        if s[o_loc] == "o":
            d_flag = 0
            d_loc = max(o_loc, d_loc)
            while d_loc < len(s) and d_flag < 2:
                if s[d_loc] == "d":
                    d_flag += 1
                if d_flag == 2:
                    count += 1
                d_loc += 1
        o_loc += 1
    return count

# r = odd("qoddoldfoodgodnooofostorodrnvdmddddeidfoi")


def circle_slash(n):
    # Simple Fun #140: Circle Slash
    for x in range(1000):
        if 2 ** x <= n < 2 ** (x+1):
            b = n - 2 ** x
            print(x, b)
            return 2 * b + 1


# r = circle_slash(3)


def three_details_slow(n):
    # slow
    dq = deque()
    rtn_num = 0
    dq.append(n)
    while len(dq):
        print(dq)
        d = dq.popleft()
        if d == 3:
            rtn_num += 1
        elif d > 3:
            dq.append(d // 2)
            dq.append(d - d // 2)
    return rtn_num

# @lru_cache(maxsize=None) 如果加上这一行，可以获得更好的性能和更好
from functools import lru_cache

@lru_cache(maxsize=None)
def three_details(n):
    if n in (3,5,7):
        return 1
    elif n < 6:
        return 0
    q, r = divmod(n, 2)
    if r:
        return three_details(q) + three_details(q+1)
    else:
        return three_details(q) * 2


def three_details_maxsize_reached(n):
    # Simple Fun #192: Three Details
    dq = {}
    rtn_num = 0
    dq[n] = 1
    while len(dq):
        k, v = dq.popitem()
        if k == 3:
            rtn_num += v
        elif k > 3:
            d, m = divmod(k, 2)
            if m == 0:
                dq[d] = dq.get(d, 0) + 2 * v
            else:
                dq[d] = dq.get(d, 0) + v
                dq[d+1] = dq.get(d+1, 0) + v
    return rtn_num


# r = three_details(728570513)


def find_max_sum(n):
    rtn_num = 3 * n

    while rtn_num > 0:
        n = rtn_num % 5
        print(rtn_num, n)
        rtn_num -= n
        # 找是否有合理解？
        z = rtn_num // 3
        print(rtn_num, z)
        if rtn_num - 2 <= z * 3 <= rtn_num:
            break
    return rtn_num


r = find_max_sum(7)


print(time.time()-starttime)
# r = three_details(728570513)

def check_max_sum(x, y, z):
    if (x + y) % 2 == 0 and (x + y + z) % 5 == 0 and (y + z) % 3 == 0:
        print(x, y, z)
        return True
    return False


def find_max_sum(n):
    # Simple Fun #302: Find The Max Sum
    for i in range(3 * n + 1):
        for x in range(i + 1):
            for y in range(i + 1):
                for z in range(i + 1):
                    if x + y + z > i:
                        break
                    if check_max_sum(n - x, n - y, n - z):
                        return 3 * n - x - y - z


def find_max_sum_useDataStructure(n):
    q = deque([(n, n, n)])
    while q:
        a, b, c = q.popleft()
        if (a + b) % 2 == 0 and (b + c) % 3 == 0 and (a + b + c) % 5 == 0:
            return a + b + c
        q.append((a - 1, b, c))
        q.append((a, b - 1, c))
        q.append((a, b, c - 1))


r = find_max_sum(10000)
print(time.time() - starttime)

print(r)

