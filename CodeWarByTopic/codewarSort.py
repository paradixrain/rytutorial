# coding=utf-8

def barista1(coffees):
    from itertools import accumulate
    coffees_sort = list(accumulate(sorted(coffees)))
    cnt = max((len(coffees_sort) - 1), 0)
    return sum(coffees_sort) + cnt * (cnt + 1)


def barista(coffees):
    from itertools import accumulate
    return sum(accumulate(sorted(coffees), lambda a, c: a + 2 + c))


def sort_reindeer(reindeer_names):
    return sorted(reindeer_names, key=lambda x: x.split()[1])


def strange_math1(n, k):
    strange_list = sorted(list(map(str, list(range(1, n + 1)))))
    return strange_list.index(str(k)) + 1


def strange_math(n, k):
    return sorted(range(n + 1), key=str).index(k)


def sum_even_numbers(seq):
    return sum(filter(lambda x: x % 2 == 0, seq))


def distribute_evenly1(lst):
    from collections import Counter
    rtn_list = []
    while True:
        cnt = Counter(lst).most_common()
        print(cnt)
        for item in cnt:
            rtn_list.append(item[0])
            lst.remove(item[0])
        if len(lst) == 0:
            break
    """ 跟题目的要求不一样，理解错了 """
    return rtn_list


def distribute_evenly(lst):
    # 用多级排序的方法解决
    from operator import itemgetter
    lst_rtn = [(lst[i], lst[:i].count(lst[i]), lst.index(lst[i])) for i in range(len(lst))]
    lst_rtn = sorted(lst_rtn, key=itemgetter(1, 2))
    return [i[0] for i in lst_rtn]


def my_languages1(results):
    results = [(k, v) for k, v in results.items() if v >= 60]
    rtn = sorted(results, key=lambda x: x[1], reverse=True)
    return [i[0] for i in rtn]


def my_languages(results):
    return sorted((q for q, r in results.items() if r >= 60), reverse=True, key=results.get)


def find_children(santas_list, children):
    return sorted(list(filter(lambda x: x in children, santas_list)))


def sort_gift_code(code):
    return ''.join(sorted(code))


def sort_grades(lst):
    grade = ['VB', 'V0', 'V0+'] + ['V' + str(i) for i in range(1, 18)]
    return list(filter(lambda x: x in lst, grade))


def sect_sort(array, start, length=None):
    length = len(array) if length is None or length == 0 else length
    return array[:start] + sorted(array[start:start + length]) + array[start + length:]


def lineup_students1(s):
    s = sorted(s.split(), reverse=True)
    return sorted(s, key=len, reverse=True)


def lineup_students(s):
    # 利用sorted函数的key参数，进行多级排序的标准用法
    return sorted(s.split(), key=lambda x: (len(x), x), reverse=True)


def sort_cards(cards):
    """Sort shuffled list of cards, sorted by rank.
    """
    card_order = 'A123456789TJQK'
    return sorted(cards, key=lambda x: card_order.index(x))


def order_word(s):
    return "Invalid String!" if s is None or len(s) == 0 else ''.join(sorted(s))


def max_product(a):
    a1 = max(a)
    a.remove(a1)
    a2 = max(a)
    return a1 * a2


def example_sort(arr, example_arr):
    rtn_list = []
    for i in example_arr:
        for fre in range(arr.count(i)):
            rtn_list.append(i)
    return rtn_list


def sort_list(sort_by, lst):
    return sorted(lst, key=lambda x: x[sort_by], reverse=True)


def bubble(l):
    rtn_list = []
    for i in range(len(l)):
        for j in range(len(l) - 1):
            if l[j] > l[j + 1]:
                l[j], l[j + 1] = l[j + 1], l[j]
                rtn_list.append(l[:])
    return rtn_list


def factors(x):
    # return a list of factors of x, in descending order
    if type(x) == int and x >= 1:
        return sorted([i for i in range(1, x + 1) if x % i == 0], reverse=True)
    return -1


songs = [{'artist': 'Marillion', 'title': 'Keyleigh', 'playback': '03:36'},
         {'artist': 'Pink Floyd', 'title': 'Time', 'playback': '06:48'},
         {'artist': 'Rush', 'title': 'YYZ', 'playback': '04:27'},
         {'artist': 'Bonobo', 'title': 'Days To Come', 'playback': '03:50'},
         {'artist': 'Coldplay', 'title': 'Yellow', 'playback': '04:32'},
         {'artist': 'Bloc Party', 'title': 'Like Eating Glass', 'playback': '04:22'},
         {'artist': 'The Killers', 'title': 'For Reasons Unknown', 'playback': '03:30'},
         {'artist': 'Arctic Monkeys', 'title': 'Teddy Picker', 'playback': '03:25'},
         {'artist': 'Joe Satriani', 'title': 'Surfing With The Alien', 'playback': '04:34'}]


def longest_possible1(playback):
    global songs
    songs = sorted(songs, key=lambda x: x['playback'])
    print(songs)
    rtn = False
    for i in range(len(songs)):
        t = songs[i]['playback'].split(':')
        t1 = int(t[0]) * 60 + int(t[1])
        if t1 >= playback:
            if i > 0:
                rtn = songs[i - 1]['title']
            break
        if i == len(songs) - 1:
            rtn = songs[i]['title']
    return rtn


def longest_possible(playback):
    # 这个方法好
    song_list = [x for x in songs if x['playback'] <= ("%02d:%02d" % divmod(playback, 60))]
    return max(song_list, key=lambda x: x["playback"])["title"] if song_list else False


def sort_dict(d):
    return sorted(d.items(), key=lambda x: x[1], reverse=True)


def sort_it(list_, n):
    # 根据list_中的第n个元素进行排序
    return ', '.join(sorted(list_.split(', '), key=lambda x: x[n - 1]))


def rank(st, we, n):
    # Prize Draw
    if st is None or len(st) == 0:
        return "No participants"
    st = st.split(",")
    if n > len(st):
        return "Not enough participants"

    d = zip(st, we)
    d = sorted(d, key=lambda x: x[0].upper())
    d = sorted(d, key=lambda x: (sum(ord(i) - 64 for i in x[0].upper()) + len(x[0])) * x[1], reverse=True)
    for j in d:
        print(j[0], j[1], (sum(ord(k) - 64 for k in j[0].upper()) + len(j[0])) * j[1])
    return d[n - 1][0]


def sort_array(source_array):
    # Return a sorted array.
    odd_arr = sorted([i for i in source_array if i % 2 == 1])
    for i in range(len(source_array)):
        if source_array[i] % 2 == 1:
            source_array[i] = odd_arr.pop(0)
    return source_array


# a = sort_array([5, 3, 2, 8, 1, 4, 11])


def sort_emotions(arr, order):
    # Emotional Sort ( ︶︿︶)
    emo_dict = {':D': 5, ':)': 4, ' :|': 3, ':(': 2, 'T_T': 1}
    return sorted(arr, key=lambda x: emo_dict[x], reverse=order)


a = sort_emotions([':D', 'T_T', ':D', ':('], True)

print(a)
