# coding=utf-8
CODE = {'A': '.-',     'B': '-...',   'C': '-.-.',
        'D': '-..',    'E': '.',      'F': '..-.',
        'G': '--.',    'H': '....',   'I': '..',
        'J': '.---',   'K': '-.-',    'L': '.-..',
        'M': '--',     'N': '-.',     'O': '---',
        'P': '.--.',   'Q': '--.-',   'R': '.-.',
        'S': '...',    'T': '-',      'U': '..-',
        'V': '...-',   'W': '.--',    'X': '-..-',
        'Y': '-.--',   'Z': '--..',
        '0': '-----',  '1': '.----',  '2': '..---',
        '3': '...--',  '4': '....-',  '5': '.....',
        '6': '-....',  '7': '--...',  '8': '---..',
        '9': '----.'
        }


def encryption(s):
    return ' '.join(CODE.get(c, ' ') for c in s.upper())


def encode(message, key):
    key_i = [int(i) for i in str(key)] * (len(message)//len(str(key)) + 1)
    message_i = [ord(i)-96 for i in message]
    return list(map(lambda x, y: x+y, message_i, key_i))


def encrypt_this(text):
    # Encrypt this!
    if len(text) == 0:
        return ""
    arr = [i for i in text.split(' ')]
    rtn_arr = []
    for i in arr:
        rtn_str = str(ord(i[0]))
        if len(i) > 2:
            rtn_str += i[-1] + i[2:-1] + i[1]
        elif len(i) == 2:
            rtn_str += i[-1]
        rtn_arr.append(rtn_str)
    return ' '.join(rtn_arr)


ryan = encrypt_this("A wise old owl lived in an oak")
print(ryan)
