# coding=utf-8
from functools import lru_cache

@lru_cache(maxsize=None)
def count_subsequences_2(a, b):
    # 标准动态规划法
    if not a: return 1
    if not b: return 0
    i = b.find(a[0])
    if i == -1: return 0
    return count_subsequences(a, b[i+1:]) + count_subsequences(a[1:], b[i+1:])


def count_subsequences(needle, haystack):
    # Counting String Subsequences
    # 这个方法简直是奇才
    count = [1] + [0] * len(needle)
    for a in haystack:
        count = [1] + [count[i] + count[i-1] * (a == b)
                       for i, b in enumerate(needle, 1)]
        print(count)
    return count[-1] % 10 ** 8


r = count_subsequences("happy birthday", "happy birthdayhappy birthday")
print(r)