# coding=utf-8

def remove_parentheses1(s):
    """
    删除括号对及其内容
    """
    while True:
        # 从右向左，挨个找到)以及其匹配的(，删掉其中的内容
        right = s.rfind(')')
        left = s.rfind('(', 0, right)
        right = s.find(')', left)
        if -1 < left < right:
            s = s[:left] + s[right + 1:]
        else:
            break
    return s


def remove_parentheses(s):
    import re
    while (t := re.sub(r'\([^()]*\)', '', s)) != s:
        s = t
    return s


def remove(s):
    return ' '.join(i.rstrip('!') for i in s.split(' '))


def remove_chars(s):
    return ''.join(i for i in s if i.isalpha() or i.isspace())


def find_e1(s):
    if s:
        return 'There is no "e".' if s.find('e')+s.find('E') == -2 else str(s.count('e')+s.count('E'))
    elif s is None:
        return None
    else:
        return ""


def find_e(s):
    return s and str(s.count('e')+s.count('E') or 'There is no "e".')


def format_poem(poem):
    return '.\n'.join(poem.split('. ')).strip('\n')


ryan = format_poem('Beautiful is better than ugly. Explicit is better than implicit. Simple is better than complex. Complex is better than complicated.')
print(ryan)
