# coding=utf-8

def per(n):
    from functools import reduce
    if 0 <= n < 10:
        return []
    else:
        n_s = str(n)
        n_i = reduce(lambda x, y: x * y, map(int, n_s))
        return [n_i] + per(n_i)


def find_constant(arr, lb, ub):
    """Finds the constant that was added to an array (arr) of values randomly
    selected from a uniform distribution of values from lb (lower bound)
    to ub (upper bound), boundaries included.
    """
    return sum(arr) / len(arr) - (lb + ub) / 2


def sensor_analysis(sensor_data):
    """ 返回sensor_data['distance']的平均值和标准差，四舍五入到小数点后四位"""
    import statistics
    distance = [i[1] for i in sensor_data]
    return round(statistics.mean(distance), 4), round(statistics.stdev(distance), 4)


def highest_age(group1, group2):
    """ 将字典 group1 和 字典group2 相同key值的value相加存入新字典group，并返回最大age值对应的key值"""
    group, max_name, max_age = {}, None, 0
    for i in group1 + group2:
        group[i['name']] = group.get(i['name'], 0) + i['age']
        if max_name is None:
            max_name = i['name']
            max_age = group[i['name']]
        if group[i['name']] >= max_age:
            max_name, max_age = i['name'], group[i['name']]
    return max_name


def distances_from_average1(test_list):
    """ 返回test_list中每个元素与平均值的差值"""
    import statistics
    return [i - statistics.mean(test_list) for i in test_list]


def distances_from_average(test_list):
    from numpy import mean
    avg = mean(test_list)
    return [round(avg - x, 2) for x in test_list]


ryan = highest_age([{'name':'kay','age':1},{'name':'john','age':13},{'name':'kay','age':76}],[{'name':'john','age':1},{'name':'alice','age':76}])
print(ryan)
