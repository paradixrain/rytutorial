# coding = utf-8


def modified_sum(a, n):
    """系统将为您提供一个正整数数组和一个附加整数 （）。n > 1
    计算数组中每个值与t
    次幂之和。然后减去原始数组的总和。"""
    return sum([x ** n for x in a]) - sum(a)


def get_slope(p1, p2):
    """ Return the slope of the line through p1 and p2
    """
    return None if p1[0] == p2[0] else (p2[1] - p1[1]) / (p2[0] - p1[0])


def f(x, y, z):
    """
    Given a box built by arranging unit boxes, write a function that returns the number of edges (hence, boxlines) of
    length 1 (both inside and outside of the box)
    """
    # stupid
    # return (x * y * z) * 3 + y * 3 + y * ((x - 1) * 2 + (z - 1) * 2) + y * 2 + (z + 1) * x + (x + 1) * z
    # clever
    a, b, c = x + 1, y + 1, z + 1
    return a * b * z + a * y * c + x * b * c


def number_of_rectangles(m, n):
    """
    Given a grid of m rows and n columns, return the number of rectangles in the grid
    """
    return (m * (m + 1) * n * (n + 1)) // 4


def symmetric_shape1(shape, q):
    """Following on from Points of Reflection, given a number of points and a single midpoint, a 2D shape can be
    inferred. """
    rtn = []
    rtn += shape
    for index, item in enumerate(shape):
        x, y = (2 * q[0] - item[0]), (2 * q[1] - item[1])
        rtn.append((x, y))
    return rtn


def symmetric_shape(shape, q):
    return shape + list(map(lambda x: (2 * q[0] - x[0], 2 * q[1] - x[1]), shape))


def type_of_triangle1(a, b, c):
    """
    Build a function that will take the length of each side of a triangle and return if it's either an Equilateral,
    an Isosceles, a Scalene or an invalid triangle.
    """
    try:
        if (0 < c < a + b) and (0 < b < a + c) and (0 < a < b + c):
            if a == b == c:
                return "Equilateral"
            elif a == b or a == c or b == c:
                return "Isosceles"
            else:
                return "Scalene"
        else:
            return "Not a valid triangle"
    except TypeError:
        return "Not a valid triangle"


def type_of_triangle(a, b, c):
    try:
        a, b, c = sorted([a, b, c])
        if a + b > c > 0:
            if a == c:
                return "Equilateral"
            elif a == b or b == c:
                return "Isosceles"
            else:
                return "Scalene"
        else:
            return "Not a valid triangle"
    except TypeError:
        return "Not a valid triangle"


def dot_product(a, b):
    """
    Given two arrays, return the dot product of the two arrays
    """
    return sum([a[i] * b[i] for i in range(3)])


def coordinates(degrees, radius):
    """
    Given a number of degrees and a radius, return the coordinates of the point on a circle
    """
    from math import cos, sin, radians
    return round(radius * cos(radians(degrees)), 10), round(radius * sin(radians(degrees)), 10)


def ellipse_contains_point(f0, f1, l, p):
    """
    Given two points f0 and f1, and a line segment l, return if the point p is inside the ellipse defined by the line
    segment
    """
    from math import sqrt
    return sqrt((p['x'] - f0['x']) ** 2 + (p['y'] - f0['y']) ** 2) + \
           sqrt((p['x'] - f1['x']) ** 2 + (p['y'] - f1['y']) ** 2) <= l


def triangle_perimeter(triangle):
    from math import sqrt
    A, B, C = triangle.a, triangle.b, triangle.c
    AB = sqrt((A.x - B.x) ** 2 + (A.y - B.y) ** 2)
    BC = sqrt((B.x - C.x) ** 2 + (B.y - C.y) ** 2)
    AC = sqrt((A.x - C.x) ** 2 + (A.y - C.y) ** 2)
    return AB + BC + AC


# def cross_product(a, b):
#     return Vector(
#         a.y * b.z - a.z * b.y,
#         a.z * b.x - a.x * b.z,
#         a.x * b.y - a.y * b.x
#     )


def solution(to_cur, value):
    rate = 1.1363636
    return [f'${round(m * rate, 2):,.2f}' if to_cur == 'USD' else f'{round(m / rate, 2):,.2f}€' for m in value]


def find_lowest_int(k):
    """
    We have two consecutive integers k1 and k2, k2 = k1 + 1
We need to calculate the lowest integer n, such that:
the values nk1 and nk2 have the same digits but in different order.
    """
    n = k + 2
    while True:
        if sorted(str(n * k)) == sorted(str(n * (k + 1))):
            return n
        n += 1


def vector_length(vector):
    return ((vector[0][0] - vector[1][0]) ** 2 + (vector[0][1] - vector[1][1]) ** 2) ** 0.5


def solution2(f1, f2, theta):
    """
    Given two forces (F1 and F2 ) and the angle F2 makes with F1
    find the resultant force R and the angle it makes with F1.
    """
    from math import cos, radians, sin, atan2, degrees, hypot
    r = radians(theta)
    x = f1 + f2 * cos(r)
    y = f2 * sin(r)
    return hypot(x, y), degrees(atan2(y, x))


def age(x, y):
    return round(x * y / (y - 1))


def dropzone1(p, d):
    (x, y), (x1, y1), (x2, y2), (x3, y3) = p, *d
    rtn = [x1, y1]
    if ((x2 - x) ** 2 + (y2 - y) ** 2) ** 0.5 < ((rtn[0] - x) ** 2 + (rtn[1] - y) ** 2) ** 0.5:
        rtn = [x2, y2]
    elif ((x2 - x) ** 2 + (y2 - y) ** 2) ** 0.5 == ((rtn[0] - x) ** 2 + (rtn[1] - y) ** 2) ** 0.5:
        if (x2 ** 2 + y2 ** 2) < (rtn[0] ** 2 + rtn[1] ** 2):
            rtn = [x2, y2]
    if ((x3 - x) ** 2 + (y3 - y) ** 2) ** 0.5 < ((rtn[0] - x) ** 2 + (rtn[1] - y) ** 2) ** 0.5:
        rtn = [x3, y3]
    elif ((x3 - x) ** 2 + (y3 - y) ** 2) ** 0.5 == ((rtn[0] - x) ** 2 + (rtn[1] - y) ** 2) ** 0.5:
        if (x3 ** 2 + y3 ** 2) < (rtn[0] ** 2 + rtn[1] ** 2):
            rtn = [x3, y3]
    return rtn


def dropzone(fire, dropzones):
    from math import hypot
    return min(dropzones, key=lambda p: hypot(p[0] - fire[0], p[1] - fire[1]))


CONVERSION_RATES = {"Armenian Dram": 10}


def convert_my_dollars(usd, currency):
    rates = CONVERSION_RATES[currency] if currency[0] in 'aeiouAEIOU' else int(CONVERSION_RATES[currency], 2)
    return f'You now have {usd * rates} of {currency}.'


def circle_area(r):
    try:
        if r + 0 > 0:
            return round(r ** 2 * 3.141592653589793, 2)
    except TypeError:
        return False
    return False


def coordinates(p1, p2, precision=0):
    # return the distance between two points on a cartesian plane, given the x and y coordinates of each point.
    return round(((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2) ** 0.5, precision)


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Circle:
    def __init__(self, p, radius):
        self.radius = radius
        self.Point = Point(p.x, p.y)


def distance_between_circles(a, b):
    # return the distance between two circles,
    # given the x and y coordinates of each circle's center and the radius of each circle.
    from math import hypot
    if hypot(a.Point.x - b.Point.x, a.Point.y - b.Point.y) > a.radius + b.radius:
        return hypot(a.Point.x - b.Point.x, a.Point.y - b.Point.y) - a.radius - b.radius
    return 0


def distance_between_circles(a, b):
    dis = ((a.center.x - b.center.x) ** 2 + (a.center.y - b.center.y) ** 2) ** 0.5
    return max(0, dis - a.radius - b.radius)


# ryan = distance_between_circles(
#     Circle(Point(10, 60), 11),
#     Circle(Point(40, 20), 7)
# )
# print(ryan)


def distance_between_points(a, b):
    # 3D cross product of vector a and b
    return ((a.x - b.x) ** 2 + (a.y - b.y) ** 2 + (a.z - b.z) ** 2) ** 0.5


def equable_triangle(a, b, c):
    # if the triangle formed by three given sides
    # it's perimeter equals the area of the triangle.
    p = (a + b + c) / 2
    return (p * (p - a) * (p - b) * (p - c)) ** 0.5 == a + b + c


def you_are_a_cube(cube):
    # if the cube's volume is a perfect cube,
    # return True, otherwise return False.
    return round(cube ** (1 / 3), 4) % 1 == 0


def cup_volume(d1, d2, height):
    # return the volume of a cup, given the length of its diameter and its height.
    from math import pi
    d1, d2 = min(d1, d2), max(d1, d2)
    if d1 == d2:
        return round(pi * d1 ** 2 * height / 4, 2)
    h2 = height * d2 / (d2 - d1)
    h1 = h2 - height
    return round(pi * (h2 * d2 ** 2 - h1 * d1 ** 2) / 12, 2)


def length_of_line(array):
    # return the length of the line defined by two points
    x1, y1, x2, y2 = array[0][0], array[0][1], array[1][0], array[1][1]
    return f'{round(((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5, 2):.2f}'


def main_diagonal_product(mat):
    # Given a list of rows of a square matrix, find the product of the main diagonal.
    # return the product of the main diagonal of the matrix.
    rtn_value = 1
    for i in range(len(mat)):
        rtn_value = rtn_value * mat[i][i]
    return rtn_value


def square_area_to_circle(size):
    return round(size * 3.141592653589793 / 4, 8)


def harmon_pointTrip(xA, xB, xC):
    a, b, c = map(float, [xA, xB, xC])
    d = ((a * c) + (b * c) - (2 * a * b)) / (2 * c - a - b)
    return round(d, 4)


def checkchoose(m, n):
    from math import comb
    # m 是组合数，n 是组合中的元素个数， 从n中最少取x个元素组成组合，返回组合数
    if comb(n, n // 2) < m:
        return -1
    for i in range(0, n // 2 + 1):
        if comb(n, i) >= m:
            if comb(n, i) == m:
                return i
            else:
                return -1
    return -1


ryan = checkchoose(6, 4)
print(ryan)
