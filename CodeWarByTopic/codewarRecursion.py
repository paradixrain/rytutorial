# coding=utf-8

from itertools import zip_longest
from itertools import groupby
from itertools import product

from collections import deque
from typing import *
from typing import Tuple


# def count_change(money, coins):
#     # 纯递归算法，可能会超过最大递归深度
#     if money<0:
#         return 0
#     if money == 0:
#         return 1
#     if money>0 and not coins:
#         return 0
#     return count_change(money-coins[-1],coins) + count_change(money,coins[:-1])


def count_change(money, coins):
    coins.sort()
    count_dict = {}
    return count_change_rec(money, coins, len(coins) - 1, count_dict)


def count_change_rec(money, coins, index, count_dict):
    if (money, index) in count_dict:
        return count_dict[(money, index)]
    if money == 0:
        count_dict[(money, index)] = 1
        return 1
    elif index == 0:
        if money % coins[index] == 0:
            count_dict[(money, index)] = 1
            return 1
        else:
            count_dict[(money, index)] = 0
            return 0
    elif money < 0 or index < 0:
        return 0
    else:
        return count_change_rec(money - coins[index], coins, index, count_dict) + count_change_rec(money, coins,
                                                                                                   index - 1,
                                                                                                   count_dict)


# 递归转循环， ryan， 学习
def count_change(money, coins):
    dp = [1] + [0] * money
    for coin in coins:
        for x in range(coin, money + 1):
            dp[x] += dp[x - coin]
    return dp[money]


# ryan = count_change(10, [5, 2, 3])
# print(ryan)


def int_sum(s: int):
    if len(str(s)) == 1:
        return s
    else:
        s = sum(int(i) for i in str(s))
        return int_sum(s)


def life_path_number(birthdate):
    b = birthdate.split('-')
    return int_sum(int_sum(int(b[0])) + int_sum(int(b[1])) + int_sum(int(b[2])))


# ryan = life_path_number("1879-03-14")
# print('ryan', ryan)

def reverse(n):
    m = 0
    while n > 0:
        n, m = n // 10, m * 10 + n % 10
    return m


def reverse1(n, acc=0):
    return acc if n == 0 else reverse(n // 10, acc * 10 + n % 10)


def replicate(times, number):
    if times <= 0:
        return []
    else:
        return [number] + replicate(times - 1, number)


def sum_nested(lst):
    # return the sum of every numerical value in the list and its sublists
    if all([type(i) != list for i in lst]):
        return sum(j for j in lst if type(j) == int)
    else:
        return sum(k for k in lst if type(k) == int) + sum(sum_nested(m) for m in lst if type(m) == list)


def sum_nested1(lst):
    return sum(sum_nested(x) if isinstance(x, list) else x for x in lst)


# This function should return n!
def factorial(n):
    if n < 0:
        return None
    elif n == 0:
        return 1
    else:
        return n * factorial(n - 1)


def f(n):
    return 1 if n == 0 else n - m(f(n - 1))


def m(n):
    return 0 if n == 0 else n - f(m(n - 1))


def smallest_doll_size(largest_doll):
    """ Russian nesting dolls    """
    if largest_doll():
        return smallest_doll_size(largest_doll())
    return largest_doll.size


def error_time(error):
    if error == 0:
        return 0
    elif error == 1:
        return 5
    else:
        return 2 * error_time(error - 1)


def alex_mistakes(number_of_katas, time_limit):
    """ Upper <body> Strength """
    for i in range(time_limit // 6 + 1):
        etime = sum(error_time(j) for j in range(i + 1))
        if etime + 6 * number_of_katas > time_limit:
            return i - 1


def sumsquares(l):
    """ Sum squares of numbers in list that may contain more lists """
    return sum(sumsquares(i) if type(i) == list else i ** 2 for i in l)


def reduce_fraction(fraction):
    """ Reduce My Fraction
    """

    def mygcd(a, b):
        while b:
            a, b = b, a % b
        return a

    return fraction[0] // mygcd(fraction[0], fraction[1]), fraction[1] // mygcd(fraction[0], fraction[1])


def poly_derivative(p):
    """ nova polynomial 4. derivative
    return the derivative of the polynomial.
    """
    if len(p) <= 1:
        return []
    return [i * j for i, j in zip(p[1:], range(1, len(p) + 1))]


def poly_subtract(p1, p2):
    """nova polynomial 3. subtract
    return the subtraction of the two polynomials p1 and p2.
    """
    return [i - j for i, j in zip_longest(p1, p2, fillvalue=0)]


def look_say_string(s):
    rtn_str = ''
    start = 0
    length = len(s)

    while start < length:
        k = start + 1
        while k < length:
            if s[k] != s[start]:
                break
            k += 1
        word = f"{k - start}{s[start]}"
        rtn_str += word
        start = k
    return rtn_str


def look_and_say(data='1', maxlen=5):
    """Starting with "1" the following lines are produced by "saying what you see",
     so that line two is "one one", line three is "two one(s)", line four is "one two one one".
    """
    rtn_list = []
    for i in range(maxlen):
        data = look_say_string(data)
        rtn_list.append(data)
    return rtn_list


def look_and_say1(data='1', maxlen=5):
    L = []
    for i in range(maxlen):
        data = "".join(str(len(list(g))) + str(n) for n, g in groupby(data))
        L.append(data)
    return L


def mormons(s, r, t):
    if t <= s:
        return 0
    return 1 + mormons(s * (r + 1), r, t)


def Ackermann(m, n):
    """
    A(m,n) = n+1                          if m=0
    A(m,n) = A(m-1,1)                     if m>0 , n=0
    A(m,n) = A(m-1,A(m,n-1))              if m,n > 0
    """
    if m == 0:
        return n + 1
    elif m > 0 and n == 0:
        return Ackermann(m - 1, 1)
    else:
        return Ackermann(m - 1, Ackermann(m, n - 1))


def delta(iter_, n):
    iter_ = iter(iter_) if n == 1 else delta(iter_, n - 1)
    prev = next(iter_)
    for v in iter_:
        yield v - prev
        prev = v


def delta1(values, n):
    if not n:
        yield from values
    else:
        it = delta1(values, n - 1)
        x = next(it)
        for y in it:
            yield y - x
            x = y


def beggars(values, n):
    values_copy = values.copy()
    if n > 0:
        count = [0] * n
    else:
        return []
    index = 0
    while values_copy:
        count[index] += values_copy.pop(0)
        if index == n - 1:
            index = 0
        else:
            index += 1
    return count


def beggars1(values, n):
    return [sum(values[i::n]) for i in range(n)]


def generate_all_possible_matches(k):
    flag = True
    new_matches = matches = [['0:0']]
    while flag:
        matches = new_matches[:]
        new_matches = []
        for match in matches:
            L, R = match[-1].split(':')
            if int(L) == k or int(R) == k:
                new_matches.append(match[:])
            else:
                node = match[:]
                node.append(f'{int(L) + 1}:{R}')
                new_matches.append(node)
                node = match[:]
                node.append(f'{L}:{int(R) + 1}')
                new_matches.append(node)
                flag = True
        if len(matches) == len(new_matches):
            flag = False
    return matches


def recurse(i, j, k, states, currState):
    currState.append("{}:{}".format(i, j))
    if i == k or j == k:
        states.append(currState.copy())
    else:
        recurse(i + 1, j, k, states, currState)
        recurse(i, j + 1, k, states, currState)
    currState.pop()


def generate_all_possible_matches1(k):
    states = []
    currState = []
    recurse(0, 0, k, states, currState)
    return states


def generate_all_possible_matches2(k):
    def generate_scores(a, b, l):
        ns = [*l, f'{a}:{b}']
        if a == k or b == k: return [ns]
        return generate_scores(a + 1, b, ns) + generate_scores(a, b + 1, ns)

    return generate_scores(0, 0, [])


def is_cyclic(p: Tuple[Tuple[int]]) -> bool:
    start = 0
    path = set()
    for _ in range(len(p[0])):
        line, other = p[0], p[1]
        temp, temp2 = line[start], other[start]
        path.add(temp)
        start = line.index(temp2)
    return set(p[0]) == set(p[1]) == path


f = lambda t: Union[tuple(to_type_hint(x) for x in t)]

d = {
    list: List,
    set: Set,
    deque: Deque,
    frozenset: FrozenSet
}

def to_type_hint(t):
    v = t.__class__
    if t is None:
        return

    if v is tuple:
        w = tuple(to_type_hint(x) for x in t)
        return Tuple[w if len(t) <= 3 else (to_type_hint(t[0]), ...) if len(set(w)) == 1 else (Union[w], ...)]

    elif t and v is dict:
        return Dict[f(t.keys()), f(t.values())]

    elif t and v in d:
        return d[v][f(t)]

    return v


def words_to_hex(words):
    rtn_lst = []
    for word in words.split():
        l1 = f'{ord(word[0]):02x}'
        l2 = f'{ord(word[1]):02x}' if len(word)>1 else '00'
        l3 = f'{ord(word[2]):02x}' if len(word)>2 else '00'
        rtn_lst.append(f"#{l1}{l2}{l3}")
    return rtn_lst


def words_to_hex1(words):
    return [f"#{w[:3].hex():0<6}" for w in words.encode().split()]


def mean_max(d, n):
    m = sum(i * (i ** n - (i - 1) ** n) for i in range(1, d + 1)) / (d ** n)
    return m - (1 + d) / 2


ryan = mean_max(293, 5)

print(ryan)
