# coding=utf-8

import re


# leecode 70
def climbStairs(n):
    """
    :type n: int
    :rtype: int
    """
    if n == 1:
        return 1
    if n == 2:
        return 2
    a, b = 1, 2
    for i in range(3, n + 1):
        a, b = b, a + b
    return b


# leecode 232
def de_crypt(s):
    """
    把3[a2[c]]解密为 accaccacc
    """
    while True:
        # 使用正则表达式替换  数字[字符] 元祖，直到没有匹配条件为止,如果[]前面没有数字，则系数为1
        s = re.sub(r'(\d*)\[(\w+)]', lambda m: m.group(2) * int(m.group(1) or 1), s)
        if not re.search(r'\d+\[\w+]', s):
            break
    return s


# 打家劫舍,leecode 198，使用动态规划法
def rob(nums):
    """
    :type nums: List[int]
    :rtype: int
    """
    if not nums:
        return 0
    if len(nums) == 1:
        return nums[0]
    if len(nums) == 2:
        return max(nums)
    dp = [0] * len(nums)
    dp[0] = nums[0]
    dp[1] = max(nums[0], nums[1])
    for i in range(2, len(nums)):
        dp[i] = max(dp[i - 1], dp[i - 2] + nums[i])
    return dp[-1]


ryana = [1, 2, 3, 1]
ryanb = [2, 7, 9, 3, 1]
ryana = rob(ryana)
ryanb = rob(ryanb)
print(ryana, ryanb)
