# coding = utf-8

# @filename: D:/work/workspace/git/rytutorial/src/base64_handle/zipsplit.py
# @desc: 将一个zip文件使用base64加密，并分割为最大长度为 MAX_CHAR 的若干文件
# @date: 2022年4月1日
# @author: Long
# @pythonVersion: 3.10

import base64

# 分割成的最大行数20行，每行80个字符，一共1600字符
LINE_SPLIT = 20 
ZIPFILE = '1.zip'

def zip2base64(zipfile, base64file):
    with open(zipfile, "rb") as fr:
        with open(base64file, "wb") as fw:
            base64.encode(fr, fw)

def send2wechat(base64file):
    with open(base64file, "r") as fr:
        flines = fr.readlines()
        for i in range(len(flines)//LINE_SPLIT + 2):
            content = ''.join(flines[i: i*LINE_SPLIT])
            print('content block',i,':',content )

if __name__ == '__main__':
    send2wechat('1.zip.txt')