# coding = utf-8

# @filename: D:/work/workspace/git/rytutorial/src/base64_handle/png2base64.py
# @desc: png2 或 zip 转base64
# @date: 2022年4月1日
# @author: Long
# @pythonVersion: 3.10



import base64
def file2base64(file, txt):
    with open(file, "rb") as f:
        base64_data = base64.b64encode(f.read())
        with open(txt, "wb") as fw:
            fw.write(base64_data)

def base642file(txt, file):
    with open(txt, "r") as f:
        txt_data = base64.b64decode(f.read())
        with open(file, "wb") as fw:
            fw.write(txt_data)

def zip2base64(zipfile, base64file):
    with open(zipfile, "rb") as fr:
        with open(base64file, "wb") as fw:
            base64.encode(fr, fw)

def base642zip(base64file, zipfile):
    with open(base64file, "r") as fr:
        with open(zipfile, "wb") as fw:
            base64.decode(fr, fw)

if __name__ == '__main__':
    png_name = 'test.png'
    base64_txt = 'test.txt'
    zip_file = 'dianzizz0408.zip'
    zip_64file = 'dianzizz0408_local.zip.txt'
    # file2base64(png_name, base64_txt)
    # base642file(base64_txt, png_name)
    zip2base64(zip_file, zip_64file)
    # base642zip(zip_64file, '1.zip')