import base64


def decode_base64(encoded_string):
    """Decodes a base64-encoded string to UTF-8 or GB2312

  Args:
    encoded_string: The base64-encoded string to decode.

  Returns:
    The decoded string in UTF-8.
  """

    decoded_bytes = base64.b64decode(encoded_string)
    # return decoded_bytes.decode("utf-8")
    return decoded_bytes.decode("GB2312")


def encode_base64(input_string):
    """Encodes a string to base64"""
    return base64.b64encode(input_string.encode("GB2312")).decode("GB2312")


if __name__ == "__main__":
    str_ = """
wNfT7qOsxPq6w6O6DQogICAguL28/srH1tC5+tL4saPQxcj9vL62yNPKvP7It8jPytXI66Osx+uy
6crVDQoNCg0KDQrXo7mk1/fLs8D7o6zJ+rvu0+S/7KOhDQqw2c37ucm33dPQz965q8u+DQqxo9DF
0rXO8bK/ICAgICDCpsPJw8kNCsrWu/qjujE3Nzc4MDUxMjAyDQpsb3VtZW5nbWVuZ0BiYWl3YW5n
LmNvbQ0Kd3d3LmJhaXdhbmcuY29tDQqxsb6pytC6o7Xtx/jTwLfhsvrStbv5tdjW0LnYtOXSvLrF
1LrSu8f4QTHCpTE0suMNCiANCreivP7Iy6O6IGhvbmdqaWFuX3d1QGNiaXQuY29tLmNuDQq3osvN
yrG85KO6IDIwMjAtMTAtMDkgMTA6MTANCsrVvP7Iy6O6IGxvdW1lbmdtZW5nDQqzrcvNo7ogIs31
1r6zzyINCtb3zOKjuiC72Li0OiDTyrz+ytXI68i3yM8NCsPJw8m6wyENCiAgICAgIDIwMjDE6jbU
wjHI1dbBMjAyMMTqONTCMzHI1b3hy+O30dPD0tHIt8jPzt7O86Gj0LvQu6OhIA0KDQoNCg0KzuK6
6b2oDQrW0Ln60vjQ0LGjz9XQxc+ivLzK9dPQz965q8u+ICDStc7xy8Syvw0Ktee7sKO6IDAxMC04
ODE5NTU2Mw0Kyta7+qO6IDE1ODA0MjQxNDIzDQq12Na3o7ogsbG+qcrQyq++sMm9x/jKtdDLtPO9
1jMwusXUujG6xcKlNLLjIA0K08qx4KO6IDEwMDE0NCANCiANCreivP7Iy6O6IGxvdW1lbmdtZW5n
QGJhaXdhbmcuY29tDQq3osvNyrG85KO6IDIwMjAtMDktMzAgMTY6NDgNCsrVvP7Iy6O6IGhvbmdq
aWFuX3d1DQqzrcvNo7ogemhpY2hlbmdfd2FuZw0K1vfM4qO6INPKvP7K1cjryLfIzw0Kwey1vKOs
xPq6w6O6DQogICAgICAgIM7SuavLvsS/x7DV/dTazerJxrmry77E2rK/v9jWxqOssLTV1bvhvMbK
prXEyfO8xtKqx/OjrNDo0qrT67nzuavLvrK5s+TT687SuavLvtTaytXI67fWs8m1xLrPzazCxNS8
uf2zzNbQsvrJ+rXEt9HTw8i3yM+ho8zY0+u588u+z8i9+NDQ08q8/si3yM+ju87Sy77T67nzuavL
vtLRvq294cvjt9HTw8jnz8Kjuw0KIDGhosrVyOu31rPJo6wyMDIwxOo21MIxyNXWwTIwMjDE6jjU
wjMxyNWjuqOkMywwODIsMjkwLjYzo6i089C0o7rI/rDbweOwxs3yt6HHqrehsNu+wcqw1KrCvb3H
yP631qOpDQrI57fR08O6y7bUzt7O86Osu7nH69PKvP672Li0vfjQ0Mi3yM+jrLjQ0LujoQ0KDQoN
Cg0K16O5pNf3y7PA+6Osyfq77tPkv+yjoQ0KsNnN+7nJt93T0M/euavLvg0Ks8m5prDLsr8gICAg
IMKmw8nDyQ0Kyta7+qO6MTc3NzgwNTEyMDINCmxvdW1lbmdtZW5nQGJhaXdhbmcuY29tDQp3d3cu
YmFpd2FuZy5jb20NCrGxvqnK0Lqjte3H+NPAt+Gy+tK1u/m12NbQudi05dK8usXUutK7x/hBMcKl
MTSy4w0K
"""
    decoded_string = decode_base64(str_)
    print(decoded_string)
    print(encode_base64(decoded_string))

