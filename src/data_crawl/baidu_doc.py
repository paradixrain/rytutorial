# coding = utf-8

# @filename: C:/Users/Ryan/git/rytutorial/src/data_crawl/baidu_doc.py
# @desc: 爬取百度文库里的文档，形成txt
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022年3月17日

import requests
from selenium import webdriver
import time
import re

def get_doc_id(doc_params):
    '''
    从一系列参数中，萃取doc_id，失败则返回 None
    '''
    for index, value in enumerate(doc_params):
        if value == 'view':
            return doc_params[index+1]
    return None

def main(bdurl):
    '''
    下载百度文档，doc文档
    '''
    # 取得文档id
    doc_params = bdurl.replace('/', '?').split('?')
    doc_id = get_doc_id(doc_params)
    print('doc_id', doc_id)
    
    # 0. 构建浏览器
    # brower = webdriver.Chrome()
    # Header = {'User-Agent' :'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'}
    # brower.get(bdurl)
    # data = brower.page_source
    # with open('baiduwk.html', 'w', encoding='utf-8') as f:
    #     f.write(data)
    
    # 调试：文件
    with open('baiduwk.html', 'r', encoding='utf-8') as f:
        data = f.read()
    time.sleep(5)
    
    # 获得文件名，文件页数
    doc_type = re.findall(r'<div class="file-type-icon (.*?)">', data)[0]
    doc_name = re.findall(r'<h3 class="doc-title">(.*?)</h3>', data)[0]
    page_size = re.findall(r'上传</span> <span class="divider"></span> <span><em>(.*?)</em>页</span>', data)[0]
    print(doc_type, doc_name)
    print(page_size)
    
    # 请求地址
    content_url = 'https://wkbjcloudbos.bdimg.com/v1/docconvert9636/wk/495ac407eb6547c0c4cb0a5c480d22a1/0.json?\
    responseContentType=application/javascript&\
    responseCacheControl=max-age=3888000&\
    responseExpires=Mon, 02 May 2022 15:56:29 +0800&\
    authorization=bce-auth-v1/fa1126e91489401fa7cc85045ce7179e/2022-03-18T07:56:29Z/3600/host/3f91da316db949bb2f833a69fa6198a342bab3ebb9d54d34423ce076c3032b3d&\
    x-bce-range=64755-77101&\
    token=eyJ0eXAiOiJKSVQiLCJ2ZXIiOiIxLjAiLCJhbGciOiJIUzI1NiIsImV4cCI6MTY0NzU5Mzc4OSwidXJpIjp0cnVlLCJwYXJhbXMiOlsicmVzcG9uc2VDb250ZW50VHlwZSIsInJlc3BvbnNlQ2FjaGVDb250cm9sIiwicmVzcG9uc2VFeHBpcmVzIiwieC1iY2UtcmFuZ2UiXX0=.cTfzatRxuZqmizA4yUIIYYQXkzPteA+mXk4m6QKMb9A=.1647593789'
    
    # with open('baiduwkcontent.html', 'w', encoding='utf-8') as f:
    #     f.write(content)
    
    
    # 1. "阅读全部"
    # brower.execute_script('document.documentElement.body.scrollHeight')
    # brower.find_element_by_xpath("/html/body/div/div[2]/div[1]/div[2]/div[3]/div[1]/div[2]/span").click()
    #
    # # 2. 往下滚屏，滚动到最后
    # brower.execute_script('document.documentElement.body.scrollHeight')
    # time.sleep(10)
    
    # 3.开始抓取所有的0.json的请求
    # content = session.get
    # data = brower.page_source
    # print(data)
    #
    # # 2. "往下翻到最后一页"
    #
    # # 3. "轮询每一个
    # brower.close()
    
    # a = "\u5168\u9762\uff0c\u4e0d\u5e7f\u6cdb\uff0c\u4e0d\u6df1\u523b\u3002\u4e8c\u662f\u5728\u5de5\u4f5c\u4e2d\u7f3a\u4e4f\u65b0\u601d\u7ef4\uff0c\u5bf9\u53d1\u5c55\u4e2d\u5b58\u5728\u7684\u95ee\u9898" 
    # print(a)

if __name__ == '__main__':
    bdurl = 'https://wenku.baidu.com/view/082e427dbb4ae45c3b3567ec102de2bd9605de1d?bfetype=new'
    main(bdurl)