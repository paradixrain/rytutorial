# coding = utf-8

# @filename: C:/Users/Ryan/git/rytutorial/src/data_crawl/baidu_doc_merge.py
# @desc: 遍历 doc_source 里的所有文件，拼装成最后的文件
#        在百度链接地址后面加上：  ?bfetype=new  就可以预览所有的页面，
#        然后将 0.json 请求返回的json文件做另存， 分别保存为 wenku1.json,wenku2.json等，放到baidu_doc_source下
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022年3月18日


''' 每行json如下，因为字体在font文件里，技术有难度，舍弃字体和字号大小不处理
                {
    "c": "组织生活批评他人与自我批评多篇",        # 表示文本内容
    "p": {
        "h": 35.212,                    # 文本高度，相同高度表示同一个字号
        "w": 494.758,                    # 文本宽度
        "x": 198.75,                    # 文本x位置，列位置
        "y": 118.794,                    # 文本y位置，行位置，相同列位置在同一行
        "z": 1
    },
    "ps": null,
    "s": {
        "font-size": "13.5"            # 字号
        "color": "#0090d2",            # 字体颜色
        "letter-spacing": "0.047"        # 字体间隔
    },
    "t": "word",
    "r": [
        2
    ]
}
                '''

import json
import os
from docx import Document


def gci(filepath):
    '''#遍历filepath下所有文件，不包括子目录'''
    files = os.listdir(filepath)
    files_list = []
    for fi in files:
        fi_d = os.path.join(filepath,fi)
        if os.path.isdir(fi_d):
            pass
        else:
            files_list.append(os.path.join(filepath,fi))
    return files_list

def file_sort(files_list):
    '''对文件名类似  ../baidu_doc_source/wenku123.json 的文件序列进行升序排序'''
    return sorted(files_list, key=lambda x: int(x.replace('wenku','').replace('.json','').split('/')[-1]))

def main():
    source_dir = '../baidu_doc_source/'
    docx_name = '简析谷歌企业文化.docx'
    
    # 遍历文件里所有文件，按文件名升序方式进行所有文件读取
    files_list = gci(source_dir)
    files_list = file_sort(files_list)
    
    # 新建word 文档
    document = Document()
    
    # 对每个文件进行解析
    cur_paragraph = ""
    for file_index, file in enumerate(files_list):
        # print('current file:', file)
        try:
            with open(file, 'r', encoding='utf-8') as f:
                content = f.read()
                firstloc = content.find('(')+1
                content = content[firstloc:-1]
                content = json.loads(content)
                # print('json of content', content)

                # 对每行进行解析，插入到word里
                for index, node in enumerate(content['body']):
                    # print('当前 paragraph start:', cur_paragraph)
                    c, y = node['c'], str(node['p']['y'])
                    if index == 0 and file_index == 0: # 文件的第一行
                        # print('所有文件第一行')
                        cur_paragraph = c
                        last_y = y
                    elif index == len(content['body']) - 1 and file_index == len(files_list) - 1:  #整个文档最后一行
                        # print('整个文档最后一行')
                        if not c[0].isspace():
                            document.add_paragraph(cur_paragraph+c)
                        else:
                            document.add_paragraph(cur_paragraph)
                            document.add_paragraph(c)
                    else:
                        # print('current paragraph:',index, c, y, last_y)
                        if y == last_y or not c[0].isspace() :
                            # 只要第一个字符不是空格和换行，就合并，不管是否翻页
                            cur_paragraph = cur_paragraph + c
                            # print('合并段落：', cur_paragraph)
                        else:
                            # print('翻页或者换行')
                            document.add_paragraph(cur_paragraph)
                            cur_paragraph = c
                            # if len(cur_paragraph) != 0:
                            #     # 有内容则输出并清空当前段落内容
                            #     print('插入段落：', cur_paragraph)
                            #     document.add_paragraph(cur_paragraph)
                            #     cur_paragraph = c
                            # elif index == 0: # 翻页的时候，第一行为空的场景要特殊处理，将内容输出
                            #     document.add_paragraph(cur_paragraph)
                            #     cur_paragraph = c
                            # else:
                            #     cur_paragraph = c
                        last_y = y
                    # print('当前 paragraph end:', cur_paragraph)
        except Exception as e:
            print(e)
    document.save(docx_name)
    print(f'{docx_name} 已保存')

if __name__ == '__main__':
    main()