# -*- coding: utf-8 -*-
# filename: handle.py

import hashlib
import web
import logging

AppID = 'wx1b19ca1e3bd58663'
AppSecret = '2426bddb812c71854da049eb9f21ca7f'
Token = 'roronoa'
EncodingAESKey = '3OWx7Qe5UT49VCMC27TBf0s7YY45q92hvozag16UcY3'

logging.basicConfig(
				level=logging.DEBUG,
				filename='webpy.log',
				filemode='a',
				format='%(message)s'
				)
logfile = 'webpy.log'

class Handle(object):
    def GET(self):
        try:
            data = web.input()
            print('data:', data)
            if len(data) == 0:
                return "hello, this is handle view from ryan"
            signature = data.signature
            timestamp = data.timestamp
            nonce = data.nonce
            echostr = data.echostr
            token = "roronoa"

            sortlist = [token, timestamp, nonce]
            sortlist.sort()
            sha1 = hashlib.sha1()
            # map(sha1.update, list)
            sha1.update("".join(sortlist).encode("utf-8"))
            hashcode = sha1.hexdigest()
            print ("handle/GET func: hashcode, signature: ", hashcode, signature)
            if hashcode == signature:
                return echostr
            else:
                return ""
        except Exception as Argument: 
            print(Argument)
            return Argument

    def POST(self):
        try:
            webinput = web.input()
            print('webinput:', webinput)
            logging.debug(webinput)
            signature = webinput.signature
            timestamp = webinput.timestamp
            nonce = webinput.nonce

            webdata = web.data()
            print('webdata', webdata)
            logging.debug(webdata)
            token = "roronoa"

            sortlist = [token, timestamp, nonce]
            sortlist.sort()
            sha1 = hashlib.sha1()
            # map(sha1.update, list)
            sha1.update("".join(sortlist).encode("utf-8"))
            hashcode = sha1.hexdigest()
            
            if hashcode != signature:
                print('收到假消息')
                return ""

            with open(logfile, 'a+', encoding='utf-8' ) as f:
                f.append(webdata)

        except Exception as Argument: 
            print(Argument)
            return Argument
        finally:
            return ""