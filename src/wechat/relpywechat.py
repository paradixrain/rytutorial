# coding = utf-8

# @filename: C:/Users/Ryan/git/rytutorial/wechat/replywechat.py
# @desc: 获取微信access_token，主动发送消息给openid对应的用户
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022年4月2日


import requests
import json
import time
import base64


appid = 'wx6533db2d0fe1970b'
AppSecret = '58d0b8d3cf81b73ee1ad5c131bd45358'
tokenFile = 'token.txt'
wx_url = f'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={AppSecret}'

openid = 'oakSB6sKPRYjYb5BIf5xTd7i08uk'  # 接收者的openid
MAX_CHAR = 500


def file2base64(file, txt):
    with open(file, "rb") as f:
        base64_data = base64.b64encode(f.read())
        with open(txt, "wb") as fw:
            fw.write(base64_data)


def get_accessToken(wx_url):
    try:
        res = requests.get(wx_url)
        res = res.text
        print('res',res)
        json_res = json.loads(res)
        access_token = json_res['access_token']
        print('access_token:', access_token)
        with open(tokenFile, 'w') as f:
            f.write(access_token)
        return access_token
    except Exception as e:
        print(e)


def sendmsg(access_token, openid, msg):
    body = {
        "touser": openid,
        "msgtype": "text",
        "text": {
            "content": msg
        }
    }
    response = requests.post(
        url="https://api.weixin.qq.com/cgi-bin/message/custom/send",
        params={
            'access_token': access_token
        },
        data=bytes(json.dumps(body, ensure_ascii=False), encoding='utf-8')
    )
    # 这里可根据回执code进行判定是否发送成功(也可以根据code根据错误信息)
    result = response.json()
    print(result)


if __name__ == '__main__':
    wx_url = f'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={AppSecret}'
    access_token = get_accessToken(wx_url)
    # 读取base64文件，一段一段发
    send_file = 'test.txt'  # 此处输入要发送的文件的文件名
    send_file_b64 = 'test.txt.b64'  # 此处输入要发送的文件的base64文件名

    file2base64(send_file, send_file_b64)

    blockcount = 0
    with open(send_file_b64, "r") as fr:
        content = fr.read(MAX_CHAR)
        while content != '':
            content = send_file_b64 + str(blockcount).rjust(20,'0') + ':' + content + ': block'
            print('content block',blockcount,':',content )
            blockcount += 1
            sendmsg(access_token, openid, content)
            content = fr.read(MAX_CHAR)
            time.sleep(2)
        sendmsg(access_token, openid, '发送结束，共有block: '+str(blockcount))
    print(access_token)
