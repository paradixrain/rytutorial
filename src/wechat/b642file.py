# coding=utf-8
import base64

def base642zip(base64file, zipfile):
  with open(base64file, "r") as fr:
    with open(zipfile, "wb") as fw:
      base64.decode(fr, fw)

if __name__ == '__main__':
  zip_file = "11.zip"
  zip_64file = "b64file.txt"
  base642zip(zip_64file, zip_file)
