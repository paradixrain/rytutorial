# coding = utf-8

# @filename: C:/Users/Ryan/git/rytutorial/src/basicTraining/optional_args.py
# @desc: 测试先定位置参数，先定名称参数
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022年3月16日

def safe_division( numerator, denominator, /,       # /前面的是必填，只能通过位置访问
                   ndigits = 10, *,                  # / 和 * 中间的参数可以通过位置，也可以通过参数名访问
                   ignore_overflow = False,          # * 后面的只能通过参数名访问
                   ignore_zero_division = False):
    try:
        fraction = numerator / denominator
        return round(fraction, ndigits)
    except OverflowError:
        if ignore_overflow:
            return 0
        else:
            raise
    except ZeroDivisionError:
        if ignore_zero_division:
            return float('inf')
        else:
            raise