# coding = utf-8

# @filename: C:/Users/Ryan/git/rytutorial/src/basicTraining/decorator_demo.py
# @desc: 试试使用自己编写的修饰器打印斐波那契数列
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022年3月16日

from functools import wraps

def trace(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        print(f'{func.__name__}({args!r}{kwargs!r}) '
              f'-> {result!r}')
        return result
    return wrapper

@trace
def fibonacci(n):
    ''' return the n-th fibonacci number'''
    if n == 0 or n == 1:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)

fibonacci(4)
help(fibonacci)