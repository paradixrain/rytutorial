# coding = utf-8

# @filename: D:/work/workspace/git/rytutorial/src/pandas_learning/csv_column_filter.py
# @desc: 读取一个回款明细表，筛选出其中2021年的数字，存成新文件
# @date: 2022年3月15日
# @author: Long
# @pythonVersion: 3.10

import pandas as pd

def main():
    income_2021_outfile = "D:\\work\\workspace\\git\\rytutorial\\dataCrawl\\data\\年度回款2021.csv"
    income_file = "D:\\work\\workspace\\git\\rytutorial\\dataCrawl\\data\\回款明细.csv"

    df = pd.read_csv(income_file, header=0, encoding='gbk')
    print(df)
    df_2021 = df[ (df['付款时间'] >='2021') & (df['付款时间'] < '2022') ]
    df_2021.to_csv(income_2021_outfile, encoding='gbk')

if __name__ == '__main__':
    main()