# coding = utf-8

# @filename: D:/work/workspace/git/rytutorial/src/pandas_learning/xzdw.py
# @desc: 测试pandas读取多数据块的excel,顺便把晓阳的excel处理问题变成pandas解决，根据就近就高原则，根据薪级输出对应岗级
# @date: 2022年3月15日
# @author: Long
# @pythonVersion: 3.10
# 测试发现读取多block，以及有合并单元格的excel的时候，data_frame会有问题，会有值的缺失

import pandas as pd
import bisect

def get_xzdw(xz, xzdw_series):
    '''
    输入xz薪资和xzdw_series薪资档位的pd.Series，返回薪资对应的档位的名字
    原则是：就近就高，如果超过最高档位，则返回最高档位
    '''
    left_pos = bisect.bisect_left(xzdw_series, xz)
    left_pos = min(left_pos, xzdw_series.size -1)
    # print('left_pos=', left_pos)
    left_pos_name = xzdw_series.index[left_pos]
    # print(left_pos_name)
    return left_pos_name

def main():
    excelFile = '../../data/薪点表设计.xlsx'
    sheetName1 = '1-薪点表-非业务类'
    sheetName2 = '3-套算'
    
    data_frame1 = pd.read_excel(excelFile, sheet_name=sheetName1,skiprows=32,nrows=12,index_col=0, usecols="B:Q")
    data_frame2 = pd.read_excel(excelFile, sheet_name=sheetName2,skiprows=1,nrows=126,index_col=0, usecols="A:I")
    
    # 输出原始dataFrame数据, 薪点表以及套算表
    # print(data_frame1)
    # print(data_frame2)
    
    # levelSeries = '1档 2档 3档 4档 5档 6档 7档 8档 9档 10档 11档 12档 13档 14档 15档'.split()
    # level = pd.Series(levelSeries)
    # 薪点表档位
    # print('levelSeries:')
    # print(level)
    
    # 遍历data_frame2中的每一行，去data_frame1中找对应职级的档位薪资，就近就高
    # print('岗级')
    # print(data_frame2.iloc[:, [0,6,7]])

    # noinspection PyBroadException
    try:
        trxd_series=[]  # 新增一列，套入薪档
        for _, row in data_frame2.iterrows():
            # print('index, row')
            # print(index, row)
            xz = row['标准月薪\n（不含补贴）']
            trxj = str(row['套入薪级']) + '级'
            # print('TRXJ:', trxj)
            df = data_frame1.loc[trxj]
            
            print('trxj:', trxj, 'xz:', xz, 'df:')
            # print(df)
            
            trxd = get_xzdw(xz, df)
            print('trxd:', trxd)
            trxd_series.append(trxd)
                    
        print('trxd_series:')
        for i, value in enumerate(trxd_series, 1):
            print(i, value)
        
        # 追加列
        data_frame2.insert(len(data_frame2.columns), '套入薪档' ,trxd_series )
        # 查看最后的结果
        print(data_frame2)
        
        # 导出到excel
        data_frame2.to_excel('../../data/套入薪档.xlsx', engine='openpyxl')
    except Exception as e:
        print(e)

if __name__ == '__main__':
    main()
