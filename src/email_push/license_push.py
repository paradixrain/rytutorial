# coding = utf-8

# @filename: license_push.py
# @title: 注册码到期了，根据excel中的项目名，进行告警推送
# @author: RyanLin
# @Version: Python 3.10
# @date: 2022/03/15
import os.path

import pandas as pd
from ryModuleTest.ryEmail.send_email import send_email


def get_mail():
    """
    从邮箱获取来自 侯思颖 housiying@baiwang.com 的  **注册码申请记录 的邮件
    下载其中的excel，存到 xlsx_license_download
    """
    return


def send_mail():
    """
    把过滤后的条目筛选后的excel发给对应的人
    """
    return


def filter_zbx(excel_file_in, sheet, keyword, excel_file_out):
    """
    筛选出excel中的快到期的项目，保存excel
    """
    try:
        df = pd.read_excel(excel_file_in, sheet_name=sheet)
        # print(df)
        df_zbx = df[df['项目'].str.contains(keyword, na=False)]
        # print(df_zbx)
        df_zbx.to_excel(excel_file_out, sheet_name=sheet, engine='openpyxl', encoding='gbk')
    except ValueError as e:
        print(e)
    return


def main():
    license_file_dir = '../xlsx_license_download/'
    if os.path.exists(license_file_dir):
        files = os.listdir(license_file_dir)
    for filename in files:
        license_file_in = os.path.join(license_file_dir, filename)
        if license_file_in.endswith('_成功二部.xlsx'):
            continue
        license_file_out = license_file_in.replace('.xlsx', '_成功二部.xlsx')
        sheet = '到期统计'

        keyword_list = ['保险']

        for keyword in keyword_list:
            filter_zbx(license_file_in, sheet, keyword, license_file_out)

        smtp_host = "smtp.qq.com"
        send_addr = '1651168647@qq.com'
        password = 'nauihlkshvvhcgig'
        recipient_addrs = ';'.join(['linyu@baiwang.com', 'yulingxin@baiwang.com', '125697011@qq.com'])
        subject = '本周注册码到期申请，请安排人员核实，并对服务企业进行提醒'
        content = '本周注册码到期申请'
        attach = license_file_out

        send_email(smtp_host, send_addr, password, recipient_addrs, subject, content, attach)


if __name__ == "__main__":
    main()
    print('文件筛选完毕')
